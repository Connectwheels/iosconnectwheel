//
//  ApiManager.swift
//  ConnectWheels
//
//  Created by  on 13/07/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import Foundation



enum ApiService {
    case login
}

//------------------------------------------------------

//MARK:- APIEnvironment

enum APIEnvironment {
    
    case live
    case local
    case localHost
}

struct NetworkManager  {
    let provider = MoyaProvider<ApiManger>(plugins: [NetworkLoggerPlugin(verbose: true)])
    static let environment : APIEnvironment = .live
}

class ApiManger : TargetType {
 
    var path: String = ""
    
     static var shared = ApiManger()
    
    let provider = MoyaProvider<ApiManger>(manager : WebService.manager())
    
    var sampleData: Data {
        return "Half measures are as bad as nothing at all.".utf8Encoded
    }
    
    
    var headers: [String : String]?
    
    var environmentBaseURL : String {
        switch NetworkManager.environment{
        case .local : return "http://13.59.201.94:5052/api/v1/"
        case .live :
            if ApiManger.shared.path == endpoints.getTripCount.rawValue || ApiManger.shared.path == endpoints.getTripList.rawValue || ApiManger.shared.path == endpoints.getTripDetail.rawValue {
               return "http://13.232.41.240/api/v1/"
            }
            return "http://13.232.41.240:5052/api/v1/"
        case .localHost : return "http://192.168.1.145:5052/api/v1/"
        }
    }
    
    // MARK: - baseURL
    var baseURL: URL {
        
        guard let url  = URL(string: environmentBaseURL) else{
            fatalError("base url could not be configured")
        }
        return url
    }
    
  //  var path : String = ""
    
    var method: Moya.Method = .post
    
    // MARK: - parameterEncoding
    var parameterEncoding: ParameterEncoding {

        return URLEncoding.default
    }
    
    // MARK: - task
    var task: Task {
        let jsonData = try? JSONSerialization.data(withJSONObject: self.parameters!, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        let encryptedData  : String = jsonString.encryptData()
        return .requestParameters(parameters: [:], encoding:  encryptedData as ParameterEncoding)
    }
    
    var parameters: [String: Any]?
    
    func setHeaders()  {
        
        var headersToSend : [String : String] = [:]
        headersToSend["api-key"] = "CONNECTWHEELS".encryptData()
        headersToSend["content-Type"] = "text/plain"
        if let token = UserDefault.value(forKey: kUserToken)
        {
            headersToSend["token"] = JSON(token).stringValue.encryptData()
        }
        debugPrint("headers : //---------------- ",headersToSend)
        self.headers = headersToSend
    }
  

    func makeRequest(method : endpoints
                     ,methodType: HTTPMethod = .post
                     ,parameter : Dictionary<String,Any>?
                     ,withErrorAlert isErrorAlert : Bool = true
        ,withLoader isLoader : Bool = true
        ,withdebugLog isDebug : Bool = true
        , withBlock completion :((JSON,Int,Error?,ApiResponseCode) -> Void)?) {
    
        setHeaders()
        if methodType == .put
        {
            self.headers!["content-type"] = "application/x-www-form-urlencoded"
        }
        self.path = method.rawValue
        self.parameters = parameter
        self.method = methodType
        
        if method == .vehicleLastLocation {
            provider.manager.session.configuration.timeoutIntervalForRequest = 10
            provider.manager.session.configuration.timeoutIntervalForResource = 10
        } else {
            provider.manager.session.configuration.timeoutIntervalForRequest = 180
            provider.manager.session.configuration.timeoutIntervalForResource = 180
        }
        
        if isLoader
        {
            //FIXME: Add Loader
            GFunction.sharedInstance.addLoader()
        }
        DispatchQueue.main.async {
          UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        
        let cryptoLib = CryptLib()
        
        if isDebug {
            
            let encyptedData = cryptoLib.encryptPlainText(with: JSON.init(rawValue: self.parameters as Any)?.rawString(), key: KMEncryption.shared.kSecretKey, iv: KMEncryption.shared.kIV)
            debugPrint("----------------------------------------URL----------------------------------------")
            debugPrint(baseURL.appendingPathComponent(method.rawValue))
            debugPrint("-----------------------------------------------------------------------------------")
            debugPrint("Encryption:-\n",encyptedData!)
        }
        
  
        
        
      
        provider.request(self) { (result) in
            
            if isLoader {
                //FIXME: Remove Loader
                GFunction.sharedInstance.removLoader()
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            switch result{
     
            case let .success(response):
                
                if response.statusCode == 401
                {
                    GFunction.logoutUser()
                    return
                }
                
                
                
                if let _ = result.value
                {
                    do{
                        
                        
                        guard let res = try response.mapJSON() as? String else {
                            return
                        }
                        
                        let resDic  = res.decryptData().convertToDictionary()
                        
                        if isDebug {
                            debugPrint("----------------------------------------Response----------------------------------------")
                            debugPrint(res )
                            debugPrint("----------------------------------------------------------------------------------------")
                            debugPrint("----------------------------------------Response DecryptData----------------------------------------")
                            debugPrint(resDic ?? "")
                            debugPrint("----------------------------------------------------------------------------------------")
                         }
                        
                        if completion != nil {
                            
                            var code = ApiResponseCode.unknown
                            
                            if let codeStatus = resDic?["code"] , let codeint = ApiResponseCode.init(rawValue: JSON(codeStatus).intValue)
                            {
                                code = codeint
                            }
                            
                            completion!(JSON(resDic as Any), response.statusCode, nil, code)
                        }
                    
                    }catch
                    {
                        
                    }
                }
                break
            case .failure(_):
                
                if isDebug {
                    debugPrint("----------------------------------------Response Error----------------------------------------")
                    debugPrint(result.error?.localizedDescription ?? "Error in \(method.rawValue)")
                    debugPrint("----------------------------------------------------------------------------------------")
                }
                
                if isErrorAlert
                {
                    //FIXME: Add Alert
                 
                    GFunction.ShowAlert(message: (result.error?.localizedDescription)!)
                }
                
                if completion != nil {
                    completion!(JSON.null,400,result.error, .unknown)
                }
                
                break
            }
        }
    }

}

extension Response {
    
    
 
    public func filterApiStatusCodes<R: RangeExpression>(statusCodes: R) throws -> Response where R.Bound == Int {
        guard statusCodes.contains(statusCode) else {
            throw MoyaError.statusCode(self)
        }
        return self
    }
}

extension String: Moya.ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
    
    func encryptData() -> String{
     
        return CryptLib().encryptPlainText(with: self, key: KMEncryption.shared.kSecretKey, iv: KMEncryption.shared.kIV)
    }
    
    func decryptData() -> String {
       
        
        
        return CryptLib().decryptCipherText(with: self, key: KMEncryption.shared.kSecretKey, iv: KMEncryption.shared.kIV)
    }
    
    
    
    func createSixteenBitIV() -> String{
        let strIV = KMEncryption.shared.kIV.data(using: String.Encoding.utf8)
        
        //    let strIVBytes:[UInt8] = Array(UnsafeBufferPointer(start: UnsafePointer<UInt8>(strIV!.bytes), count: 16))
        let strIVBytes:[UInt8] = strIV!.withUnsafeBytes {
            [UInt8](UnsafeBufferPointer(start: $0, count: 16))
        }
        
        return String(bytes: strIVBytes, encoding: String.Encoding.utf8)!
        
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
    
    
}

//func += <Key, Value> ( left: inout [Key:Value], right: [Key:Value]) {
//    for (key, value) in right {
//        left.updateValue(value, forKey: key)
//    }
//}

class KMEncryption: NSObject{
    
    static var shared : KMEncryption = KMEncryption()
    
    var kSecretKey : String{
        return "PSB6EB4LSH6T5IF1D2G7DLPJNHBTL4TT"
    }
    var kIV : String {
        return "PSB6EB4LSH6T5IF1"
    }
   
}

extension CharacterSet {
    static var NULLCharacter: CharacterSet {
        return CharacterSet(charactersIn: "\0")
    }
}



class WebService {
    // session manager
    static func manager() -> Alamofire.SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 180 // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = 180 // as seconds, you can set your resource timeout
        let manager = Alamofire.SessionManager(configuration: configuration)
        //        manager.adapter = CustomRequestAdapter()
        
        return manager
    }
    
    
    // request adpater to add default http header parameter
    private class CustomRequestAdapter: RequestAdapter {
        public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
            var urlRequest = urlRequest
            
            
            return urlRequest
        }
    }
   
    
}

