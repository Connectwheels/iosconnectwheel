//
//	ShareTiming.swift
//
//	Create by - on 6/12/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import SwiftyJSON


class DriverListShareTiming : NSObject, NSCoding{

	var day : String!
	var endTime : Int!
	var id : Int!
	var insertdate : String!
	var startTime : Int!
	var vehicleId : Int!
	var vehicleShareId : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		day = json["day"].stringValue
		endTime = json["end_time"].intValue
		id = json["id"].intValue
		insertdate = json["insertdate"].stringValue
		startTime = json["start_time"].intValue
		vehicleId = json["vehicle_id"].intValue
		vehicleShareId = json["vehicle_share_id"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if day != nil{
			dictionary["day"] = day
		}
		if endTime != nil{
			dictionary["end_time"] = endTime
		}
		if id != nil{
			dictionary["id"] = id
		}
		if insertdate != nil{
			dictionary["insertdate"] = insertdate
		}
		if startTime != nil{
			dictionary["start_time"] = startTime
		}
		if vehicleId != nil{
			dictionary["vehicle_id"] = vehicleId
		}
		if vehicleShareId != nil{
			dictionary["vehicle_share_id"] = vehicleShareId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         day = aDecoder.decodeObject(forKey: "day") as? String
         endTime = aDecoder.decodeObject(forKey: "end_time") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         insertdate = aDecoder.decodeObject(forKey: "insertdate") as? String
         startTime = aDecoder.decodeObject(forKey: "start_time") as? Int
         vehicleId = aDecoder.decodeObject(forKey: "vehicle_id") as? Int
         vehicleShareId = aDecoder.decodeObject(forKey: "vehicle_share_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if day != nil{
			aCoder.encode(day, forKey: "day")
		}
		if endTime != nil{
			aCoder.encode(endTime, forKey: "end_time")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if insertdate != nil{
			aCoder.encode(insertdate, forKey: "insertdate")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}
		if vehicleId != nil{
			aCoder.encode(vehicleId, forKey: "vehicle_id")
		}
		if vehicleShareId != nil{
			aCoder.encode(vehicleShareId, forKey: "vehicle_share_id")
		}

	}

}
