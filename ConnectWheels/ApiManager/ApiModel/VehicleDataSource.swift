//
//  VehicleDataSource.swift
//  ConnectWheels
//
//  Created by  on 30/07/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import Foundation
import AVFoundation

class VehicleDataSource : NSObject {
    
    static let shared : VehicleDataSource = VehicleDataSource()
    
    //------------------------------------------------------
    
    var arrVehicles : [Vehicle] = []{
        didSet{
            NotificationCenter.default.post(name: NSNotification.Name.init(kvehicleUpdateObserver), object: nil)
            setEmergencyCallApi()
        }
    }
    
    var selectedIndex = 0
    
    var timer = Timer.init()
    var player: AVAudioPlayer?
    var isEmergency : Bool = false{
        didSet{
            emergncyCompl?(isEmergency)
            if isEmergency{
                
                if let player = self.player , player.isPlaying{
                    return
                }
                
                if let urlpath = Bundle.main.url(forResource: "EmergencyAlert", withExtension: "wav") {
                    do {
                        self.player = try AVAudioPlayer(contentsOf: urlpath)
                        guard let player = self.player else { return }
                        player.numberOfLoops = -1
                        player.prepareToPlay()
                        player.delegate = self
                        try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                        player.play()
                        
                    } catch let error as NSError {
                        print(error.description)
                    }
                }
            }else{
                player?.pause()
                player?.rate = 0.0
            }
        }
    }
    
    

    
    var selectedVehicle : Vehicle? {
        if selectedIndex > arrVehicles.count || arrVehicles.isEmpty{
            return nil
        }
        return arrVehicles[selectedIndex]
    }
    
    private var emergncyCompl : ((Bool) -> Void)?
    
    func clearData()  {
        self.arrVehicles = []
        selectedIndex = Int.max
    }
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didEnterBackGround(notification:)),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil)
    
    }
    
    @objc func didEnterBackGround(notification: NSNotification) {
        // do something
        emergncyCompl?(self.isEmergency)
    }
    
    //MARK:- Web Service
    
    func setEmergencyCallApi()  {
        timer.invalidate()
        if #available(iOS 10.0, *) {
            
            let emergencyIds = self.arrVehicles.filter{ JSON($0.parking as Any).stringValue == "On" }.map{ JSON($0.id as Any).stringValue }
            
            if !emergencyIds.isEmpty{
               
                timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: { (_) in
                    self.lookOnEmergencyWs()
                })
                
                timer.fire()
               
            } else {
                // Fallback on earlier versions
            }
            
        }
        
    }
    
    func getEmegencyStatus(isEmergency : @escaping (Bool) -> Void)  {
        emergncyCompl = isEmergency
        emergncyCompl?(self.isEmergency)
    }
    
    func lookOnEmergencyWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/check_emergency_status/
         
         Parameter   : vehicle_ids
         
         Optional    :
         
         Comment     : This api for user can his vehicle list.
         
         ==============================
         */
        
        let emergencyIds = self.arrVehicles.filter{ JSON($0.parking as Any).stringValue == "On" }.map{ JSON($0.id as Any).stringValue }
        
        if emergencyIds.isEmpty{
            timer.invalidate()
            return
        }
        
        var param : [String : Any] = [:]
        param["vehicle_ids"] = emergencyIds.joined(separator: ",")
        
        ApiManger.shared.makeRequest(method: .chekcEmergency , parameter: param, withErrorAlert : false , withLoader : false) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    //self.isEmergency = false
                    break
                case .success:
                    // play sound
                    
                    //EmergencyAlert
                    self.isEmergency = true
                    break
                    
                case .nodata:
                   // self.isEmergency = false
                    break
                    
                default :
                    break
                }
                
            }
        }
    }
    
    func getVehicleList(isShowAlert : Bool = false, isShowLoader : Bool = false , completion : completion?) {

        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_vehicle_list/
         
         Parameter   : vehicle_id
         
         Optional    :
         
         Comment     : This api for user can his vehicle list.
         
         ==============================
         */
        
        
        ApiManger.shared.makeRequest(method: .getVehicleList , parameter: [:], withErrorAlert : isShowAlert , withLoader : isShowLoader) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    
                    if isShowAlert
                    {
                        GFunction.ShowAlert(message: response[kMessage].stringValue)
                    }
                    break
                case .success:
                    
                    self.arrVehicles = JsonList<Vehicle>.createModelArray(model: Vehicle.self, json: response[kData].arrayValue)
                    guard let cb = completion else { return}
                    cb(self.arrVehicles)
                    break
                    
                case .nodata:
                    guard let cb = completion else { return}
                    cb([])
                    break
                    
                default :
                    break
                }
            }
        }
        
    }
}

extension VehicleDataSource : AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag{
//            self.isEmergency = false
        }
    }
}
