/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct UserData : Codable {
	let id : Int?
	let fname : String?
	let lname : String?
	let email : String
	let country_code : String?
	let phone : String?
	let address : String?
	let latitude : String?
	let longitude : String?
	let profile_image : String?
	let token : String?
	let device_id : String?
	let device_type : String?
    let isSharedUser : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case fname = "fname"
		case lname = "lname"
		case email = "email"
		case country_code = "country_code"
		case phone = "phone"
		case address = "address"
		case latitude = "latitude"
		case longitude = "longitude"
		case profile_image = "profile_image"
		case token = "token"
		case device_id = "device_id"
		case device_type = "device_type"
        case isSharedUser = "is_shared_user"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		fname = try values.decodeIfPresent(String.self, forKey: .fname)
		lname = try values.decodeIfPresent(String.self, forKey: .lname)
		email = try values.decodeIfPresent(String.self, forKey: .email) ?? ""
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		token = try values.decodeIfPresent(String.self, forKey: .token)
		device_id = try values.decodeIfPresent(String.self, forKey: .device_id)
		device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        isSharedUser = try values.decodeIfPresent(Int.self, forKey: .isSharedUser)
	}

    init() {
        id = -1
        fname = ""
        lname = ""
        email = ""
        country_code = ""
        phone = ""
        address = ""
        latitude = ""
        longitude = ""
        profile_image = ""
        token = ""
        device_id = ""
        device_type = ""
        isSharedUser = 0
    }
    
    
}
