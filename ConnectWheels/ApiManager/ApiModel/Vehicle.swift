/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

class Vehicle : NSObject , mappable{
    
    
    var carMake : String!
    var carModel : String!
    var carPhoto : String!
    var defaultSettings : String!
    var deviceImei : String!
    var documents : String!
    var id : Int!
    var insertdate : String!
    var parking : String!
    var qrCode : String!
    var registrationNumber : String!
    var stateId : Int!
    var userId : Int!
    var variant : String!
    var vehicleMakeId : Int!
    var vehicleModelId : Int!
    var vehicleType : String!
    var isEmergency : String!
    var alertsCount : String!
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON) {
        if json.isEmpty{
            return
        }
        carMake = json["car_make"].stringValue
        carModel = json["car_model"].stringValue
        carPhoto = json["car_photo"].stringValue
        defaultSettings = json["default_settings"].stringValue
        deviceImei = json["device_imei"].stringValue
        documents = json["documents"].stringValue
        id = json["id"].intValue
        insertdate = json["insertdate"].stringValue
        parking = json["parking"].stringValue
        qrCode = json["qr_code"].stringValue
        registrationNumber = json["registration_number"].stringValue
        stateId = json["state_id"].intValue
        userId = json["user_id"].intValue
        variant = json["variant"].stringValue
        vehicleMakeId = json["vehicle_make_id"].intValue
        vehicleModelId = json["vehicle_model_id"].intValue
        vehicleType = json["vehicle_type"].stringValue
        isEmergency = json["is_emergency"].stringValue
        alertsCount = json["alert_counts"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if carMake != nil{
            dictionary["car_make"] = carMake
        }
        if carModel != nil{
            dictionary["car_model"] = carModel
        }
        if carPhoto != nil{
            dictionary["car_photo"] = carPhoto
        }
        if defaultSettings != nil{
            dictionary["default_settings"] = defaultSettings
        }
        if deviceImei != nil{
            dictionary["device_imei"] = deviceImei
        }
        if documents != nil{
            dictionary["documents"] = documents
        }
        if id != nil{
            dictionary["id"] = id
        }
        if insertdate != nil{
            dictionary["insertdate"] = insertdate
        }
        if parking != nil{
            dictionary["parking"] = parking
        }
        if qrCode != nil{
            dictionary["qr_code"] = qrCode
        }
        if registrationNumber != nil{
            dictionary["registration_number"] = registrationNumber
        }
        if stateId != nil{
            dictionary["state_id"] = stateId
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if variant != nil{
            dictionary["variant"] = variant
        }
        if vehicleMakeId != nil{
            dictionary["vehicle_make_id"] = vehicleMakeId
        }
        if vehicleModelId != nil{
            dictionary["vehicle_model_id"] = vehicleModelId
        }
        if vehicleType != nil{
            dictionary["vehicle_type"] = vehicleType
        }
        if isEmergency != nil{
            dictionary["is_emergency"] = isEmergency
        }
        if alertsCount != nil{
            dictionary["alert_counts"] = alertsCount
        }
        return dictionary
    }
    
    
    
}
