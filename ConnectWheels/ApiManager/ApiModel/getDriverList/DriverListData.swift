//
//	Data.swift
//
//	Create by - on 6/12/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
//import SwiftyJSON


class DriverListData : NSObject, NSCoding{

	var countryCode : String!
	var email : String!
	var fname : String!
	var id : Int!
	var lname : String!
	var phone : String!
	var profileImage : String!
	var shareTiming : [DriverListShareTiming]!
	var userId : Int!
	var vehicleId : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		countryCode = json["country_code"].stringValue
		email = json["email"].stringValue
		fname = json["fname"].stringValue
		id = json["id"].intValue
		lname = json["lname"].stringValue
		phone = json["phone"].stringValue
		profileImage = json["profile_image"].stringValue
		shareTiming = [DriverListShareTiming]()
		let shareTimingArray = json["share_timing"].arrayValue
		for shareTimingJson in shareTimingArray{
			let value = DriverListShareTiming(fromJson: shareTimingJson)
			shareTiming.append(value)
		}
		userId = json["user_id"].intValue
		vehicleId = json["vehicle_id"].intValue
	}
    
    class func fromArray(json : [JSON]) -> [DriverListData] {
        var dataArray = [DriverListData]()
        for dataJson in json{
            let value = DriverListData(fromJson: dataJson)
            dataArray.append(value)
        }
        return dataArray
    }

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if countryCode != nil{
			dictionary["country_code"] = countryCode
		}
		if email != nil{
			dictionary["email"] = email
		}
		if fname != nil{
			dictionary["fname"] = fname
		}
		if id != nil{
			dictionary["id"] = id
		}
		if lname != nil{
			dictionary["lname"] = lname
		}
		if phone != nil{
			dictionary["phone"] = phone
		}
		if profileImage != nil{
			dictionary["profile_image"] = profileImage
		}
		if shareTiming != nil{
			var dictionaryElements = [[String:Any]]()
			for shareTimingElement in shareTiming {
				dictionaryElements.append(shareTimingElement.toDictionary())
			}
			dictionary["share_timing"] = dictionaryElements
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		if vehicleId != nil{
			dictionary["vehicle_id"] = vehicleId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         countryCode = aDecoder.decodeObject(forKey: "country_code") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         fname = aDecoder.decodeObject(forKey: "fname") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         lname = aDecoder.decodeObject(forKey: "lname") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String
         shareTiming = aDecoder.decodeObject(forKey: "share_timing") as? [DriverListShareTiming]
         userId = aDecoder.decodeObject(forKey: "user_id") as? Int
         vehicleId = aDecoder.decodeObject(forKey: "vehicle_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if countryCode != nil{
			aCoder.encode(countryCode, forKey: "country_code")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if fname != nil{
			aCoder.encode(fname, forKey: "fname")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if lname != nil{
			aCoder.encode(lname, forKey: "lname")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if profileImage != nil{
			aCoder.encode(profileImage, forKey: "profile_image")
		}
		if shareTiming != nil{
			aCoder.encode(shareTiming, forKey: "share_timing")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}
		if vehicleId != nil{
			aCoder.encode(vehicleId, forKey: "vehicle_id")
		}

	}

}
