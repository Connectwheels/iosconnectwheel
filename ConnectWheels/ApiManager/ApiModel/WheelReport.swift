//
//	WheelReport.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 



class WheelReport : NSObject{

	var totalDistance : Int!
	var totalPerson : Int!
	var totalTime : Int!
	var totalTrips : Int!
    var totalAlertCount : Int!
    var messages : Int!
    var networkOperator : String!
    

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		totalDistance = json["total_distance"].intValue
		totalPerson = json["total_person"].intValue
		totalTime = json["total_time"].intValue
		totalTrips = json["total_trips"].intValue
        totalAlertCount = json["total_alert"].intValue
        messages = json["messages"].intValue
        networkOperator = json["network_operator"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if totalDistance != nil{
			dictionary["total_distance"] = totalDistance
		}
		if totalPerson != nil{
			dictionary["total_person"] = totalPerson
		}
		if totalTime != nil{
			dictionary["total_time"] = totalTime
		}
		if totalTrips != nil{
			dictionary["total_trips"] = totalTrips
		}
        if totalAlertCount != nil{
            dictionary["total_alert"] = totalAlertCount
        }
        if messages != nil{
            dictionary["messages"] = messages
        }
        if networkOperator != nil{
            dictionary["network_operator"] = networkOperator
        }
		return dictionary
	}

    

}
