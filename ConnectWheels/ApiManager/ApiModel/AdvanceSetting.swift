//
//  AdvanceSetting.swift
//  ConnectWheels
//
//  Created by  on 31/01/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import Foundation

class AdvanceSetting : NSObject{
    
    var brakeInKm : Int!
    var defaultSettings : String!
    var id : Int!
    var insertdate : String!
    var refreshSec : Int!
    var settingTiming : [WeeKDays] = []
    var speedInKm : Int!
    var speedInSec : Int!
    var vehicleId : Int!
    var volume : String!
    
    override init() {
        brakeInKm = 0
        defaultSettings = ""
        id = 0
        insertdate = ""
        refreshSec = 0
        
    }
    
    
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        brakeInKm = json["brake_in_km"].intValue
        defaultSettings = json["default_settings"].stringValue
        id = json["id"].intValue
        insertdate = json["insertdate"].stringValue
        refreshSec = json["refresh_sec"].intValue
        settingTiming = JsonList<WeeKDays>.createModelArray(model: WeeKDays.self, json: json["setting_timing"].arrayValue)
        speedInKm = json["speed_in_km"].intValue
        speedInSec = json["speed_in_sec"].intValue
        vehicleId = json["vehicle_id"].intValue
        volume = json["volume"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if brakeInKm != nil{
            dictionary["brake_in_km"] = brakeInKm
        }
        if defaultSettings != nil{
            dictionary["default_settings"] = defaultSettings
        }
        if id != nil{
            dictionary["id"] = id
        }
        if insertdate != nil{
            dictionary["insertdate"] = insertdate
        }
        if refreshSec != nil{
            dictionary["refresh_sec"] = refreshSec
        }
        
        dictionary["days"] = settingTiming.map{ $0.toDictionary() }
        
        if speedInKm != nil{
            dictionary["speed_in_km"] = speedInKm
        }
        if speedInSec != nil{
            dictionary["speed_in_sec"] = speedInSec
        }
        if vehicleId != nil{
            dictionary["vehicle_id"] = vehicleId
        }
        if volume != nil{
            dictionary["volume"] = volume
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    
    
}
