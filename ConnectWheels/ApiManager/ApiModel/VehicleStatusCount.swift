//
//  VehicleStatusCount.swift
//  ConnectWheels
//
//  Created by  on 04/02/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import Foundation


class VehicleStatusCount : NSObject, NSCoding{
    
    var idleCounts : Int!
    var movingCounts : Int!
    var notinrangeCounts : Int!
    var stopCounts : Int!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        idleCounts = json["idle_counts"].intValue
        movingCounts = json["moving_counts"].intValue
        notinrangeCounts = json["notinrange_counts"].intValue
        stopCounts = json["stop_counts"].intValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if idleCounts != nil{
            dictionary["idle_counts"] = idleCounts
        }
        if movingCounts != nil{
            dictionary["moving_counts"] = movingCounts
        }
        if notinrangeCounts != nil{
            dictionary["notinrange_counts"] = notinrangeCounts
        }
        if stopCounts != nil{
            dictionary["stop_counts"] = stopCounts
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        idleCounts = aDecoder.decodeObject(forKey: "idle_counts") as? Int
        movingCounts = aDecoder.decodeObject(forKey: "moving_counts") as? Int
        notinrangeCounts = aDecoder.decodeObject(forKey: "notinrange_counts") as? Int
        stopCounts = aDecoder.decodeObject(forKey: "stop_counts") as? Int
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if idleCounts != nil{
            aCoder.encode(idleCounts, forKey: "idle_counts")
        }
        if movingCounts != nil{
            aCoder.encode(movingCounts, forKey: "moving_counts")
        }
        if notinrangeCounts != nil{
            aCoder.encode(notinrangeCounts, forKey: "notinrange_counts")
        }
        if stopCounts != nil{
            aCoder.encode(stopCounts, forKey: "stop_counts")
        }
        
    }
    
}
