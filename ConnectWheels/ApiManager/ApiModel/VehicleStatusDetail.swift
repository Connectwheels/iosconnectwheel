//
//  VehicleStatusDetail.swift
//  ConnectWheels
//
//  Created by  on 04/02/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import Foundation

class VehicleStatusDetail : NSObject, NSCoding , mappable{
   
    
    var carMake : String!
    var carModel : String!
    var carPhoto : String!
    var date : Int!
    var defaultSettings : String!
    var deviceImei : String!
    var documents : String!
    var emergencyStatus : String!
    var heading : String!
    var id : Int!
    var ignitionStatus : String!
    var insertdate : String!
    var lastUpdates : String!
    var latitude : String!
    var latitudeDirection : String!
    var longitude : String!
    var longitudeDirection : String!
    var powerStatus : String!
    var qrCode : String!
    var registrationNumber : String!
    var speed : Int!
    var stateId : Int!
    var time : Int!
    var userId : Int!
    var variant : String!
    var vehicleId : Int!
    var vehicleModelId : Int!
    var vehicleStatus : String!
    var vehicleType : String!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        carMake = json["car_make"].stringValue
        carModel = json["car_model"].stringValue
        carPhoto = json["car_photo"].stringValue
        date = json["date"].intValue
        defaultSettings = json["default_settings"].stringValue
        deviceImei = json["device_imei"].stringValue
        documents = json["documents"].stringValue
        emergencyStatus = json["emergency_status"].stringValue
        heading = json["heading"].stringValue
        id = json["id"].intValue
        ignitionStatus = json["ignition_status"].stringValue
        insertdate = json["insertdate"].stringValue
        lastUpdates = json["last_updates"].stringValue
        latitude = json["latitude"].stringValue
        latitudeDirection = json["latitude_direction"].stringValue
        longitude = json["longitude"].stringValue
        longitudeDirection = json["longitude_direction"].stringValue
        powerStatus = json["power_status"].stringValue
        qrCode = json["qr_code"].stringValue
        registrationNumber = json["registration_number"].stringValue
        speed = json["speed"].intValue
        stateId = json["state_id"].intValue
        time = json["time"].intValue
        userId = json["user_id"].intValue
        variant = json["variant"].stringValue
        vehicleId = json["vehicle_id"].intValue
        vehicleModelId = json["vehicle_model_id"].intValue
        vehicleStatus = json["vehicle_status"].stringValue
        vehicleType = json["vehicle_type"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if carMake != nil{
            dictionary["car_make"] = carMake
        }
        if carModel != nil{
            dictionary["car_model"] = carModel
        }
        if carPhoto != nil{
            dictionary["car_photo"] = carPhoto
        }
        if date != nil{
            dictionary["date"] = date
        }
        if defaultSettings != nil{
            dictionary["default_settings"] = defaultSettings
        }
        if deviceImei != nil{
            dictionary["device_imei"] = deviceImei
        }
        if documents != nil{
            dictionary["documents"] = documents
        }
        if emergencyStatus != nil{
            dictionary["emergency_status"] = emergencyStatus
        }
        if heading != nil{
            dictionary["heading"] = heading
        }
        if id != nil{
            dictionary["id"] = id
        }
        if ignitionStatus != nil{
            dictionary["ignition_status"] = ignitionStatus
        }
        if insertdate != nil{
            dictionary["insertdate"] = insertdate
        }
        if lastUpdates != nil{
            dictionary["last_updates"] = lastUpdates
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if latitudeDirection != nil{
            dictionary["latitude_direction"] = latitudeDirection
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if longitudeDirection != nil{
            dictionary["longitude_direction"] = longitudeDirection
        }
        if powerStatus != nil{
            dictionary["power_status"] = powerStatus
        }
        if qrCode != nil{
            dictionary["qr_code"] = qrCode
        }
        if registrationNumber != nil{
            dictionary["registration_number"] = registrationNumber
        }
        if speed != nil{
            dictionary["speed"] = speed
        }
        if stateId != nil{
            dictionary["state_id"] = stateId
        }
        if time != nil{
            dictionary["time"] = time
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if variant != nil{
            dictionary["variant"] = variant
        }
        if vehicleId != nil{
            dictionary["vehicle_id"] = vehicleId
        }
        if vehicleModelId != nil{
            dictionary["vehicle_model_id"] = vehicleModelId
        }
        if vehicleStatus != nil{
            dictionary["vehicle_status"] = vehicleStatus
        }
        if vehicleType != nil{
            dictionary["vehicle_type"] = vehicleType
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        carMake = aDecoder.decodeObject(forKey: "car_make") as? String
        carModel = aDecoder.decodeObject(forKey: "car_model") as? String
        carPhoto = aDecoder.decodeObject(forKey: "car_photo") as? String
        date = aDecoder.decodeObject(forKey: "date") as? Int
        defaultSettings = aDecoder.decodeObject(forKey: "default_settings") as? String
        deviceImei = aDecoder.decodeObject(forKey: "device_imei") as? String
        documents = aDecoder.decodeObject(forKey: "documents") as? String
        emergencyStatus = aDecoder.decodeObject(forKey: "emergency_status") as? String
        heading = aDecoder.decodeObject(forKey: "heading") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        ignitionStatus = aDecoder.decodeObject(forKey: "ignition_status") as? String
        insertdate = aDecoder.decodeObject(forKey: "insertdate") as? String
        lastUpdates = aDecoder.decodeObject(forKey: "last_updates") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        latitudeDirection = aDecoder.decodeObject(forKey: "latitude_direction") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        longitudeDirection = aDecoder.decodeObject(forKey: "longitude_direction") as? String
        powerStatus = aDecoder.decodeObject(forKey: "power_status") as? String
        qrCode = aDecoder.decodeObject(forKey: "qr_code") as? String
        registrationNumber = aDecoder.decodeObject(forKey: "registration_number") as? String
        speed = aDecoder.decodeObject(forKey: "speed") as? Int
        stateId = aDecoder.decodeObject(forKey: "state_id") as? Int
        time = aDecoder.decodeObject(forKey: "time") as? Int
        userId = aDecoder.decodeObject(forKey: "user_id") as? Int
        variant = aDecoder.decodeObject(forKey: "variant") as? String
        vehicleId = aDecoder.decodeObject(forKey: "vehicle_id") as? Int
        vehicleModelId = aDecoder.decodeObject(forKey: "vehicle_model_id") as? Int
        vehicleStatus = aDecoder.decodeObject(forKey: "vehicle_status") as? String
        vehicleType = aDecoder.decodeObject(forKey: "vehicle_type") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if carMake != nil{
            aCoder.encode(carMake, forKey: "car_make")
        }
        if carModel != nil{
            aCoder.encode(carModel, forKey: "car_model")
        }
        if carPhoto != nil{
            aCoder.encode(carPhoto, forKey: "car_photo")
        }
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if defaultSettings != nil{
            aCoder.encode(defaultSettings, forKey: "default_settings")
        }
        if deviceImei != nil{
            aCoder.encode(deviceImei, forKey: "device_imei")
        }
        if documents != nil{
            aCoder.encode(documents, forKey: "documents")
        }
        if emergencyStatus != nil{
            aCoder.encode(emergencyStatus, forKey: "emergency_status")
        }
        if heading != nil{
            aCoder.encode(heading, forKey: "heading")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if ignitionStatus != nil{
            aCoder.encode(ignitionStatus, forKey: "ignition_status")
        }
        if insertdate != nil{
            aCoder.encode(insertdate, forKey: "insertdate")
        }
        if lastUpdates != nil{
            aCoder.encode(lastUpdates, forKey: "last_updates")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if latitudeDirection != nil{
            aCoder.encode(latitudeDirection, forKey: "latitude_direction")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if longitudeDirection != nil{
            aCoder.encode(longitudeDirection, forKey: "longitude_direction")
        }
        if powerStatus != nil{
            aCoder.encode(powerStatus, forKey: "power_status")
        }
        if qrCode != nil{
            aCoder.encode(qrCode, forKey: "qr_code")
        }
        if registrationNumber != nil{
            aCoder.encode(registrationNumber, forKey: "registration_number")
        }
        if speed != nil{
            aCoder.encode(speed, forKey: "speed")
        }
        if stateId != nil{
            aCoder.encode(stateId, forKey: "state_id")
        }
        if time != nil{
            aCoder.encode(time, forKey: "time")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        if variant != nil{
            aCoder.encode(variant, forKey: "variant")
        }
        if vehicleId != nil{
            aCoder.encode(vehicleId, forKey: "vehicle_id")
        }
        if vehicleModelId != nil{
            aCoder.encode(vehicleModelId, forKey: "vehicle_model_id")
        }
        if vehicleStatus != nil{
            aCoder.encode(vehicleStatus, forKey: "vehicle_status")
        }
        if vehicleType != nil{
            aCoder.encode(vehicleType, forKey: "vehicle_type")
        }
        
    }
    
}
