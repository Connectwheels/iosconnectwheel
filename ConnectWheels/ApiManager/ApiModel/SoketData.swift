/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import UIKit

class SoketData : NSObject {
    
    var mainPower: String!
    var tiltAngle: String!
    var emergencyStatus: String!
    var insertdate: String!
    var longitudeDir: String!
    var tamperAlert: String!
    var altitude: String!
    var latitude: String!
    var firmwareVersion: String!
    var date: String!
    var deviceImei: String!
    var packet: String!
    var tripId: Int!
    var ignitionStatus: String!
    var time: String!
    var isEmergency: Int!
    var id: Int!
    var oddometer: Double!
    var heading: String!
    var packetType: String!
    var vehicleBatteryVolt: Double!
    var internalBatteryVolt: Double!
    var longitude: String!
    var vendorId: String!
    var gsmStrength: String!
    var alertCounts: Int!
    var speedInKmH: Double!
    var networkOperator: String!
    var numberOfSatellite: String!
    var latitudeDir: String!
    var alertId: String!
    var gpsStatus: Int!
    
    init(_ json: JSON) {
        mainPower = json["main_power"].stringValue
        tiltAngle = json["tilt_angle"].stringValue
        emergencyStatus = json["emergency_status"].stringValue
        insertdate = json["insertdate"].stringValue
        longitudeDir = json["longitude_dir"].stringValue
        tamperAlert = json["tamper_alert"].stringValue
        altitude = json["altitude"].stringValue
        latitude = json["latitude"].stringValue
        firmwareVersion = json["firmware_version"].stringValue
        date = json["date"].stringValue
        deviceImei = json["device_imei"].stringValue
        packet = json["packet"].stringValue
        tripId = json["trip_id"].intValue
        ignitionStatus = json["ignition_status"].stringValue
        time = json["time"].stringValue
        isEmergency = json["is_emergency"].intValue
        id = json["id"].intValue
        oddometer = json["oddometer"].doubleValue
        heading = json["heading"].stringValue
        packetType = json["packet_type"].stringValue
        vehicleBatteryVolt = json["vehicle_battery_volt"].doubleValue
        internalBatteryVolt = json["internal_battery_volt"].doubleValue
        longitude = json["longitude"].stringValue
        vendorId = json["vendor_id"].stringValue
        gsmStrength = json["gsm_strength"].stringValue
        alertCounts = json["alert_counts"].intValue
        speedInKmH = json["speed_in_km_h"].doubleValue
        networkOperator = json["network_operator"].stringValue
        numberOfSatellite = json["number_of_satellite"].stringValue
        latitudeDir = json["latitude_dir"].stringValue
        alertId = json["alert_id"].stringValue
        gpsStatus = json["gps_status"].intValue
    }
    
}
