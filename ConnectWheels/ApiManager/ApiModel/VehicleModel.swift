//
//  VehicleModel.swift
//  ConnectWheels
//
//  Created by  on 28/01/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import Foundation

class VehicleModel : mappable {
   
    var id : Int!
    var image : String!
    var insertdate : String!
    var name : String!
    var status : String!
    var vehiclType : String!
    
    
    //commonm model for car model and car makes
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON) {
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        image = json["image"].stringValue
        insertdate = json["insertdate"].stringValue
        name = json["name"].stringValue
        status = json["status"].stringValue
        vehiclType = json["vehicle_type"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if insertdate != nil{
            dictionary["insertdate"] = insertdate
        }
        if name != nil{
            dictionary["name"] = name
        }
        if status != nil{
            dictionary["status"] = status
        }
        if vehiclType != nil{
            dictionary["vehicle_type"] = status
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        insertdate = aDecoder.decodeObject(forKey: "insertdate") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if insertdate != nil{
            aCoder.encode(insertdate, forKey: "insertdate")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }}
