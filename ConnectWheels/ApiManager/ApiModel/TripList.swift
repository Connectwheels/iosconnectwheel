//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 



class TripList : NSObject , mappable{
   
    
    var alertCounts : Int!
    var carMake : String!
    var carModel : String!
    var carPhoto : String!
    var date : String!
    var deviceImei : String!
    var distance : Float!
    var endPoint : String!
    var endTime : String!
    var endTrip : String!
    var id : Int!
    var insertdate : String!
    var maxSpeed : Float!
    var profileImage : String!
    var routes : String!
    var sharePersonCounts : Int!
    var sharePersonIds : String!
    var startPoint : String!
    var startTime : String!
    var startTrip : String!
    var status : String!
    var time : Int!
    var userId : Int!
    var username : String!
    var vehicleId : Int!
    var vehicleType : String!
    var registrationNumber: String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON) {
        if json.isEmpty{
            return
        }
        alertCounts = json["alert_counts"].intValue
        carMake = json["car_make"].stringValue
        carModel = json["car_model"].stringValue
        carPhoto = json["car_photo"].stringValue
        date = json["date"].stringValue
        deviceImei = json["device_imei"].stringValue
        distance = json["distance"].floatValue
        endPoint = json["end_point"].stringValue
        endTime = json["end_time"].stringValue
        endTrip = json["end_trip"].stringValue
        id = json["id"].intValue
        insertdate = json["insertdate"].stringValue
        maxSpeed = json["max_speed"].floatValue
        profileImage = json["profile_image"].stringValue
        routes = json["routes"].stringValue
        sharePersonCounts = json["share_person_counts"].intValue
        sharePersonIds = json["share_person_ids"].stringValue
        startPoint = json["start_point"].stringValue
        startTime = json["start_time"].stringValue
        startTrip = json["start_trip"].stringValue
        status = json["status"].stringValue
        time = json["time"].intValue
        userId = json["user_id"].intValue
        username = json["username"].stringValue
        vehicleId = json["vehicle_id"].intValue
        vehicleType = json["vehicle_type"].stringValue
        registrationNumber = json["registration_number"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if alertCounts != nil{
            dictionary["alert_counts"] = alertCounts
        }
        if carMake != nil{
            dictionary["car_make"] = carMake
        }
        if carModel != nil{
            dictionary["car_model"] = carModel
        }
        if carPhoto != nil{
            dictionary["car_photo"] = carPhoto
        }
        if date != nil{
            dictionary["date"] = date
        }
        if deviceImei != nil{
            dictionary["device_imei"] = deviceImei
        }
        if distance != nil{
            dictionary["distance"] = distance
        }
        if endPoint != nil{
            dictionary["end_point"] = endPoint
        }
        if endTime != nil{
            dictionary["end_time"] = endTime
        }
        if endTrip != nil{
            dictionary["end_trip"] = endTrip
        }
        if id != nil{
            dictionary["id"] = id
        }
        if insertdate != nil{
            dictionary["insertdate"] = insertdate
        }
        if maxSpeed != nil{
            dictionary["max_speed"] = maxSpeed
        }
        if profileImage != nil{
            dictionary["profile_image"] = profileImage
        }
        if routes != nil{
            dictionary["routes"] = routes
        }
        if sharePersonCounts != nil{
            dictionary["share_person_counts"] = sharePersonCounts
        }
        if sharePersonIds != nil{
            dictionary["share_person_ids"] = sharePersonIds
        }
        if startPoint != nil{
            dictionary["start_point"] = startPoint
        }
        if startTime != nil{
            dictionary["start_time"] = startTime
        }
        if startTrip != nil{
            dictionary["start_trip"] = startTrip
        }
        if status != nil{
            dictionary["status"] = status
        }
        if time != nil{
            dictionary["time"] = time
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if username != nil{
            dictionary["username"] = username
        }
        if vehicleId != nil{
            dictionary["vehicle_id"] = vehicleId
        }
        if vehicleType != nil{
            dictionary["vehicle_type"] = vehicleType
        }
        return dictionary
    }
    
    
    
}
