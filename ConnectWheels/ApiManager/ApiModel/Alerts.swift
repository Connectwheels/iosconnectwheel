//
//  Alerts.swift
//  ConnectWheels
//
//  Created by  on 12/02/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import Foundation

class Alerts : mappable {
    
    var id : Int!
    var vehicleId : Int!
    var carPhoto : String!
    var insertdate : String!
    var alertType : String!
    var message : String!
    var status : String!
     var tripId : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON) {
        if json.isEmpty{
            return
        }
        id = json["id"].intValue
        vehicleId = json["vehicle_id"].intValue
        carPhoto = json["car_photo"].stringValue
        insertdate = json["insertdate"].stringValue
        alertType = json["alert_type"].stringValue
        message = json["message"].stringValue
        status = json["status"].stringValue
        tripId = json["trip_id"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if carPhoto != nil{
            dictionary["image"] = carPhoto
        }
        if insertdate != nil{
            dictionary["insertdate"] = insertdate
        }
        if alertType != nil{
            dictionary["name"] = alertType
        }
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if tripId != nil{
            dictionary["trip_id"] = tripId
        }
        return dictionary
    }
    
    
}
