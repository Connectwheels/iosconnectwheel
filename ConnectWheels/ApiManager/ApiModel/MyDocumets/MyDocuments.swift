//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 



class MyDocuments : NSObject, NSCoding{

    
    var imageToAdd : [Any] = []
    
	var folderName : String!
	var id : Int!
    var image : [String] = []
	var insertdate : String!
	var validity : String!
	var vehicleId : Int!
    var preImages : String = ""
    
    
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    
    override init() {
        
    }
    
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		folderName = json["folder_name"].stringValue
		id = json["id"].intValue
		image = [String]()
		let imageArray = json["image"].arrayValue
		for imageJson in imageArray{
			image.append(imageJson.stringValue)
		}
		insertdate = json["insertdate"].stringValue
		validity = json["validity"].stringValue
		vehicleId = json["vehicle_id"].intValue
        preImages = json["pre_images"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if folderName != nil{
			dictionary["folder_name"] = folderName
		}
		if id != nil{
			dictionary["id"] = id
		}
		
        dictionary["image"] = image
		
		if insertdate != nil{
			dictionary["insertdate"] = insertdate
		}
		if validity != nil{
			dictionary["validity"] = validity
		}
		if vehicleId != nil{
			dictionary["vehicle_id"] = vehicleId
		}
        if preImages != nil{
            dictionary["pre_images"] = preImages
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         folderName = aDecoder.decodeObject(forKey: "folder_name") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? [String] ?? []
         insertdate = aDecoder.decodeObject(forKey: "insertdate") as? String
         validity = aDecoder.decodeObject(forKey: "validity") as? String
         vehicleId = aDecoder.decodeObject(forKey: "vehicle_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if folderName != nil{
			aCoder.encode(folderName, forKey: "folder_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if insertdate != nil{
			aCoder.encode(insertdate, forKey: "insertdate")
		}
		if validity != nil{
			aCoder.encode(validity, forKey: "validity")
		}
		if vehicleId != nil{
			aCoder.encode(vehicleId, forKey: "vehicle_id")
		}

	}

    class func fromArray(json : [JSON]) -> [MyDocuments] {
        var dataArray = [MyDocuments]()
        for dataJson in json{
            let value = MyDocuments(fromJson: dataJson)
            dataArray.append(value)
        }
        return dataArray
    }
    
}

