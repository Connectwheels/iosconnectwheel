//
//  ImageUpload.swift
//  
//
//  Created by  on 16/01/18.
//  Copyright © 2018 -. All rights reserved.
//

import UIKit
import AWSS3

enum ImageType : String {
    case user
    case vehicle
    case vehicleDocuments
    case userDocuments
}

class ImageUpload: NSObject {

    
    /*
     ===================================== - Live
    AWS S3 Access::
    =====================================
    Bucket: connectwheels
    Access key ID: AKIAI5JSUML6ZSNPUUAQ
    Secret access key: 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
    Region: eu-west-1
    URL: https://s3-eu-west-1.amazonaws.com/connectwheels/
   */
    
    static let shared : ImageUpload = ImageUpload()
    
    let kAWSAccessKeyId                 = "AKIAI5JSUML6ZSNPUUAQ"
    let kAWSSecretKey                   = "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"
    let kfolder                         = ""
    let kAWSBucket                      = "connectwheels"
    let kAWSPath                        = "https://s3-eu-west-1.amazonaws.com/connectwheels/"//"https://s3-eu-west-1.amazonaws.com/hlink-bucket/"
  
    
    func uploadImage(_ showLoader : Bool = true,_ image : UIImage , _ mimeType : String, _ imageType : ImageType, withBlock completion :((String? , String?) -> Void)?) {
        if showLoader {
            GFunction.sharedInstance.addLoader()
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //S3 bucket setup
        
        let credentialsProvider : AWSStaticCredentialsProvider = AWSStaticCredentialsProvider(accessKey: kAWSAccessKeyId, secretKey: kAWSSecretKey)
        let configuration : AWSServiceConfiguration = AWSServiceConfiguration(region: AWSRegionType.EUWest1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        var bodyURL : URL?
        if let url : URL = self.storeDataInDirectory(image) {
            bodyURL = url
        }
        else {
            return
        }
        
        let transferManager : AWSS3TransferManager = AWSS3TransferManager.default()
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest.bucket = kAWSBucket
        
        switch imageType {
        case ImageType.user:
            uploadRequest.key = kfolder + "user/"
            break
        case ImageType.vehicle:
            uploadRequest.key = kfolder + "vehicle/"
        case .vehicleDocuments:
            uploadRequest.key = kfolder + "vehicle/document/"
            break
       case .userDocuments:
            uploadRequest.key = kfolder + "user/document/"
        }
        
        
        
        uploadRequest.key = uploadRequest.key! + bodyURL!.lastPathComponent
        
        uploadRequest.body = bodyURL!
        uploadRequest.contentType = mimeType
        uploadRequest.acl = .publicRead
        
        transferManager.upload(uploadRequest).continueWith { (task : AWSTask) -> Any? in
            if showLoader {
                GFunction.sharedInstance.removLoader()
            }
            
            DispatchQueue.main.async { // Correct
               UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            if task.error == nil {
                debugPrint("Complete task")
                debugPrint(task.result ?? "")
                completion!(self.kAWSPath + uploadRequest.key! , bodyURL!.lastPathComponent)
            }
            else {
            
                debugPrint(task.result as Any)
                debugPrint(task.error?.localizedDescription as Any)
                completion!(nil,nil)
            }
            return nil
        }
    }
    
    func removeImage(_ showLoader : Bool = true,_ image : UIImage, _ imageType : ImageType, withBlock completion :((String? , String?) -> Void)?) {
        if showLoader {
            GFunction.sharedInstance.addLoader()
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //S3 bucket setup
        
        let credentialsProvider : AWSStaticCredentialsProvider = AWSStaticCredentialsProvider(accessKey: kAWSAccessKeyId, secretKey: kAWSSecretKey)
        let configuration : AWSServiceConfiguration = AWSServiceConfiguration(region: AWSRegionType.EUWest1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        var bodyURL : URL?
        if let url : URL = self.storeDataInDirectory(image) {
            bodyURL = url
        }
        else {
            return
        }
        
        let s3 = AWSS3.default()
//        let transferManager : AWSS3TransferManager = AWSS3TransferManager.default()
        let removeRequest : AWSS3DeleteObjectRequest = AWSS3DeleteObjectRequest()
        removeRequest.bucket = kAWSBucket
        
        switch imageType {
        case ImageType.user:
            removeRequest.key = kfolder + "user/"
            break
        case ImageType.vehicle:
            removeRequest.key = kfolder + "vehicle/"
            break
        case ImageType.vehicleDocuments:
            removeRequest.key = kfolder + "vehicle/document/"
            break
         case .userDocuments:
            removeRequest.key = kfolder + "user/document/"
            break
        }
   
        removeRequest.key = removeRequest.key! + bodyURL!.lastPathComponent
        
        s3.deleteObject(removeRequest).continueWith { (task: AWSTask) -> Any? in
            if showLoader {
                GFunction.sharedInstance.removLoader()
            }
            
            if let error = task.error {
                debugPrint("Error occurred: \(error)")
                return nil
            }
            debugPrint("Deleted successfully.")
            return nil
        }
    }

    func storeDataInDirectory(_ data : UIImage) -> URL? {
        
        let imgData : Data = UIImageJPEGRepresentation(data, 0.5)!
        
        // *** Get documents directory path *** //
        let paths1 = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory1 = paths1[0] as String
        
        // *** Append video file name *** //
        let datapathT = documentsDirectory1.appending("/\(self.getRandomString()).jpeg")
        
        let tempURl : URL = URL(fileURLWithPath: datapathT)
        
        //        let urlT = tempURl.absoluteURL
        
        let urlT = tempURl
        // *** Remove video file data to path *** //
        let fileManager = FileManager.default
        
        do {
            try fileManager.removeItem(at: urlT)
        }
        catch {
            
            print("No Duplicate video")
        }
        
        // *** Write image file data to path *** //
        var strPathForFile = ""
        do {
            try imgData.write(to: urlT)
            return urlT
            
        } catch  {
            print("some error in preview image: ")
            return nil
        }
    }
    
    func getSizeOfData(data : Data) -> String {
        let byteCount = data.count//512_000 // replace with data.count
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
        bcf.countStyle = .file
        let string = bcf.string(fromByteCount: Int64(byteCount))
        print(string)
        if byteCount == 0 {
            return "0"
        }
        return string
    }
    
    func getTimeStampFromDate() -> (double : Double,string : String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1 : String = dateFormatter.string(from: Date())
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date2 : Date = dateFormatter1.date(from: date1)!
        let timeStamp = date2.timeIntervalSince1970
        
        return (timeStamp,String(format: "%f", timeStamp))
    }
    
    //------------------------------------------------------
    
    func getRandomString() -> String {
        let timeStamp = self.getTimeStampFromDate()
        return "\(String(format: "%0.0f", timeStamp.double))"
    }
}
