//
//  ApiConstant.swift
//  ConnectWheels
//
//  Created by  on 17/07/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import Foundation


enum endpoints : String {
    case login = "user/login/"
    case forgotPassword  = "user/forgotpassword/"
    case updateDeviceId = "user/updatedeviceid/"
    case otpVerification = "user/otp_verification/"
    case signUp =   "user/signup"
    case changePassword = "user/changepassword/"
    case logout = "user/logout/"
    case contactUs = "user/contactus/"
    case editProfile = "user/edit/"
    case verifyVehicle = "vehicle/verify_vehicle/"
    
    //Documents
    case getUserDocument = "user/get_document_list/"
    case addUserDocuments = "user/add_document/"
    case editUserDocuments = "user/edit_document/"
    case deleteUserDocuments = "user/remove_document/"
    
    case getVehicleDocument = "vehicle/get_document_list/"
    case addVehicleDocuments = "vehicle/add_document/"
    case editVehicleDocuments = "vehicle/edit_document/"
    case deleteVehicleDocuments = "vehicle/remove_document/"
    
    case updateLocation = "user/updatelatlong/"
    case addVehicle = "vehicle/add_vehicle/"
    case getVehicleList = "vehicle/get_vehicle_list/"
    case deleteVehicle = "vehicle/delete_vehicle/"
    case getVehicle = "vehicle/get_vehicle/"
    case chekcEmergency = "vehicle/check_emergency_status/"
    case vehicleLastLocation = "vehicle/get_vehicle_last_location/"
    
    case getDriverList = "share/get_driver_list/"
    case addDriver = "share/add_driver/"
    case vehicleModelList = "vehicle/get_vehicle_model_list/"
    case getAdvanceSetting = "vehicle/get_vehicle_settings/"
    case saveAdvanceSetting = "vehicle/save_vehicle_settings/"
    case vehicleCarMakeList = "vehicle/get_vehicle_make_list"
    case changeParking = "vehicle/change_parking/"
    
    //trip
    
    case getTripCount = "trip/get_trip_counts/"
    case getTripList = "trip/get_trip_list/"
    case getTripDetail = "trip/get_trip_details/"
    
    //get status
    case getStatusList = "vehicle/get_vehicle_status_counts/"
    case getStatusDetails = "vehicle/get_vehicle_status_details/"
    
    //alerts
    case getAlerts = "vehicle/get_alert_notifications/"
    case clearAlerts = "vehicle/clear_alerts/"
    
    //sos
    case getSOSUserList = "vehicle/get_user_sos_numbers/"
    case sendSOS = "vehicle/send_sos/"
    case removeSOS = "vehicle/remove_sos_numbers/"
    case addSos = "vehicle/add_sos/"
}


enum ApiResponseCode : Int {
    
    case invalidrequest
    case success
    case nodata
    case inactive
    case otpberification
    case emailVerification
    case forceupdate
    case custom
    case unknown
}


//MARK:- API Response Key Constant

let kData                           :String = "data"
let kMessage                        :String = "message"
let kCode                           :String = "code"
let kUserDetail                     :String = "userDetail"
let kUserToken                      :String = "token"
let kRememberDetails                :String = "RememberDetails"
let keyTag = "flag"

//MARK:- API Key Constant

//login

let kemail_phone                =   "email_phone"
let kpassword                   =   "password"
let kdevice_id                  =   "device_id"
let kdevice_type                =   "device_type"
let kos_version                 =   "os_version"
let kdevice_name                =   "device_name"
let kmodel_name                 =   "model_name"
let kip                         =   "ip"
let kuuid                       =   "uuid"
let kSocialId                   =   "social_id"

//Sign up
let kfname                      =   "fname"
let klname                      =   "lname"
let kemail                      =   "email"
let kcountry_code               =   "country_code"
let kphone                      =   "phone"
let kaddress                    =   "address"
let klatitude                   =   "latitude"
let klongitude                  =   "longitude"
let kApiOtp                     =   "otp"
let ksignupType                 =   "signup_type"

// Add Vehicle
let kqr_code                    =   "qr_code"
let kregistration_number        =   "registration_number"
let kcar_make                   =   "car_make"
let kcar_model                  =   "car_model"
let kvariant                    =   "variant"
let kdocuments                  =   "documents"
let kvehicle_id                 =   "vehicle_id"
let kdocuments_id               =   "document_id"
let kvehicleType                =   "vehicle_type"
let kvehicleModelId             =   "vehicle_model_id"
let kvehicleMakeId              =   "vehicle_make_id"

//change password

let kold_password               =   "old_password"
let knew_password               =   "new_password"

// contact us

let ksubject                    =   "subject"
let kmessage                    =   "message"

// images key

let kprofile_image                  =  "profile_image"
let kimage                          =  "image"
let kcarphoto                       =  "car_photo"

// Add Driver

let kdays                       =   "days"
let kstart_time                 =   "start_time"
let kend_time                 =   "end_time"

// Trip Count

let kstartDate                  = "start_date"
let kendDate                    = "end_date"

// trip

let kpage                       =   "page"
let ktripId                     =   "trip_id"
let kDeviceImei                 =   "device_imei"
