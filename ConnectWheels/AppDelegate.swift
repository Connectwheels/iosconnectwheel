//
//  AppDelegate.swift
//  ConnectWheels
//
//  Created by  on 23/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit
@_exported import IQKeyboardManagerSwift
@_exported import GooglePlacePicker
@_exported import GoogleMaps
@_exported import FSCalendar
@_exported import Moya
@_exported import Alamofire
@_exported import CryptoSwift
@_exported import SDWebImage
@_exported import Firebase
@_exported import Contacts
@_exported import ContactsUI
@_exported import GoogleSignIn
@_exported import FBSDKCoreKit
@_exported import FBSDKLoginKit

import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var device_token            : String                    = "0"
    
    override init() {
        
        GMSPlacesClient.provideAPIKey(AppCredential.goolePlaceApiKey.rawValue)
        GMSServices.provideAPIKey(AppCredential.googleApiKey.rawValue)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().toolbarPreviousNextAllowedClasses = [subview.self]
        self.registerForRemoteNotification()
        LocationManager.shared.getLocation()
        if UserDefault.bool(forKey: keyisTutorialSeen){
            GFunction.SetHomeScreen()
        }
        
        // set up google
        GIDSignIn.sharedInstance().clientID = AppCredential.googleClientId.rawValue
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        UINavigationBar.appearance().shadowImage = UIImage()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        updateDeviceIdWs()
        updateLocationWs()
        UIApplication.shared.applicationIconBadgeNumber = 1
        UIApplication.shared.applicationIconBadgeNumber = 0
        UIApplication.shared.cancelAllLocalNotifications()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    //------------------------------------------------------
    
    //MARK:- Permissions
    
    //For Notification
    func registerForRemoteNotification() {
        UIApplication.shared.registerForRemoteNotifications()
        let settings = UIUserNotificationSettings(types: [.alert , .sound , .badge], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        
        if #available(iOS 10.0, *) {
            let center : UNUserNotificationCenter = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound , .alert , .badge], completionHandler: { (granted, error) in
                if ((error != nil)) {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func updateLocationWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/updatelatlong/
         
         Parameter   : latitude, longitude
         
         Optional    :
         
         Comment     : This api are used to user can update his current latlong.
         
         ==============================
         */
        
        if currentUser.id == -1 {
            return
        }
        
        var param = [String : Any]()
        param[klatitude] = LocationManager.shared.getUserLat()
        param[klongitude] = LocationManager.shared.getUserLong()
     
        ApiManger.shared.makeRequest(method: .updateLocation , parameter: param , withErrorAlert : false , withLoader : false ,  withdebugLog : false) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                   
                    break
                case .success:
               
                    
                    break
                    
                case .inactive:
                  
                    break
                    
                default :
                    break
                }
            }
        }
    }

    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func updateDeviceIdWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/updatedeviceid/
         
         Parameter   : device_id, device_type
         
         Optional    :
         
         Comment     : This api are used to user can update his device info.
         
         ==============================
         */
        
        if currentUser.id == -1 {
            return
        }
        
        var param = [String : Any]()
        param[kdevice_id] = appDelegate.device_token
        param[kdevice_type] = "I"
        
        ApiManger.shared.makeRequest(method: .updateDeviceId , parameter: param , withErrorAlert : false , withLoader : false , withdebugLog : false) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    
                    break
                case .success:
                    
                    
                    break
                    
                case .inactive:
                    
                    break
                    
                default :
                    break
                }
            }
        }
    }
}


//------------------------------------------------------
//MARK:- Open Url , Device Token & Push Methods

extension AppDelegate : UNUserNotificationCenterDelegate{
    
    //------------------------------------------------------
    
    //MARK:- Reister for notification
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceId = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        debugPrint(deviceId)
        device_token = deviceId
        updateDeviceIdWs()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let info = JSON(notification.request.content.userInfo as Any)
        
        switch info[keyTag].stringValue {
        case "Emergency" ,"Parking" :
            VehicleDataSource.shared.isEmergency = true
            completionHandler([.alert,.badge])
            break
        case "Test" : // for development testing , nothing related to real data
            VehicleDataSource.shared.isEmergency = true
            completionHandler([.alert,.badge])
            break
        default :
            break
        }
        
        
        completionHandler([.alert,.badge,.sound])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme == AppCredential.facebookAppID.rawValue {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if url.scheme == AppCredential.facebookAppID.rawValue {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
}


//------------------------------------------------------

//MARK:- Handle Push

extension AppDelegate {

    func handlePushOnPresent(info : JSON)  {
//        debugPrint("Push :-----",info)
//         if let _ = UserDefault.value(forKey: kUserDetail) {
//            switch info[keyTag].stringValue {
//            case "Emergency" ,"Parking" :
//                VehicleDataSource.shared.isEmergency = true
//                break
//            case "Test" : // for development testing , nothing related to real data
//                VehicleDataSource.shared.isEmergency = true
//                break
//            default :
//                break
//            }
//        }
    }

}
