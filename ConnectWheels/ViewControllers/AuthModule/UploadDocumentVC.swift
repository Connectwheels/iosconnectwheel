//
//  UploadDocumentVC.swift
//  ConnectWheels
//
//  Created by  on 25/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit


class UploadDocumentVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var colDocuments: UICollectionView!
    @IBOutlet var viewTransperentBG: [UIImageView]!
     @IBOutlet weak var btnSkip: UIButton!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [UploadModel] = []
    var imagePicker : UIImagePickerController = UIImagePickerController()
    var isEditVehicle : Bool = false
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    
    @objc func imgPickerOpen(isShowRemove : Bool = false) {
        
        self.view.endEditing(true)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if TARGET_OS_SIMULATOR == 1
            {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    self.imagePicker.sourceType = .photoLibrary
                    OperationQueue.main.addOperation({() -> Void in
                        
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                }
                
                return
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
                
            }
            
        }))
        
       
        
        actionSheet.addAction(UIAlertAction(title: "Gallery".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.sourceType = .photoLibrary
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
            }
            else {
                // GFunction.sharedInstance.alert("Gallery is not available")
            }
        }))
        
//        if isShowRemove {
//            actionSheet.addAction(UIAlertAction(title: "Remove Photo".Localized(), style: .default, handler: { (UIAlertAction) in
//
//                if let index = self.colDocuments.indexPathsForSelectedItems?.first
//                {
//                    self.arrList[index.row].isDefaultImage = true
//                    
//                    self.colDocuments.reloadData()
//                }
//                
//            }))
//        }
        
    
        
        actionSheet.addAction(UIAlertAction(title: "CANCEL".Localized(), style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func setUpView() {
        
        arrList.removeAll()
        
        let detail1 = UploadModel(title: "Registration Photo", image: #imageLiteral(resourceName: "uploadReg"))
        arrList.append(detail1)
        
        let detail2 = UploadModel(title: "National ID Photo", image: #imageLiteral(resourceName: "uploadID"))
        arrList.append(detail2)
        
        let detail3 = UploadModel(title: "Car Front Photo", image: #imageLiteral(resourceName: "uploadFront"))
        arrList.append(detail3)
        
        let detail4 = UploadModel(title: "Car Back Photo", image: #imageLiteral(resourceName: "uploadBack"))
        arrList.append(detail4)
        
        let detail5 = UploadModel(title: "Vehicle Photo", image: #imageLiteral(resourceName: "vehicle_info_upload_image"))
        arrList.append(detail5)
        
        let detail6 = UploadModel(title: "", image: #imageLiteral(resourceName: "uplus"))
        arrList.append(detail6)
        
        colDocuments.reloadData()
        
        if isEditVehicle {
            btnSkip.superview?.backgroundColor = colors.whiteColor
            self.view.backgroundColor = colors.blueColor
            _ = viewTransperentBG.map{ $0.isHidden = false }
            self.navigationItem.leftBarButtonItem?.tintColor = colors.whiteColor
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    @IBAction func btnNextClicked(_ sender: Any) {
     
        
        view.endEditing(true)
        
        if let _ = self.parent?.parent as? SignUpVC  {
            
            GFunction.SetHomeScreen()
            return
            
        }else if let _ = self.parent?.parent as? AddVehicleVC  {
            
            GFunction.SetHomeScreen()
            return
        }
        
        GFunction.SetHomeScreen()
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        view.endEditing(true)
        
        if let _ = self.parent?.parent as? SignUpVC  {
            
            GFunction.SetHomeScreen()
            return
            
        }else if let _ = self.parent?.parent as? AddVehicleVC  {
            
            GFunction.SetHomeScreen()
            return
        }
        
        GFunction.SetHomeScreen()
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        
        if let parent = self.parent?.parent as? SignUpVC  {
            parent.colList.isHidden = false
            if parent.selectIndex < SignUpViews.uploadDocuments.rawValue  {
                setUpView()
            }
            parent.selectIndex = SignUpViews.uploadDocuments.rawValue
        }else if let parent = self.parent?.parent as? AddVehicleVC  {
            parent.colList.isHidden = false
            if parent.selectIndex < AddVehicleViews.uploadDocuments.rawValue  {
                setUpView()
            }
            parent.selectIndex = AddVehicleViews.uploadDocuments.rawValue
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension UploadDocumentVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) {
            
        }
        
        if let index = colDocuments.indexPathsForSelectedItems?.first
        {
            if index.item == arrList.count - 1
            {
                let detail = UploadModel(title: "", image: #imageLiteral(resourceName: "uplus"))
                arrList.append(detail)
            }
            arrList[index.row].isDefaultImage = false
            arrList[index.row].selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            colDocuments.reloadData()
        }
        
//        imgProfile.image = info[UIImagePickerControllerEditedImage] as? UIImage
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true)
        {
            
        }
    }
}

//------------------------------------------------------

//MARK:- Collection view method

extension UploadDocumentVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : UploadDocumentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadDocumentCell", for: indexPath) as! UploadDocumentCell
        
        let detail = arrList[indexPath.row]
        
        cell.lblDesc.text = detail.title
        
        if detail.isDefaultImage {
            cell.imgPhoto.image = detail.image
            cell.lblDesc.isHidden = true
            cell.blackLayer.isHidden = true
        }else
        {
            cell.imgPhoto.image = detail.selectedImage
            cell.lblDesc.isHidden = false
            cell.blackLayer.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        imgPickerOpen(isShowRemove: !arrList[indexPath.row].isDefaultImage)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (kScreenWidth - 100) / 2, height: (kScreenWidth - 100) / 2)
    }
}


//------------------------------------------------------

//MARK:- UploadCell

class UploadDocumentCell : UICollectionViewCell {
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var blackLayer: UIImageView!
    
    override func awakeFromNib() {
        if let _ = lblDesc
        {
            lblDesc.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.whiteColor)
            blackLayer.isHidden = true
            lblDesc.isHidden = true
        }
    }
}

