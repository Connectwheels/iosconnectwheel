//
//  SearchVC.swift
//  ConnectWheels
//
//  Created by  on 02/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class SearchVC: BlueNavigationBar {

    //MARK:- Outlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblSearch: UITableView!
    
    //------------------------------------------------------
    
    //MARK:- Class Variable
    var codes = [Codes]()
    var countries = [Countries]()
    var countryCodeArr = [String]()
    var completion : completion!
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setCountryData(){
        
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        
        do {
            let fetchedData = try Data(contentsOf: url)
            
            // let car = try JSONSerialization.jsonObject(with: data, options: [])
            let fetchedCodes = try JSONDecoder().decode(Countries.self, from: fetchedData)
            countries = [fetchedCodes]
        }catch{
            print("Error")
        }
        
    }
    
    func setUpView() {
        setCountryData()
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.tintColor = colors.whiteColor
        
        searchBar.becomeFirstResponder()
        tblSearch.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

}

extension SearchVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !searchBar.text!.isEmpty
        {
            
            
            let filterArray = (countries[0].countries ).filter ({
                
                $0.name?.range(of: searchBar.text!, options: .caseInsensitive) != nil || $0.code!.range(of: searchBar.text!, options: .caseInsensitive) != nil
                
            })
            
           return filterArray.count
        }
        
        return countries[0].countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryData", for: indexPath) as! CountryData
        
        var details = "\(countries[0].countries[indexPath.row].code!)  \(countries[0].countries[indexPath.row].name!)"
        
        if  !searchBar.text!.isEmpty
        {
            
            
            let filterArray = (countries[0].countries ).filter ({
                
                $0.name?.range(of: searchBar.text!, options: .caseInsensitive) != nil || $0.code!.range(of: searchBar.text!, options: .caseInsensitive) != nil
                
            })
            
            details = "\(filterArray[indexPath.row].code!)  \(filterArray[indexPath.row].name!)"
        }
        
        cell.lblCountry.text = details
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cb = completion else { return }
        
        var details = countries[0].countries[indexPath.row]
        
        if  !searchBar.text!.isEmpty
        {
            
            
            let filterArray = (countries[0].countries ).filter ({
                
                $0.name?.range(of: searchBar.text!, options: .caseInsensitive) != nil || $0.code!.range(of: searchBar.text!, options: .caseInsensitive) != nil
                
            })
            
            details = filterArray[indexPath.row]
        }
        
        cb(details)
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

class CountryData : UITableViewCell {
    
    @IBOutlet weak var lblCountry: UILabel!
    
}

extension SearchVC : UISearchBarDelegate , UIScrollViewDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tblSearch.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        tblSearch.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblSearch.contentOffset.y < -75 {
            self.dismiss(animated: true, completion: nil)
        }
    }
}



