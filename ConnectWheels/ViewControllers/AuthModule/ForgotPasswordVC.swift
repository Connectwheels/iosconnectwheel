//
//  ForgotPasswordVC.swift
//  ConnectWheels
//
//  Created by  on 23/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit



class ForgotPasswordVC: WhiteNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var lblResetPasswordDescription: UILabel!
    @IBOutlet weak var txtEmail: FloatingCommon!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String {
        
        if txtEmail.text!.isEmpty {
            return kvBlankPhoneNumber
        } else if (txtEmail.text!.count < 6 || txtEmail.text!.count > 14) {
            return kvValidPhoneNumber
         }
        /*else if txtEmail.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) != nil && !(GFunction.isValidEmail(text: txtEmail.text!))
        {
            return kvValidEmail
            
        }*/
        
        return String()
    }
    
    func setUpView() {
        lblResetPasswordDescription.setLineSpacing(lineHeightMultiple: 1.5)
        txtEmail.RightPaddingView(image: #imageLiteral(resourceName: "imgCall").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtEmail.titleHeight(), width: 30, height: txtEmail.frame.size.height - txtEmail.titleHeight()))
        txtEmail.tintColor = colors.blackColor
        txtEmail.regex = "^[0-9]{0,16}$"
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        
        let error = validate()
        
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        //self.navigationController?.popViewController(animated: true)
        forgotWs()
        
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Ws
    
    func forgotWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/forgotpassword/
         
         Parameter   : email_phone
         
         Optional    :
         
         Comment     : This api will be used for user can change his personal password by email or phone.
         
         ==============================
         */
        
        let param : [String : Any] = [kemail_phone: txtEmail.text as Any]
        
        ApiManger.shared.makeRequest(method: .forgotPassword , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    (response[kMessage].stringValue).showAlertsWithAction(action: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    break
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}
