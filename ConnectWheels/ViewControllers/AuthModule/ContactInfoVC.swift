//
//  ContactInfoVC.swift
//  ConnectWheels
//
//  Created by  on 23/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit


class ContactInfoVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtMobile: FloatingCommon!
    @IBOutlet weak var txtCode: FloatingCommon!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    let picker : UIPickerView = UIPickerView()
    var codes = [Codes]()
    var countries = [Countries]()
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        
        /*if txtCode.text!.isEmpty{
            return kvBlankCountry
            
        }else*/
        
        if txtMobile.text!.isEmpty{
            return kvBlankPhoneNumber
        }else if txtMobile.text!.count < 6 {
            return kvValidPhoneNumber
        }
        
        return String()
    }
    
    func setCountryData(){
        
        let path = Bundle.main.path(forResource: "countryCode", ofType: "json")
        let url = URL(fileURLWithPath: path!)
        
        do {
            let fetchedData = try Data(contentsOf: url)
            
            // let car = try JSONSerialization.jsonObject(with: data, options: [])
            let fetchedCodes = try JSONDecoder().decode(Countries.self, from: fetchedData)
            countries = [fetchedCodes]
        }catch{
            print("Error")
        }
        
    }
    
    func setUpView() {
        setCountryData()
        txtMobile.regex = "^[0-9]{0,16}$"
         txtMobile.RightPaddingView(image: #imageLiteral(resourceName: "imgCall").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
        
        picker.delegate = self
        picker.dataSource = self
        // keep country code if required
       // txtCode.inputView = picker
        
       //txtCode.RightPaddingView(image: #imageLiteral(resourceName: "downArrow").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    @IBAction func btnCountryClicked(_ sender: Any) {
        let obj : SearchVC  = AuthStoryBoard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        obj.completion = { data in
            if let details = data as? Codes
            {
                self.txtCode.text = details.code
            }
        }

        self.present(obj, animated: true, completion: nil)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
      
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        
        verificationWs()
        
       
        
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func verificationWs()  {
        
        
        /**
         ===========API CALL===========
         
         Method Name : user/otp_verification/
         
         Parameter   : country_code, phone
         
         Optional    :
         
         Comment     : This api for user can send OTP for registration.
         
         ==============================
         */
        
        var param = [String : Any]()
        param[kcountry_code] = "+91"//txtCode.text
        param[kphone] = txtMobile.text

        
        
        ApiManger.shared.makeRequest(method: .otpVerification , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                 
                    guard let parent = self.parent?.parent as? SignUpVC else {
                        return
                    }
                    
                    parent.param[kApiOtp] = response[kData][kApiOtp].stringValue
                    parent.param[kcountry_code] = "+91"//self.txtCode.text
                    parent.param[kphone] = self.txtMobile.text
                    parent.pageViewController.setViewControllers([parent.arrViewController[parent.selectIndex + 1]], direction: .forward, animated: true, completion: nil)
                    
                    break
                    
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.isNavigationBarHidden = true
        guard let parent = self.parent?.parent else {
            return
        }
        (parent as! SignUpVC).selectIndex = SignUpViews.contactInfo.rawValue
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- UITextFieldDelegate Methods

extension ContactInfoVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if textField == txtCode{
//            return false
//        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        picker.reloadAllComponents()
        
        
    }
    
}

//-----------------------------------------------------------------------------------

//MARK:- UIPickerViewDelegate Methods

extension ContactInfoVC : UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries[0].countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(countries[0].countries[row].code!)  \(countries[0].countries[row].name!)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtCode.text! = countries[0].countries[row].code!
    }
    
    //-----------------------------------------------------------------------------------
    
}

