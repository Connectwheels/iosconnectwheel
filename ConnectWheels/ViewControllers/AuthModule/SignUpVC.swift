//
//  SignUpVC.swift
//  ConnectWheels
//
//  Created by  on 24/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

enum SignUpViews : Int {
    case contactInfo
    case verification
    case personal
    case connectGps
    case vehicleInfo
    case uploadDocuments
    
}

enum AddVehicleViews : Int {
    case connectGps
    case vehicleInfo
    case uploadDocuments
    
}

extension UIView {
    
    func startBlink() {
        
//        UIView.animate(withDuration: 0.8, delay: 0.0, options: [.beginFromCurrentState , .autoreverse, .repeat], animations: {
//            self.alpha = 0.0
//        }) { (isCompleted) in
////            if isCompleted{
//                self.alpha = 1.0
////            }
//
//        }
    }
    
    func stopBlink() {
//        layer.removeAllAnimations()
//        alpha = 1
    }
}

class SignUpVC: TransparentBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet var colList: UICollectionView!
    @IBOutlet var viewToAdd: UIView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var pageViewController : UIPageViewController = UIPageViewController()
    var param : [String : Any] = [:]
    
    var selectIndex : Int = 0
    {
        didSet{
            if selectIndex <= arrViewController.count
            {
                colList.isHidden = false
                colList.reloadData()
            }
            
            if selectIndex == SignUpViews.connectGps.rawValue {
                self.navigationItem.leftBarButtonItem = nil
            }
        }
    }
    var arrViewController : [UIViewController] = []
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
   
    
    func setUpView() {
        
        colList.allowsMultipleSelection = true
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.backgroundColor = UIColor.clear
        
        let connectvc : ContactInfoVC = AuthStoryBoard.instantiateViewController(withIdentifier: "ContactInfoVC") as! ContactInfoVC
        arrViewController.append(connectvc)
        
        let verification : VerificationVC = AuthStoryBoard.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
        arrViewController.append(verification)
       
        
        let personal : PersonalInfoVC = AuthStoryBoard.instantiateViewController(withIdentifier: "PersonalInfoVC") as! PersonalInfoVC
        arrViewController.append(personal)
        
        let connectGps : ConnectGpsVC = AuthStoryBoard.instantiateViewController(withIdentifier: "ConnectGpsVC") as! ConnectGpsVC
        arrViewController.append(connectGps)
        
        let vehicleInfo : VehicleInfoVC = AuthStoryBoard.instantiateViewController(withIdentifier: "VehicleInfoVC") as! VehicleInfoVC
        arrViewController.append(vehicleInfo)
        
//        let uploadDocument : UploadDocumentVC = AuthStoryBoard.instantiateViewController(withIdentifier: "UploadDocumentVC") as! UploadDocumentVC
//        arrViewController.append(uploadDocument)
        
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: viewToAdd.frame.size.width, height: viewToAdd.frame.size.height)
        self.addChildViewController(pageViewController)
        pageViewController.setViewControllers([arrViewController.first!], direction: .forward, animated: false, completion: nil)
        viewToAdd.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
//        if TARGET_OS_SIMULATOR == 1 {
//            pageViewController.delegate = self
//            pageViewController.dataSource = self
//        }
        
        colList.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        

    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        if selectIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        }else
        {
            if selectIndex == SignUpViews.connectGps.rawValue
            {
                return
            }
            selectIndex = selectIndex - 1
            pageViewController.setViewControllers([arrViewController[selectIndex]], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    

}

//------------------------------------------------------

//MARK:- Collection view method

extension SignUpVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrViewController.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : signupListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "signupListCell", for: indexPath) as! signupListCell
        
        if indexPath.item <= selectIndex
        {
            cell.viewLine.backgroundColor = colors.blueColor
        }else
        {
            cell.viewLine.backgroundColor =  colors.greyColor
        }
        
        if indexPath.item == selectIndex
        {
            cell.viewLine.startBlink()
        }else
        {
            cell.viewLine.stopBlink()
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: ((kScreenWidth / 1.5) / CGFloat(arrViewController.count)), height: collectionView.frame.size.height)
    }
    
   
}


//------------------------------------------------------

//MARK:- Home Header List Cell

class signupListCell : UICollectionViewCell {
    
    @IBOutlet weak var viewLine: UIView!
   
    override func awakeFromNib() {
        self.viewLine.backgroundColor =  colors.greyColor
    }
    
   
    override var isSelected: Bool
    {
        didSet{
            //isSelected ? self.viewLine.startBlink() : self.viewLine.stopBlink()
        }
    }
    
    override func prepareForReuse() {
        
    }

}


//--------------------------------------------------------------------------
//MARK:- Page View Methods

extension SignUpVC : UIPageViewControllerDelegate , UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard arrViewController.count > previousIndex else {
            return nil
        }
        
        return arrViewController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = arrViewController.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return arrViewController[nextIndex]
    }
    
    
}
