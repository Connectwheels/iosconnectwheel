//
//  StartUpVC.swift
//  Ned
//
//  Created by - on 07/06/17.
//  Copyright © 2017 -. All rights reserved.
//

import UIKit


class StartUpVC: UIViewController {

    //--------------------------------------------------------------------------
    //MARK:- Outlets
    

    
    @IBOutlet var pageIndicator: SMPageControl!
    
    @IBOutlet var btnSkip: UIButton!
    
    @IBOutlet var viewToAdd: UIView!
    
    //--------------------------------------------------------------------------
    //MARK:- Variables
    
    let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: [:])
    
    var arrViewController = [UIViewController]()
    
    var timer = Timer()
    
    var currentIndex = 0
    
    var maxCount = 2
    
    //--------------------------------------------------------------------------
    //MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------------------
    //MARK:- Custom Method
    
    @objc func updatePage()  {
        
        if currentIndex == maxCount + 1 {
            currentIndex = 0
        }
        
  
        self.pageIndicator.currentPage = self.currentIndex
        
       
        
        pageViewController.setViewControllers([arrViewController[currentIndex]], direction: .forward, animated: true, completion: nil)
        currentIndex += 1
        
       
        
    }
    
    
    //--------------------------------------------------------------------------
    //MARK: SetUp
    
    func setUp(){
        
       UserDefault.set(true, forKey: keyisTutorialSeen)
       UserDefault.synchronize()
        
        btnSkip.setLayout(detail: [layout.kfont : UIFont.init(name: fonts.montserratSemiBoldFont, size: 10.0 * scaleFactor) as Any,
                                   layout.ktitleColor : colors.redColor
                                 ])
        
        pageViewController.view.backgroundColor = UIColor.clear
        
        arrViewController = (0...maxCount).map({
            
            let view : TutorialCommonVC = AuthStoryBoard.instantiateViewController(withIdentifier: "TutorialCommonVC") as! TutorialCommonVC
            view.pageIndex = $0
            return view
        })
        
     
        pageViewController.delegate = self
      //  pageViewController.dataSource = self
        
        pageViewController.setViewControllers([arrViewController.first!], direction: .forward, animated: false, completion: nil)
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: viewToAdd.frame.size.width, height: viewToAdd.frame.size.height)
        self.addChildViewController(pageViewController)
        viewToAdd.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
        
        
        pageIndicator.numberOfPages = maxCount + 1
        pageIndicator.indicatorMargin = 3.0
        pageIndicator.indicatorDiameter = 5.0
        pageIndicator.currentPageIndicatorImage = #imageLiteral(resourceName: "imgDash")
        pageIndicator.pageIndicatorImage = #imageLiteral(resourceName: "imgDot").withRenderingMode(.alwaysTemplate)
        pageIndicator.pageIndicatorMaskImage = #imageLiteral(resourceName: "imgDot").withRenderingMode(.alwaysTemplate)
        pageIndicator.pageIndicatorTintColor = colors.blueColor
        pageIndicator.currentPage = currentIndex
        
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(updatePage), userInfo: nil, repeats: true)
        timer.fire()

        btnSkip.addAction(for: .touchUpInside) { [unowned self] in
            _ = self
            GFunction.setLoginVC()
            
        }
        
    }
    
    //--------------------------------------------------------------------------
    //MARK:- Action Method
    
    

   
    //--------------------------------------------------------------------------
    //MARK:- View Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp()
    }
    
   
}

extension StartUpVC : UIPageViewControllerDelegate , UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard arrViewController.count > previousIndex else {
            return nil
        }
        
        return arrViewController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = arrViewController.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return arrViewController[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int
    {
        
        return arrViewController.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int
    {
        return 0
    }
    
    
}

