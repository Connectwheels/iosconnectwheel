//
//  ConnectGpsVC.swift
//  ConnectWheels
//
//  Created by  on 24/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit
import AVFoundation

class ConnectGpsVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    
    @IBOutlet weak var txtCode: FloatingCommon!
    
    @IBOutlet weak var btnSkip: UIButton!
    
  //  @IBOutlet var viewCapture: UIView!
    //@IBOutlet weak var viewCamera: UIView!
   // @IBOutlet weak var imgScan: UIImageView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var isAddVehicle : Bool = false
    
    var session: AVCaptureSession!
    
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var isStopAnimation = false
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    @objc func animation()  {
        
        /* imgScan.translatesAutoresizingMaskIntoConstraints = false
        
        
        UIView.animate(withDuration: 1.0,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: {
                        
            var frame = self.imgScan.frame
                
            frame.origin.y = self.viewCapture.frame.size.height - self.imgScan.frame.size.height - 10
            self.imgScan.frame = frame
                  self.imgScan.isHidden = false
                        
        }) { (bool) in
            
            var frame = self.imgScan.frame
            frame.origin.y = 0
            self.imgScan.frame = frame
            
            if !self.isStopAnimation
            {
                self.perform(#selector(self.animation), with: nil, afterDelay: 0.1)
            }else
            {
                self.imgScan.isHidden = true
            }
            
        }*/

    }
    
    func removeAnimation()  {
      /*  DispatchQueue.main.async {
            self.imgScan.layer.removeAllAnimations()
            //        viewCapture.layer.removeAllAnimations()
            self.isStopAnimation = true
            self.view.layoutIfNeeded()
        }*/
    }
    
    func scanningNotPossible() {
        // Let the user know that scanning isn't possible with the current device.
        let alert = UIAlertController(title: "Can't Scan.", message: "Let's try a device equipped with a camera.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        session = nil
    }
    
    func barcodeDetected(code: String) {
        
        removeAnimation()
        
        // Let the user know we've found something.
        let alert = UIAlertController(title: "Found a Barcode!", message: code, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            // Remove the spaces.
            
            let trimmedCode = code.trimmingCharacters(in: NSCharacterSet.whitespaces)
            
            let trimmedCodeString = "\(trimmedCode)"
            var trimmedCodeNoZero: String
            
            if trimmedCodeString.hasPrefix("0") && trimmedCodeString.count > 1 {
                trimmedCodeNoZero = String(trimmedCodeString.dropFirst())
                debugPrint(trimmedCodeNoZero)
                // Send the doctored UPC to DataService.searchAPI()
                
                //DataService.searchAPI(trimmedCodeNoZero)
            } else {
                debugPrint(trimmedCodeString)
                // Send the doctored EAN to DataService.searchAPI()
                //DataService.searchAPI(trimmedCodeString)
            }
            
            print(code)
            
            self.view.endEditing(true)
           self.txtCode.text = code

        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            
                    DispatchQueue.main.async {
                        self.session.startRunning()
                        self.isStopAnimation = true
                        self.animation()
                    }

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func validate() -> String{
        
        if txtCode.text!.trim().isEmpty {
            return kvBlankCode
            
        }else if txtCode.text!.trim().rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) != nil
        {
            return kvValidCode
        }
        
        return String()
    }
    
    @objc func setUpView() {
//        txtCode.textAlignment = .center
//        txtCode.titleLabel.textAlignment = .center
        
        if TARGET_OS_SIMULATOR == 1{
            txtCode.text = "861359039906105"
        }
        
//        if isAddVehicle {
            btnSkip.isHidden = true
//        }
        
        /* session = AVCaptureSession()
        
        if let videoCaptureDevice = AVCaptureDevice.default(for: .video)
        {
            // Create input object.
            let videoInput: AVCaptureDeviceInput?
            
            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                return
            }
            
            // Add input to the session.
            if (session.canAddInput(videoInput!)) {
                session.addInput(videoInput!)
            } else {
                scanningNotPossible()
            }
            
            let metadataOutput = AVCaptureMetadataOutput()
            
            //            session.sessionPreset = .photo
            
            // Add output to the session.
            if (session.canAddOutput(metadataOutput)) {
                session.addOutput(metadataOutput)
                
                // Send captured data to the delegate object via a serial queue.
                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                
                // Set barcode type for which to scan: EAN-13.
                metadataOutput.metadataObjectTypes = [.ean13,.ean8,.code93,.code128,.pdf417,.code39,.itf14,.qr]
                
                
            } else {
                scanningNotPossible()
            }
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: session);
        previewLayer.frame = viewCamera.layer.bounds;
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill;
        viewCamera.layer.addSublayer(previewLayer)*/
        
        self.txtCode.addTarget(self, action: #selector(textChanged), for: .editingChanged)
        
        /*if !session.isRunning
        {
             session.startRunning()
            animation()
        }*/
        
        self.txtCode.delegate = self
        self.txtCode.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(handleDoneButton))
        


    }
    
    @objc func textChanged()  {
        if TARGET_OS_SIMULATOR == 1{
            return
        }
       // self.session.stopRunning()
        
    }
    
    @objc func handleDoneButton()  {
        
        if txtCode.text!.isEmpty{
           return
        }
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: "Are you sure you want to register this device with \(self.txtCode.text!) IMEI number".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                self.btnNextClicked(UIButton())
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel".Localized(), style: .default, handler: { (UIAlertAction) in
                self.view.endEditing(true)
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        
        
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        
        if let parent = self.parent?.parent as? SignUpVC  {
            parent.param = [:]
            parent.param[kqr_code] = txtCode.text!.trim()
            parent.pageViewController.setViewControllers([parent.arrViewController[parent.selectIndex + 1]], direction: .forward, animated: true, completion: nil)
            
        }else if let parent = self.parent?.parent as? AddVehicleVC  {
            
            parent.param = [:]
            parent.param[kqr_code] = txtCode.text!.trim()
            parent.pageViewController.setViewControllers([parent.arrViewController[parent.selectIndex + 1]], direction: .forward, animated: true, completion: nil)
        }
        
    
        
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
       
        GFunction.SetHomeScreen()

    }
    
    

    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //imgScan.isHidden = true
        if isAddVehicle {
            btnSkip.isHidden = true
        }
        self.perform(#selector(setUpView), with: nil, afterDelay: 0.3)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if let _ = session{
//            session.startRunning()
//        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let parent = self.parent?.parent as? SignUpVC  {
            parent.colList.isHidden = false
            if parent.selectIndex < SignUpViews.connectGps.rawValue {
                _ = self.view.subviewsRecursive().filter{ $0 is FloatingCommon }.map{ ($0 as! FloatingCommon).text = "" }
            }
            parent.selectIndex = SignUpViews.connectGps.rawValue
        }else if let parent = self.parent?.parent as? AddVehicleVC  {
            parent.colList.isHidden = false
            if parent.selectIndex < AddVehicleViews.connectGps.rawValue {
                _ = self.view.subviewsRecursive().filter{ $0 is FloatingCommon }.map{ ($0 as! FloatingCommon).text = "" }
            }
            parent.selectIndex = AddVehicleViews.connectGps.rawValue
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- AVCaptureMetadataOutputObjectsDelegate

extension ConnectGpsVC : AVCaptureMetadataOutputObjectsDelegate{
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let barcodeData = metadataObjects.first {
            // Turn it into machine readable code
            let barcodeReadable = barcodeData as? AVMetadataMachineReadableCodeObject;
            if let readableCode = barcodeReadable {
                // Send the barcode as a string to barcodeDetected()
                barcodeDetected(code: readableCode.stringValue!);
            }
            
            // Vibrate the device to give the user some feedback.
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            // Avoid a very buzzy device.
            session.stopRunning()
        }
    }
    
    
    
}


//------------------------------------------------------

//MARK:- AVCaptureMetadataOutputObjectsDelegate

extension ConnectGpsVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // self.session.stopRunning()
       // self.removeAnimation()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if txtCode.text!.isEmpty{
           // self.session.startRunning()
           // self.animation()
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
}
