//
//  LoginVC.swift
//  ConnectWheels
//
//  Created by  on 23/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class LoginVC: TransparentBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtEmail: FloatingAuth!
    @IBOutlet weak var txtPassword: FloatingAuth!
    @IBOutlet weak var btnCheck: UIButton!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var btnShowPassword : UIButton = {
        let btn = UIButton.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        
        btn.setImage(UIImage.init(named: "imgShowPassword"), for: .selected)
        btn.setImage(UIImage.init(named: "imgHidePassword"), for: .normal)
        btn.imageView?.contentMode = .center
        return btn
    }()
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String {
      
        
        if txtEmail.text!.isEmpty
        {
            return kvBlankPhoneNumber
            
        }else if txtEmail.text!.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil && (txtEmail.text!.count < 6 || txtEmail.text!.count > 14)
        {
             return kvValidPhoneNumber
            
        }
        else if txtPassword.text!.isEmpty {
            return kvBlankPassword
        }
        
        return String()
    }
    
    func setUpView() {
        
        if TARGET_OS_SIMULATOR == 1{
            txtEmail.text = "9313524499"
            txtPassword.text = "aaaa0000"
        }
        
        if let details = UserDefault.value(forKey: kRememberDetails) as? [String:Any]{
            txtEmail.text = JSON(details[kemail_phone] as Any).stringValue
            txtPassword.text = JSON(details[kpassword] as Any).stringValue
        }
        
        txtEmail.RightPaddingView(image: #imageLiteral(resourceName: "imgLoginEmail"), frame: CGRect(x: 0, y: txtEmail.titleHeight(), width: 30, height: txtEmail.frame.size.height - txtEmail.titleHeight()))
        
//        txtPassword.RightPaddingView(image: #imageLiteral(resourceName: "imgLoginPassword"), frame: CGRect(x: 0, y: txtPassword.titleHeight(), width: 30, height: txtPassword.frame.size.height - txtPassword.titleHeight()))
        
        btnCheck.addAction(for: .touchUpInside) { [unowned self] in
            self.btnCheck.isSelected = !self.btnCheck.isSelected
        }
        
        txtPassword.rightView = btnShowPassword
        txtPassword.rightViewMode = UITextFieldViewMode.always
       
        
        btnShowPassword.addAction(for: .touchUpInside) { [unowned self] in
            self.btnShowPassword.isSelected = !self.btnShowPassword.isSelected
            self.txtPassword.isSecureTextEntry = !self.btnShowPassword.isSelected
        }
    }
    
    func getFacebookDetails(token : String)  {
        
        GFunction.sharedInstance.addLoader()
        
        let connection =  FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"])?.start(completionHandler: { (connection, result, error) in
            
            GFunction.sharedInstance.removLoader()
            
            if error != nil || result == nil{
                debugPrint("Facebook Login error",error!.localizedDescription)
                return
            }
            //signup_type('Normal', 'Facebook', 'Google')
            
            let info = JSON(result as Any)
            var param : [String : Any] = [:]
            param[kfname] = info["first_name"].stringValue
            param[klname] = info["last_name"].stringValue
            param[kSocialId] = info["id"].stringValue
            param[kemail] = info[kemail].stringValue
            param[ksignupType] = "Facebook"
           self.loginWs(socialParam: param)
            
//            let email = info[kemail].stringValue
//            if email.isEmpty{
//                GFunction.ShowAlert(message: "Sorry, We can't proceed further, We do not have access to your email id(reason could be private profile).")
//                return
//            }
            
          //  self.socialLoginWs(param: [kfacebook_id : JSON(info["id"].stringValue) , kkeep_login : self.btnCheck.isSelected ? "1" : "0"])
            
        })
        
        connection?.start()
        
        
    }
    
    
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
     @IBAction func btnFbClicked(_ sender: Any) {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        loginManager.logIn(withReadPermissions: ["email","public_profile"], from: self, handler: { (result, error) in
            if error != nil || result == nil{
                debugPrint("Facebook Login error",error!.localizedDescription)
                return
            }
            
            if result!.isCancelled{
                return
            }
            
            if !result!.grantedPermissions.contains(kemail){
                GFunction.ShowAlert(message : "You might just forget to give email permission. Please log in with facebook again and give us email permission.")
                return
            }else{
                self.getFacebookDetails(token: FBSDKAccessToken.current().tokenString)
            }
        })
    }
    
     @IBAction func btnGoogleClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        
        
        let error = validate()
        if error != String()
        {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
      //  GFunction.SetHomeScreen()
        loginWs()
      

        
    }
    
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func loginWs(socialParam : [String : Any] = [:])  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/login/
         
         Parameter   : email_phone, password , social_id
         
         Optional    : device_id, device_type, uuid, os_version, device_name, model_name, ip
         
         Comment     : This api will be used for user login with email/phone.
         
         ==============================
         */
        
        var param = [String : Any]()
        
        if socialParam[kSocialId] != nil{
            param[kSocialId] = socialParam[kSocialId]
        }else{
            param[kemail_phone] = txtEmail.text
            param[kpassword] = txtPassword.text
        }
        
        
        param[kdevice_id] = appDelegate.device_token
        param[kdevice_type] = "I"
        param[kos_version] = DeviceInfo.shared.OSVersion
        param[kdevice_name] = DeviceInfo.shared.DeviceName
        param[kmodel_name] = DeviceInfo.shared.ModelName
        param[kip] = DeviceInfo.shared.IP
        param[kuuid] = DeviceInfo.shared.UUID
        
        
        ApiManger.shared.makeRequest(method: .login , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    
                    UserDefault.setValue(response[kData].dictionaryObject, forKey: kUserDetail)
                    UserDefault.setValue(response[kData][kUserToken].stringValue, forKey: kUserToken) //kRememberDetails
                    if socialParam[kSocialId] == nil{
                        if self.btnCheck.isSelected{
                            UserDefault.setValue(param, forKey: kRememberDetails)
                        }else{
                            UserDefault.removeObject(forKey: kRememberDetails)
                        }
                    }
                     UserDefault.synchronize()
                    GFunction.SetHomeScreen()
                    break
                    
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    if response[kCode].stringValue == "11"{
                        let obj : SignUpVC = AuthStoryBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                        obj.param = socialParam
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
//        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView{
//            statusBar.backgroundColor = UIColor.clear
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}


//------------------------------------------------------

//MARK:- GoogleSignin Delegates

extension LoginVC : GIDSignInUIDelegate , GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error != nil{
            return
        }

        //signup_type('Normal', 'Facebook', 'Google')
        
        var param : [String : Any] = [:]
        param[kemail] = user.profile.email
        param[kfname] = user.profile.givenName
        param[klname] = user.profile.familyName
        param[kSocialId] = user.authentication.idToken
        param[ksignupType] = "Google"
        self.loginWs(socialParam: param)
        
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
}
