//
//  TutorialCommonVC.swift
//  Ned
//
//  Created by - on 08/06/17.
//  Copyright © 2017 -. All rights reserved.
//

import UIKit

class TutorialCommonVC: UIViewController {

    //--------------------------------------------------------------------------
    //MARK:- Outlets
    
    @IBOutlet var imgPhoto: UIImageView!
    @IBOutlet var lblDetail: UILabel!
    @IBOutlet var lblHeader: UILabel!
    //--------------------------------------------------------------------------
    //MARK:- Variables
    
    var pageIndex : Int = 0
    
    var arrList = [Dictionary<String,Any>]()
    
    let kvHeader = "kvHeader"
    let kvDetail = "kvDetail"
    let kvImage  = "kvImage"
    
    //--------------------------------------------------------------------------
    //MARK:- Memory Management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //--------------------------------------------------------------------------
    //MARK:- Custom Method
    
    
    
    //--------------------------------------------------------------------------
    //MARK: SetUp
    
    func setUp(){
        
        lblHeader.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 13.0, color: colors.blueColor)
        
        lblDetail.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 12.0, color: colors.blackColor)
        
      
        
        arrList = [ [kvHeader : "" , kvDetail : "Welcome to Connect Wheels!!" , kvImage : #imageLiteral(resourceName: "loginBG")],
                    [kvHeader : "" , kvDetail : "Experience Best in Class GPS Technology." , kvImage : #imageLiteral(resourceName: "loginBG")],
                    [kvHeader : "" , kvDetail : "Stay connected & know your valuable reports." , kvImage : #imageLiteral(resourceName: "loginBG")]
                ]
        
        lblHeader.text = arrList[pageIndex][kvHeader] as? String
        lblDetail.text = arrList[pageIndex][kvDetail] as? String
        lblDetail.setLineSpacing(lineHeightMultiple: 1.5)
        imgPhoto.image = arrList[pageIndex][kvImage] as? UIImage
    }
    
    //--------------------------------------------------------------------------
    //MARK:- Action Method
    
    
    
    
    //--------------------------------------------------------------------------
    //MARK:- View Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

}
