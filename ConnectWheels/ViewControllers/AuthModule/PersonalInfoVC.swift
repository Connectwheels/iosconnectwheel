//
//  PersonalInfoVC.swift
//  ConnectWheels
//
//  Created by  on 24/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class PersonalInfoVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtFirstName: FloatingCommon!
    @IBOutlet weak var txtLastName: FloatingCommon!
    @IBOutlet weak var txtEmail: FloatingCommon!
    @IBOutlet weak var txtPassword: FloatingCommon!
    @IBOutlet weak var txtConfirmPassword: FloatingCommon!
    @IBOutlet weak var txtAddress: FloatingCommon!
    
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var txtvTerms: UITextView!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var lat : Double = LocationManager.shared.location.coordinate.latitude
    var long : Double = LocationManager.shared.location.coordinate.longitude
    let btnShowPassword : UIButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    let btnShowConfirmPassword : UIButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        
        if txtFirstName.text!.isEmpty {
            return kvBlankFirstName
            
        }else if txtLastName.text!.isEmpty {
            return kvBlankLastName
            
        }else if txtEmail.text!.isEmpty && !txtEmail.isHidden {
            return kvBlankEmail
            
        }else if !(GFunction.isValidEmail(text: txtEmail.text!)) && !txtEmail.isHidden{
            return kvValidEmail
            
        }else if txtPassword.text!.isEmpty && txtPassword.isEnabled && !txtPassword.isHidden{
            return kvBlankPassword
            
        }else if txtPassword.text!.count < 6  && txtPassword.isEnabled && !txtPassword.isHidden{
            return kvMinLenPassword
            
        }else if txtConfirmPassword.text!.isEmpty && txtConfirmPassword.isEnabled && !txtPassword.isHidden{
            return kvBlankConfirmPassword
            
        }else if txtPassword.text != txtConfirmPassword.text && txtConfirmPassword.isEnabled && !txtPassword.isHidden{
            return kvMismatchPassword
            
        }else if txtAddress.text!.isEmpty {
            return kvSelectAddress
            
        }else if !btnCheck.isSelected {
            return kvTermsAgreement
            
        }
        
        return String()
    }
    
    @objc func btnShowPasswordClicked()  {
        btnShowPassword.isSelected = !btnShowPassword.isSelected
        self.txtPassword.isSecureTextEntry = !btnShowPassword.isSelected
    }
    
    @objc func btnShowConfirmPasswordClicked()  {
        btnShowConfirmPassword.isSelected = !btnShowConfirmPassword.isSelected
        self.txtConfirmPassword.isSecureTextEntry = !btnShowConfirmPassword.isSelected
    }
    
    func setUpView() {
        
        
        
        txtFirstName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        txtLastName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        txtAddress.RightPaddingView(image: #imageLiteral(resourceName: "imgLocationGray").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtAddress.titleHeight(), width: 30, height: txtAddress.frame.size.height - txtAddress.titleHeight()))
        
        
        btnShowPassword.setImage(UIImage.init(named: "imgShowPassword"), for: .selected)
        btnShowPassword.setImage(UIImage.init(named: "imgHidePassword"), for: .normal)
        btnShowPassword.imageView?.contentMode = .center
        txtPassword.rightView = btnShowPassword
        txtPassword.rightViewMode = UITextFieldViewMode.always
        btnShowPassword.addTarget(self, action: #selector(btnShowPasswordClicked), for: .touchUpInside)
        
        btnShowConfirmPassword.setImage(UIImage.init(named: "imgShowPassword"), for: .selected)
        btnShowConfirmPassword.setImage(UIImage.init(named: "imgHidePassword"), for: .normal)
        btnShowConfirmPassword.imageView?.contentMode = .center
        txtConfirmPassword.rightView = btnShowConfirmPassword
        txtConfirmPassword.rightViewMode = UITextFieldViewMode.always
        btnShowConfirmPassword.addTarget(self, action: #selector(btnShowConfirmPasswordClicked), for: .touchUpInside)
        
        guard let parent = self.parent?.parent as? SignUpVC else {
            return
        }
        
        if parent.param[kSocialId] != nil{
            let param = JSON(parent.param)
            txtFirstName.text = param[kfname].stringValue
            txtLastName.text = param[klname].stringValue
            txtPassword.isHidden = true
            txtConfirmPassword.isHidden = true
        }
        
        let attributed = NSMutableAttributedString(string: txtvTerms.text!, attributes: [NSAttributedString.Key.foregroundColor : colors.whiteColor, NSAttributedString.Key.font : UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0) as Any])
        attributed.addAttribute(NSAttributedString.Key.link, value: "Terms", range: (txtvTerms.text! as NSString).range(of: "Terms & Conditions"))
        
        let linkAttributes : [String: Any] = [NSAttributedString.Key.font.rawValue : UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0) as Any, NSAttributedString.Key.foregroundColor.rawValue : colors.redColor]
        
        txtvTerms.linkTextAttributes = linkAttributes
        
        txtvTerms.attributedText = attributed
        
        txtvTerms.delegate = self
   }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnNextClicked(_ sender: Any) {
     
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
       
        signUpWs()
     
        
    }
    @IBAction func btnCheckClicked(_ sender: Any) {
        self.btnCheck.isSelected = !self.btnCheck.isSelected
    }
    
    @IBAction func btnSelectAddressClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func signUpWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/signup/
         
         Parameter   : fname, lname, email, password, country_code, phone,address, latitude, longitude
         
         Optional    : device_id, device_type, uuid, os_version, device_name, model_name, ip
         
         Comment     : This api will be used for new user signup.
         
         ==============================
         */
        
        guard let parent = self.parent?.parent as? SignUpVC else {
            return
        }
        
        parent.param[kfname] = txtFirstName.text!.trim()
        parent.param[klname] = txtLastName.text!.trim()
        parent.param[kemail] = txtEmail.text
        parent.param[kpassword] = txtPassword.text
        parent.param[kaddress] = txtAddress.text!.trim()
        parent.param[kdevice_id] = appDelegate.device_token
        parent.param[kdevice_type] = "I"
        parent.param[kos_version] = DeviceInfo.shared.OSVersion
        parent.param[kdevice_name] = DeviceInfo.shared.DeviceName
        parent.param[kmodel_name] = DeviceInfo.shared.ModelName
        parent.param[kip] = DeviceInfo.shared.IP
        parent.param[kuuid] = DeviceInfo.shared.UUID
        parent.param[klatitude] = lat
        parent.param[klongitude] = long
        if parent.param[ksignupType] == nil{
            parent.param[ksignupType] = "Normal"
            parent.param[kSocialId] = ""
        }
        
        ApiManger.shared.makeRequest(method: .signUp , parameter: parent.param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    
                    UserDefault.setValue(response[kData].dictionaryObject, forKey: kUserDetail)
                    UserDefault.setValue(response[kData][kUserToken].stringValue, forKey: kUserToken)
                    UserDefault.synchronize()
                    
                    if JSON(currentUser.isSharedUser as Any).intValue == 0{
                         parent.pageViewController.setViewControllers([parent.arrViewController[parent.selectIndex + 1]], direction: .forward, animated: true, completion: nil)
                    }else{
                        GFunction.SetHomeScreen()
                    }
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let parent = self.parent?.parent as? SignUpVC else {
            return
        }
        
        if parent.selectIndex <= SignUpViews.personal.rawValue {
            _ = self.view.subviewsRecursive().filter{ $0 is FloatingCommon }.map{ ($0 as! FloatingCommon).text = "" }
        }
        
        parent.colList.isHidden = false
        parent.selectIndex = SignUpViews.personal.rawValue
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        
    }

}


//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension PersonalInfoVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.absoluteString == "Terms" {
            let obj : TermsVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            obj.title = kTermsTitle
            obj.webUrl = kTermsUrl
            self.navigationController?.pushViewController(obj, animated: true)
        }
        return false
    }
}

//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension PersonalInfoVC : GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtAddress.text = place.formattedAddress
        lat = place.coordinate.latitude
        long = place.coordinate.longitude
        dismiss(animated: true) {
          
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
}
