//
//  SplashVC.swift
//  ConnectWheels
//
//  Created by  on 06/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var imgRight: UIImageView!
    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var imgCenter: UIImageView!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {

        
        UIView.animate(withDuration: 1.5, delay: 0, options: [.curveEaseOut], animations: {

            self.imgLeft.frame = CGRect(x: (kScreenWidth / 2) - (self.imgLeft.frame.size.width / 2) , y: (kScreenHeight / 2) - (self.imgLeft.frame.size.height / 2)  , width: self.imgLeft.frame.size.width, height: self.imgLeft.frame.size.height)
            self.imgRight.frame = CGRect(x: (kScreenWidth / 2) - (self.imgLeft.frame.size.width / 2) , y: (kScreenHeight / 2) - (self.imgLeft.frame.size.height / 2)  , width: self.imgLeft.frame.size.width, height: self.imgLeft.frame.size.height)
            
        }) { (isCompleted) in
            self.imgLeft.isHidden = true
            self.imgRight.isHidden = true
            self.imgCenter.isHidden = false
            
            sleep(UInt32(1.0))
            
//            UIView.animate(withDuration: 0.8,delay: 2.0, options:[.allowUserInteraction, .curveEaseInOut],
//                                   animations: {
//
////                                    self.imgCenter.alpha = 0
////                                    self.imgCenter.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
//
//            }){ ( isCompleted) in
            
                let obj : StartUpVC = AuthStoryBoard.instantiateViewController(withIdentifier: "StartUpVC") as! StartUpVC
                appDelegate.window?.rootViewController = obj
                appDelegate.window?.makeKeyAndVisible()
//            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.setUpView()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}
