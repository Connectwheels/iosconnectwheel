//
//  VerificationVC.swift
//  ConnectWheels
//
//  Created by  on 24/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class VerificationVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var tfFirstDigit: UITextField!
    @IBOutlet weak var tfSecondDigit: UITextField!
    @IBOutlet weak var tfThirdDigit: UITextField!
    @IBOutlet weak var tfFourthDigit: UITextField!
    @IBOutlet weak var lblMobileNumber: UILabel!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnVerifyClicked(_ sender: UIButton) {
        
        if tfFirstDigit.text!.isEmpty && tfSecondDigit.text!.isEmpty && tfThirdDigit.text!.isEmpty && tfFourthDigit.text!.isEmpty  {
            GFunction.ShowAlert(message: kvBlankOTP)
            return
        }
        
        
        guard !tfFirstDigit.text!.isEmpty && !tfSecondDigit.text!.isEmpty && !tfThirdDigit.text!.isEmpty && !tfFourthDigit.text!.isEmpty else {
            GFunction.ShowAlert(message: kvValidOTP)
            return
        }
        
        guard let parent = self.parent?.parent as? SignUpVC else {
            return
        }
        
        let code = tfFirstDigit.text! + tfSecondDigit.text! +  tfThirdDigit.text! + tfFourthDigit.text!
        
        
        if code != JSON(parent.param[kApiOtp] as Any).stringValue {
            GFunction.ShowAlert(message: kvValidOTP)
            return
        }
        
        self.view.endEditing(true)
        
        
        parent.pageViewController.setViewControllers([parent.arrViewController[parent.selectIndex + 1]], direction: .forward, animated: true, completion: nil)
    }
    
    @IBAction func btnResendClicked(_ sender: UIButton) {
        
        self.view.endEditing(true)
        verificationWs()
        
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func verificationWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/otp_verification/
         
         Parameter   : country_code, phone
         
         Optional    :
         
         Comment     : This api for user can send OTP for registration.
         
         ==============================
         */
        
        guard let parent = self.parent?.parent as? SignUpVC else {
            return
        }
   
        var param = [String : Any]()
        param[kcountry_code] = parent.param[kcountry_code]
        param[kphone] = parent.param[kphone]
        
        ApiManger.shared.makeRequest(method: .otpVerification , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    
                    guard let parent = self.parent?.parent as? SignUpVC else {
                        return
                    }
                    
                    parent.param[kApiOtp] = response[kData][kApiOtp].stringValue
                    
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    
                    break
                    
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let parent = self.parent?.parent as? SignUpVC else {
            return
        }
        lblMobileNumber.text = "\(parent.param[kcountry_code]!)" + " " + "\(parent.param[kphone]!)"
//        parent.colList.isHidden = true
        if parent.selectIndex < SignUpViews.verification.rawValue {
            tfFirstDigit.text = ""
            tfSecondDigit.text = ""
            tfThirdDigit.text = ""
            tfFourthDigit.text = ""
        }
        parent.selectIndex = SignUpViews.verification.rawValue
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//MARK:- Extension

extension VerificationVC : UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField == tfSecondDigit {
            tfFirstDigit.becomeFirstResponder()
        } else if textField == tfThirdDigit {
            tfSecondDigit.becomeFirstResponder()
        } else if textField == tfFourthDigit {
            tfThirdDigit.becomeFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let isBackSpace = strcmp(string.cString(using: String.Encoding.utf8)!, "\\b")
        
        if isBackSpace == -92 {
            if(textField == tfFirstDigit){
                if ((tfFirstDigit.text?.count)! !=  0){
                    tfFirstDigit.text = ""
                }
            } else if textField == tfSecondDigit {
                if ((tfSecondDigit.text?.count)! !=  0) {
                    
                    tfSecondDigit.text = ""
                    tfFirstDigit.becomeFirstResponder()
                    return false
                }
            } else if textField == tfThirdDigit {
                if ((tfThirdDigit.text?.count)! !=  0) {
                    
                    tfThirdDigit.text = ""
                    tfSecondDigit.becomeFirstResponder()
                    return false
                }
            } else if textField == tfFourthDigit {
                if ((tfFourthDigit.text?.count)! !=  0){
                    
                    tfFourthDigit.text = ""
                    tfThirdDigit.becomeFirstResponder()
                    return false
                }
            }
            return true
        }
        
        if textField == tfFirstDigit {
            if (tfFirstDigit.text?.count)! ==  0 && string.count > 0 {
                
                tfFirstDigit.text = string
                tfSecondDigit.becomeFirstResponder()
                return false
            }
        } else if textField == tfSecondDigit {
            if (tfSecondDigit.text?.count)! ==  0 && string.count > 0 {
                
                tfSecondDigit.text = string
                tfThirdDigit.becomeFirstResponder()
                return false
            }
        } else if textField == tfThirdDigit {
            if (tfThirdDigit.text?.count)! ==  0 && string.count > 0 {
                
                tfThirdDigit.text = string
                tfFourthDigit.becomeFirstResponder()
                return false
            }
        } else if textField == tfFourthDigit {
            if (tfFourthDigit.text?.count)! ==  0 && string.count > 0 {
                
                tfFourthDigit.text = string
                tfFourthDigit.resignFirstResponder()
                return false
            }
        }
        return false
    }
}
