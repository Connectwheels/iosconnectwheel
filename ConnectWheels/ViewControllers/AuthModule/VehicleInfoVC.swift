//
//  VehicleInfoVC.swift
//  ConnectWheels
//
//  Created by  on 25/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

enum VehicleTypes : CaseIterable  {
    
    case twoWheeler, threeWheeler, car, bus, truck
    
    var title : String {
        switch self {
        case .twoWheeler:
            return "2 WHEELER"
        case .threeWheeler:
            return "3 WHEELER"
        case .car:
            return "CAR"
        case .bus:
            return "BUS"
        case .truck:
            return "TRUCK"
        }
    }
    
    static func getImageName(type: String) -> String {
        
        //static due to api do not give image
        switch type.uppercased() {
        case "2 WHEELER", "SCOOTER":
            return "Scooter"
        case "3 WHEELER", "AUTORISKSHAW":
            return "Autorickshaw"
        case "CAR":
            return "Car"
        case "BUS":
            return "Bus"
        case "TRUCK":
            return "Truck"
        default:
            return "Car"
        }
        
    }
}



class VehicleInfoVC: TransparentBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtVehicleRegNumber: FloatingCommon!
    @IBOutlet weak var txtCarMake: FloatingCommon!
    @IBOutlet weak var txtCarModel: FloatingCommon!
    @IBOutlet weak var txtVARIANT: FloatingCommon!
    @IBOutlet weak var txtVehicleType: FloatingAuth!
    @IBOutlet weak var imgUploadImage: UIImageView!
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet var viewTransperentBG: [UIImageView]!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    var isAddVehicle : Bool = false
    var isEditVehicle : Bool = false
    var isIsImageChange = false
    var vehicleInfo : Vehicle?
    var arrCarMake : [VehicleModel] = []
    var arrCarModel : [VehicleModel] = []
    var arrCarVariant = ["PETROL","DIESEL","CNG"].sorted()
    var arrVehicleType = VehicleTypes.allCases
    var picker = UIPickerView()
    
    var modelId = ""
    var carMakeId = ""
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        if txtVehicleRegNumber.text!.isEmpty {
            return kvBlankCarRegNo
            
        }
        if txtVehicleType.text!.isEmpty {
            return kvBlankVehicleType
            
        }
        if txtCarMake.text!.isEmpty {
            return kvBlankCarMake
            
        }
        
        /*if imgUploadImage.image == #imageLiteral(resourceName: "placeHolder")
        {
            return kvSelectImage
        }*/
        
        if txtCarModel.text!.isEmpty {
            return kvBlankCarModel
         }
        
        if txtVARIANT.text!.isEmpty {
            return kvBlankCarVariant
         }
   
        return String()
    }
    
    @objc func imgPickerOpen(isShowRemove : Bool = false) {
        
        self.view.endEditing(true)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if TARGET_OS_SIMULATOR == 1
            {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    self.imagePicker.sourceType = .photoLibrary
                    OperationQueue.main.addOperation({() -> Void in
                        
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                }
                
                return
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                OperationQueue.main.addOperation({() -> Void in
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
                
            }
            
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Gallery".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.sourceType = .photoLibrary
                OperationQueue.main.addOperation({() -> Void in
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
            }
            else {
                // GFunction.sharedInstance.alert("Gallery is not available")
            }
        }))
        

        
        
        
        actionSheet.addAction(UIAlertAction(title: "CANCEL".Localized(), style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func setUpView() {
        
        if TARGET_OS_SIMULATOR == 1{
            txtVehicleRegNumber.text = "861359039906105"
        }
        
        
        
        txtCarMake.RightPaddingView(image: #imageLiteral(resourceName: "downArrow").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtCarMake.titleHeight(), width: 30, height: txtCarMake.frame.size.height - txtCarMake.titleHeight()))
         txtCarModel.RightPaddingView(image: #imageLiteral(resourceName: "downArrow").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtCarMake.titleHeight(), width: 30, height: txtCarMake.frame.size.height - txtCarMake.titleHeight()))
         txtVARIANT.RightPaddingView(image: #imageLiteral(resourceName: "downArrow").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtCarMake.titleHeight(), width: 30, height: txtCarMake.frame.size.height - txtCarMake.titleHeight()))
        txtVehicleRegNumber.RightPaddingView(image: #imageLiteral(resourceName: "search").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtCarMake.titleHeight(), width: 30, height: txtCarMake.frame.size.height - txtCarMake.titleHeight()))
        txtVehicleType.RightPaddingView(image: #imageLiteral(resourceName: "downArrow").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtCarMake.titleHeight(), width: 30, height: txtCarMake.frame.size.height - txtCarMake.titleHeight()))
        
        
        picker.delegate = self
        picker.dataSource = self
        txtCarModel.inputView = picker
        txtCarMake.inputView = picker
        txtVARIANT.inputView = picker
        txtVehicleType.inputView = picker
        
        self.navigationItem.leftBarButtonItem?.tintColor = colors.whiteColor
        
        if isAddVehicle || isEditVehicle {
            btnSkip.isHidden = true
        }
        
        
        if isEditVehicle {
            
            if let info = vehicleInfo{
                self.txtCarModel.text = info.carModel
                self.txtCarMake.text = info.carMake
                self.txtVARIANT.text = info.variant
                self.txtVehicleRegNumber.text = info.registrationNumber
                self.txtVehicleType.text = info.vehicleType
               // imgUploadImage.sd_setImage(with: info.car_photo.getUrl(), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
                
                self.txtCarModel.isEnabled = false
                self.txtCarMake.isEnabled = false
                self.txtVARIANT.isEnabled = false
                self.txtVehicleRegNumber.isEnabled = false
                self.txtVehicleType.isEnabled = false
            }
            
            
            btnSkip.superview?.backgroundColor = UIColor.clear
            self.view.backgroundColor = colors.blueColor
            _ = viewTransperentBG.map{ $0.isHidden = false }
            
            
        }
        
        txtCarModel.addAction(for: .editingDidBegin) { [unowned self] in
            self.picker.reloadAllComponents()
            
            if self.txtCarModel.text!.isEmpty && !self.arrCarModel.isEmpty{
                 self.txtCarModel.text = self.arrCarModel[0].name
                 self.modelId = JSON(self.arrCarModel[0].id).stringValue
            }
        }
        
        txtCarMake.addAction(for: .editingDidBegin) { [unowned self] in
            self.picker.reloadAllComponents()
            if self.txtCarMake.text!.isEmpty && !self.arrCarMake.isEmpty
            {
                self.txtCarMake.text = JSON(self.arrCarMake[0].name).stringValue
                self.carMakeId = JSON(self.arrCarMake[0].id).stringValue
            }
        }
        
        txtVARIANT.addAction(for: .editingDidBegin) { [unowned self] in
            self.picker.reloadAllComponents()
            if self.txtVARIANT.text!.isEmpty
            {
                self.txtVARIANT.text = self.arrCarVariant[0]
            }
        }
        
        txtVehicleType.addAction(for: .editingDidBegin) { [unowned self] in
            self.picker.reloadAllComponents()
            if self.txtVehicleType.text!.isEmpty
            {
                self.txtVehicleType.text = self.arrVehicleType[0].title.uppercased()
            }
        }
        
        /*let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imgPickerOpen(isShowRemove:)))
        imgUploadImage.addGestureRecognizer(tapGesture)
        imgUploadImage.isUserInteractionEnabled = true*/
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    

    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        
        //registration_number, car_make, car_model,variant, qr_code, documents, car_photo
        
        
       // GFunction.SetHomeScreen()
        
        // changes
        
        if isEditVehicle{
            GFunction.SetHomeScreen()
        }else{
            uploadImage()
        }
        
      //  if isIsImageChange {
//           uploadImage()
//            return
//        }else if isEditVehicle
//        {
//            GFunction.SetHomeScreen()
//        }
        
       
        
       
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        view.endEditing(true)
        
        GFunction.SetHomeScreen()
        
//        if let parent = self.parent?.parent as? SignUpVC  {
//
//            parent.pageViewController.setViewControllers([parent.arrViewController[parent.selectIndex + 1]], direction: .forward, animated: true, completion: nil)
//
//        }else if let parent = self.parent?.parent as? AddVehicleVC  {
//
//            parent.pageViewController.setViewControllers([parent.arrViewController[parent.selectIndex + 1]], direction: .forward, animated: true, completion: nil)
//        }
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    
    func uploadImage()  {
        
//        GFunction.sharedInstance.addLoader()
//        ImageUpload.shared.uploadImage(false, self.imgUploadImage.image!, "image/jpeg", .vehicle) { (path, lastComponent) in
//
//            if lastComponent == nil
//            {
//                GFunction.sharedInstance.removLoader()
//                return
//            }
        
            var param : [String:Any] = [:]
            param[kregistration_number] = self.txtVehicleRegNumber.text?.trim()
            param[kvehicleMakeId] = carMakeId
            param[kvariant] = self.txtVARIANT.text!.trim()
            param[kvehicleType] = self.txtVehicleType.text!.trim()
            param[kvehicleModelId] = modelId
           // param[kcarphoto] = JSON(lastComponent as Any).stringValue
            if let parent = self.parent?.parent as? SignUpVC  {
                param[kqr_code] = parent.param[kqr_code]
                self.addVehicleWs(param: param, parent: parent)
            }else if let parent = self.parent?.parent as? AddVehicleVC  {
                param[kqr_code] = parent.param[kqr_code]
                self.addVehicleWs(param: param, parent: parent)
            }else if self.isEditVehicle{
                
            }
            
       
            
            //self.editProfileWs()
        //}
    }
    
    func addVehicleWs(param : [String:Any] , parent : UIViewController)  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/add_vehicle/
         
         Parameter   : registration_number, vehicle_type, car_make, vehicle_model_id,
         variant, qr_code, documents
         
         Optional    :
         
         Comment     : This api for user can his new vehicle details. 
         
         ==============================
         */
        
        ApiManger.shared.makeRequest(method: .addVehicle , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    GFunction.SetHomeScreen()
                    break
                case .nodata:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                default :
                    break
                }
            }
        }
    }
    
    func getVehicleModelWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_vehicle_model_list
         
         Parameter   : vehicle_make_id
         
         Optional    :
         
         Comment     : This api for user can his new vehicle details.
         
         ==============================
         */
        
        ApiManger.shared.makeRequest(method: .vehicleModelList , parameter: ["vehicle_make_id" : carMakeId]) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    print(response[kData])
                   
                    let details = JsonList<VehicleModel>.createModelArray(model: VehicleModel.self, json: response[kData].arrayValue)
                    self.arrCarModel = details
                    self.picker.reloadAllComponents()
                    break
                    
                    
                default :
                    break
                }
            }
        }
    }
    
    func getVehicleCarMakeWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_vehicle_make_list
         
         Parameter   : vehicle_type(Scooter,Autorickshaw,Car,Bus,Truck)
         
         Optional    :
         
         Comment     : This api for user can his new vehicle details.
         
         ==============================
         */
        
        ApiManger.shared.makeRequest(method: .vehicleCarMakeList , parameter: ["vehicle_type": txtVehicleType.text!]) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    print(response[kData])
                    
                    let details = JsonList<VehicleModel>.createModelArray(model: VehicleModel.self, json: response[kData].arrayValue)
                    self.arrCarMake = details
                    self.picker.reloadAllComponents()
                    break
                    
                    
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        
        if let parent = self.parent?.parent as? SignUpVC  {
            parent.colList.isHidden = false
            if parent.selectIndex < SignUpViews.vehicleInfo.rawValue && !isEditVehicle {
                _ = self.view.subviewsRecursive().filter{ $0 is FloatingCommon }.map{ ($0 as! FloatingCommon).text = "" }
                //imgUploadImage.image = #imageLiteral(resourceName: "placeHolder")
            }
            parent.selectIndex = SignUpViews.vehicleInfo.rawValue
        }else if let parent = self.parent?.parent as? AddVehicleVC  {
            parent.colList.isHidden = false
            if parent.selectIndex < AddVehicleViews.vehicleInfo.rawValue && !isEditVehicle {
                _ = self.view.subviewsRecursive().filter{ $0 is FloatingCommon }.map{ ($0 as! FloatingCommon).text = "" }
                //imgUploadImage.image = #imageLiteral(resourceName: "placeHolder")
            }
            parent.selectIndex = AddVehicleViews.vehicleInfo.rawValue
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension VehicleInfoVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) {
            
        }
    
        
       self.imgUploadImage.image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.isIsImageChange = true
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true)
        {
            
        }
    }
}

//--------------------------------------------------------------------------
//MARK:- UIPickerView Method

extension VehicleInfoVC : UIPickerViewDelegate , UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtCarMake.isFirstResponder {
            return arrCarMake.count
        }
        if txtCarModel.isFirstResponder
        {
            return arrCarModel.count
        }
        if txtVARIANT.isFirstResponder
        {
            return arrCarVariant.count
        }
        if txtVehicleType.isFirstResponder
        {
            return arrVehicleType.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtCarMake.isFirstResponder && row < arrCarMake.count{
            return arrCarMake[row].name
        }
        if txtCarModel.isFirstResponder && row < arrCarModel.count
        {
            return arrCarModel[row].name
        }
        if txtVARIANT.isFirstResponder && row < arrCarVariant.count
        {
            return arrCarVariant[row]
        }
        
        if txtVehicleType.isFirstResponder && row < arrVehicleType.count
        {
            return arrVehicleType[row].title.uppercased()
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if txtCarMake.isFirstResponder && row < arrCarMake.count{
            txtCarMake.text =  arrCarMake[row].name
            carMakeId = JSON(arrCarMake[row].id).stringValue
            arrCarModel = []
            
        }
        
        if txtCarModel.isFirstResponder && row < arrCarModel.count
        {
            txtCarModel.text = arrCarModel[row].name
            self.modelId = JSON(self.arrCarModel[row].id).stringValue
        }
        if txtVARIANT.isFirstResponder && row < arrCarVariant.count {
            txtVARIANT.text = arrCarVariant[row]
        }
        
        if txtVehicleType.isFirstResponder && row < arrVehicleType.count {
            txtVehicleType.text = arrVehicleType[row].title.uppercased()
            arrCarModel = []
            arrCarMake = []
        }
    }
}

//--------------------------------------------------------------------------
//MARK:- Uitextfield Delegate Method

extension VehicleInfoVC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCarMake{
            return !txtVehicleType.text!.isEmpty && !arrCarMake.isEmpty
        }
        
        if textField == txtCarModel{
            return !txtCarMake.text!.isEmpty && !arrCarModel.isEmpty
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtVehicleType:
            self.getVehicleCarMakeWs()
            txtCarMake.text = ""
            txtCarModel.text = ""
        case txtCarMake:
            self.getVehicleModelWs()
            txtCarModel.text = ""
            break
        default:
            break
        }
    }
}
