//
//  AdvanceSettingVC.swift
//  ConnectWheels
//
//  Created by  on 05/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class SpeedView : UIView {
    
    let lblTitle : UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    let vwline : UIView = {
        let vw = UIView()
        vw.backgroundColor = colors.blueColor
        return vw
    }()
    
    func setUp()  {
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        vwline.translatesAutoresizingMaskIntoConstraints = false
        // add subview
        self.addSubview(lblTitle)
        self.addSubview(vwline)
        
        // add Constraints
        
        NSLayoutConstraint.activate([
            lblTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 8.0),
            lblTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8.0),
            lblTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0),
            lblTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0)
            ])
        
        NSLayoutConstraint.activate([
            vwline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 1.0),
            vwline.leadingAnchor.constraint(equalTo: self.lblTitle.leadingAnchor, constant: 0.0),
            vwline.trailingAnchor.constraint(equalTo: self.lblTitle.trailingAnchor, constant: 0.0),
            vwline.heightAnchor.constraint(equalToConstant: 2.0)
            ])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


class RefreshCell : UICollectionViewCell {
    
    @IBOutlet weak var lblDay: UILabel!
   
    override func awakeFromNib() {
      lblDay.textColor = colors.greyColor
      lblDay.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
     }
    
}

class AdvanceSettingVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    


    @IBOutlet var btnEngineCollection: [UIButton]!
    @IBOutlet weak var colWeekDays: UICollectionView!
    @IBOutlet weak var colTime: UICollectionView!
    @IBOutlet weak var colRefresh: UICollectionView!
    @IBOutlet weak var btnDefault: UIButton!
    
    @IBOutlet weak var pickerSpeed: UIPickerView!
    @IBOutlet weak var pickerBreak: UIPickerView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrWeekDays : [WeeKDays] = []
    var arrRefresh = ["5","10","15","30","60"]
    
    var dateFormatter = DateFormatter()
    
    var selctedRefreshIndex = 999
    
    var arrSpeed : [Int] = Array(10...140).filter{ $0 % 10 == 0 }
    
    var setting = AdvanceSetting()
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {

        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale.init(identifier: "en_US")
        
        let keys = dateFormatter.shortWeekdaySymbols
        
        dateFormatter.locale = Locale.init(identifier: (Locale.preferredLanguages.first)!)
        
        
        if let weekdays = dateFormatter.veryShortWeekdaySymbols
        {
            arrWeekDays = (0...weekdays.count - 1).map { (index) -> WeeKDays in
                let week = WeeKDays()
                week.day = weekdays[index]
                week.startTime = nil
                week.endTime = nil
                week.keyDay = JSON(keys?[index] as Any).stringValue
                return week
            }
            
        }
        
        
        
        pickerSpeed.showsSelectionIndicator = false
        
        
        colWeekDays.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
    }
    
    func SetUpSlider(slider : GBSliderBubbleView , maxValue : Float = 100)  {
        slider.delegate = self
        slider.isBubbleHideAnimation = false
        slider.maxValue = maxValue
        slider.thumbImage = #imageLiteral(resourceName: "advance_setting_selector")

        var frame = slider.frame
        frame.size.width = kScreenWidth - 85
        slider.frame = frame
        slider.renderSliderBubbleView()
        slider.bubbleView.backgroundColor = UIColor.clear
        slider.slider.minimumTrackTintColor = colors.blueColor
        slider.slider.maximumTrackTintColor = colors.blueColor
        slider.valueLabel.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.blueColor)
      
    }
    
    func setData()  {
        
        if let index = arrRefresh.firstIndex(where: { $0 == JSON(setting.refreshSec).stringValue }){
            selctedRefreshIndex = index
            colRefresh.reloadData()
        }
        
        if let indexSpeed = arrSpeed.index(of: setting.speedInKm){
           pickerSpeed.selectRow(indexSpeed, inComponent: 0, animated: true)
        }
        
        if let indexSpeedSec = arrSpeed.index(of: setting.speedInSec){
             pickerSpeed.selectRow(indexSpeedSec, inComponent: 1, animated: true)
        }
        
       
        
        if setting.brakeInKm < arrSpeed.count {
            pickerBreak.selectRow(setting.brakeInKm, inComponent: 0, animated: true)
        }
        
        if let index = btnEngineCollection.firstIndex(where: { $0.titleLabel!.text?.capitalized == JSON(setting.volume).stringValue }){
            btnEngineCollectionCLicked(btnEngineCollection[index])
        }
        
        _ = setting.settingTiming.map { (day) -> Void in
            if let index = arrWeekDays.firstIndex(where: { $0.keyDay == day.day}){
                let details = arrWeekDays[index]
                details.keyDay = day.day
                details.startTime = day.startTime
                details.endTime = day.endTime
                arrWeekDays[index] = details
            }
        }
        
        colTime.reloadData()
        
        btnDefault.isSelected = setting.defaultSettings != "Off"
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
 
    @IBAction func btnSOSClicked(_ sender: Any) {
        
        let obj : SOSVC = HomeStoryBoard.instantiateViewController(withIdentifier: "SOSVC") as! SOSVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    
    @IBAction func btnEngineCollectionCLicked(_ sender: UIButton) {

        _ = btnEngineCollection.enumerated().map { (index , btn) -> Void in
            if sender == btn{
                btn.setTitleColor(colors.blueColor, for:.normal)
                setting.volume = sender.titleLabel?.text
            }else{
                btn.setTitleColor(colors.greyColor, for: .normal)
            }
        }
    }
    @IBAction func btnDefaultSettingsClicked(_ sender: UIButton) {
        sender.isSelected =  !sender.isSelected
        setting.defaultSettings = btnDefault.isSelected ? "On" : "Off"
    }
    
    @IBAction func btnAlertClicked(_ sender: UIButton) {
        sender.isSelected =  !sender.isSelected
    }
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
        
        self.updateSettingWs()
    }
    
    //------------------------------------------------------
    
    //MARK:- Ws Methods
    
    func getAdvacneSettingWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_vehicle_settings/
         
         Parameter   : vehicle_id
         
         Optional    :
         
         Comment     : This api for user can get his vehicle defualt settings data.
         
         ==============================
         */
        
        var param : [String : Any] = [:]
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue

        ApiManger.shared.makeRequest(method: .getAdvanceSetting , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode   {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    debugPrint(response)
                    self.setting = AdvanceSetting.init(fromJson: response[kData])
                    self.setData()
                    break
                case .nodata:
                    break
                default :
                    break
                }
            }
        }
    }
    
    @objc func updateSettingWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/save_vehicle_settings/
         
         Parameter   : vehicle_id, speed_in_km, speed_in_sec,brake_in_km
         volume('Default','Low','Med','High'),
         refresh_sec, default_settings(Off/On)
         
         Optional    :
         
         Comment     : This api for user can get his vehicle defualt settings data.
         
         ==============================
         */
        
        
        
        var param : [String : Any] = setting.toDictionary()
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        param[kdays] = (arrWeekDays.filter{ $0.startTime != 0 ||  $0.endTime != 0}).map{ $0.toDictionary() }
        
        ApiManger.shared.makeRequest(method: .saveAdvanceSetting , parameter: param, withLoader: true) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode   {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
//                    self.navigationController?.popViewController(animated: true)
                    //self.setting = AdvanceSetting.init(fromJson: response[kData])
                    break
                case .nodata:
                    break
                default :
                    break
                }
            }
        }
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
           setUpView()
           getAdvacneSettingWs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

extension UISlider {
    var thumbCenterX: CGFloat {
        let trackRect = self.trackRect(forBounds: frame)
        let thumbRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: value)
        return thumbRect.midX
    }
}

//------------------------------------------------------

//MARK:- Slider Delegate

extension AdvanceSettingVC : GBSliderBubbleViewDelegate
{
    
    
    func getSliderDidEndChangeValue(_ value: Int, slider: GBSliderBubbleView!) {
       // slider.unit = "\(value)" as! NSMutableString
    
    }
    
    func getSliderDidChangeValue(_ value: Int, slider: GBSliderBubbleView!) {
       // slider.unit = "\(value)" as! NSMutableString
        /*if slider == slideSecond {
            
            
            switch value {
            case 0:
                slider.valueLabel.text = "10 sec"
                break;
            case 1:
                slider.valueLabel.text = "20 sec"
                break;
            case 2:
                slider.valueLabel.text = "30 sec"
                break;
            case 3:
                slider.valueLabel.text = "45 sec"
                break;
            case 4:
                slider.valueLabel.text = "1 min"
                break;
            case 5:
                slider.valueLabel.text = "2 min"
                break;
            case 6:
                slider.valueLabel.text = "3 min"
                break;
            case 7:
                slider.valueLabel.text = "4 min"
                break
            case 8:
                slider.valueLabel.text = "5 min"
                break
            default:
                break
            }
            
        }*/
    }
}


//------------------------------------------------------

//MARK:- Collection view method

extension AdvanceSettingVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == colRefresh {
            return arrRefresh.count
        }
        return arrWeekDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == colRefresh {
            let cell : RefreshCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RefreshCell", for: indexPath) as! RefreshCell
            cell.lblDay.text = arrRefresh[indexPath.row] + " Sec"
            if selctedRefreshIndex == indexPath.item
            {
                cell.lblDay.textColor = colors.blueColor
            }else
            {
                cell.lblDay.textColor = colors.greyColor
            }
            return cell
        }
        
        if collectionView == colWeekDays {
            let cell : WeekCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvanceWeekCell", for: indexPath) as! WeekCell
            cell.lblDay.text = arrWeekDays[indexPath.row].day.uppercased()
            return cell
        }
        
        let cell : SetTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvanceTimeCell", for: indexPath) as! SetTimeCell
        
        dateFormatter.dateFormat = "hh:mm a"
        
        cell.txtStartTime.text = arrWeekDays[indexPath.row].startTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].startTime))
        
        cell.txtEndTime.text = arrWeekDays[indexPath.row].endTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].endTime))
        
        cell.txtStartTime.inputView = cell.datePicker
        cell.txtEndTime.inputView = cell.datePicker
        
        cell.datePicker.datePickerMode = .time
        
        cell.txtStartTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("StartTime" , indexPath.row)
            cell.datePicker.minimumDate = nil
            //cell.txtStartTime.rightView = nil
            if cell.txtStartTime.text!.isEmpty{
                
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                
                if (self.arrWeekDays.filter{ $0.startTime != nil }).isEmpty{
                   self.arrWeekDays = self.arrWeekDays.map({ (day) -> WeeKDays in
                        let dayToModify = day
                        dayToModify.startTime = Date().timeIntervalSince1970
                        dayToModify.endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                        return dayToModify
                    })
                }else{
                    self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                    self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                }
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
        

        
        cell.txtEndTime.addAction(for: .editingDidBegin) { [unowned self] in
           
            if cell.txtStartTime.text!.isEmpty && cell.txtEndTime.text!.isEmpty{
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.datePicker.minimumDate = Date().addingTimeInterval(60*60*1)
            }else if !cell.txtStartTime.text!.isEmpty
            {
                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime)
                cell.txtEndTime.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
                cell.datePicker.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
        

        
        cell.datePicker.addAction(for: .valueChanged) { [unowned self] in
            
            if cell.txtStartTime.isFirstResponder
            {
                cell.txtStartTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date.addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = cell.datePicker.date.timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.addingTimeInterval(60*60*1).timeIntervalSince1970
            }
            
            if cell.txtEndTime.isFirstResponder
            {
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.timeIntervalSince1970
            }
        }
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == colRefresh{
            selctedRefreshIndex = indexPath.item
            setting.refreshSec = JSON(arrRefresh[indexPath.item]).intValue
            collectionView.reloadData()
            return
        }
        
        if collectionView == colWeekDays {
            colTime.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == colRefresh {
            return CGSize(width: (kScreenWidth -  90 - CGFloat(arrRefresh.count * 5)) / CGFloat(arrRefresh.count), height: collectionView.frame.size.height)
        }
        
        if collectionView == colWeekDays {
            return CGSize(width: (kScreenWidth -  93 - 35 - 12) / 7, height: collectionView.frame.size.height)
        }
        return CGSize(width: (kScreenWidth - 93 - 12), height: collectionView.frame.size.height)
    }
    
    
    
}

//------------------------------------------------------

//MARK:- Collection view method

extension AdvanceSettingVC : UIPickerViewDelegate , UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        if pickerBreak == pickerView {
//            return 1
//        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrSpeed.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return JSON(arrSpeed[row]).stringValue
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        pickerView.subviews[1].backgroundColor = UIColor.clear
        pickerView.subviews[2].backgroundColor = UIColor.clear
        
        let vw = SpeedView.init(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width / 2.0, height: 40))
        vw.lblTitle.text = JSON(arrSpeed[row]).stringValue
        return vw
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerSpeed{
            if component == 0{
                setting.speedInKm = JSON(arrSpeed[row]).intValue
                //NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(updateSettingWs), object: self)
                //self.perform(#selector(updateSettingWs), with: self, afterDelay: 0.5)
                
            }else{
                setting.speedInSec = JSON(arrSpeed[row]).intValue
            }
        }else if pickerView == pickerBreak{
            setting.brakeInKm = JSON(arrSpeed[row]).intValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.frame.width / 2.0
    }
}
