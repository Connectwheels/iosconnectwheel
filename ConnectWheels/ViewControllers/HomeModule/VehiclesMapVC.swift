//
//  VehiclesMapVC.swift
//  ConnectWheels
//
//  Created by  on 05/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class UserPin : UIView {
    @IBOutlet weak var imgPhoto: imageView!
    @IBOutlet weak var imgPin: UIImageView!
    
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}


class VehiclesMapVC: BlueNavigationBar , GMSMapViewDelegate {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var viewCarInfo: UIView!
    
    @IBOutlet weak var imgCarPhoto: imageView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblCarNumber: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var bounds = GMSCoordinateBounds()
    
    var vehicledetails : vehicleStatus = .keyStop
    var isTrackLocation = false
    
    var arrCarList : [VehicleStatusDetail] = []
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("Deinit for VehicleMapVC")
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setMarker() {
        
        self.map.clear()
        
        _ = arrCarList.map { (detail) -> Void in
            let current = GMSMarker(position: CLLocationCoordinate2D(latitude: JSON(detail.latitude).doubleValue, longitude: JSON(detail.longitude).doubleValue))
            let view : UserPin = UserPin.fromNib()
            view.imgPin.image = #imageLiteral(resourceName: "movingWheel").withRenderingMode(.alwaysTemplate)
            view.imgPhoto.sd_setImage(with: detail.carPhoto.getUrl())
            
            switch self.vehicledetails
            {
                
            case .keyNotInRange:
                view.imgPin.tintColor = colors.greyColor
                
                // current.icon = #imageLiteral(resourceName: "VGrey")
                break
            case .keyMoving:
                view.imgPin.tintColor =  UIColor.colorFromHex(hex: 0x44A973) // green
                //current.icon =  #imageLiteral(resourceName: "vGreen")
                break
            case .keyStop:
                // current.icon = #imageLiteral(resourceName: "vRed")
                view.imgPin.tintColor = UIColor.colorFromHex(hex: 0xEE3840) //red
                break
            case .keyNotMoving:
                //current.icon =  #imageLiteral(resourceName: "vYellow")
                view.imgPin.tintColor = UIColor.colorFromHex(hex: 0xEEBD38) //yellow
                break
            }
            
            current.userData = detail
            current.iconView = view
            current.map = map
            bounds = bounds.includingCoordinate(current.position)
        }
        
        self.map.animate(with: GMSCameraUpdate.fit(self.bounds, withPadding: 100))
    }
    
    func setUpView() {
        
        getVehicleDetailStatusWs()
        if !isTrackLocation {
            self.navigationItem.rightBarButtonItem = nil
        }
        map.delegate = self
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Google map method
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        UIView.animate(withDuration: 0.2) {
            self.viewCarInfo.isHidden = false
            self.viewCarInfo.superview?.constraints.filter{ $0.firstAttribute == .bottom}.first?.constant = 0
            self.view.layoutIfNeeded()
            
            if let cardetail = marker.userData as? VehicleStatusDetail{
                self.imgCarPhoto.sd_setImage(with: cardetail.carPhoto.getUrl())
                self.lblCarName.text = cardetail.carModel
                self.lblCarNumber.text = cardetail.registrationNumber
                self.btnCall.isHidden = true
            }
            
            
        }
      
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
      
        UIView.animate(withDuration: 0.3) {
            self.viewCarInfo.isHidden = true
            self.viewCarInfo.superview?.constraints.filter{ $0.firstAttribute == .bottom}.first?.constant = -200
            self.view.layoutIfNeeded()
        }
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    @IBAction func btnShareClicked(_ sender: Any) {
        let text = "Click below link to track vehicle \n http://maps.google.com/maps?daddr=23.0752,72.5257"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func btnCallClicked(_ sender: Any) {
        
        let phoneNum = "+919876543210"
        
        if let url = URL(string: "tel://\(phoneNum)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    //MARK:- Ws
    
    @objc func getVehicleDetailStatusWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_vehicle_status_details/
         
         Parameter   :
         
         Optional    :
         
         Comment     : This api for user can his state list list.
         
         ==============================
         */
        
        
        ApiManger.shared.makeRequest(method: .getStatusDetails , parameter: ["vehicle_status" : vehicledetails.key]) { [weak self] (response,httpCode,error,statuscode) in
            
            guard let strong = self else { return }
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
//                    self.vehicleStatusCount = VehicleStatusCount.init(fromJson: response[kData])
//                    self.tblVehicle.reloadData()
                    strong.arrCarList = JsonList<VehicleStatusDetail>.createModelArray(model: VehicleStatusDetail.self, json: response[kData].arrayValue)
                    strong.setMarker()
                    if strong.vehicledetails == .keyMoving {
                        strong.perform(#selector(strong.getVehicleDetailStatusWs), with: nil, afterDelay: 10.0)
                    }
                    break
                    
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            } else {
                if strong.vehicledetails == .keyMoving {
                    strong.perform(#selector(strong.getVehicleDetailStatusWs), with: nil, afterDelay: 10.0)
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
          self.setUpView()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }


}
