//
//  CompleteTripVC.swift
//  ConnectWheels
//
//  Created by  on 14/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class CompleteTripVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var completion : completion!
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    @IBAction func btnOkClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let cb = completion else { return }
        cb(true)
    }


}
