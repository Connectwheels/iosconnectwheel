//
//  SearchContactVC.swift
//  ConnectWheels
//
//  Created by  on 13/12/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class SearchContactVC : BlueNavigationBar {
    
    //MARK:- Outlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblSearch: UITableView!
    
    
    //------------------------------------------------------
    
    //MARK:- Class Variable
    
    var arrContacts : [UserContact] = []
    var arrFilter : [UserContact] = []
    var completion : completion!
    let formatter = CNContactFormatter()
    
    var btnAdd : UIBarButtonItem!
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    @objc func btnAddClicked() {
       let vc = CNContactViewController(forNewContact: CNContact())
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true, completion: nil)
    }
    
    func setUpView() {
        
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.tintColor = colors.whiteColor
        searchBar.becomeFirstResponder()
        ContactsList.shared.getContacts(completion: { (response) in
            if let res = response as? [UserContact]{
                self.arrContacts = res
                self.tblSearch.reloadData()
            }
        })
        
        self.navigationItem.rightBarButtonItem = nil
        
        btnAdd = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(btnAddClicked))
        btnAdd.tintColor = UIColor.white
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
}

extension SearchContactVC  : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchBar.text!.isEmpty{
           arrFilter = arrContacts
        }
        
        return arrFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "cell")
      
        let details = arrFilter[indexPath.row]
        cell.textLabel?.text = details.name
        cell.detailTextLabel?.text = details.phoneNumber
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cb = completion else { return }
        
        let details = arrFilter[indexPath.row]
        cb(details)
        
        self.dismiss(animated: true, completion: nil)
    }
    
}



extension SearchContactVC : UISearchBarDelegate , UIScrollViewDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        arrFilter = arrContacts.filter ({
            
            $0.name?.range(of: searchBar.text!, options: .caseInsensitive) != nil || $0.phoneNumber!.range(of: searchBar.text!, options: .caseInsensitive) != nil
            
        })
        
        self.navigationItem.rightBarButtonItem = arrFilter.isEmpty ? btnAdd : nil
        tblSearch.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        tblSearch.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblSearch.contentOffset.y < -75 {
            self.dismiss(animated: true, completion: nil)
        }
    }
}


extension SearchContactVC : CNContactViewControllerDelegate{
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        if let con = contact{
            let name = JSON(formatter.string(from: con) as Any).stringValue
            let number = JSON(con.phoneNumbers.first?.value.stringValue as Any).stringValue
            if number.isEmpty{
                return
            }
            guard let cb = completion else { return }
            cb(UserContact.init(name: name, phoneNumber: number))
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
  
}
