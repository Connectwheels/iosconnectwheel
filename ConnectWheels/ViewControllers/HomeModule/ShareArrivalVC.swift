//
//  ShareArrivalVC.swift
//  ConnectWheels
//
//  Created by  on 05/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ShareArrivalVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var colWeekDays: UICollectionView!
    @IBOutlet weak var colTime: UICollectionView!
    @IBOutlet weak var txtFirstName: FloatingCommon!
    @IBOutlet weak var txtLastName: FloatingCommon!
    @IBOutlet weak var txtMobile: FloatingCommon!
    @IBOutlet weak var txtArea: FloatingCommon!
    
    @IBOutlet weak var txtCode: FloatingCommon!
    
    @IBOutlet weak var btnAdd: ThemeActionButton!
    @IBOutlet weak var viewArea: UIView!
    
    @IBOutlet weak var viewSelectedDriverDetails: UIView!
    @IBOutlet weak var lblParents: Bold!
    @IBOutlet weak var lblAddPerson: Bold!
    
    
    @IBOutlet weak var lblDriverPhone: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var imgDriverPhoto: imageView!
    
    @IBOutlet weak var tblDriverList: IntrinsicTableView!
    
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrWeekDays : [WeeKDays] = []
    
    var arrPerson : [PersonDetails] = []
    
    var dateFormatter = DateFormatter()
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    
    var isShareDriver : Bool = false
    var isSharePerson : Bool = false
    var arrPickrList : [String] = [String]()
    var arrDriverList : [DriverListData] = []
    
   
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    

    
    func validate() -> String{
        
//        if txtFirstName.text!.isEmpty && !txtFirstName.isHidden{
//            return kvBlankFirstName
//
//        }else if txtLastName.text!.isEmpty && !txtLastName.isHidden{
//            return kvBlankLastName
//
//        }else
        
        if txtMobile.text!.isEmpty{
            return kvBlankPhoneNumber
         }else if txtMobile.text!.count < 6 {
            return kvValidPhoneNumber
            
        }else if txtArea.text!.isEmpty && !viewArea.isHidden{
            return kvBlankArea
            
        }
        
       
        
        return String()
    }
    

    
    func setUpView() {
        
        // Do not remove commented code until app is aproved
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: (Locale.preferredLanguages.first)!)
        
        if let weekdays = dateFormatter.veryShortWeekdaySymbols{
            arrWeekDays = (0...weekdays.count - 1).map { (index) -> WeeKDays in
                let week = WeeKDays()
                week.day = weekdays[index].capitalized
                week.startTime = nil
                week.endTime = nil
                return week
            }
       }
        
        arrPickrList = ["2" , "5" ,"10" ,"15" ,"30"]
        arrPerson = (0...10).map({ (index) -> PersonDetails in
            let person = PersonDetails()
            person.name = "User name"
            person.phone = "+00 123 123 1234"
            return person
        })
        
        txtArea.RightPaddingView(image: #imageLiteral(resourceName: "imgLocationGray"), frame: CGRect(x: 0, y: txtArea.titleHeight(), width: 30, height: txtArea.frame.size.height - txtArea.titleHeight()))
        txtMobile.RightPaddingView(image: #imageLiteral(resourceName: "imgCall"), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
//        txtDuration.RightPaddingView(image: #imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtDuration.titleHeight(), width: 30, height: txtDuration.frame.size.height - txtDuration.titleHeight()))
        
        txtMobile.regex = "^[0-9]{0,16}$"
        txtFirstName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        txtLastName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        colWeekDays.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        
   
        
       // txtCode.RightPaddingView(image: #imageLiteral(resourceName: "downArrow").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
        
        if isShareDriver {
            viewArea.isHidden = true
            
            
            self.title = "Select Driver"
            self.lblAddPerson.text = "Add Driver"
            self.lblParents.text = "Old Drivers"
          //  viewSelectedDriverDetails.isHidden = false
            lblDriverName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
            lblDriverName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
            imgDriverPhoto.image = #imageLiteral(resourceName: "B.jpg")
            viewSelectedDriverDetails.dropShadow()
            colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = txtFirstName.frame.size.height
        }else if isSharePerson
        {
            viewArea.isHidden = true
            
            self.lblParents.text = "Persons"
            self.lblAddPerson.text = "Add Person"
            viewSelectedDriverDetails.isHidden = true
            colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = txtFirstName.frame.size.height
        }else
        {
            viewSelectedDriverDetails.isHidden = true
            colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = txtFirstName.frame.size.height * 2
            
        }
        
        
        viewSelectedDriverDetails.isHidden = true
        
        //self.getDriverList()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnCountryClicked(_ sender: Any) {
        let obj : SearchVC  = AuthStoryBoard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        obj.completion = { data in
            if let details = data as? Codes
            {
                self.txtCode.text = details.code
            }
        }
        self.present(obj, animated: true, completion: nil)
    }
    
    
    @IBAction func btnAddNumberCliked(_ sender: Any) {
        GFunction.sharedInstance.openContactVC(from: self) { data in
            if let details = data as? UserContact{
                self.txtMobile.text = details.phoneNumber
            }
        }
    }
    
    @IBAction func btnSelectAreaClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        
        //addDriver()
        self.navigationController?.popViewController(animated: true)
    }
   
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func addDriver()  {
        
        /**
         ===========API CALL===========
         
         Method Name : share/add_driver/
         
         Parameter   : vehicle_id, fname, lname, country_code, phone, days
         
         Optional    :
         
         Comment     : This api for vehicle owner can add new driver for trip booking and tracking.
         
         ==============================
         */
        
        
        var param : [String:Any] = [:]
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        param[kfname] = txtFirstName.text
        param[klname] = txtLastName.text
        param[kcountry_code] = "+91"//txtCode.text
        param[kphone] = txtMobile.text
         param[kdays] = JSON(arrWeekDays as Any).stringValue
        
        ApiManger.shared.makeRequest(method: .addDriver  , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
//                    self.getDriverList()
                    
                    break
                    
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    
    
    func getDriverList()  {
        
        /**
         ===========API CALL===========
         
         Method Name : share/get_driver_list/
         
         Parameter   : vehicle_id
         
         Optional    :
         
         Comment     : This api for vehicle owner can get his added driver list.
         
         ==============================
         */
        
        var param : [String:Any] = [:]
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        
        ApiManger.shared.makeRequest(method: .getDriverList  , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.arrDriverList.append(contentsOf: DriverListData.fromArray(json: response[kData].arrayValue))
                    self.tblDriverList.reloadData()
                    break
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isShareDriver {
           colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = kScreenHeight * 0.0829187 + 30
        }else if isSharePerson
        {
          colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = kScreenHeight * 0.0829187 + 30
        }else
        {
           colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = txtFirstName.frame.size.height * 2 + 50
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension ShareArrivalVC: GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtArea.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
}

//------------------------------------------------------

//MARK:- Collection view method

extension ShareArrivalVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWeekDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == colWeekDays {
            let cell : WeekCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeekCell", for: indexPath) as! WeekCell
            cell.lblDay.text = arrWeekDays[indexPath.row].day.uppercased()
            return cell
        }
        
        let cell : SetTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetTimeCell", for: indexPath) as! SetTimeCell
        
        dateFormatter.dateFormat = "hh:mm a"
        
        cell.txtStartTime.text = arrWeekDays[indexPath.row].startTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].startTime))
        
        cell.txtEndTime.text = arrWeekDays[indexPath.row].endTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].endTime))
        
        cell.txtStartTime.inputView = cell.datePicker
        cell.txtEndTime.inputView = cell.datePicker
        
        cell.txtStartTime2.inputView = cell.datePicker2
        cell.txtEndTime2.inputView = cell.datePicker2
        
        cell.datePicker.datePickerMode = .time
        cell.datePicker2.datePickerMode = .time
        
        if isShareDriver || isSharePerson {
            cell.txtEndTime2.isHidden = true
            cell.txtStartTime2.isHidden = true
            cell.viewSecondTime.isHidden = true
        }else
        {
            cell.txtEndTime2.isHidden = false
            cell.txtStartTime2.isHidden = false
            cell.viewSecondTime.isHidden = false
        }
        
        
        cell.txtStartTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("StartTime" , indexPath.row)
            cell.datePicker.minimumDate = nil
            
            if cell.txtStartTime.text!.isEmpty{
                
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                
                if (self.arrWeekDays.filter{ $0.startTime != nil }).isEmpty{
                    self.arrWeekDays = self.arrWeekDays.map({ (day) -> WeeKDays in
                        let dayToModify = day
                        dayToModify.startTime = Date().timeIntervalSince1970
                        dayToModify.endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                        return dayToModify
                    })
                }else{
                    self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                    self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                }
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
        
        cell.txtStartTime2.addAction(for: .editingDidBegin) { [unowned self] in
            print("StartTime" , indexPath.row)
            cell.datePicker2.minimumDate = nil
            
            if cell.txtStartTime2.text!.isEmpty{
                cell.txtStartTime2.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime2.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime2 = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime2 = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
                
                
            }
        }
        
        
        
        cell.txtEndTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("EndTime" , indexPath.row)
            
            
            if cell.txtStartTime.text!.isEmpty && cell.txtEndTime.text!.isEmpty{
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.datePicker.minimumDate = Date().addingTimeInterval(60*60*1)
            }else if !cell.txtStartTime.text!.isEmpty
            {
                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime)
                cell.txtEndTime.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
                cell.datePicker.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
           
        }
        
        cell.txtEndTime2.addAction(for: .editingDidBegin) { [unowned self] in
            print("EndTime" , indexPath.row)
            
            
            if cell.txtStartTime2.text!.isEmpty && cell.txtEndTime2.text!.isEmpty{
                cell.txtStartTime2.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime2.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime2 = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime2 = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.datePicker2.minimumDate = Date().addingTimeInterval(60*60*1)
            }else if !cell.txtStartTime2.text!.isEmpty
            {
                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime2)
                cell.txtEndTime2.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
                cell.datePicker2.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
            
        }
        
        
        
        cell.datePicker.addAction(for: .valueChanged) { [unowned self] in
            
            if cell.txtStartTime.isFirstResponder
            {
                cell.txtStartTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date.addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = cell.datePicker.date.timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.addingTimeInterval(60*60*1).timeIntervalSince1970
            }
            
            if cell.txtEndTime.isFirstResponder
            {
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.timeIntervalSince1970
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
            
        }
        
        cell.datePicker2.addAction(for: .valueChanged) { [unowned self] in
            
            if cell.txtStartTime2.isFirstResponder
            {
                cell.txtStartTime2.text = self.dateFormatter.string(from: cell.datePicker2.date)
                cell.txtEndTime2.text = self.dateFormatter.string(from: cell.datePicker2.date.addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime2 = cell.datePicker2.date.timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime2 = cell.datePicker.date.addingTimeInterval(60*60*1).timeIntervalSince1970
            }
            
            if cell.txtEndTime2.isFirstResponder
            {
                cell.txtEndTime2.text = self.dateFormatter.string(from: cell.datePicker2.date)
                self.arrWeekDays[indexPath.row].endTime2 = cell.datePicker2.date.timeIntervalSince1970
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
            
        }
        
        return cell
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colWeekDays {
            colTime.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == colWeekDays {
            return CGSize(width: (kScreenWidth - 80 - 35) / 7, height: collectionView.frame.size.height)
        }
        return CGSize(width: (kScreenWidth - 80), height: collectionView.frame.size.height)
    }
    
    
    
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension ShareArrivalVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrPerson.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DriverCell = tableView.dequeueReusableCell(withIdentifier: "DriverCell") as! DriverCell
        
        cell.rightSwipeSettings.transition = .drag
        cell.imgPhoto.image = #imageLiteral(resourceName: "B.jpg")
        cell.lblUserName.text = arrPerson[indexPath.section].name
        cell.lblNumber.text = arrPerson[indexPath.section].phone
        
        if isShareDriver && indexPath.section == 0{
//            cell.accessoryType = .checkmark
            let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: cell.frame.size.height))
            imageView.image = #imageLiteral(resourceName: "Tick")
            imageView.contentMode = .left
            cell.accessoryView = imageView
        }else
        {
            cell.accessoryType = .none
        }
        
        let delete = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete"), backgroundColor: colors.deleteColor) { (cell) -> Bool in
            
            let alert = UIAlertController(title: nil, message: "Are you sure you want to remove this person?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                self.arrPerson.remove(at: indexPath.row)
                tableView.reloadData()
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
            return true
        }
        
        delete.buttonWidth = 60
        
        cell.rightButtons = [delete]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isShareDriver {
            self.btnBackClicked()
        }
    }
    
}


//------------------------------------------------------

//MARK:- UITextFieldDelegate Methods

extension ShareArrivalVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
       /* if textField == txtCode{
            return false
        }*/
        
        return true
    }
    
   
}

//------------------------------------------------------

//MARK:- CNContactPickerDelegate Methods

extension ShareArrivalVC : CNContactPickerDelegate{
    
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way
        
        // user name
        let userName:String = contact.givenName
        
        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
        
        
        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
        
        print(primaryPhoneNumberStr)
        
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
}
