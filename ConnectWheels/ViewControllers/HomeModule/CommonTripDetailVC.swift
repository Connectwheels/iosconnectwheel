//
//  CommonTripDetailVC.swift
//  ConnectWheels
//
//  Created by  on 03/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class CommonTripDetailVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblTrip: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var pageIndex : Int = 0
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
//        tblTrip.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

extension CommonTripDetailVC : UIScrollViewDelegate
{
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        if let parentView = self.parent?.parent as? TripDetailVC
//        {
//            var scrollViewHeight: CGFloat = scrollView.frame.size.height
//            var scrollContentSizeHeight: CGFloat = scrollView.contentSize.height
//            var scrollOffset: CGFloat = scrollView.contentOffset.y
//            var offset: CGFloat = scrollContentSizeHeight - (scrollOffset + scrollViewHeight)
//            if offset <= 0 {
//                parentView.scroll.setContentOffset(CGPoint(x: parentView.scroll.contentOffset.x, y: parentView.scroll.contentOffset.y + 50), animated: true)
//            }
//        }
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        scrollView.bounces = false
//    }
}
