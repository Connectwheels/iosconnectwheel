//
//  MenuVC.swift
//  ConnectWheels
//
//  Created by  on 05/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class MenuVC: TransparentBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var imgBgProfile: UIImageView!
    @IBOutlet weak var imgProfile: imageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [String] = []
    let keyHome = "Home"
    let keyWheelStatus = "Vehicle Status"
    let keySettings = "Settings"
    let keyAboutUs = "About Us"
    let keyContactUs = "Contact Us"
    let keyLogOut  = "Logout"
    let keySOS  = "SOS Numbers"
    
    let keyGeoFence = "Geo Fencing"
    let keyBooking = "Booking"
    let keyMeeting = "Meeting"
    let keyRecharge = "Recharge"
    let keyImmobolize = "Immobolize"
    let keyMyDocuments = "Personal ID"
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
//        arrList = [keyHome,keyWheelStatus,keyMyDocuments,keyGeoFence,keyBooking,keyMeeting,keyRecharge,keySettings,keyAboutUs,keyContactUs, keyImmobolize,keyLogOut]
        arrList = [keyHome,keyWheelStatus,keyMyDocuments,keySettings,keyAboutUs,keyContactUs,keyLogOut]
        lblUserName.montserrat(ofType: fonts.montserratBoldFont, withfontSize: 18.0, color: colors.whiteColor)
        let tap = UITapGestureRecognizer(target: self, action: #selector(imgProfileTapped))
        imgProfile.addGestureRecognizer(tap)
        imgProfile.isUserInteractionEnabled = true
     }
    
    @objc func imgProfileTapped() {
        let obj : ProfileVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnCloseClicked(_ sender: Any) {
       hideContentController(content: self , delay: 0.2)
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func logOutWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/logout/
         
         Parameter   :
         
         Optional    :
         
         Comment     : This api will be used for user can logout from the app.
         
         ==============================
         */
        
        ApiManger.shared.makeRequest(method: .logout , parameter: [:]) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    
                   GFunction.setLoginVC()
                    
                    break
 
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgProfile.sd_setImage(with: currentUser.profile_image?.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultProfilePic"))
        imgBgProfile.sd_setImage(with: currentUser.profile_image?.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultProfilePic"))
        lblUserName.text = JSON(currentUser.fname as Any).stringValue + " " + JSON(currentUser.lname as Any).stringValue
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension MenuVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MenuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
       
        cell.lblTitle.text = arrList[indexPath.row]
        
        switch arrList[indexPath.row] {
        case keyHome:
           cell.icon.image = #imageLiteral(resourceName: "mhome")
            break
        case keyWheelStatus:
           cell.icon.image = #imageLiteral(resourceName: "mwheel_report")
            break
        case keySettings:
           cell.icon.image = #imageLiteral(resourceName: "msettings")
            break
        case keyAboutUs:
           cell.icon.image = #imageLiteral(resourceName: "maboutus")
            break
        case keyContactUs:
          cell.icon.image = #imageLiteral(resourceName: "mcontactUs")
            break
        case keyLogOut:
            cell.icon.image = #imageLiteral(resourceName: "mlogout")
            break
           
        case keySOS:
            cell.icon.image = UIImage.init(named: "H3Sos")?.withRenderingMode(.alwaysTemplate)
            cell.icon.tintColor = colors.blueColor
            
            break
            
            // new added Module
            
        case keyRecharge :
           cell.icon.image = #imageLiteral(resourceName: "mrecharge")
            break
        case keyBooking :
          cell.icon.image = #imageLiteral(resourceName: "mBooking")
            break
        case keyMeeting :
            cell.icon.image = #imageLiteral(resourceName: "mmeeting")
            break
        case keyGeoFence :
            cell.icon.image = #imageLiteral(resourceName: "mgeoFence")
            break
            
             // new added changes 27-06-11
            
        case keyMyDocuments :
            cell.icon.image = UIImage.init(named: "mDocuments")?.withRenderingMode(.alwaysTemplate)
            cell.icon.tintColor = colors.blueColor
            break
        case keyImmobolize :
            cell.icon.image = #imageLiteral(resourceName: "immobolize")
            break
           
        default:
           
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch arrList[indexPath.row] {
        case keyHome:
            
            break
        case keyWheelStatus:
            let obj : WheelStatusVC = HomeStoryBoard.instantiateViewController(withIdentifier: "WheelStatusVC") as! WheelStatusVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case keySettings:
            let obj : SettingVC = HomeStoryBoard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case keyAboutUs:
            let obj : AboutUsVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case keyContactUs:
            let obj : ContactUsVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case keyLogOut:
            
            DispatchQueue.main.async {
                let alert = UIAlertController(title: nil, message: "Are you sure you want to logout?".Localized(), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                    self.logOutWs()
                }))
                
                alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                    
                }))
                
                appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
            
            
            return
            
        case keySOS:
            let obj : SOSVC = HomeStoryBoard.instantiateViewController(withIdentifier: "SOSVC") as! SOSVC
            self.navigationController?.pushViewController(obj, animated: true)
            
            // new added Module
            
        case keyRecharge :
            let obj : RechargeListVC = BookingStoryBoard.instantiateViewController(withIdentifier: "RechargeListVC") as! RechargeListVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case keyBooking :
            let obj : BookingVC = BookingStoryBoard.instantiateViewController(withIdentifier: "BookingVC") as! BookingVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case keyMeeting :
            let obj : MeetingVC = BookingStoryBoard.instantiateViewController(withIdentifier: "MeetingVC") as! MeetingVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case keyGeoFence :
            let obj : GeofenceVC = BookingStoryBoard.instantiateViewController(withIdentifier: "GeofenceVC") as! GeofenceVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
            
            // new added changes 27-06-11
            
        case keyMyDocuments :
            let obj : MyDocumentsVC = BookingStoryBoard.instantiateViewController(withIdentifier: "MyDocumentsVC") as! MyDocumentsVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
            
        case keyImmobolize :
            let obj : ImmobilizeListVC = BookingStoryBoard.instantiateViewController(withIdentifier: "ImmobilizeListVC") as! ImmobilizeListVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
            
        default:
            
            break
        }
        
        hideContentController(content: self ,delay: 0.5)
    }
}

//------------------------------------------------------

//MARK:- Menu Cell

class MenuCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        lblTitle.montserrat(ofType: fonts.montserratMediumFont, withfontSize: 12.0, color: colors.blackColor)
    }
    
}
