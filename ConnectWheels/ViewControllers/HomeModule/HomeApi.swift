//
//  HomeApi.swift
//  ConnectWheels
//
//  Created by  on 22/03/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import UIKit
import MessageUI

extension HomeVC{
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func parkingWS()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/change_parking/
         
         Parameter   : vehicle_id,parking('Off', 'On')
         
         Optional    :
         
         Comment     : This api will be used for user can change send his contect us request.
         
         ==============================
         */
        
        if JSON(VehicleDataSource.shared.selectedVehicle?.parking as Any).stringValue == "Off" && JSON(soketData?.ignitionStatus as Any).boolValue{
            return
        }
        
        var param : [String : Any] = [:]
        
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        
        param["parking"] =   (JSON(VehicleDataSource.shared.selectedVehicle?.parking as Any).stringValue == "On") || (VehicleDataSource.shared.isEmergency) ? "Off" : "On"
        
        ApiManger.shared.makeRequest(method: .changeParking , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    let vehicle = Vehicle.init(fromJson: response[kData])
                    VehicleDataSource.shared.arrVehicles[VehicleDataSource.shared.selectedIndex] = vehicle
                    if VehicleDataSource.shared.isEmergency{
                        VehicleDataSource.shared.isEmergency = false
                        VehicleDataSource.shared.selectedVehicle?.isEmergency = "0"
                    }
                    self.tblHome.reloadData()
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    func getSoSListWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_user_sos_numbers/
         
         Parameter   :
         
         Optional    :
         
         Comment     : This api for user can get his sent SOS phone numbers.
         
         ==============================
         */
        
        
        ApiManger.shared.makeRequest(method: .getSOSUserList , parameter: [:] ) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    
                    break
                case .success:
                    let data = response[kData].arrayValue
                    let toUsers = data.map({ (detail) -> String in
                        return detail["country_code"].stringValue + detail["phone"].stringValue
                    })
                    self.openMessage(toUsers: toUsers)
                    break
                case .nodata:
                    
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    func openMessage(toUsers recipients: [String]) {
        
        if MFMessageComposeViewController.canSendText() {
            
            let controller = MFMessageComposeViewController()
            
            let vehicleNumber = JSON(VehicleDataSource.shared.selectedVehicle?.registrationNumber as Any).stringValue
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date = dateFormatter.string(from: Date())
            dateFormatter.dateFormat = "hh:mm a"
            let time = dateFormatter.string(from: Date())
            let location = "\(JSON(self.soketData?.latitude as Any).stringValue),\(JSON(self.soketData?.longitude as Any).stringValue)"
            let base64Imei = JSON(VehicleDataSource.shared.selectedVehicle?.deviceImei as Any).stringValue.toBase64() ?? ""
            controller.body = "Emergency!! Alert for Vehicle Number : \(vehicleNumber)" +
                              "\nDate : \(date)" +
                              "\nTime: \(time)" +
                              "\nLocation : \(location)" +
                              "\nCourtesy: Connect Wheels 18001218026" +
                              "\nYou can track the status : http://13.232.41.240/home/livetracking/\(base64Imei)"
            controller.recipients = recipients
            controller.messageComposeDelegate = self
            
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    /*func sendSOSMessage()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/send_sos/
         
         Parameter   :
         
         Optional    :
         
         Comment     : This api will be used for user can change send his contect us request.
         
         ==============================
         */
        

        
        ApiManger.shared.makeRequest(method: .sendSOS , parameter: [:]) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    self.tblHome.reloadData()
                    break
                case .nodata:
                    GFunction.ShowAlert(message: "Please add SOS numbers from Settings Menu")
                    break
                    
                default :
                    break
                }
            }
        }
    }*/
    
    @objc func getVehicleLastLocation() {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_vehicle_last_location/
         
         Parameter   : device_imei
         
         Optional    :
         
         Comment     : This api for user can his vehicle list.
         
         ==============================
         */
        
        guard let _ = VehicleDataSource.shared.selectedVehicle else {
            
            self.perform(#selector(self.getVehicleLastLocation), with: nil, afterDelay: 5.0)
            
            return
            
        }
        
        ApiManger.shared.makeRequest(method: .vehicleLastLocation , parameter: [kDeviceImei:JSON(VehicleDataSource.shared.selectedVehicle?.deviceImei as Any).stringValue], withErrorAlert : false , withLoader : false, withdebugLog: false) { (response,httpCode,error,statuscode) in
            
            self.perform(#selector(self.getVehicleLastLocation), with: nil, afterDelay: 5.0)
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    
                    break
                case .success:
                    self.soketData = SoketData.init(response[kData])
                    break
                    
                case .nodata:
                    
                    break
                    
                default :
                    break
                }
            }
        }
        
    }
    
    func getVehicle(isShowAlert : Bool = false, isShowLoader : Bool = false , completion : completion?) {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_vehicle_list/
         
         Parameter   : vehicle_id
         
         Optional    :
         
         Comment     : This api for user can his vehicle list.
         
         ==============================
         */
        
        
        ApiManger.shared.makeRequest(method: .getVehicle , parameter: [kvehicle_id:JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue], withErrorAlert : isShowAlert , withLoader : isShowLoader) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    
                    if isShowAlert
                    {
                        GFunction.ShowAlert(message: response[kMessage].stringValue)
                    }
                    break
                case .success:
                    
                    self.arrVehicles[self.selectedVehicleIndex] = Vehicle.init(fromJson: response[kData])
                   
                    break
                    
                case .nodata:
                    guard let cb = completion else { return}
                    cb([])
                    break
                    
                default :
                    break
                }
            }
        }
        
    }
}

extension HomeVC: MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true) {
            self.navigationController?.isNavigationBarHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
            })
        }
        
    }
    
}
