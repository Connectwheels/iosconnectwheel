//
//  SOSVC.swift
//  ConnectWheels
//
//  Created by  on 04/02/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import UIKit

class SosNumberCell : UITableViewCell{
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
         lblUserName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.whiteColor)
    }
}

class SOSVC : BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var tblSosUserList: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var txtSosNumber: FloatingCommon!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrUserList : [JSON] = []
    var arrSelectedIndex : [Int] = []
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
    
        if txtSosNumber.text!.isEmpty{
            return kvBlankPhoneNumber
        }else if txtSosNumber.text!.count < 6 || txtSosNumber.text!.prefix(1) == "0"{
            return kvValidPhoneNumber
            
        }
        return String()
    }
    
    func setUpView() {
        txtSosNumber.RightPaddingView(image: UIImage(), frame: CGRect.init(x: 0, y: 0, width: 50, height: txtSosNumber.frame.height))
        txtSosNumber.regex = "^[0-9]{0,16}$"
        btnAdd.addAction(for: .touchUpInside) {
            let error = self.validate()
            if error != String() {
                GFunction.ShowAlert(message: error)
                return
            }
            
            self.view.endEditing(true)
            var json : [String:Any] = [:]
            json["phone"] = self.txtSosNumber.text!
            json["country_code"] = "+91"
            self.arrUserList.append(JSON(json))
            self.arrSelectedIndex.append(self.arrUserList.count - 1)
            self.tblSosUserList.reloadData()
            
            self.sendSOSNumbersWs()
        }
        
        getSoSListWs()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        sendSOSNumbersWs()
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Ws
    
    func getSoSListWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_user_sos_numbers/
         
         Parameter   :
         
         Optional    :
         
         Comment     : This api for user can get his sent SOS phone numbers.
         
         ==============================
         */
        
        
        ApiManger.shared.makeRequest(method: .getSOSUserList , parameter: [:] ) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                   self.arrUserList.append(contentsOf: response[kData].arrayValue)
                   self.tblSosUserList.reloadData()
                    
                    break
                case .nodata:
                    
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    func deleteSOSNumbersWs(id : String)  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/remove_sos_numbers/
         
         Parameter   : id
         
         Optional    :
         
         Comment     : This api for user can get his sent SOS phone numbers.
         
         ==============================
         */
        var param : [String : Any] = [:]
        param["id"] = id
        
        
        ApiManger.shared.makeRequest(method: .removeSOS , parameter: param ) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                   
                    break
                case .nodata:
                    
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
   
    
    func sendSOSNumbersWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/send_sos/
         
         Parameter   : vehicle_id , sos_numbers
         
         Optional    :
         
         Comment     : This api for user can get his sent SOS phone numbers.
         
         ==============================
         */
        var param : [String : Any] = [:]
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        param["sos_numbers"] = (arrUserList.enumerated().compactMap{ $0.element.dictionaryObject })
        
        ApiManger.shared.makeRequest(method: .addSos , parameter: param ) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.arrUserList.append(contentsOf: response[kData].arrayValue)
                    self.tblSosUserList.reloadData()
                    
                    break
                case .nodata:
                    
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}


//------------------------------------------------------

//MARK:- Table VIew Method

extension SOSVC : UITableViewDelegate , UITableViewDataSource{
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SosNumberCell = tableView.dequeueReusableCell(withIdentifier: "SosNumberCell") as! SosNumberCell
        let data = arrUserList[indexPath.row]
        cell.lblUserName.text = data["country_code"].stringValue + data["phone"].stringValue
        cell.btnCheck.isSelected = arrSelectedIndex.contains(indexPath.row)
        cell.btnCheck.isHidden = true
        /*cell.btnCheck.addAction(for: .touchUpInside) {
            if self.arrSelectedIndex.contains(indexPath.row){
                self.arrSelectedIndex = self.arrSelectedIndex.filter{ $0 != indexPath.row }
            }else{
                self.arrSelectedIndex.append(indexPath.row)
            }
            self.tblSosUserList.reloadData()
        }*/
        
        cell.btnDelete.addAction(for: .touchUpInside) {
            let strId = self.arrUserList[indexPath.row]["id"].stringValue
            if !strId.isEmpty{
                self.deleteSOSNumbersWs(id: strId)
            }

            self.arrSelectedIndex = self.arrSelectedIndex.filter{ $0 != indexPath.row }
            self.arrUserList.remove(at: indexPath.row)
            self.tblSosUserList.reloadData()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
   
}
