//
//  ViewDocumentVC.swift
//  ConnectWheels
//
//  Created by  on 03/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ViewDocumentVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var colDocuments: UICollectionView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [UploadModel] = []
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    
    
    
    func setUpView() {
        
        let detail1 = UploadModel(title: "Registration Photo", image: #imageLiteral(resourceName: "uploadReg"))
        arrList.append(detail1)
        
        let detail2 = UploadModel(title: "National ID Photo", image: #imageLiteral(resourceName: "uploadID"))
        arrList.append(detail2)
        
        let detail3 = UploadModel(title: "Car Front Photo", image: #imageLiteral(resourceName: "uploadFront"))
        arrList.append(detail3)
        
        let detail4 = UploadModel(title: "Car Back Photo", image: #imageLiteral(resourceName: "uploadBack"))
        arrList.append(detail4)
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    @IBAction func btnEditDocumentsClicked(_ sender: Any) {
        let obj : EditDocumentVC = HomeStoryBoard.instantiateViewController(withIdentifier: "EditDocumentVC") as! EditDocumentVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}



//------------------------------------------------------

//MARK:- Collection view method

extension ViewDocumentVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : UploadDocumentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadDocumentCell", for: indexPath) as! UploadDocumentCell
        
        let detail = arrList[indexPath.row]
        
        cell.lblDesc.text = detail.title
        
//        if detail.isDefaultImage {
//            cell.imgPhoto.image = detail.image
//            cell.lblDesc.isHidden = true
//            cell.blackLayer.isHidden = true
//        }else
//        {
            cell.imgPhoto.image = #imageLiteral(resourceName: "2.jpg")
            cell.lblDesc.isHidden = false
            cell.blackLayer.isHidden = false
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        

        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (kScreenWidth - 100) / 2, height: (kScreenWidth - 100) / 2)
    }
}
