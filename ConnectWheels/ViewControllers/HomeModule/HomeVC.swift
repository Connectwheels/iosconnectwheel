//
//  HomeVC.swift
//  ConnectWheels
//
//  Created by  on 25/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit



var btnWheelMultiplier = CGFloat(0.23)

//------------------------------------------------------

//MARK:- HomeHeader Cell

class HomeHeaderCell : UITableViewCell {
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var imgBg: UIImageView!
    
    
    
    override func awakeFromNib() {
        lblHeader.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.whiteColor)
        
    }
    
    
}

class HomeHeaderVehicleCell : UITableViewCell {
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnStartTrip: UIButton!
    
    override func awakeFromNib() {
        lblHeader.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 12.0, color: colors.blueColor)
        lblDetail.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.whiteColor)
        btnStartTrip.setLayout(detail: [layout.kfont : UIFont(name: fonts.montserratMediumFont, size: 12.0 * scaleFactor) as Any])
        btnStartTrip.isHidden = true
        
        btnStartTrip.constraints.filter{$0.firstAttribute == .width}.first?.constant = kScreenWidth  - (kScreenWidth * btnWheelMultiplier)
    }
    
    
}

//------------------------------------------------------

//MARK:- Required Classes

class HomeListButton : UIButton {
    
}

class HomeHeaderButton : UIButton {
    
}



class HomeTable : UITableView {
    
    var isCollapse = false
    var isStarTripShown = false
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        if self.point(inside: point, with: event) {
            return super.hitTest(point, with: event)
        }
        guard isUserInteractionEnabled, !isHidden, alpha > 0 else {
            return nil
        }
        
        for subview in self.subviewsRecursive().filter({ $0 is HomeListButton || $0 is HomeHeaderButton }) {
            
            // Return touched view outside of bounds
            
            
            let convertedPoint = subview.convert(point, from: self)
            
            if  let hitview = subview.hitTest(convertedPoint, with: event) , hitview is HomeListButton && isCollapse
            {
                return hitview
            }
            
            if  let hitview = subview.hitTest(convertedPoint, with: event) , hitview is HomeHeaderButton && isStarTripShown
            {
                return hitview
            }
            
            
            
        }
        
        
        return nil
    }
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIViewNoIntrinsicMetric, height: contentSize.height)
    }
    
    
    
}

extension UIView {
    
    func subviewsRecursive() -> [UIView] {
        return subviews + subviews.flatMap { $0.subviewsRecursive() }
    }
    
}



class HomeVC: TransparentBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblHome: HomeTable!
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var blurViewColor: UIView!
    @IBOutlet weak var constTableLeading: NSLayoutConstraint!
    @IBOutlet weak var btnDriver: UIButton!
    @IBOutlet weak var constBtnDriverTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var constBtnNotificationTrailing: NSLayoutConstraint!
    
    
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var btnAlerts: UIButton!
    @IBOutlet weak var viewCustomerInfo: UIView!
    
    @IBOutlet weak var constCustomIfoBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblCarname: UILabel!
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
     var arrVehicles = VehicleDataSource.shared.arrVehicles{
        didSet{
            tblHome.reloadSections(IndexSet(integer: 0), with: .none)
        }
    }
    
    //changes now list based on sorting order of alphabets
    
    var isShowRed : Bool = false // Changes (Trainee)
    var arrOptionList : [String] = []
    var keyVehicles = "0"
    var keySpeed = "keySpeed"
    var keyAc = "A/C"
    var keyShareLocation = "Share"
    var keyIgnition = "keyIgnition"
    var keyTrack = "keyTrack"
    var keyParent = "Parent"
    var keyDocuments = "keyDocuments"
    var keySOS = "keySOS"
    var keyParking = "keyParking"
    var keyPreAlert = "Pre Alert"
    let keyHistory = "keyHistory"
    
    let obj : MenuVC = HomeStoryBoard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
    
    var source = GMSMarker.init()
       
        
    
    
    // Mark All DidSet Variables
    
    //------------------------------------------------------
    
    //MARK:- user
    
    var user : UserType = .Owner{
        didSet{
            tblHome.isStarTripShown = user == .Driver
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- isShowVehicle
    
    var isShowVehicle = false{
        didSet{
            //            blurView.isHidden = !isShowVehicle
            blurViewColor.isHidden = !isShowVehicle
            self.tblHome.isCollapse = self.isShowVehicle
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- isShowLeftMenu
    
    var isShowLeftMenu = true    {
        didSet{
            
            if isShowVehicle {
                isShowVehicle = false
                tblHome.reloadData()
            }
            
            if user == .Owner {

            }
            
            if user == .Driver{
                //hide start button
                self.tblHome.reloadSections(IndexSet(integer: 0), with: .automatic)
            }
            
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseInOut], animations: {
                if self.isShowLeftMenu {
                    self.constTableLeading.constant = 0
                }else{
                    self.constTableLeading.constant = -self.tblHome.frame.size.width
                }
                self.view.layoutIfNeeded()
            }, completion: nil)
         }
    }
    
    //------------------------------------------------------
    
    //MARK:- istripStarted
    
    var istripStarted = false{
        didSet{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [], animations: {
                self.constCustomIfoBottom.constant = self.istripStarted ? 0 : -self.viewCustomerInfo.frame.size.height
                self.view.layoutIfNeeded()
            })
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- selectedVehicleIndex
    
    var selectedVehicleIndex = 0 {
        didSet{
            if arrVehicles.isEmpty{
                lblRegNo.text = ""
                lblCarname.text = ""
                lblDistance.text = ""
               // listener?.remove()
                listener = nil
                return
            }
            
            VehicleDataSource.shared.selectedIndex = selectedVehicleIndex
            
            getVehicle { (_) in
                self.tblHome.reloadData()
            }
            
            /*let db = Firestore.firestore()
       
            if let id = VehicleDataSource.shared.selectedVehicle?.qrCode{
               // listener?.remove()
             
                listener = nil
             
             
                listener = self.observe(\.soketData, options: [.new]) { [unowned self] (source, change) in
             
             
                }
             
                /*listener = db.collection("vehicles").document(id)
                    .addSnapshotListener { documentSnapshot, error in
                        guard let document = documentSnapshot else {
                            print("Error fetching document: \(error!)")
                            return
                        }
                       // let source = document.metadata.hasPendingWrites ? "Local" : "Server"
                      
                        do {
                            let dict = try JSON(document.data() as Any).rawData()
                            self.soketData = try JSONDecoder().decode(SoketData.self, from: dict)
                            
                        }catch let soketError{
                            debugPrint("Soket Data Error : " , soketError.localizedDescription)
                            self.source.icon = GFunction.sharedInstance.getStatusWiseImage(type: self.arrVehicles[self.selectedVehicleIndex].vehicleType, status: .off)
                        }
                }*/
            }*/
            
            
            
            source.icon = GFunction.sharedInstance.getStatusWiseImage(type: arrVehicles[selectedVehicleIndex].vehicleType, status: .off)
            lblRegNo.text = arrVehicles[selectedVehicleIndex].registrationNumber.uppercased()
            lblCarname.text = arrVehicles[selectedVehicleIndex].carModel.uppercased()
            lblDistance.text = ""
            lblNotificationCount.text = arrVehicles[selectedVehicleIndex].alertsCount
            lblNotificationCount.isHidden = JSON(arrVehicles[selectedVehicleIndex].alertsCount).intValue == 0
            
            self.getVehicleLastLocation()
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Soket Update Data
    
   // var ref: DocumentReference? = nil
   // var listener : ListenerRegistration?
    var listener : NSKeyValueObservation?
    
    var timer = Timer()
    
    var soketData : SoketData?{
        willSet{
            if let oldData = soketData , let  newData = newValue{
                if oldData.latitude != newData.latitude || oldData.longitude != newData.longitude{
                    source.icon = GFunction.sharedInstance.getStatusWiseImage(type: arrVehicles[selectedVehicleIndex].vehicleType, status: .moving)
                }
                if oldData.speedInKmH != newData.speedInKmH {
                    timer.invalidate()
                    timer = Timer.init(timeInterval: 5*60, target: self, selector: #selector(updateVehicleTime), userInfo: nil, repeats: false)
                    source.icon = GFunction.sharedInstance.getStatusWiseImage(type: arrVehicles[selectedVehicleIndex].vehicleType, status: .moving)
                }
                
                
                if oldData.date == newData.date && oldData.time == newData.time && newData.ignitionStatus == "1"{
                    
                    let formatterNw = DateFormatter()
                    formatterNw.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    formatterNw.timeZone = TimeZone(abbreviation: "UTC")
                    if let oldTime = formatterNw.date(from: newData.date + " " + newData.time), Date() > oldTime.addingTimeInterval(5*60) {
                        source.icon = GFunction.sharedInstance.getStatusWiseImage(type: arrVehicles[selectedVehicleIndex].vehicleType, status: .notinrange)
                    }
                }
            }
  
            let oldLocation = CLLocationCoordinate2D(latitude: JSON(self.soketData?.latitude as Any).doubleValue, longitude: JSON(self.soketData?.longitude as Any).doubleValue)
            let newLocation = CLLocationCoordinate2D(latitude: JSON(newValue?.latitude as Any).doubleValue, longitude: JSON(newValue?.longitude as Any).doubleValue)
            
            self.source.rotation = oldLocation.getBearing(toPoint: newLocation)
        }
        
        didSet{
            
            //check for lat long Change
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            self.source.position = CLLocationCoordinate2D(latitude: JSON(self.soketData!.latitude as Any).doubleValue, longitude: JSON(self.soketData!.longitude as Any).doubleValue)
            
            
            let camera = GMSCameraPosition.camera(withLatitude: JSON(self.soketData!.latitude as Any).doubleValue, longitude: JSON(self.soketData!.longitude as Any).doubleValue, zoom: 16.0, bearing: 0.0, viewingAngle: 0)
            
            CATransaction.commit()
            
           
//            if let oldData = oldValue, oldData.latitude != self.soketData?.latitude {
                map.camera = camera
//            }
           
            
            // Check For Speed :
            
            if isShowVehicle  {
                if let index = arrOptionList.index(of: keySpeed) , index < arrOptionList.count {
                    self.tblHome.reloadSections(IndexSet(integer: index), with: .automatic)
                }
                
                if let index = arrOptionList.index(of: keyParking) , index < arrOptionList.count {
                    self.tblHome.reloadSections(IndexSet(integer: index), with: .automatic)
                }
            } else {
                self.tblHome.reloadData()
            }
            
        }
    }
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    let path = GMSMutablePath()
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    @objc func updateVehicleTime(){
        source.icon = GFunction.sharedInstance.getStatusWiseImage(type: arrVehicles[selectedVehicleIndex].vehicleType, status: .idel)
    }
    
    @objc func updateVehicleRecord(){
        self.arrVehicles = VehicleDataSource.shared.arrVehicles
        if arrVehicles.isEmpty {
            selectedVehicleIndex = 0
        }
     
    }
    
    func setUser()  {
        
//        switch user {
//        case .Owner:
        arrOptionList = [keyVehicles,keySpeed,keyDocuments,keyHistory,keyIgnition,keyParking,keySOS,keyTrack]
//            self.showBtnDriverWithAnimation(isShow: true)
            btnAlerts.isHidden = false
       
//            break
//        case .Viewer:
//            arrOptionList = [keyVehicles,keySpeed,keyAc]
//            self.showBtnDriverWithAnimation(isShow: false)
//
//            btnAlerts.isHidden = false
//
//            break
//        case .Driver:
//
//            btnAlerts.isHidden = false
//
//            arrOptionList = [keyVehicles,keySOS]
//            self.showBtnDriverWithAnimation(isShow: false)
//            break
//
//        }
        
    }
    
    
    func showBtnDriverWithAnimation(isShow : Bool) {
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseInOut], animations: {
            if isShow {
                self.constBtnDriverTrailing.constant =  15
                
            }else
            {
                self.constBtnDriverTrailing.constant = -self.btnDriver.frame.size.width - 15
                
            }
            
            self.btnDriver.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    func setUpView() {
        
         NotificationCenter.default.addObserver(self, selector: #selector(updateVehicleRecord), name: NSNotification.Name.init(kvehicleUpdateObserver), object: nil)
        
        btnDriver.layer.borderWidth = 2.0
        btnDriver.layer.borderColor = colors.whiteColor.cgColor
        
        btnDriver.setLayout(detail: [layout.kRadiusRatio : 2])
        
        
        
        
       // btnDriver.setImage(#imageLiteral(resourceName: "B.jpg"), for: .normal)
        btnDriver.imageView?.contentMode = .scaleAspectFill
        tblHome.sectionHeaderHeight = UITableViewAutomaticDimension
        tblHome.estimatedSectionHeaderHeight = 85

        source.icon = UIImage.init(named: "CarG")
        source.map = map
        source.appearAnimation = .pop
       

        VehicleDataSource.shared.getVehicleList { (response) in
            if let data = response as? [Vehicle]{
                self.arrVehicles = data
                self.selectedVehicleIndex = 0
            }
         }
        
        setUser()
        
        btnDriver.isHidden = true
        
        btnDriver.addAction(for: .touchUpInside) { [unowned self] in
      
            if self.arrVehicles.count != 0 && !self.isShowVehicle{
                let obj : ShareArrivalVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ShareArrivalVC") as! ShareArrivalVC
                obj.isShareDriver = true
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
 
        btnHelp.addAction(for: .touchUpInside) { [unowned self] in
            DispatchQueue.main.async {
             
             
             
             let alert = UIAlertController(title: nil, message: "Are you sure to send the HELP message?".Localized(), preferredStyle: .alert)
             
             alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                self.getSoSListWs()
             }))
             
             alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
             
             }))
             
             appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
             }
        }
        
        
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Status Bar method
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
   
    //------------------------------------------------------
    
    //MARK:- Action Method
    @IBAction func btnMapStyleClicked(_ sender: Any) {
        self.map.mapType =  self.map.mapType == GMSMapViewType.normal ? GMSMapViewType.satellite : GMSMapViewType.normal
    }
    
    @IBAction func btnWheelClicked(_ sender: Any) {
        isShowLeftMenu = !isShowLeftMenu
    }
    
    @IBAction func btnWheelReportClicked(_ sender: Any) {
        
//        let obj : WheelReportVC = HomeStoryBoard.instantiateViewController(withIdentifier: "WheelReportVC") as! WheelReportVC
//        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnMenuClicked(_ sender: Any) {
        
        displayContentController(content: obj , frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        
        if isShowVehicle {
            isShowVehicle = false
            if let selectcell = tblHome.cellForRow(at: IndexPath(row: self.selectedVehicleIndex, section: 0)) as? HomeVehicleCell{
                selectcell.hideAnimation(table: tblHome)
            }
        }
        
    }
    
    @IBAction func btnNotificationClicked(_ sender: Any) {
        let obj : AlertVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnCallClicked(_ sender: Any) {
        
        let phoneNum = "+919876543210"
        
        if let url = URL(string: "tel://\(phoneNum)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //   map.delegate = self
        setUpView()
        self.constCustomIfoBottom.constant = -2 * self.viewCustomerInfo.frame.size.height // not to show at the start
        self.view.layoutIfNeeded()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
         super.viewWillAppear(animated)
        
    }
    
  
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //update constraint when we get proper frame
        self.constCustomIfoBottom.constant = self.istripStarted ? 0 : -self.viewCustomerInfo.frame.size.height
        lblNotificationCount.layer.cornerRadius = lblNotificationCount.frame.height / 2.0
        lblNotificationCount.clipsToBounds = true
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
        hideContentController(content: obj ,delay: 0.5)
        if isShowVehicle {
            isShowVehicle = false
        }
        isShowRed = false
        tblHome.reloadData()
    }
    
    @objc func isShowRedChange(){
        isShowRed = true
        self.tblHome.reloadData()
    }
    
}

//------------------------------------------------------

//MARK:- Table View Method

extension HomeVC : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrOptionList.count
    }
    
  
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell : HomeHeaderCell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderCell") as! HomeHeaderCell
        cell.imgBg.image = UIImage()
        cell.lblHeader.isHidden = false
        
        
        switch arrOptionList[section] {
        case keyVehicles:
            
            let cell : HomeHeaderVehicleCell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderVehicleCell") as! HomeHeaderVehicleCell
            addTripSetup(cell: cell)
            
            if !isShowVehicle{
                
                cell.btnStartTrip.addAction(for: .touchUpInside) { [unowned self] in
                    
                    if self.istripStarted {
                        
                        let obj : CompleteTripVC = HomeStoryBoard.instantiateViewController(withIdentifier: "CompleteTripVC") as! CompleteTripVC
                        obj.completion = { _ in
                            self.istripStarted = false
                            self.tblHome.reloadData()
                        }
                        self.present(obj, animated: true, completion: {
                            obj.view.backgroundColor = UIColor.clear
                        })
                        
                        return
                        
                    }
                    
                    self.istripStarted = !self.istripStarted
                    self.tblHome.reloadData()
                    
                }
            }
            
            
            cell.lblHeader.text = "\(arrVehicles.count)"
            cell.lblDetail.text = "Vehicle".uppercased()
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                
                self.isShowVehicle = !self.isShowVehicle
                
                if !self.isShowVehicle{
                    if let selectcell = tableView.cellForRow(at: IndexPath(row: self.selectedVehicleIndex, section: 0)) as? HomeVehicleCell , !self.arrVehicles.isEmpty
                    {
                        selectcell.hideAnimation(table: tableView)
                    }else
                    {
                        self.tblHome.reloadData()
                    }
                }else
                {
                    self.tblHome.reloadData()
                }
                
                //
            }
            
            return cell.contentView
            
        case keySpeed:
            
            
            cell.imgBg.image = #imageLiteral(resourceName: "H3Speed")

            if isShowRed {
                cell.lblHeader.textColor = UIColor.red
            }else {
                cell.lblHeader.textColor = colors.whiteColor
            }
            
            
            cell.lblHeader.text = "OVER-SPEED"
                
                //JSON(soketData?.speed_in_km_h as Any).stringValue + " km/h"
            
            cell.lblHeader.isHidden = false
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle && !self.arrVehicles.isEmpty{
                     let obj : AdvanceSettingVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AdvanceSettingVC") as! AdvanceSettingVC
                     self.navigationController?.pushViewController(obj, animated: true)
                }
            }
            
            
            break
            
        case keyAc:
            cell.imgBg.image = #imageLiteral(resourceName: "H3Ac_on")
            cell.lblHeader.text = "AC ON".uppercased()
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle {
                   GFunction.sharedInstance.showUpcommingAlert()
                }
            }
            break
            
        case keyHistory:
            cell.imgBg.image = UIImage.init(named: "h-history")
            cell.lblHeader.text = "History".uppercased()
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle {
                    let obj : WheelReportVC = HomeStoryBoard.instantiateViewController(withIdentifier: "WheelReportVC") as! WheelReportVC
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
            break
        case keyShareLocation:
            cell.imgBg.image = #imageLiteral(resourceName: "H3Share")
            cell.lblHeader.text = "Share".uppercased()
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle {
                    GFunction.sharedInstance.showUpcommingAlert()
                    //Next phase
                    /*let obj : ShareArrivalVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ShareArrivalVC") as! ShareArrivalVC
                    obj.isSharePerson = true
                    self.navigationController?.pushViewController(obj, animated: true)*/
                }
            }
            
            break
        case keyIgnition:
            cell.imgBg.image = #imageLiteral(resourceName: "H3Ignition On").withRenderingMode(.alwaysTemplate)
            cell.lblHeader.text = (JSON(soketData?.ignitionStatus as Any).boolValue ? "KEY-ON" : "KEY-OFF").uppercased()
            cell.imgBg.tintColor = JSON(soketData?.ignitionStatus as Any).boolValue ? .green  : .red
            
            if JSON(soketData?.ignitionStatus as Any).boolValue{
                //08-02
                
            }else{
                if selectedVehicleIndex < arrVehicles.count{
                    source.icon = GFunction.sharedInstance.getStatusWiseImage(type: arrVehicles[selectedVehicleIndex].vehicleType, status: .off)
                }
            }
            
            cell.btnList.addAction(for: .touchUpInside) {
                
            }
            
            break
        case keyTrack:
            cell.imgBg.image = #imageLiteral(resourceName: "H3Track").withRenderingMode(.alwaysTemplate)
            cell.lblHeader.text = "Track".uppercased()
            cell.imgBg.tintColor = VehicleDataSource.shared.isEmergency ? UIColor.colorFromHex(hex: 0xB2EA1C) : colors.whiteColor
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle {
                    
                    let cutentLocationStr = "\(LocationManager.shared.getUserLat()),\(LocationManager.shared.getUserLong())"
                    let destinationLocationStr = "\(JSON(self.soketData?.latitude as Any).stringValue),\(JSON(self.soketData?.longitude as Any).stringValue)"
                    
                    
                    if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
                        //                        let urlString = "http://maps.google.com/?daddr=\(destinationLocation.latitude),\(destinationLocation.longitude)&directionsmode=driving"

                        // use bellow line for specific source location
                        let urlString = "http://maps.google.com/?saddr=\(cutentLocationStr)&daddr=\(destinationLocationStr)&directionsmode=driving"

                        UIApplication.shared.openURL(URL(string: urlString)!)
                    } else {
                        let urlString = "http://maps.apple.com/maps?saddr=\(cutentLocationStr)&daddr=\(destinationLocationStr)&dirflg=d"

                        //                        let urlString = "http://maps.apple.com/maps?daddr=\(destinationLocation.latitude),\(destinationLocation.longitude)&dirflg=d"

                        UIApplication.shared.openURL(URL(string: urlString)!)
                    }
                    
                  /*
                     #error("Update Soket data before use track vc, firebase data is replace with api so replace it before use")
                     let obj : TrackVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TrackVC") as! TrackVC
                    obj.vehiclDetail = self.soketData
                    self.navigationController?.pushViewController(obj, animated: true)*/
                }
            }
            
            
            break
        case keyParent:
            
            cell.imgBg.image = #imageLiteral(resourceName: "H3Arrival")
            cell.lblHeader.text = "Arrival".uppercased()
            
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle {
                    GFunction.sharedInstance.showUpcommingAlert()
                    //Next phase
                   /* let obj : ShareArrivalParentVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ShareArrivalParentVC") as! ShareArrivalParentVC
                    self.navigationController?.pushViewController(obj, animated: true)*/
                }
            }
            
            
            break
            
            //change
            
        case keyDocuments:
            cell.imgBg.image = UIImage.init(named: "documents")?.withRenderingMode(.alwaysTemplate)
            cell.imgBg.tintColor = UIColor.white
            cell.lblHeader.text = "Vehicle ID".uppercased()
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle && !self.arrVehicles.isEmpty {
                    let obj : MyDocumentsVC = BookingStoryBoard.instantiateViewController(withIdentifier: "MyDocumentsVC") as! MyDocumentsVC
                    obj.title = "Vehicle Documents"
                    obj.isVehicleDocument = true
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
            
            break
            
        case keySOS:
            cell.imgBg.image = #imageLiteral(resourceName: "H3Sos")
            cell.lblHeader.text = "HELP NO".uppercased()
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle && !self.arrVehicles.isEmpty {
                    
                    let obj : SOSVC = HomeStoryBoard.instantiateViewController(withIdentifier: "SOSVC") as! SOSVC
                    self.navigationController?.pushViewController(obj, animated: true)
                    
                }
            }
            break
            
        case keyParking:
            cell.imgBg.image = UIImage.init(named: "park-off")
            VehicleDataSource.shared.getEmegencyStatus { [unowned self] (isEmergency) in
                if isEmergency || JSON(VehicleDataSource.shared.selectedVehicle?.isEmergency as Any).boolValue{
                    cell.imgBg.image = UIImage.init(named: "park-onr")
                    cell.imgBg.blink()
//                    if let index = self.arrOptionList.firstIndex(where: { $0 == self.keyParking}) {
                       // self.tblHome.reloadSections(IndexSet(integer: index), with: .none)
//                    }
                }else{
                    cell.imgBg.image = JSON(VehicleDataSource.shared.selectedVehicle?.parking as Any).stringValue != "On" ? UIImage.init(named: "park-off") : UIImage.init(named: "park-ong")
                    cell.imgBg.removeBlink()
                }
            }
            
            cell.lblHeader.text = "PARK-SAFE".uppercased()
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle && !self.arrVehicles.isEmpty {
                    self.parkingWS()
                }
            }
            break
        case keyPreAlert:
            cell.imgBg.image = UIImage.init(named: "preAlert")
            cell.lblHeader.text = keyPreAlert.uppercased()
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                if !self.isShowVehicle && !self.arrVehicles.isEmpty {
                    GFunction.sharedInstance.showUpcommingAlert()
                    //Next phase
                    /*let obj : GeofenceVC = BookingStoryBoard.instantiateViewController(withIdentifier: "GeofenceVC") as! GeofenceVC
                    self.navigationController?.pushViewController(obj, animated: true)*/
                }
            }
     
        default:
            break
        }

        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrOptionList[section] == keyVehicles  && isShowVehicle {
            return arrVehicles.count + 1
        }
        
        return 0
    }
    
    
    func addTripSetup(cell : HomeHeaderVehicleCell)  {
        
        switch user {
        case .Driver:
            cell.btnStartTrip.isHidden = !isShowLeftMenu
            if isShowVehicle
            {
                cell.btnStartTrip.backgroundColor = UIColor.colorFromHex(hex: 0xD1D1D1)
                cell.btnStartTrip.setLayout(detail: [layout.ktitleColor : colors.greyColor])
                
                if istripStarted
                {
                    cell.btnStartTrip.setTitle("End Trip", for: .normal)
                }else
                {
                    cell.btnStartTrip.setTitle("Start Trip", for: .normal)
                }
                
            }else if istripStarted
            {
                cell.btnStartTrip.setTitle("End Trip", for: .normal)
                cell.btnStartTrip.backgroundColor = UIColor.colorFromHex(hex: 0xF93F3F)
                cell.btnStartTrip.setLayout(detail: [layout.ktitleColor : colors.whiteColor])
                
            }else if !isShowVehicle
            {
                cell.btnStartTrip.setTitle("Start Trip", for: .normal)
                cell.btnStartTrip.backgroundColor = UIColor.colorFromHex(hex: 0xE36B2E)
                cell.btnStartTrip.setLayout(detail: [layout.ktitleColor : colors.whiteColor])
                
            }
            
            break
        default:
            cell.btnStartTrip.isHidden = true
            break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell : HomeVehicleCell = tableView.dequeueReusableCell(withIdentifier: "HomeVehicleCell") as! HomeVehicleCell
        
        if indexPath.row == arrVehicles.count {
            let cell : HomeVehicleCell = tableView.dequeueReusableCell(withIdentifier: "HomeAddVehicleCell", for: indexPath) as! HomeVehicleCell
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                let obj : AddVehicleVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AddVehicleVC") as! AddVehicleVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            return cell
        }
        
        let cell : HomeVehicleCell = tableView.dequeueReusableCell(withIdentifier: "HomeVehicleCell", for: indexPath) as! HomeVehicleCell
        cell.btnCheck.isHidden = true
        
        if selectedVehicleIndex == indexPath.row {
            cell.btnCheck.isHidden = false
        }
        
        let data = arrVehicles[indexPath.row]
        cell.lblCarName.text = data.carModel
        cell.lblCarRegNo.text = data.registrationNumber
        cell.imgCar.sd_setImage(with: data.carPhoto.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultCarPlaceholder"))
        
        
        
        cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
            self.selectedVehicleIndex = indexPath.row
//            switch indexPath.row {
//            case 0:
                self.user = .Owner
                self.istripStarted = false
//                break
//            case 1:
//                self.user = .Viewer
//                self.istripStarted = false
//                break
//            case 2:
//                self.user = .Driver
//                break
//            default:
//                break
//            }
            
            
            
            cell.hideAnimation(table: tableView)
            
            self.setUser()
            
            self.isShowVehicle = !self.isShowVehicle
        }
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
//        switch indexPath.row {
//        case 0:
            user = .Owner
            istripStarted = false
//            break
//        case 1:
//            user = .Viewer
//            istripStarted = false
//            break
//        case 2:
//            user = .Driver
//            break
//        default:
//            let obj : AddVehicleVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AddVehicleVC") as! AddVehicleVC
//            self.navigationController?.pushViewController(obj, animated: true)
//            return
//        }
        
        
        selectedVehicleIndex = indexPath.row
        
        if let cell = tblHome.cellForRow(at: indexPath) as? HomeVehicleCell{
            cell.hideAnimation(table: tableView)
        }
        
        setUser()
        
        isShowVehicle = !isShowVehicle
        
        
    }
    
    
    //------------------------------------------------------
    
    //MARK:-  Set fix row height for displaying sqaure
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        //        if arrOptionList[section] == keySpeed {
        //            return UITableViewAutomaticDimension
        //        }
        return UITableViewAutomaticDimension //tableView.frame.size.width
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 {
            //set it for add vehicle
            return tableView.frame.size.width
        }
        return tableView.frame.size.width + 10
    }
    
}



//------------------------------------------------------

//MARK:- HomeVehicle Cell

class HomeVehicleCell : UITableViewCell {
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblCarRegNo: UILabel!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var viewDetails: UIView!
    
    
    override func awakeFromNib() {
        
        if self.reuseIdentifier == "HomeAddVehicleCell" {
            btnList.constraints.filter{ $0.firstAttribute == .width }.first?.constant = kScreenWidth - 10
            btnList.backgroundColor = colors.redColor
            btnList.setLayout(detail: [layout.ktitleColor : colors.whiteColor])
            btnList.setLayout(detail: [layout.kfont : UIFont(name: fonts.montserratMediumFont, size: 12.0 * scaleFactor) as Any])
            return
        }
        
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        lblCarRegNo.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        imgCar.constraints.filter{$0.firstAttribute == .height}.first?.constant = (kScreenWidth *  btnWheelMultiplier)
        visibleanimation()
    }
    
    func visibleanimation()  {
        
        //
        
        if self.reuseIdentifier == "HomeAddVehicleCell" {
            
            return
        }
        
        self.viewWidth.constant = 0
        
        
      
        var frame = viewDetails.frame
        frame.size.width = 0
        viewDetails.frame = frame
        
        //hide
        viewDetails.isHidden = true
        lblCarName.isHidden = true
        lblCarRegNo.isHidden = true
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut , .beginFromCurrentState], animations: {
            var frame = self.viewDetails.frame
            
            frame.size.width = kScreenWidth - (kScreenWidth * btnWheelMultiplier) - 10
            self.viewWidth.constant = kScreenWidth - (kScreenWidth * btnWheelMultiplier) - 10
            
            
            self.viewDetails.frame = frame
            self.viewDetails.isHidden = false
            
        }) { (isCompleted) in
            self.lblCarName.isHidden = false
            self.lblCarRegNo.isHidden = false
        }
        
        
        
        btnCheck.isHidden = true
    }
    
    func hideAnimation(table : UITableView)  {
        
        if self.reuseIdentifier == "HomeAddVehicleCell" {
            return
        }
        
        
        self.viewWidth.constant = kScreenWidth - (kScreenWidth * btnWheelMultiplier) - 10
     
        var frame = viewDetails.frame
        frame.size.width = kScreenWidth - (kScreenWidth * btnWheelMultiplier) - 10
        viewDetails.frame = frame
        
        //hide
        viewDetails.isHidden = false
        lblCarName.isHidden = false
        lblCarRegNo.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseInOut], animations: {
            var frame = self.viewDetails.frame
            frame.size.width = 0
            self.viewWidth.constant = 0
            self.viewDetails.frame = frame
           
            
        }) { (isCompleted) in
            self.lblCarName.isHidden = true
            self.lblCarRegNo.isHidden = true
             self.viewDetails.isHidden = true
            table.reloadData()
            
        }
        
        
        
        btnCheck.isHidden = true
        
        
    }
    
    override func prepareForReuse() {
        visibleanimation()
    }
    
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        // return true for get touch outside of bounds
        return true
    }
    

    
}

