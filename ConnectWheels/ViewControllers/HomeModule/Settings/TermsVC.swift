//
//  TermsVC.swift
//  ConnectWheels
//
//  Created by  on 03/09/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import UIKit

class TermsVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var webUrl : String = ""
    var isFAQ = false
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
        var webView:WKWebView!
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints=false
        webView.backgroundColor = UIColor.clear
        webView.translatesAutoresizingMaskIntoConstraints=false
        webView.scrollView.backgroundColor = UIColor.clear
        //Indicator Setup
        webView.isOpaque = false
        
        
        self.view.addSubview(webView)
        
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.topAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        
        //3.Loading with URL
        let url = webUrl.getUrl()
        let myRequest = URLRequest(url: url)
        webView.load(myRequest)
        
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFAQ {
            self.title = kHelp
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

extension TermsVC: WKNavigationDelegate, WKUIDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Done loading WebView")
        
    }
}

