//
//  EditProfileVC.swift
//  ConnectWheels
//
//  Created by  on 02/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class EditProfileVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtFirstName: FloatingCommon!
    @IBOutlet weak var txtLastName: FloatingCommon!
    @IBOutlet weak var txtEmail: FloatingCommon!
    @IBOutlet weak var txtAddress: FloatingCommon!
    @IBOutlet weak var imgProfile: UIImageView!

    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    var lat : Double = LocationManager.shared.location.coordinate.latitude
    var long : Double = LocationManager.shared.location.coordinate.longitude
    var isImageChanged = false
    var imgUrl = ""
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    @objc func imgPickerOpen(isShowRemove : Bool = false) {
        
        self.view.endEditing(true)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if TARGET_OS_SIMULATOR == 1
            {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    self.imagePicker.sourceType = .photoLibrary
                    OperationQueue.main.addOperation({() -> Void in
                        
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                }
                
                return
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
                
            }
            
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Gallery".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.sourceType = .photoLibrary
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
            }
            else {
                // GFunction.sharedInstance.alert("Gallery is not available")
            }
        }))
        
//        if isShowRemove {
//            actionSheet.addAction(UIAlertAction(title: "Remove Photo".Localized(), style: .default, handler: { (UIAlertAction) in
//                
//                
//                
//            }))
//        }
        
        
        
        actionSheet.addAction(UIAlertAction(title: "CANCEL".Localized(), style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func validate() -> String{
        
        if txtFirstName.text!.trim().isEmpty {
            return kvBlankFirstName
            
        }else if txtLastName.text!.trim().isEmpty {
            return kvBlankLastName
            
        }else if txtEmail.text!.isEmpty && !txtEmail.isHidden{
            return kvBlankEmail
            
        }else if !(GFunction.isValidEmail(text: txtEmail.text!))  && !txtEmail.isHidden{
            return kvValidEmail
            
        }
        
        return String()
    }
    
    func setUpView() {
        
        txtFirstName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        txtLastName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        
        txtAddress.RightPaddingView(image: #imageLiteral(resourceName: "imgLocationGray"), frame: CGRect(x: 0, y: txtAddress.titleHeight(), width: 30, height: txtAddress.frame.size.height - txtAddress.titleHeight()))
        
        
        txtFirstName.text = JSON(currentUser.fname as Any).stringValue
        txtEmail.text = JSON(currentUser.email as Any).stringValue
        txtAddress.text = JSON(currentUser.address as Any).stringValue
        txtLastName.text =  JSON(currentUser.lname as Any).stringValue

        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imgPickerOpen(isShowRemove:)))
        imgProfile.addGestureRecognizer(tapGesture)
        imgProfile.isUserInteractionEnabled = true
        
        imgProfile.sd_setImage(with: currentUser.profile_image?.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultProfilePic"))
        lat = JSON(currentUser.latitude as Any).doubleValue
        long = JSON(currentUser.longitude as Any).doubleValue
    }
    
    
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
        
 
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        
        if isImageChanged
        {
            uploadImage()
            return
        }
        
        editProfileWs()
        
        
    }
    
    @IBAction func btnSelectAddressClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func uploadImage()  {
        GFunction.sharedInstance.addLoader()
        ImageUpload.shared.uploadImage(false, self.imgProfile.image!, "image/jpeg", .user) { (path, lastComponent) in
            
            debugPrint(path!, lastComponent!)
            self.imgUrl = lastComponent!
            self.editProfileWs()
        }
    }
    
    func editProfileWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/edit/
         
         Parameter   : fname, lname, email, address, latitude, longitude
         
         Optional    : profile_image
         
         Comment     : This api will be used for user can edit his profile details.
         
         ==============================
         */
        
        var param : [String : Any] = [:]
        param[kfname] = txtFirstName.text
        param[klname] = txtLastName.text
//        if txtEmail.text != currentUser.email {
            param[kemail] = txtEmail.text
//        }else
//        {
//             param[kemail] = ""
//        }
       
        param[kaddress] = txtAddress.text
        param[klatitude] = lat
        param[klongitude] = long
        
        if !imgUrl.isEmpty {
            param[kprofile_image] = imgUrl
        }
        
        ApiManger.shared.makeRequest(method: .editProfile , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    UserDefault.setValue(response[kData].dictionaryObject, forKey: kUserDetail)
                    UserDefault.synchronize()
                    self.navigationController?.popViewController(animated: true)
                    break
                case .nodata:
                    break
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

}

//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension EditProfileVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) {
            
        }
        
        isImageChanged = true
        self.imgProfile.image = info[UIImagePickerControllerEditedImage] as? UIImage
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true)
        {
            
        }
    }
}

//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension EditProfileVC: GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtAddress.text = place.formattedAddress
        lat = place.coordinate.latitude
        long = place.coordinate.longitude
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
}
