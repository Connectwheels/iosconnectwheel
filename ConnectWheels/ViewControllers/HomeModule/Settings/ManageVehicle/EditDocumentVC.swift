//
//  EditDocumentVC.swift
//  ConnectWheels
//
//  Created by  on 02/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class EditDocumentVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var colDocuments: UICollectionView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [UploadModel] = []
    var imagePicker : UIImagePickerController = UIImagePickerController()
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    
    @objc func imgPickerOpen(isShowRemove : Bool = false) {
        
        self.view.endEditing(true)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if TARGET_OS_SIMULATOR == 1
            {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    self.imagePicker.sourceType = .photoLibrary
                    OperationQueue.main.addOperation({() -> Void in
                        
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                }
                
                return
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
                
            }
            
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Gallery".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.sourceType = .photoLibrary
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
            }
            else {
                // GFunction.sharedInstance.alert("Gallery is not available")
            }
        }))
        
//        if isShowRemove {
//            actionSheet.addAction(UIAlertAction(title: "Remove Photo".Localized(), style: .default, handler: { (UIAlertAction) in
//
//                if let index = self.colDocuments.indexPathsForSelectedItems?.first
//                {
//                    self.arrList[index.row].isDefaultImage = true
//
//                    self.colDocuments.reloadData()
//                }
//
//            }))
//        }
        
        
        
        actionSheet.addAction(UIAlertAction(title: "CANCEL".Localized(), style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func setUpView() {
        
        let detail1 = UploadModel(title: "Registration Photo", image: #imageLiteral(resourceName: "uploadReg") , selectedImage: #imageLiteral(resourceName: "2.jpg"), isDefaultImage: false)
      
        arrList.append(detail1)
        
        let detail2 = UploadModel(title: "National ID Photo", image: #imageLiteral(resourceName: "uploadID"), selectedImage: #imageLiteral(resourceName: "2.jpg"), isDefaultImage: false)
        arrList.append(detail2)
        
        let detail3 = UploadModel(title: "Car Front Photo", image: #imageLiteral(resourceName: "uploadFront"), selectedImage: #imageLiteral(resourceName: "2.jpg"), isDefaultImage: false)
        arrList.append(detail3)
        
        let detail4 = UploadModel(title: "Car Back Photo", image: #imageLiteral(resourceName: "uploadBack"), selectedImage: #imageLiteral(resourceName: "2.jpg"), isDefaultImage: false)
        arrList.append(detail4)
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let parent = self.parent?.parent as? SignUpVC else {
            return
        }
        
        parent.colList.isHidden = false
        parent.selectIndex = SignUpViews.uploadDocuments.rawValue
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension EditDocumentVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) {
            
        }
        
        if let index = colDocuments.indexPathsForSelectedItems?.first
        {
            arrList[index.row].isDefaultImage = false
            arrList[index.row].selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage
            colDocuments.reloadData()
        }
        
        //        imgProfile.image = info[UIImagePickerControllerEditedImage] as? UIImage
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true)
        {
            
        }
    }
}

//------------------------------------------------------

//MARK:- Collection view method

extension EditDocumentVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : UploadDocumentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadDocumentCell", for: indexPath) as! UploadDocumentCell
        let detail = arrList[indexPath.row]
        
        
        cell.lblDesc.text = detail.title
        
        if detail.isDefaultImage {
            cell.imgPhoto.image = detail.image
            cell.lblDesc.isHidden = true
            cell.blackLayer.isHidden = true
        }else
        {
            cell.imgPhoto.image = detail.selectedImage
            cell.lblDesc.isHidden = false
            cell.blackLayer.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        imgPickerOpen(isShowRemove: !arrList[indexPath.row].isDefaultImage)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (kScreenWidth - 100) / 2, height: (kScreenWidth - 100) / 2)
    }
}
