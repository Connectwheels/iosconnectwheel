//
//  AddVehicleVC.swift
//  ConnectWheels
//
//  Created by  on 02/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class AddVehicleVC: TransparentBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet var colList: UICollectionView!
    @IBOutlet var viewToAdd: UIView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var pageViewController : UIPageViewController = UIPageViewController()
    var param : [String : Any] = [:]
    
    var selectIndex : Int = 0
    {
        didSet{
            if selectIndex <= arrViewController.count
            {
                colList.isHidden = false
                colList.reloadData()
            }
        }
    }
    var arrViewController : [UIViewController] = []
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setNextView() {
        
    }
    
    func setPreviousView() {
        
    }
    
    func setUpView() {
        
         colList.allowsMultipleSelection = true
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.backgroundColor = UIColor.clear
        

        let connectGps : ConnectGpsVC = AuthStoryBoard.instantiateViewController(withIdentifier: "ConnectGpsVC") as! ConnectGpsVC
        connectGps.isAddVehicle = true
        arrViewController.append(connectGps)
        
        let vehicleInfo : VehicleInfoVC = AuthStoryBoard.instantiateViewController(withIdentifier: "VehicleInfoVC") as! VehicleInfoVC
        vehicleInfo.isAddVehicle = true
        arrViewController.append(vehicleInfo)
        
//        let uploadDocument : UploadDocumentVC = AuthStoryBoard.instantiateViewController(withIdentifier: "UploadDocumentVC") as! UploadDocumentVC
//        arrViewController.append(uploadDocument)
        
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: viewToAdd.frame.size.width, height: viewToAdd.frame.size.height)
        self.addChildViewController(pageViewController)
        pageViewController.setViewControllers([arrViewController.first!], direction: .forward, animated: false, completion: nil)
        viewToAdd.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
        if TARGET_OS_SIMULATOR == 1 {
            pageViewController.delegate = self
            pageViewController.dataSource = self
        }
        
        colList.constraints.filter{ $0.firstAttribute == .width }.first?.constant = min(kScreenWidth - 80, CGFloat(50 * arrViewController.count))
        
        colList.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnBackTapped(_ sender: Any) {
        
        if selectIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        }else
        {
            selectIndex = selectIndex - 1
            pageViewController.setViewControllers([arrViewController[selectIndex]], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    
}

//------------------------------------------------------

//MARK:- Collection view method

extension AddVehicleVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrViewController.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : signupListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "signupListCell", for: indexPath) as! signupListCell
        if indexPath.item <= selectIndex
        {
            cell.viewLine.backgroundColor = colors.blueColor
        }else
        {
            cell.viewLine.backgroundColor =  colors.greyColor
        }
        
        if indexPath.item == selectIndex
        {
            cell.viewLine.startBlink()
        }else
        {
            cell.viewLine.stopBlink()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 50, height: collectionView.frame.size.height)
    }
    
    
}





//--------------------------------------------------------------------------
//MARK:- Page View Methods

extension AddVehicleVC : UIPageViewControllerDelegate , UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard arrViewController.count > previousIndex else {
            return nil
        }
        
        return arrViewController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = arrViewController.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return arrViewController[nextIndex]
    }
    
    
}

