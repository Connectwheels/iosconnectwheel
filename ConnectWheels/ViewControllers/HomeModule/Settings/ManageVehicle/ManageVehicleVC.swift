//
//  ManageVehicleVC.swift
//  ConnectWheels
//
//  Created by  on 26/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ManageVehicleVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblVehicle: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    var arrVehicles = VehicleDataSource.shared.arrVehicles
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
//        tblVehicle.tableHeaderView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
        tblVehicle.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    @IBAction func btnAddVehicleClicked(_ sender: Any) {
        let obj : AddVehicleVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AddVehicleVC") as! AddVehicleVC
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Services
    
    func deleteVehicleWs(index : Int)  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/delete_vehicle/
         
         Parameter   : vehicle_id
         
         Optional    :
         
         Comment     : This api for user can delete his vehicle details. 
         
         ==============================
         */
        
        var param : [String:Any] = [:]
        param[kvehicle_id] = arrVehicles[index].id
        
        ApiManger.shared.makeRequest(method: .deleteVehicle , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    
                    self.arrVehicles.remove(at: index)
                    VehicleDataSource.shared.arrVehicles.remove(at: index)
                    self.tblVehicle.reloadData()
                    break
                    
                    
                default :
                    break
                }
            }
        }
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension ManageVehicleVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrVehicles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ManageVehicleCell = tableView.dequeueReusableCell(withIdentifier: "ManageVehicleCell") as! ManageVehicleCell
        
        let data = arrVehicles[indexPath.section]
        cell.lblCarName.text = data.carMake
        cell.lblRegNumber.text = data.registrationNumber
        cell.imgCar.sd_setImage(with: data.carPhoto.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultCarPlaceholder"))
        cell.lblVariant.text = data.variant
        cell.lblCarModel.text = data.carModel
       
        cell.btnEdit.addAction(for: .touchUpInside) {
            let obj : VehicleInfoVC = AuthStoryBoard.instantiateViewController(withIdentifier: "VehicleInfoVC") as! VehicleInfoVC
            obj.isEditVehicle = true
            obj.vehicleInfo = self.arrVehicles[indexPath.section]
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
        cell.btnDelete.addAction(for: .touchUpInside) {
            let alert = UIAlertController(title: nil, message: "Are you sure you want to remove this vehicle?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                //self.arrVehicles.remove(at: indexPath.section)
                self.deleteVehicleWs(index: indexPath.section)
                
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
   
       /*
       cell.rightSwipeSettings.transition = .drag
        
        let edit = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "editVehicle"), backgroundColor: colors.blueColor) { (cell) -> Bool in
            let obj : VehicleInfoVC = AuthStoryBoard.instantiateViewController(withIdentifier: "VehicleInfoVC") as! VehicleInfoVC
            obj.isEditVehicle = true
            obj.vehicleInfo = self.arrVehicles[indexPath.section]
            self.navigationController?.pushViewController(obj, animated: true)
            return true
        }
        
        edit.buttonWidth = 80
        
        let delete = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete"), backgroundColor: colors.deleteColor) { (cell) -> Bool in
            
            let alert = UIAlertController(title: nil, message: "Are you sure you want to remove this vehicle?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                //self.arrVehicles.remove(at: indexPath.section)
                self.deleteVehicleWs(index: indexPath.section)
             
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
            return true
        }
        
        delete.buttonWidth = 80
        
        cell.rightButtons = [delete,edit]*/
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let obj : WheelReportVC = HomeStoryBoard.instantiateViewController(withIdentifier: "WheelReportVC") as! WheelReportVC
//        self.navigationController?.pushViewController(obj, animated: true)

    }
    
  
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    
    
    
   
}




//------------------------------------------------------

//MARK:- WheelStatusCell

class ManageVehicleCell : MGSwipeTableCell {
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblCarMake: UILabel!
    @IBOutlet weak var lblCarModel: UILabel!
    @IBOutlet weak var lblVariant: UILabel!
    @IBOutlet weak var lblRegNumber: UILabel!
    @IBOutlet var lblTitles: [UILabel]!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
 
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        lblCarMake.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblCarModel.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblVariant.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblRegNumber.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        
        _ = lblTitles.map({ (label) -> Void in
            label.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.blackColor)
        })
        
        viewBG.dropShadow()
    }
    
}
