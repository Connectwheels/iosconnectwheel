//
//  BusDetailVC.swift
//  ConnectWheels
//
//  Created by  on 27/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class BusDetailVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblVehicle: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [String] = []
    
    let kTrip = "Trip"
    let kDistance = "Distance"
    let kTime = "Time"
    let kAlerts = "Alerts"
    let kDocuments = "Documents"
    let kPerson = "Person"
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        arrList = [kTrip,kDistance,kTime,kAlerts,kDocuments,kPerson]
        tblVehicle.estimatedSectionHeaderHeight = 104
        tblVehicle.sectionHeaderHeight = UITableViewAutomaticDimension
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension BusDetailVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell : ManageVehicleCell = tableView.dequeueReusableCell(withIdentifier: "ManageVehicleCell") as! ManageVehicleCell
        cell.imgCar.image = #imageLiteral(resourceName: "2.jpg")
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : VehicleDetailCell = tableView.dequeueReusableCell(withIdentifier: "VehicleDetailCell") as! VehicleDetailCell
        
        cell.lblTitle.text = arrList[indexPath.row]
        
        switch arrList[indexPath.row] {
        case kTrip:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_trip")
            
            let string = "35"
            let suffix = " Trips"
            
            let attStr = NSMutableAttributedString(string: string)
            
            let appendStr = NSAttributedString(string: suffix, attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor])
            
            attStr.append(appendStr)
            
            cell.lblDetail.attributedText = attStr
            
            break
            
        case kDistance:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_distance")
            
            let string = "845"
            let suffix = " Kms"
            
            let attStr = NSMutableAttributedString(string: string)
            
            let appendStr = NSAttributedString(string: suffix, attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor])
            
            attStr.append(appendStr)
            
            cell.lblDetail.attributedText = attStr
            
            break
        case kTime:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_time")
            
            
            let string = "34:25"
            let suffix = " Min"
            
            let attStr = NSMutableAttributedString(string: string)
            
            let appendStr = NSAttributedString(string: suffix, attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor])
            
            attStr.append(appendStr)
            
            cell.lblDetail.attributedText = attStr
            
            break
        case kAlerts:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_alert")
            
            let string = "10"
            let attStr = NSMutableAttributedString(string: string)
            cell.lblDetail.attributedText = attStr
            
            break
        case kDocuments:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_document")
            
            let string = "Edit"
            let attStr = NSAttributedString(string: string, attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.blueColor])
            cell.lblDetail.attributedText = attStr
            
            break
            
        case kPerson:
            cell.imgIcon.image = #imageLiteral(resourceName: "Person ")
            
            let string = "4"
            let attStr = NSMutableAttributedString(string: string)
            cell.lblDetail.attributedText = attStr
            
            break
        default:
            
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch arrList[indexPath.row] {
        case kTrip:
            let obj : TripListVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TripListVC") as! TripListVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
            
        case kDistance:
            
            break
        case kTime:
           
            
            break
        case kAlerts:
            let obj : AlertVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kDocuments:
            let obj : EditDocumentVC = HomeStoryBoard.instantiateViewController(withIdentifier: "EditDocumentVC") as! EditDocumentVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
            
        case kPerson:
            let obj : ManagePersonVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ManagePersonVC") as! ManagePersonVC
            self.navigationController?.pushViewController(obj, animated: true)
            
            break
        default:
            
            break
        }
    }
}
