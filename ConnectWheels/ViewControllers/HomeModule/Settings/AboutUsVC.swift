//
//  AboutUsVC.swift
//  ConnectWheels
//
//  Created by  on 05/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit
import WebKit

class AboutUsVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var lblTitles: [UILabel]!
    @IBOutlet var lblDetails: [UILabel]!
    @IBOutlet var vwLines: [UIView]!
    @IBOutlet weak var vwBg: UIView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
        lblHeader.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 13.0, color: colors.blackColor)
//        lblHeader.setLineSpacing()
        lblTitles.forEach{ $0.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 13.0, color: colors.blueColor) }
       lblDetails.forEach{ $0.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.blackColor) }
        vwLines.forEach{ $0.backgroundColor = colors.blueColor }
        vwBg.dropShadow()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}


