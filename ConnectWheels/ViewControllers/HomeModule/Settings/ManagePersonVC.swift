//
//  ManagePersonVC.swift
//  ConnectWheels
//
//  Created by  on 01/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ManagePersonVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    var arrPerson : [PersonDetails] = []
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        arrPerson = (0...10).map({ (index) -> PersonDetails in
            let person = PersonDetails()
            person.name = "User name"
            person.phone = "+00 123 123 1234"
            return person
        })
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    @IBAction func btnAddPersonClicked(_ sender: Any) {
        let obj : AddPersonVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AddPersonVC") as! AddPersonVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension ManagePersonVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrPerson.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : DriverCell = tableView.dequeueReusableCell(withIdentifier: "DriverCell") as! DriverCell
        
        cell.rightSwipeSettings.transition = .drag
        
        cell.imgPhoto.image = #imageLiteral(resourceName: "B.jpg")
        
        
        cell.lblUserName.text = arrPerson[indexPath.section].name
        cell.lblNumber.text = arrPerson[indexPath.section].phone
        
        let edit = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "editVehicle"), backgroundColor: colors.blueColor) { (cell) -> Bool in
            let obj : AddPersonVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AddPersonVC") as! AddPersonVC
            obj.isEdit = true
            self.navigationController?.pushViewController(obj, animated: true)
            return true
        }
        
        edit.buttonWidth = 60
        
        let delete = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete"), backgroundColor: colors.deleteColor) { (cell) -> Bool in
            
            
            let alert = UIAlertController(title: nil, message: "Are you sure you want to remove this person?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                self.arrPerson.remove(at: indexPath.row)
                tableView.reloadData()
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
            
            return true
        }
        
        delete.buttonWidth = 60
        
        cell.rightButtons = [delete,edit]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

    }
    
    
    
    
    /*
     @available(iOS 11.0, *)
     func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
     
     let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
     // Call edit action
     
     // Reset state
     success(true)
     })
     
     let editAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
     // Call edit action
     
     // Reset state
     success(true)
     })
     
     editAction.image = #imageLiteral(resourceName: "edit")
     editAction.backgroundColor = colors.blueColor
     
     
     deleteAction.image = #imageLiteral(resourceName: "delete")
     deleteAction.backgroundColor = colors.deleteColor
     return UISwipeActionsConfiguration(actions: [deleteAction,editAction])
     }
     */
    
}

//------------------------------------------------------

//MARK:- WheelStatusCell

class PersonCell : MGSwipeTableCell {
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    
    
    
    override func awakeFromNib() {
        
        lblUserName.textColor = colors.blackColor
        lblUserName.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
        
        lblNumber.textColor = colors.greyColor
        lblNumber.font = UIFont(name: fonts.montserratRegularFont, size: 8.0 * scaleFactor)
        
    }
    
}
