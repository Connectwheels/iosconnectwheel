//
//  AccountDetailVC.swift
//  ConnectWheels
//
//  Created by  on 03/09/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import UIKit

class AccountDetailVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var txtRegNumber: FloatingCommon!
    @IBOutlet weak var txtImeiNo: FloatingCommon!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    
    
    func setUpView() {
        txtRegNumber.text = "+91" + " - " + JSON(currentUser.phone as Any).stringValue
        txtImeiNo.text = JSON(VehicleDataSource.shared.selectedVehicle?.deviceImei as Any).stringValue
        vwBG.dropShadow()
        txtRegNumber.lineColor = colors.blueColor
        txtImeiNo.lineColor = colors.blueColor
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    

    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}
