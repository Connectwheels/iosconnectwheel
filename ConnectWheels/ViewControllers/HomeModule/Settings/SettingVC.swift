//
//  SettingVC.swift
//  ConnectWheels
//
//  Created by  on 26/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit
import StoreKit



let kHelp = "Help & FAQ's"
class SettingVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblSetting: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [String] = []
    
    let kChangePassword = "Change Password"
    let kManageDriver = "Manage Drivers"
    let kManageVehicles = "Manage Vehicles"
    let kAdvanceSettings = "Advance Settings"
    let kUpdateSubscription = "Update Subscription"
    let kTerms = "Terms & Conditions"
    let kPrivacyPolicy = "Privacy Policy"
    let kAccountDetails = "Account Details"
    let kShare = "Share App"
    let kRate = "Rate The App"
    
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        arrList = [kChangePassword, kManageVehicles, kShare, kRate, kAccountDetails, kTerms, kPrivacyPolicy]
        tblSetting.tableFooterView = UIView()
  
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension SettingVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SettingCell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.lblTitle.text = arrList[indexPath.row]
        
        switch arrList[indexPath.row] {
        case kChangePassword:
            cell.imgIcon.image = #imageLiteral(resourceName: "settings_ch_pw")
            break
        case kManageDriver:
           cell.imgIcon.image = #imageLiteral(resourceName: "settings_manage_driver")
            break
        case kManageVehicles:
           cell.imgIcon.image = #imageLiteral(resourceName: "settings_manage_vehicle")
            break
        case kAdvanceSettings:
            cell.imgIcon.image = #imageLiteral(resourceName: "settings_advance_settings")
         break
        case kUpdateSubscription:
            cell.imgIcon.image = #imageLiteral(resourceName: "settings_update_plan")
            break
        case kTerms, kPrivacyPolicy:
            cell.imgIcon.image = #imageLiteral(resourceName: "settings_terms_privacy")
            break
        case kHelp:
            cell.imgIcon.image = #imageLiteral(resourceName: "settings_help_faq")
            break
        case kShare:
            cell.imgIcon.image = #imageLiteral(resourceName: "settings_share_app")
            break
        case kRate:
            cell.imgIcon.image = #imageLiteral(resourceName: "settings_rate_app")
            break
        case kAccountDetails:
            cell.imgIcon.image = UIImage(named: "settings_account")
        default:
           
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch arrList[indexPath.row] {
        case kChangePassword:
            let obj : ChangePasswordVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kManageDriver:
            let obj : ManageDriverVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ManageDriverVC") as! ManageDriverVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kManageVehicles:
            let obj : ManageVehicleVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ManageVehicleVC") as! ManageVehicleVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kAdvanceSettings:
            let obj : AdvanceSettingVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AdvanceSettingVC") as! AdvanceSettingVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kUpdateSubscription:
            
            break
        case kTerms:
            let obj : TermsVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            obj.title = kTermsTitle
            obj.webUrl = kTermsUrl
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kPrivacyPolicy:
            let obj : TermsVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            obj.title = kPolicyTitle
            obj.webUrl = kPrivacyUrl
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kHelp:
            let obj : TermsVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
            obj.title = kHelp
            obj.isFAQ = true
            obj.webUrl = kFaqUrl
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kShare:
            GFunction.sharedInstance.share(vc: self, link: kShareAppMessage)
            break
        case kAccountDetails:
            guard let _ = VehicleDataSource.shared.selectedVehicle else { return }
            let obj : AccountDetailVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AccountDetailVC") as! AccountDetailVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kRate:
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
//                guard let writeReviewURL = URL(string: "https://itunes.apple.com/app/id1166117473?action=write-review")
//                    else { fatalError("Expected a valid URL") }
//                if #available(iOS 10.0, *) {
//                    UIApplication.shared.open(writeReviewURL, options: [:]
//                        , completionHandler: nil)
//                } else {
//                    // Fallback on earlier versions
//                }
            }
            break
        default:
            
            break
        }
        
    }
}

//------------------------------------------------------

//MARK:- WheelStatusCell

class SettingCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    override func awakeFromNib() {
        lblTitle.textColor = colors.blackColor
        lblTitle.font = UIFont(name: fonts.montserratMediumFont, size: 11.0 * scaleFactor)
    }
    
}
