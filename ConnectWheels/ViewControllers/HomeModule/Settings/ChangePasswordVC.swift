//
//  ChangePasswordVC.swift
//  ConnectWheels
//
//  Created by  on 26/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ChangePasswordVC: WhiteNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtOldPassword: FloatingCommon!
    @IBOutlet weak var txtNewPassword: FloatingCommon!
    @IBOutlet weak var txtConfirmPassword: FloatingCommon!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func getAuthentication() -> String {
        
        if txtOldPassword.text!.isEmpty {
            return kvBlankOldPassword
        }
//        else if txtOldPassword.text!.count < 3 {
//            return kvMinLenOldPassword
//        }
        else if txtNewPassword.text!.isEmpty {
            return kvBlankNewPassword
        }else if txtNewPassword.text!.count < 6 {
            return kvNewMinLenPassword
        }else if txtConfirmPassword.text!.isEmpty{
            return kvBlankConfirmPassword
        }else if txtNewPassword.text! != txtConfirmPassword.text! {
            return kvMismatchPassword
        }
        return String()
        
    }
    
    func setUpView() {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnSubmitPasswordAction(_ sender: UIButton) {
        
        let error = getAuthentication()
      
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        
        view.endEditing(true)
        changePasswordWs()
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func changePasswordWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/changepassword/
         
         Parameter   : old_password, new_password
         
         Optional    :
         
         Comment     : This api will be used for user can change his password.
         
         ==============================
         */
     
        var param = [String : Any]()
        param[kold_password] = txtOldPassword.text
        param[knew_password] = txtNewPassword.text
        
        ApiManger.shared.makeRequest(method: .changePassword , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    
                    response[kMessage].stringValue.showAlertsWithAction(action: { (action) in
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    break
                    
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}
