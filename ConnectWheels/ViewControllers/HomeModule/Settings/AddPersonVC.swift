//
//  AddPersonVC.swift
//  ConnectWheels
//
//  Created by  on 01/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit



//------------------------------------------------------

//MARK:- Col Day List

class WeekCell : UICollectionViewCell {
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var viewLine: UIView!
    
    var isShowGreen = false
    {
        didSet{
            if isShowGreen {
                lblDay.textColor = isSelected ? colors.blueColor : colors.greyColor
            }
        }
    }
    
    
    override func awakeFromNib() {
        viewLine.isHidden = true
        viewLine.backgroundColor = colors.blueColor
        
        lblDay.textColor = colors.greyColor
        
        if self.reuseIdentifier == "AdvanceWeekCell"{
             lblDay.font = UIFont(name: fonts.montserratSemiBoldFont, size: 10.0 * scaleFactor)
        }else  if self.reuseIdentifier == "AdvanceRefreshCell" {
            lblDay.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
        }else{
             lblDay.font = UIFont(name: fonts.montserratSemiBoldFont, size: 10.0 * scaleFactor)
        }
        
       
    }
    
    override var isSelected: Bool
        {
        didSet{
            
           if self.reuseIdentifier == "AdvanceRefreshCell" {
                lblDay.textColor = isSelected ? colors.blueColor : colors.greyColor
                viewLine.isHidden = true
            }else{
                lblDay.textColor =  isSelected ? colors.blueColor : colors.greyColor//isShowGreen ? colors.blueColor : colors.greyColor
                viewLine.isHidden = !isSelected
            }
        }
    }
    
}


//------------------------------------------------------

//MARK:- Col Time List

class SetTimeCell : UICollectionViewCell {
    
    @IBOutlet weak var txtStartTime: FloatingTime!
    @IBOutlet weak var txtEndTime: FloatingTime!
    
    @IBOutlet weak var txtStartTime2: FloatingTime!
    @IBOutlet weak var txtEndTime2: FloatingTime!
    @IBOutlet weak var viewSecondTime: UIStackView!
    
    var datePicker = UIDatePicker()
    var datePicker2 = UIDatePicker()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if self.reuseIdentifier == "AdvanceTimeCell"{
            // ned to setup here as height is too small
           let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtStartTime.frame.size.height))
           let imageView = UIImageView(frame: CGRect(x: 0, y: 10, width: 10, height: txtStartTime.frame.size.height - txtStartTime.titleHeight()))
            imageView.image = #imageLiteral(resourceName: "imgTime")
            imageView.contentMode = .scaleAspectFit
            paddingView.addSubview(imageView)
            txtStartTime.rightView = paddingView
            txtStartTime.rightViewMode = UITextFieldViewMode.unlessEditing
            
            let paddingView1 : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtStartTime.frame.size.height))
            let imageView1 = UIImageView(frame: CGRect(x: 0, y: 10, width: 10, height: txtStartTime.frame.size.height - txtStartTime.titleHeight()))
            imageView1.image = #imageLiteral(resourceName: "imgTime")
            imageView1.contentMode = .scaleAspectFit
            paddingView1.addSubview(imageView1)
            txtEndTime.rightView = paddingView1
            txtEndTime.rightViewMode = UITextFieldViewMode.unlessEditing
            
            /*
            let paddingView2 : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtStartTime.frame.size.height))
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: 10, width: 10, height: txtStartTime.frame.size.height - txtStartTime.titleHeight()))
            imageView2.image = #imageLiteral(resourceName: "imgTime")
            imageView2.contentMode = .scaleAspectFit
            paddingView2.addSubview(imageView2)
            txtStartTime2.rightView = paddingView2
            txtStartTime2.rightViewMode = UITextFieldViewMode.always
            
            
            let paddingView3 : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: txtStartTime.frame.size.height))
            let imageView3 = UIImageView(frame: CGRect(x: 0, y: 10, width: 10, height: txtStartTime.frame.size.height - txtStartTime.titleHeight()))
            imageView3.image = #imageLiteral(resourceName: "imgTime")
            imageView3.contentMode = .scaleAspectFit
            paddingView3.addSubview(imageView3)
            txtEndTime2.rightView = paddingView3
            txtEndTime2.rightViewMode = UITextFieldViewMode.always*/
          
        }else
        {
            txtStartTime.RightPaddingView(image:#imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtStartTime.titleHeight(), width: 30, height: txtStartTime.frame.size.height - txtStartTime.titleHeight()))
            txtEndTime.RightPaddingView(image: #imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtEndTime.titleHeight(), width: 30, height: txtEndTime.frame.size.height - txtEndTime.titleHeight()))
            
            if let _ = txtStartTime2
            {
                
                txtStartTime2.RightPaddingView(image:#imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtStartTime.titleHeight(), width: 30, height: txtStartTime.frame.size.height - txtStartTime.titleHeight()))
                txtEndTime2.RightPaddingView(image: #imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtEndTime.titleHeight(), width: 30, height: txtEndTime.frame.size.height - txtEndTime.titleHeight()))
            }
        }
        
//        txtStartTime.textColor = colors.greenColor
//        txtEndTime.textColor = colors.greenColor
//      
//        txtStartTime.textColor = colors.greenColor
//        txtEndTime.textColor = colors.greenColor
    }
    
    func updateWeeKCellColor(collectionView : UICollectionView , indexpath : IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexpath) as? WeekCell
        {
            cell.isShowGreen = true
        }
        
    }
}


class AddPersonVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var colWeekDays: UICollectionView!
    @IBOutlet weak var colTime: UICollectionView!
    @IBOutlet weak var txtName: FloatingCommon!
    @IBOutlet weak var txtMobile: FloatingCommon!
    @IBOutlet weak var txtArea: FloatingCommon!
    @IBOutlet weak var txtDuration: FloatingCommon!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnAdd: ThemeActionButton!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrWeekDays : [WeeKDays] = []
    
    var dateFormatter = DateFormatter()
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    
    var isEdit : Bool = false
    
    var picker = UIPickerView()
    var arrPickrList : [String] = [String]()
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
  
//        if txtName.text!.isEmpty && !txtName.isHidden{
//            return kvBlankName
//        }else
        if txtMobile.text!.isEmpty{
            return kvBlankPhoneNumber
        }else if txtMobile.text!.count < 6 {
            return kvValidPhoneNumber
        }else if txtArea.text!.isEmpty{
            return kvBlankArea
        }else if txtDuration.text!.isEmpty{
            return kvBlankDuration
        }
        return String()
    }
    
    @objc func imgPickerOpen(isShowRemove : Bool = false) {
        
        self.view.endEditing(true)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
 
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if TARGET_OS_SIMULATOR == 1
            {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    self.imagePicker.sourceType = .photoLibrary
                    OperationQueue.main.addOperation({() -> Void in
                        
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                }
                
                return
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
                
            }
            
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Gallery".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.sourceType = .photoLibrary
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
            }
            else {
                // GFunction.sharedInstance.alert("Gallery is not available")
            }
        }))
        
//        if isShowRemove {
//            actionSheet.addAction(UIAlertAction(title: "Remove Photo".Localized(), style: .default, handler: { (UIAlertAction) in
//                
//              
//                
//            }))
//        }
        
        
        
        actionSheet.addAction(UIAlertAction(title: "CANCEL".Localized(), style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func setUpView() {
        
        arrPickrList = (3...59).map({ (index) -> String in
            return "\(index) Mins"
        })
        
        arrPickrList.append("1 Hour")
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: (Locale.preferredLanguages.first)!)
        
        if let weekdays = dateFormatter.veryShortWeekdaySymbols
        {
            arrWeekDays = (0...weekdays.count - 1).map { (index) -> WeeKDays in
                let week = WeeKDays()
                week.day = weekdays[index]
                week.startTime = nil
                week.endTime = nil
                return week
            }
            
        }
        
        txtArea.RightPaddingView(image: #imageLiteral(resourceName: "imgLocationGray"), frame: CGRect(x: 0, y: txtArea.titleHeight(), width: 30, height: txtArea.frame.size.height - txtArea.titleHeight()))
        txtMobile.RightPaddingView(image: #imageLiteral(resourceName: "imgCall"), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
        txtDuration.RightPaddingView(image: #imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtDuration.titleHeight(), width: 30, height: txtDuration.frame.size.height - txtDuration.titleHeight()))
        
        txtMobile.regex = "^[0-9]{0,16}$"
        txtName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        
        colWeekDays.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imgPickerOpen(isShowRemove:)))
        imgProfile.addGestureRecognizer(tapGesture)
        imgProfile.isUserInteractionEnabled = true
        
        picker.delegate = self
        picker.dataSource = self
        txtDuration.inputView = picker
        
        
        if isEdit {
            txtName.text = "John Make"
            txtMobile.text = "+00 123 123 1234"
            txtArea.text = "3599 Washigton Strret Corpus Christi"
            txtDuration.text = "1 Hr"
            self.title = "Edit Person Details"
            btnAdd.setTitle("Update", for: UIControlState())
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnSelectAreaClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNumberCliked(_ sender: Any) {
        GFunction.sharedInstance.openContactVC(from: self) { data in
            if let details = data as? UserContact{
                self.txtMobile.text = details.phoneNumber
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Collection view method

extension AddPersonVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWeekDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == colWeekDays {
            let cell : WeekCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeekCell", for: indexPath) as! WeekCell
            cell.lblDay.text = arrWeekDays[indexPath.row].day.uppercased()
            return cell
        }
        
        let cell : SetTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetTimeCell", for: indexPath) as! SetTimeCell
        
        dateFormatter.dateFormat = "hh:mm a"
        
        cell.txtStartTime.text = arrWeekDays[indexPath.row].startTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].startTime))
        
        cell.txtEndTime.text = arrWeekDays[indexPath.row].endTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].endTime))
        
        cell.txtStartTime.inputView = cell.datePicker
        cell.txtEndTime.inputView = cell.datePicker
        
        cell.datePicker.datePickerMode = .time
        
        cell.txtStartTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("StartTime" , indexPath.row)
            cell.datePicker.minimumDate = nil
            
            if cell.txtStartTime.text!.isEmpty{
                
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                
                if (self.arrWeekDays.filter{ $0.startTime != nil }).isEmpty{
                    self.arrWeekDays = self.arrWeekDays.map({ (day) -> WeeKDays in
                        let dayToModify = day
                        dayToModify.startTime = Date().timeIntervalSince1970
                        dayToModify.endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                        return dayToModify
                    })
                }else{
                    self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                    self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                }
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
      
        cell.txtEndTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("EndTime" , indexPath.row)
            
            
            if cell.txtStartTime.text!.isEmpty && cell.txtEndTime.text!.isEmpty{
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.datePicker.minimumDate = Date().addingTimeInterval(60*60*1)
            }else if !cell.txtStartTime.text!.isEmpty
            {
                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime)
                cell.txtEndTime.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
                cell.datePicker.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
        
        
        
        cell.datePicker.addAction(for: .valueChanged) { [unowned self] in
            
            if cell.txtStartTime.isFirstResponder
            {
                cell.txtStartTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date.addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = cell.datePicker.date.timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.addingTimeInterval(60*60*1).timeIntervalSince1970
            }
            
            if cell.txtEndTime.isFirstResponder
            {
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.timeIntervalSince1970
            }
        }
        
        return cell
        
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colWeekDays {
            colTime.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == colWeekDays {
            return CGSize(width: (kScreenWidth - 80 - 35) / 7, height: collectionView.frame.size.height)
        }
        return CGSize(width: (kScreenWidth - 80), height: collectionView.frame.size.height)
    }
    
    
    
}

//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension AddPersonVC: GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
   
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtArea.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
}


//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension AddPersonVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) {
            
        }
        
        
        self.imgProfile.image = info[UIImagePickerControllerEditedImage] as? UIImage
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true)
        {
            
        }
    }
}

//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension AddPersonVC : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtArea {
            return false
        }
        return true
    }
}

//------------------------------------------------------

//MARK:- PickerView Method

extension AddPersonVC : UIPickerViewDelegate , UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPickrList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrPickrList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtDuration.text = arrPickrList[row]
    }
}
