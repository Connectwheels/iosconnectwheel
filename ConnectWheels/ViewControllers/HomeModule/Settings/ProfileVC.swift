//
//  ProfileVC.swift
//  ConnectWheels
//
//  Created by  on 01/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ProfileVC: TransparentBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var imgProfileBG: UIImageView!
    @IBOutlet weak var imgProfile: imageView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        lblMobile.montserrat(ofType: fonts.montserratMediumFont, withfontSize: 12.0, color: colors.blackColor)
        lblAddress.montserrat(ofType: fonts.montserratMediumFont, withfontSize: 12.0, color: colors.blackColor)
        lblEmail.montserrat(ofType: fonts.montserratMediumFont, withfontSize: 12.0, color: colors.blackColor)
        lblName.montserrat(ofType: fonts.montserratBoldFont, withfontSize: 30.0, color: colors.whiteColor)
 }
    
    func setData()  {
        lblName.text = JSON(currentUser.fname as Any).stringValue + " " + JSON(currentUser.lname as Any).stringValue
        lblEmail.text = JSON(currentUser.email as Any).stringValue
        lblAddress.text = JSON(currentUser.address as Any).stringValue
        lblMobile.text = JSON(currentUser.country_code as Any).stringValue + " " + JSON(currentUser.phone as Any).stringValue
        imgProfile.sd_setImage(with: currentUser.profile_image?.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultProfilePic"))
        imgProfileBG.sd_setImage(with: currentUser.profile_image?.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultProfilePic"))
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}
