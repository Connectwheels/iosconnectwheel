//
//  AddDriverVC.swift
//  ConnectWheels
//
//  Created by  on 01/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class AddDriverVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var colWeekDays: UICollectionView!
    @IBOutlet weak var colTime: UICollectionView!
    @IBOutlet weak var txtName: FloatingCommon!
    @IBOutlet weak var txtMobile: FloatingCommon!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnAdd: ThemeActionButton!
    
    @IBOutlet weak var txtValidity: FloatingCommon!
    @IBOutlet weak var imgLicense: UIImageView!
    @IBOutlet weak var imgNationalID: UIImageView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrWeekDays : [WeeKDays] = []
    
    var dateFormatter = DateFormatter()
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    
    var isEdit : Bool = false
    
    let datPicker = UIDatePicker()
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        
//        if txtName.text!.isEmpty && !txtName.isHidden{
//            return kvBlankName
//        }else
        if txtMobile.text!.isEmpty{
            return kvBlankPhoneNumber
        }else if txtMobile.text!.count < 6 {
            return kvValidPhoneNumber
        }
        return String()
    }
    
    @objc func imgPickerOpen(sender : Any) {
        
        self.view.endEditing(true)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        if sender is UITapGestureRecognizer
        {
            imagePicker.view.tag = 0
            
        }else if sender is UIImageView
        {
            
            if (sender as! UIImageView) == imgLicense
            {
                imagePicker.view.tag = 1
                
            }else if (sender as! UIImageView) == imgNationalID
            {
                imagePicker.view.tag = 2
            }
        }
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if TARGET_OS_SIMULATOR == 1
            {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    self.imagePicker.sourceType = .photoLibrary
                    OperationQueue.main.addOperation({() -> Void in
                        
                        self.present(self.imagePicker, animated: true, completion: nil)
                    })
                }
                
                return
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
                
            }
            
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Gallery".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.sourceType = .photoLibrary
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                })
            }
            else {
                // GFunction.sharedInstance.alert("Gallery is not available")
            }
        }))
        
//        if isShowRemove {
//            actionSheet.addAction(UIAlertAction(title: "Remove Photo".Localized(), style: .default, handler: { (UIAlertAction) in
//                
//                
//                
//            }))
//        }
        
        
        
        actionSheet.addAction(UIAlertAction(title: "CANCEL".Localized(), style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func setUpView() {
        
        txtValidity.inputView = datPicker
        datPicker.datePickerMode = .date
        
        datPicker.minimumDate = Date()
        datPicker.addAction(for: .valueChanged) { [unowned self] in
            self.dateFormatter.dateFormat = "dd/MM/yyyy"
            self.txtValidity.text = self.dateFormatter.string(from: self.datPicker.date)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: (Locale.preferredLanguages.first)!)
        
        if let weekdays = dateFormatter.veryShortWeekdaySymbols
        {
            arrWeekDays = (0...weekdays.count - 1).map { (index) -> WeeKDays in
                let week = WeeKDays()
                week.day = weekdays[index]
                week.startTime = nil
                week.endTime = nil
                return week
            }
            
        }
        
      
        txtMobile.RightPaddingView(image: #imageLiteral(resourceName: "imgCall"), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
   
        txtMobile.regex = "^[0-9]{0,16}$"
        txtName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        
        colWeekDays.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imgPickerOpen(sender:)))
        imgProfile.addGestureRecognizer(tapGesture)
        imgProfile.isUserInteractionEnabled = true
            
      
        
        if isEdit {
            txtName.text = "John Make"
            txtMobile.text = "+00 123 123 1234"
            self.title = "Edit Driver Details"
            btnAdd.setTitle("Update", for: UIControlState())
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func addDriver()  {
        
        /**
         ===========API CALL===========
         
         Method Name : share/add_driver/
         
         Parameter   : vehicle_id, fname, lname, country_code, phone, days
         
         Optional    :
         
         Comment     : This api for vehicle owner can add new driver for trip booking and tracking.
         
         ==============================
         */
        
        
        var param : [String:Any] = [:]
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        param[kfname] = txtName.text
        param[klname] = txtName.text
        param[kcountry_code] = txtMobile.text
        param[kfname] = txtName.text
        param[kfname] = txtName.text
        
        ApiManger.shared.makeRequest(method: .addDriver  , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    
                    break
                    
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnAddClicked(_ sender: Any) {
        
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLicenceImageClicked(_ sender: UIButton) {
       imgPickerOpen(sender: imgLicense)
    }
    
    @IBAction func btnNationalIdClicked(_ sender: UIButton) {
        imgPickerOpen(sender: imgNationalID)
    }
    
    
    @IBAction func btnAddNumberCliked(_ sender: Any) {
        GFunction.sharedInstance.openContactVC(from: self) { data in
            if let details = data as? UserContact{
                self.txtMobile.text = details.phoneNumber
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- Collection view method

extension AddDriverVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWeekDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == colWeekDays {
            let cell : WeekCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeekCell", for: indexPath) as! WeekCell
            cell.lblDay.text = arrWeekDays[indexPath.row].day.uppercased()
            return cell
        }
        
        let cell : SetTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetTimeCell", for: indexPath) as! SetTimeCell
        
        dateFormatter.dateFormat = "hh:mm a"
        
        cell.txtStartTime.text = arrWeekDays[indexPath.row].startTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].startTime))
        
        cell.txtEndTime.text = arrWeekDays[indexPath.row].endTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].endTime))
        
        cell.txtStartTime.inputView = cell.datePicker
        cell.txtEndTime.inputView = cell.datePicker
        
        cell.datePicker.datePickerMode = .time
        
        cell.txtStartTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("StartTime" , indexPath.row)
            cell.datePicker.minimumDate = nil
            
            if cell.txtStartTime.text!.isEmpty{
                
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                
                if (self.arrWeekDays.filter{ $0.startTime != nil }).isEmpty{
                    self.arrWeekDays = self.arrWeekDays.map({ (day) -> WeeKDays in
                        let dayToModify = day
                        dayToModify.startTime = Date().timeIntervalSince1970
                        dayToModify.endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                        return dayToModify
                    })
                }else{
                    self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                    self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                }
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
        
        
        
        cell.txtEndTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("EndTime" , indexPath.row)
            
            
            if cell.txtStartTime.text!.isEmpty && cell.txtEndTime.text!.isEmpty{
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.datePicker.minimumDate = Date().addingTimeInterval(60*60*1)
            }else if !cell.txtStartTime.text!.isEmpty
            {
                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime)
                cell.txtEndTime.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
                cell.datePicker.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
        
        
        
        cell.datePicker.addAction(for: .valueChanged) { [unowned self] in
            
            if cell.txtStartTime.isFirstResponder
            {
                cell.txtStartTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date.addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = cell.datePicker.date.timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.addingTimeInterval(60*60*1).timeIntervalSince1970
            }
            
            if cell.txtEndTime.isFirstResponder
            {
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.timeIntervalSince1970
            }
        }
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == colWeekDays {
            colTime.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == colWeekDays {
            return CGSize(width: (kScreenWidth - 80 - 35) / 7, height: collectionView.frame.size.height)
        }
        return CGSize(width: (kScreenWidth - 80), height: collectionView.frame.size.height)
    }
    
    
    
}




//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension AddDriverVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true) {
            
        }
        
        switch picker.view.tag {
        case 0:
            self.imgProfile.image = info[UIImagePickerControllerEditedImage] as? UIImage
            break
        case 1:
            imgLicense.contentMode = .scaleAspectFill
            self.imgLicense.image = info[UIImagePickerControllerEditedImage] as? UIImage
            break
        case 2:
            imgNationalID.contentMode = .scaleAspectFill
            self.imgNationalID.image = info[UIImagePickerControllerEditedImage] as? UIImage
            break
        default:
            break
        }
        
        
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true)
        {
            
        }
    }
}



