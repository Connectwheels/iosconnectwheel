//
//  ContactUsVC.swift
//  ConnectWheels
//
//  Created by  on 07/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ContactUsVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var btnGPSTrack: UIButton!
    
    @IBOutlet weak var tvComment: IQTextView!
    @IBOutlet weak var lblContactUs: UILabel!
    @IBOutlet weak var vwBG: UIView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        
        if tvComment.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty {
            return "Please enter your message"
            
        }
        
        return String()
    }
    
    func setUpView() {
        
        tvComment.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
        vwBG.dropShadow()
        btnGPSTrack.imageView?.contentMode = .scaleAspectFit
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnGpsTrackClicked(_ sender: Any) {
        let phoneNum = "18001218026"
        
        if let url = URL(string: "tel://\(phoneNum)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        contactUsWs()
        
        
//        self.navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func contactUsWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/contactus/
         
         Parameter   : subject, email, message
         
         Optional    :
         
         Comment     : This api will be used for user can change send his contect us request.
         
         ==============================
         */
        
        var param : [String : Any] = [:]
        param[ksubject] = "Connect Wheels"
        param[kemail] = ""
        param[kmessage] = tvComment.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
     
        
        ApiManger.shared.makeRequest(method: .contactUs , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.lblContactUs.isHidden = false
                   self.tvComment.text = ""
                    break
                    
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}
