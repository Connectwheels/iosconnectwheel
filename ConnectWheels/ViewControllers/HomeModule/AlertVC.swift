//
//  AlertVC.swift
//  ConnectWheels
//
//  Created by  on 03/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit




class AlertVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblAlerts: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
 
    
    
    var arrAlerts : [Alerts] = []
    
    var isCallWS : Bool = false
    var pageNumber : Int = 1
    var vehicleId = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
    var apiParam: [String: Any] = [:]
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
        tblAlerts.tableHeaderView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
        getAlertsWs()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    @IBAction func btnClearAllClicked(_ sender: Any) {
        clearAlertsWs()
    }
    
    //MARK:- Ws
    
    func clearAlertsWs(id  : String = "" , index : Int? = nil)  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/clear_alerts/
         
         Parameter   :
         
         Optional    : alert_id
         
         Comment     : This api for user can clear all alert or any individual alert. 
         
         ==============================
         */
        
        var param : [String : Any] = apiParam
        
        if !id.isEmpty{
            param["alert_id"] = id
        }
        
        param[kvehicle_id] = vehicleId
        
        
        ApiManger.shared.makeRequest(method: .clearAlerts , parameter: param , withLoader : pageNumber == 1) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    if let indexOfAlerts = index{
                        self.arrAlerts.remove(at: indexOfAlerts)
                     }else{
                        self.arrAlerts.removeAll()
                    }
                    self.tblAlerts.reloadData()
                    break
                case .nodata:
                    
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    
    func getAlertsWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_alert_notifications/
         
         Parameter   : vehicle_id, page(default 1)
         
         Optional    :
         
         Comment     : This api for vehicle owner can get his trip counts.
         
         ==============================
         */
        
        var param : [String : Any] = apiParam
        param["page"] = pageNumber
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        param[kDeviceImei] = JSON(VehicleDataSource.shared.selectedVehicle?.deviceImei as Any).stringValue
        
        
        ApiManger.shared.makeRequest(method: .getAlerts , parameter: param , withLoader : pageNumber == 1) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.isCallWS = true
                    let data = JsonList<Alerts>.createModelArray(model: Alerts.self, json: response[kData].arrayValue)
                    self.arrAlerts.append(contentsOf: data)
                    self.tblAlerts.reloadData()
                    
                    break
                case .nodata:
                    
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }else{
                self.isCallWS = true
                if self.pageNumber > 1
                {
                    self.pageNumber -= 1
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension AlertVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAlerts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : AlertDetailCell = tableView.dequeueReusableCell(withIdentifier: "AlertDetailCell") as! AlertDetailCell
        let detail = arrAlerts[indexPath.section]
        cell.lblMessage.text = detail.message
        cell.lblMessage.setLineSpacing()
        cell.lblCarName.text = ""
        cell.lblDate.text = detail.insertdate.convertFormatOnly(currentformat: "yyyy-MM-dd HH:mm:ss", changeformat: "dd/MM/yyyy")//"25/01/2017"
        cell.lblTime.text = detail.insertdate.convertFormatOnly(currentformat: "yyyy-MM-dd HH:mm:ss", changeformat: "hh:mm a")//"10:30 Am"
        
        cell.btnDelete.addAction(for: .touchUpInside) {
            self.clearAlertsWs(id: JSON(detail.id).stringValue, index: indexPath.section)
        }
        
        cell.viewBG.bringSubview(toFront: cell.btnDelete)
        cell.imgIcon.image = UIImage(named: detail.alertType)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj : VehiclesMapVC = HomeStoryBoard.instantiateViewController(withIdentifier: "VehiclesMapVC") as! VehiclesMapVC
        obj.title = "Track Vehicle"
        obj.isTrackLocation = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
}

//------------------------------------------------------

//MARK:- AlertDetailCell

class AlertDetailCell : UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet var lblTitles: [UILabel]!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        
        lblMessage.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.redColor)
        lblDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblTime.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
       
        viewBG.dropShadow()
        
        _ = lblTitles.map({ (label) -> Void in
            label.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.blackColor)
        })
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if hitView == self {
            return nil
        } else {
            return hitView
        }
    }
    
    
    
}

//--------------------------------------------------------------------------
//MARK:- ScrollDelegate

extension AlertVC : UIScrollViewDelegate{
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset: CGFloat = scrollView.contentOffset.y
        let maximumOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height + 20
        if maximumOffset - currentOffset <= 1  {
            if isCallWS  {
                isCallWS = false
                pageNumber += 1
                getAlertsWs()
                
            }
            
        }
    }
    
}
