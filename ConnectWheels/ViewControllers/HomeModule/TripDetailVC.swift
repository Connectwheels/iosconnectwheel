//
//  TripDetailVC.swift
//  ConnectWheels
//
//  Created by  on 03/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class TripDetailVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var lblWhether: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
//    @IBOutlet var viewToAdd: UIView!
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
     @IBOutlet weak var tblAlerts: UITableView!
     @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var constMapHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCarDetails: UIView!
    @IBOutlet weak var lblBreakPoint: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblSpeed: UILabel!
    @IBOutlet weak var lblMaxSpeed: UILabel!
    @IBOutlet weak var lblAcStatus: UILabel!
    @IBOutlet weak var lblAlerts: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblStartPoint: UILabel!
    @IBOutlet weak var lblEndPoint: UILabel!
    @IBOutlet weak var lblAvgSpeed: UILabel!
    
    //car info
    @IBOutlet weak var imgCarPhoto: imageView!
   
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
//    var pageViewController : UIPageViewController = UIPageViewController()
    var arrViewController : [UIViewController] = []
    var arrCarDetail : [CarDetails] = []
    let kNotinRange = "Vehicle not in a range"
    let kMisHappen = "We found some mishappening"
    let kAcOn = "AC ON at ideal for more than 10 minutes"
    let kSharpTurn = "Sharp Turn"
    
    
    
    var bounds = GMSCoordinateBounds()
    var currentIndex = 0
    var currentMarker = GMSMarker()
    var tripDetail : TripList?
    
    var selectIndex : Int = 0
    {
        didSet{
            
        /*    if let _ = btnPrevious {
       
//                if selectIndex == 0
//                {
//                    self.btnPrevious.isHidden = true
//                    self.btnNext.isHidden = false
//
//                }else if selectIndex == arrViewController.count - 1
//                {
//                    self.btnPrevious.isHidden = false
//                    self.btnNext.isHidden = true
//                }else
//                {
                    self.btnPrevious.isHidden = false
                    self.btnNext.isHidden = false
//                }
               
                
            }
            
            var tripNumber = "\(selectIndex + 1)"
            
            switch (tripNumber) {
            case "1" , "21" , "31":
                tripNumber.append("st")
            case "2" , "22":
                tripNumber.append("nd")
            case "3" ,"23":
                tripNumber.append("rd")
            default:
                tripNumber.append("th")
            }
            
            self.title = tripNumber + " Trip"*/
        }
    }
    
    var arrCoordinates : [(lat : Double, long : Double)] = []
    var preLocation : CLLocation =  CLLocation()
    let path = GMSPath.init(fromEncodedPath: "geoaEfyk}P}@s@aBoA}D{CwBcBmB{AiCsBiBuA}@s@c@[eF}DoAaA")
    
    var arrAlerts : [Alerts] = []
    
    var isCallWS : Bool = false
    var pageNumber : Int = 1
    
    
    var apiParam : [String : Any] = [:]
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setData()  {
        lblCarName.text = JSON(tripDetail?.carModel as Any).stringValue
        lblRegNo.text = JSON(tripDetail?.registrationNumber as Any).stringValue
        
        lblDate.text = JSON(tripDetail?.startTrip as Any).stringValue.convertFormatOnly(currentformat: "yyyy-MM-dd HH:mm:ss", changeformat: "dd/MM/yyyy")
        lblAlerts.text = JSON(tripDetail?.alertCounts as Any).stringValue
        lblStartTime.text = JSON(tripDetail?.startTime as Any).stringValue
        lblEndTime.text = JSON(tripDetail?.endTime as Any).stringValue
        
        let givenTime = JSON(tripDetail?.time as Any).intValue
        let hr = Int(givenTime / 60)
        let min = Int(givenTime % 60)
        
        lblTime.text = JSON(hr).stringValue + ":" + JSON(min).stringValue + " Min"
        
        
        lblDistance.text = String.init(format: "%.2f", JSON(tripDetail?.distance as Any).floatValue) + " kmph"
        lblMaxSpeed.text = String.init(format: "%.2f", JSON(tripDetail?.maxSpeed as Any).floatValue) + " kms"
        lblAvgSpeed.text = String.init(format: "%.2f", (JSON(tripDetail?.maxSpeed as Any).doubleValue / 2.0)) + " kms"
        lblStartPoint.text = JSON(tripDetail?.startPoint as Any).stringValue
        lblEndPoint.text = JSON(tripDetail?.endPoint as Any).stringValue
        
        imgCarPhoto.sd_setImage(with: JSON(tripDetail?.carPhoto as Any).stringValue.getUrl(), placeholderImage: UIImage.init(named: "defaultCarPlaceholder"))
       
       self.map.clear()
        
        let path = GMSMutablePath()
        arrCoordinates = JSON(tripDetail?.routes as Any).stringValue.components(separatedBy: ";").filter{ !$0.isEmpty }.map { (latlong) -> (Double,Double) in
            var coordinate = (0.0,0.0)
            let cooordinatedValues = latlong.components(separatedBy: ",")
            if let lat = cooordinatedValues.first{
                coordinate.0 = JSON(lat).doubleValue.rounded(toPlaces: 6)
            }
            if cooordinatedValues.count == 2{
                coordinate.1 = JSON(cooordinatedValues[1]).doubleValue.rounded(toPlaces: 6)
            }
            //            print("Coordinates",coordinate.0,coordinate.1)
            path.add(CLLocationCoordinate2D(latitude: coordinate.0, longitude: coordinate.1))
            return coordinate
        }
        
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.black
        polyline.strokeWidth = 2.0
        polyline.map = self.map
        
        if !arrCoordinates.isEmpty {
            let source = GMSMarker(position: CLLocationCoordinate2D(latitude: arrCoordinates.first!.lat, longitude: arrCoordinates.first!.long))
            source.icon = #imageLiteral(resourceName: "trip_detail_pin_orange")
            source.map = map
            source.appearAnimation = .pop
            bounds = bounds.includingCoordinate(source.position)
        }
        
        if let last = arrCoordinates.last{
            let current = GMSMarker(position: CLLocationCoordinate2D(latitude: last.lat, longitude: last.long))
            current.icon = #imageLiteral(resourceName: "trip_detail_pin_blue")
            current.map = map
            current.appearAnimation = .pop
            bounds = bounds.includingCoordinate(current.position)
        }
        
        bounds = bounds.includingPath(path)
        
        self.map.animate(with: GMSCameraUpdate.fit(self.bounds, withPadding: 30))
    }
    
    func setUpView() {
        
        setData()
        
        constMapHeight.constant = kScreenHeight / 3.5
        
       
        tblAlerts.tableHeaderView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
        
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        lblRegNo.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)

        
//        let input = "28&deg;, 15:33"
        
//        lblWhether.text = input.replacingOccurrences(of: "&deg;", with: "°")
        
        
        
        
        
        
        
       /* let icon = GMSMarker(position: CLLocationCoordinate2D(latitude: arrCoordinates[4].lat, longitude: arrCoordinates[4].long))
        
        let view : BrakePoint = BrakePoint.fromNib()
        view.lblDetails.text = "2 min , Satelite"
        
        
        icon.iconView = view
        icon.map = map
//        icon.isFlat = true
        icon.tracksViewChanges = true
        bounds = bounds.includingCoordinate(icon.position)
        
        lblBreakPoint.text = "1. Stop 2 min in satelite"
        lblBreakPoint.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.redColor)*/
        

        
        
//        arrCarDetail = (0...20).map({ (i) -> CarDetails in
//            let car1 = CarDetails.init(name: "Car\(i)", make : "Mercedes" , model: "AMT GT", variant: "Diesel", regNo: "ABC12345\(i)")
//            return car1
//        })
        
//        _ = (0...arrCarDetail.count - 1).map({ (i) -> Void in
//            let obj : CommonTripDetailVC = HomeStoryBoard.instantiateViewController(withIdentifier: "CommonTripDetailVC") as! CommonTripDetailVC
//            obj.pageIndex = i
//            arrViewController.append(obj)
//        })
        

        
        self.selectIndex = 0
        btnNext.isHidden = true
        btnPrevious.isHidden = true
        
//        btnNext.addAction(for: .touchUpInside) { [unowned self] in
//            if self.selectIndex != self.arrViewController.count - 1
//            {
//                self.selectIndex = self.selectIndex + 1
//
//            }
//        }
//
//        btnPrevious.addAction(for: .touchUpInside) { [unowned self] in
//            if self.selectIndex != 0
//            {
//                self.selectIndex = self.selectIndex - 1
//
//            }
//        }
        
        self.getVehicleTripDetailsWs()
       // self.getAlertsWs()
  
    }
    
    
    func addCar()  {
        currentMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 31.87404, longitude: -94.10471))
        currentMarker.icon = UIImage.init(named: "CarG")
        currentMarker.map = map
        currentMarker.appearAnimation = .pop
        bounds = bounds.includingCoordinate(currentMarker.position)
    }
    
    func removeCar()  {
        currentMarker.map = nil
    }
    
    @objc func changeLocation() {
        
        if currentIndex >= arrCoordinates.count {
            currentIndex = 0
            removeCar()
            if !arrCoordinates.isEmpty{
                self.currentMarker.position = CLLocationCoordinate2D(latitude: arrCoordinates.first!.lat, longitude: arrCoordinates.first!.long)
            }
            let bounds = GMSCoordinateBounds().includingCoordinate(self.currentMarker.position)
            self.map.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30))

            return
        }
        
        
        
        let currentLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: arrCoordinates[currentIndex].lat, longitude: arrCoordinates[currentIndex].long)
        if preLocation.coordinate.latitude != currentLocation.latitude {
            
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            let driverPosition  = currentLocation
            
            
            
            //only get bearing if driver change his location
            //            self.currentMarker.rotation = self.preLocation.coordinate.getBearing(toPoint: LocationManager.shared.getUserLocation().coordinate)
            
            self.currentMarker.position = driverPosition
            
            CATransaction.commit()
            
            
            //for bound the car
            //self.mapView.camera = GMSCameraPosition(target: self.currentMarker.position, zoom: 18, bearing: 0, viewingAngle: 0)
            
            /*
             self.mapView.moveCamera(GMSCameraUpdate.setCamera(GMSCameraPosition(target: LocationManager.shared().currentLocation.coordinate, zoom: 16, bearing: annotationDriver.position.getBearing(toPoint: LocationManager.shared().currentLocation.coordinate), viewingAngle: 0)))
             
             
             */
            
            
            
            self.map.animate(with: GMSCameraUpdate.setCamera(GMSCameraPosition(target: currentLocation, zoom: 16, bearing: self.preLocation.coordinate.getBearing(toPoint: currentLocation), viewingAngle: 0)))
            
            // to stop the car
            
            //            if currentIndex == 4 {
            //                currentIndex += 1
            //                self.preLocation = CLLocation.init(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
            //                self.perform(#selector(changeLocation), with: 0.0, afterDelay: 6.0)
            //                return
            //            }
           
        }
        
        currentIndex += 1
        self.preLocation = CLLocation.init(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
        
        self.perform(#selector(changeLocation), with: 0.0, afterDelay: 2.0)
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnAlertsClicked(_ sender: Any) {
        let obj : AlertVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
        var param = self.apiParam
        param[ktripId] = JSON(tripDetail?.id as Any).stringValue
        param["start_trip"] = JSON(tripDetail?.startTrip as Any).stringValue
        param["end_trip"] = JSON(tripDetail?.endTrip as Any).stringValue
        obj.apiParam = param
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnPlayClicked(_ sender: Any) {
        
        if  currentIndex == 0 {
            addCar()
            currentIndex = 1
            if !arrCoordinates.isEmpty{
                preLocation = CLLocation.init(latitude: arrCoordinates[0].lat, longitude: arrCoordinates[0].long)
                self.currentMarker.position = preLocation.coordinate
                self.perform(#selector(changeLocation), with: 0.0, afterDelay: 2.0)
            }
        }
    }
    
    @IBAction func btnFullScreenMapClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseInOut], animations: {
            
            self.viewCarDetails.isHidden = sender.isSelected
            self.lblBreakPoint.isHidden = !sender.isSelected
            self.scroll.setContentOffset(CGPoint.zero, animated:true)
            if sender.isSelected {
                
                if #available(iOS 11.0, *) {
                    self.constMapHeight.constant = self.view.safeAreaLayoutGuide.layoutFrame.size.height
                } else {
                    
                    var height : CGFloat = 64.0
                    
                    if let heightBar = self.navigationController?.navigationBar.frame.size.height
                    {
                        height = CGFloat(heightBar)
                    }
                    
                    self.constMapHeight.constant = kScreenHeight - height
                }
                
                self.viewCarDetails.alpha =  0.0
                
            }else
            {
                
                self.constMapHeight.constant = kScreenHeight / 3.5
                self.viewCarDetails.alpha =  1.0
            }
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Ws
    
    func getVehicleTripDetailsWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : trip/get_trip_details/
         
         Parameter   : vehicle_id, device_imei, start_trip(2019-07-10 06:45:09),
         end_trip(2019-07-10 07:37:14)
         
         Optional    :
         
         Comment     : This api for get the trip details.
         
         ==============================
         */
        
        var param = self.apiParam
        param[ktripId] = JSON(tripDetail?.id as Any).stringValue
        param["start_trip"] = JSON(tripDetail?.startTrip as Any).stringValue
        param["end_trip"] = JSON(tripDetail?.endTrip as Any).stringValue
        
        ApiManger.shared.makeRequest(method: .getTripDetail , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                   self.tripDetail = TripList.init(fromJson: response[kData])
                   self.setData()
                    break
                    
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    func getAlertsWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_alert_notifications/
         
         Parameter   : vehicle_id, page(default 1)
         
         Optional    :
         
         Comment     : This api for vehicle owner can get his trip counts.
         
         ==============================
         */
        
        var param : [String : Any] = [:]
        param["page"] = pageNumber
        param[kvehicle_id] = JSON(VehicleDataSource.shared.selectedVehicle?.id as Any).stringValue
        param[ktripId] = JSON(tripDetail?.id as Any).stringValue
        
        ApiManger.shared.makeRequest(method: .getAlerts , parameter: param , withLoader : pageNumber == 1) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.isCallWS = true
                    let data = JsonList<Alerts>.createModelArray(model: Alerts.self, json: response[kData].arrayValue)
                    self.arrAlerts.append(contentsOf: data)
                    self.tblAlerts.reloadData()
                    
                    break
                case .nodata:
                    
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }else{
                self.isCallWS = true
                if self.pageNumber > 1
                {
                    self.pageNumber -= 1
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//--------------------------------------------------------------------------
//MARK:- Page View Methods

extension TripDetailVC : UIPageViewControllerDelegate , UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard arrViewController.count > previousIndex else {
            return nil
        }
        
        return arrViewController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = arrViewController.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return arrViewController[nextIndex]
    }
    
    
}



//------------------------------------------------------

//MARK:- Table VIew Method

extension TripDetailVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 0// arrAlerts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : AlertDetailCell = tableView.dequeueReusableCell(withIdentifier: "AlertDetailCell") as! AlertDetailCell
        
        let detail = arrAlerts[indexPath.section]
        cell.lblMessage.text = detail.message
        cell.lblCarName.text = ""
        cell.lblDate.text = detail.insertdate.convertFormatOnly(currentformat: "yyyy-MM-dd HH:mm:ss", changeformat: "dd/MM/yyyy")//"25/01/2017"
        cell.lblTime.text = detail.insertdate.convertFormatOnly(currentformat: "yyyy-MM-dd HH:mm:ss", changeformat: "hh:mm a")//"10:30 Am"
        
//        cell.btnDelete.addAction(for: .touchUpInside) {
//            self.clearAlertsWs(id: JSON(detail.id).stringValue, index: indexPath.section)
//        }
        
        cell.viewBG.bringSubview(toFront: cell.btnDelete)
        cell.imgIcon.image = UIImage(named: detail.alertType)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj : VehiclesMapVC = HomeStoryBoard.instantiateViewController(withIdentifier: "VehiclesMapVC") as! VehiclesMapVC
        obj.title = "Track Vehicle"
        obj.isTrackLocation = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
}


//--------------------------------------------------------------------------
//MARK:- ScrollDelegate

extension TripDetailVC : UIScrollViewDelegate{
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset: CGFloat = scrollView.contentOffset.y
        let maximumOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height + 20
        if maximumOffset - currentOffset <= 1  {
            if isCallWS  {
                isCallWS = false
                pageNumber += 1
                //getAlertsWs()
                
            }
            
        }
    }
    
}
