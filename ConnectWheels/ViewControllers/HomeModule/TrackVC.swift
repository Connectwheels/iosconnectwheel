//
//  TrackVC.swift
//  ConnectWheels
//
//  Created by  on 22/03/19.
//  Copyright © 2019 Connect Wheels. All rights reserved.
//

import UIKit

class TrackVC: BlueNavigationBar {
    
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet var mapView: GMSMapView!
    
   
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var annotationDestination   = GMSMarker()
    var mapManager = MapManager()
    var vehiclDetail : SoketData?
    
    var path  : GMSPath?
    var traveledDistance    : Double = Double()
    var currentLocation     : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    var distanceTimer       : Timer = Timer()
    
    
    var currentMarker = GMSMarker()
    var polyLine = GMSPolyline()
    
    var ref: DocumentReference? = nil
    var listener : ListenerRegistration?
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    @objc func distanceCalculation() {
        
        // keep this if smooth navigation is asked
        
        
        
        if currentLocation.latitude != 0.0 && currentLocation.longitude != 0.0 {
            let destinationLocation = CLLocationCoordinate2D(latitude: JSON(vehiclDetail?.latitude as Any).doubleValue, longitude: JSON(vehiclDetail?.longitude as Any).doubleValue)
            
            if let _ = self.path{
                
                if !(GMSGeometryIsLocationOnPathTolerance(currentLocation, self.path!, true, 100)) || !(GMSGeometryIsLocationOnPathTolerance(destinationLocation, self.path!, true, 100)){
                    
                    drawPath()
                }
            }
        }
        
        
        
    }
    
    
    
    func setUpView() {
        
        currentLocation = LocationManager.shared.getUserLocation().coordinate
        distanceTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.distanceCalculation), userInfo: nil, repeats: true)
    
        drawPath()
       
        let db = Firestore.firestore()
        
        if let id = VehicleDataSource.shared.selectedVehicle?.qrCode{
            listener?.remove()
            listener = db.collection("vehicles").document(id)
                .addSnapshotListener { documentSnapshot, error in
                    guard let document = documentSnapshot else {
                        print("Error fetching document: \(error!)")
                        return
                    }
                    // let source = document.metadata.hasPendingWrites ? "Local" : "Server"
                    
                    do {
                        let dict = try JSON(document.data() as Any).rawData()
                        //self.vehiclDetail = try JSONDecoder().decode(SoketData.self, from: dict)
                        
                    }catch let soketError{
                        debugPrint("Soket Data Error : " , soketError.localizedDescription)
                        
                    }
            }
        }
    }


    func drawPath()  {
        
        currentLocation = LocationManager.shared.getUserLocation().coordinate
        
        currentMarker = GMSMarker()
        currentMarker.position = currentLocation
        currentMarker.icon = #imageLiteral(resourceName: "trip_detail_pin_orange")
        currentMarker.map = self.mapView
        
        let destinationLocation = CLLocationCoordinate2D(latitude: JSON(vehiclDetail?.latitude as Any).doubleValue, longitude: JSON(vehiclDetail?.longitude as Any).doubleValue)
        annotationDestination = GMSMarker()
        annotationDestination.position = destinationLocation
        annotationDestination.icon = #imageLiteral(resourceName: "trip_detail_pin_blue")
        annotationDestination.map = self.mapView
        
        let cutentLocationStr = "\(currentLocation.latitude),\(currentLocation.longitude)"
        let destinationLocationStr = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
        
        mapManager.getDirectionsUsingGoogleForGoogleMap(origin: cutentLocationStr, destination: destinationLocationStr) { (path, directionInfo, error) in
            if let _ = path , error == nil{
                self.path = path?.path
                self.polyLine.map = nil
                self.polyLine = path!
                self.polyLine.map = self.mapView
                self.polyLine.strokeColor = UIColor.black
                self.polyLine.strokeWidth = 2.0
                
                let camera = GMSCameraPosition.camera(withTarget: self.currentLocation, zoom: 16.0)
                self.mapView.camera = camera
                
                var bounds = GMSCoordinateBounds()
                
                bounds = bounds.includingCoordinate(self.currentMarker.position)
                bounds = bounds.includingCoordinate(self.annotationDestination.position)
                let update = GMSCameraUpdate.fit(bounds, withPadding: 60)
                self.mapView.animate(with: update)
            }
        }
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
   
    
   
    
    //------------------------------------------------------
    
    //MARK:- Ws
    
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        LocationManager.shared.getLocation()
        LocationManager.shared.getLocationUpdate { (location) in
            if let loc = location as? CLLocation{
                self.currentLocation = loc.coordinate
                self.currentMarker.position = self.currentLocation
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LocationManager.shared.clearUpdates()
        distanceTimer.invalidate()
        listener?.remove()
    }
    
    
}
