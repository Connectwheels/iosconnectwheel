//
//  WheelReportVC.swift
//  ConnectWheels
//
//  Created by  on 02/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit



class WheelReportVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet var viewToAdd: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var txtStartTime: UITextField!
    @IBOutlet weak var txtEndTime: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var imgCar: imageView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
     var pageViewController : UIPageViewController = UIPageViewController()
     var arrViewController : [UIViewController] = []
    var arrCarDetail : [Vehicle] = []
     var dateFormatter = DateFormatter()
    var timeDetail : WeeKDays = WeeKDays()
    var datePicker = UIDatePicker()
    
    var selectIndex : Int = 0
    {
        didSet{
//            if selectIndex == 0
//            {
//                btnPrevious.isHidden = true
//                btnNext.isHidden = false
////
//            }else if selectIndex == arrViewController.count - 1
//            {
//                btnPrevious.isHidden = false
//                btnNext.isHidden = true
//            }else
//            {
//                btnPrevious.isHidden = false
//                btnNext.isHidden = false
//            }
        
            btnPrevious.isHidden = true
            btnNext.isHidden = true
            lblCarName.text = arrCarDetail[selectIndex].carModel
            lblRegNo.text = arrCarDetail[selectIndex].registrationNumber
            imgCar.sd_setImage(with: arrCarDetail[selectIndex].carPhoto.getUrl(), placeholderImage: UIImage.init(named: "defaultCarPlaceholder"))
        }
    }
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
        txtStartTime.setValue(colors.whiteColor, forKeyPath: "_placeholderLabel.textColor")
        txtEndTime.setValue(colors.whiteColor, forKeyPath: "_placeholderLabel.textColor")
        
        txtStartTime.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
        txtEndTime.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
        
        txtStartTime.textColor = colors.whiteColor
        txtEndTime.textColor = colors.whiteColor
        
        dateFormatter.dateFormat = "dd MMM yyyy"
        
       // txtStartTime.text = timeDetail.startTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: timeDetail.startTime))
        
       // txtEndTime.text = timeDetail.endTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: timeDetail.endTime))
        
        txtStartTime.inputView = datePicker
        txtEndTime.inputView = datePicker
        
        txtStartTime.delegate = self
        txtEndTime.delegate = self
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        
        
        txtStartTime.addAction(for: .editingDidBegin) { [unowned self] in
          self.datePicker.minimumDate = nil
        }
        
        
        
        txtEndTime.addAction(for: .editingDidBegin) { [unowned self] in
            if let _ = self.timeDetail.startTime
            {
                self.datePicker.minimumDate = Date(timeIntervalSince1970: self.timeDetail.startTime)
            }
        }
        
        
        txtStartTime.addAction(for: .editingDidEnd) { [unowned self] in
            
            if self.txtStartTime.text!.isEmpty || self.txtEndTime.text!.isEmpty{
                return
            }
            
            var param : [String : Any] = [:]
            param[kstartDate] = self.txtStartTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
            param[kendDate] = self.txtEndTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
            (self.arrViewController[self.selectIndex] as? CommonReportVC)?.getVehicleTripWs(paramToPass: param)
        }
        
        txtEndTime.addAction(for: .editingDidEnd) { [unowned self] in
            
            if self.txtStartTime.text!.isEmpty || self.txtEndTime.text!.isEmpty{
                return
            }
            
            var param : [String : Any] = [:]
            param[kstartDate] = self.txtStartTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
            param[kendDate] = self.txtEndTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
            (self.arrViewController[self.selectIndex] as? CommonReportVC)?.getVehicleTripWs(paramToPass: param)
        }
        
        datePicker.addAction(for: .valueChanged) { [unowned self] in
            
            if self.txtStartTime.isFirstResponder
            {
                self.txtStartTime.text = self.dateFormatter.string(from: self.datePicker.date)
                self.timeDetail.startTime = self.datePicker.date.timeIntervalSince1970
                
            }
            
            if self.txtEndTime.isFirstResponder
            {
                self.txtEndTime.text = self.dateFormatter.string(from: self.datePicker.date)
                self.timeDetail.endTime = self.datePicker.date.timeIntervalSince1970
            }
        }
        
        self.txtStartTime.text = self.dateFormatter.string(from: Date())
        self.timeDetail.startTime = Date().timeIntervalSince1970
        self.txtEndTime.text = self.dateFormatter.string(from: Date())
        self.timeDetail.endTime = Date().timeIntervalSince1970
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.backgroundColor = UIColor.clear
        
        arrCarDetail = VehicleDataSource.shared.arrVehicles

        _ = (0...arrCarDetail.count - 1).map({ (i) -> Void in
            let obj : CommonReportVC = HomeStoryBoard.instantiateViewController(withIdentifier: "CommonReportVC") as! CommonReportVC
            obj.pageIndex = i
            arrViewController.append(obj)
        })
        
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: viewToAdd.frame.size.width, height: viewToAdd.frame.size.height)
        self.addChildViewController(pageViewController)
        pageViewController.setViewControllers([arrViewController.first!], direction: .forward, animated: false, completion: nil)
        viewToAdd.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
//        if TARGET_OS_SIMULATOR == 1 {
//            pageViewController.delegate = self
//            pageViewController.dataSource = self
//        }
        
        self.selectIndex = 0
        
        btnNext.addAction(for: .touchUpInside) { [unowned self] in
            if self.selectIndex != self.arrViewController.count - 1
            {
                self.selectIndex = self.selectIndex + 1
                self.pageViewController.setViewControllers([self.arrViewController[self.selectIndex]], direction: .forward, animated: true, completion: nil)
            }
        }
        
        btnPrevious.addAction(for: .touchUpInside) { [unowned self] in
            if self.selectIndex != 0
            {
                self.selectIndex = self.selectIndex - 1
                self.pageViewController.setViewControllers([self.arrViewController[self.selectIndex]], direction: .reverse, animated: true, completion: nil)
            }
        }
        
        btnSearch.addAction(for: .touchUpInside) { [unowned self] in
            self.view.endEditing(true)
        }
        
        
    
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnGetTipDetailsClicked(_ sender: Any) {
        var param : [String : Any] = [:]
        param[kstartDate] = txtStartTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
        param[kstartDate] = txtStartTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
        (arrViewController[selectIndex] as? CommonReportVC)?.getVehicleTripWs(paramToPass: param)
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//--------------------------------------------------------------------------
//MARK:- Page View Methods

extension WheelReportVC : UIPageViewControllerDelegate , UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard arrViewController.count > previousIndex else {
            return nil
        }
        
        return arrViewController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = arrViewController.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return arrViewController[nextIndex]
    }
    
    
}

extension WheelReportVC : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == txtStartTime {
            txtEndTime.text = ""
        }
        return true
    }
}
