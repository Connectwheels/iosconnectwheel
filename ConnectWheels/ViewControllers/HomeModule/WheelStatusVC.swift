//
//  WheelStatusVC.swift
//  ConnectWheels
//
//  Created by  on 26/04/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

enum vehicleStatus : String
{
    case keyNotInRange = "Not In Range"
    //  let keyEngineOff = "Engine Off"
    case keyMoving = "Moving"
    case keyStop = "Stop"
    case keyNotMoving = "Idle wih Engine ON"
    
    var key : String{
        switch self{
            
        case .keyNotInRange:
            return "Notinrange"
        case .keyMoving:
            return "Moving"
        case .keyStop:
            return "Stop"
        case .keyNotMoving:
            return "Idle"
        }
    }
}


class WheelStatusVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblVehicle: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [vehicleStatus] = []
    
    let keyNotInRange = "Not In Range"
    //  let keyEngineOff = "Engine Off"
    let keyMoving = "Moving"
    let keyStop = "Stop"
    let keyNotMoving = "Idle wih Engine ON"
    
    var vehicleStatusCount : VehicleStatusCount = VehicleStatusCount(fromJson: JSON.null)
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
//        arrList = [keyNotInRange,keyEngineOff,keyMoving,keyStop,keyNotMoving]
          arrList = [vehicleStatus.keyStop,vehicleStatus.keyNotMoving,vehicleStatus.keyMoving,vehicleStatus.keyNotInRange]
        getVehicleStatusWs()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    //------------------------------------------------------
    
    //MARK:- Ws
    
    func getVehicleStatusWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : vehicle/get_state_list/
         
         Parameter   :
         
         Optional    :
         
         Comment     : This api for user can his state list list.
         
         ==============================
         */

        
        ApiManger.shared.makeRequest(method: .getStatusList , parameter: [:]) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.vehicleStatusCount = VehicleStatusCount.init(fromJson: response[kData])
                    self.tblVehicle.reloadData()
                     break
                    
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension WheelStatusVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : WheelStatusCell = tableView.dequeueReusableCell(withIdentifier: "WheelStatusCell") as! WheelStatusCell
        cell.btnWheelStatus.setBackgroundImage(#imageLiteral(resourceName: "movingWheel").withRenderingMode(.alwaysTemplate), for: .normal)
        cell.lblTitle.text = arrList[indexPath.row].rawValue
        
        switch arrList[indexPath.row] {
        case .keyNotInRange:
            cell.btnWheelStatus.setTitle(JSON(vehicleStatusCount.notinrangeCounts).stringValue, for: .normal)
            
            cell.btnWheelStatus.tintColor = colors.greyColor // grey
            break
//        case keyEngineOff:
//            cell.btnWheelStatus.setTitle("20", for: .normal)

//            cell.btnWheelStatus.tintColor = UIColor.colorFromHex(hex: 0x27A2DB) // blue
//            break
        case .keyMoving:
            cell.btnWheelStatus.setTitle(JSON(vehicleStatusCount.movingCounts).stringValue, for: .normal)
            cell.btnWheelStatus.tintColor = UIColor.colorFromHex(hex: 0x44A973) // green
            
            break
        case .keyStop:
            cell.btnWheelStatus.setTitle(JSON(vehicleStatusCount.stopCounts).stringValue, for: .normal)
            cell.btnWheelStatus.tintColor = UIColor.colorFromHex(hex: 0xEE3840) //red
            break
        case .keyNotMoving:
            cell.btnWheelStatus.setTitle(JSON(vehicleStatusCount.idleCounts).stringValue, for: .normal)
            cell.btnWheelStatus.tintColor = UIColor.colorFromHex(hex: 0xEEBD38) //yellow
            break
       
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj : VehiclesMapVC = HomeStoryBoard.instantiateViewController(withIdentifier: "VehiclesMapVC") as! VehiclesMapVC
        obj.vehicledetails = arrList[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//------------------------------------------------------

//MARK:- WheelStatusCell

class WheelStatusCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnWheelStatus: UIButton!
    
    override func awakeFromNib() {
        btnWheelStatus.setLayout(detail: [layout.kfont : UIFont(name: fonts.montserratSemiBoldFont, size: 10.0 * scaleFactor) as Any] )
        lblTitle.textColor = colors.blackColor
        lblTitle.font = UIFont(name: fonts.montserratMediumFont, size: 12.0 * scaleFactor)
    }
    
}
