//
//  ImmobilizeListVC.swift
//  ConnectWheels
//
//  Created by  on 28/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ImmobilizeListVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblVehicle: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrPerson : [PersonDetails] = []
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        arrPerson = (0...4).map({ (index) -> PersonDetails in
            let person = PersonDetails()
            person.name = "Vehicle\(index)"
            person.phone = "+00 123 123 1234"
            return person
        })
        
        tblVehicle.tableHeaderView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    @IBAction func btnAddVehicleClicked(_ sender: Any) {
        let obj : AddVehicleVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AddVehicleVC") as! AddVehicleVC
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension ImmobilizeListVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrPerson.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ManageVehicleCell = tableView.dequeueReusableCell(withIdentifier: "ManageVehicleCell") as! ManageVehicleCell
        
        cell.imgCar.image = #imageLiteral(resourceName: "2.jpg")
        
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: "Are you sure you want to immobilize this vehicle?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                self.navigationController?.popViewController(animated: true)
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    
    
    
    
}






