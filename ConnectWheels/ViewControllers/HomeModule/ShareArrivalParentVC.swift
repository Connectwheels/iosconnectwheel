//
//  ShareArrivalParentVC.swift
//  ConnectWheels
//
//  Created by  on 19/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class ShareArrivalParentVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var colWeekDays: UICollectionView!
    @IBOutlet weak var colTime: UICollectionView!
    @IBOutlet weak var colDuration : UICollectionView!
    @IBOutlet weak var txtName: FloatingCommon!
    @IBOutlet weak var txtMobile: FloatingCommon!
    @IBOutlet weak var txtArea: FloatingCommon!
    @IBOutlet weak var txtDuration: FloatingCommon!
    
    @IBOutlet weak var btnAdd: ThemeActionButton!
    @IBOutlet weak var viewArea: UIView!
    
    
    @IBOutlet weak var lblParents: Bold!
    @IBOutlet weak var lblAddPerson: Bold!
    
  //  @IBOutlet weak var lblMins: UILabel!
   // @IBOutlet weak var viewDuration: UIView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrWeekDays : [WeeKDays] = []
    
    var arrPerson : [PersonDetails] = []
    
    var dateFormatter = DateFormatter()
    
    var imagePicker : UIImagePickerController = UIImagePickerController()

    
    
    var arrPickrList : [String] = [String]()
    
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        
//        if txtName.text!.isEmpty && !txtName.isHidden{
//            return kvBlankName
//        }else
        if txtMobile.text!.isEmpty{
            return kvBlankPhoneNumber
        }else if txtMobile.text!.count < 6 {
            return kvValidPhoneNumber
        }else if txtArea.text!.isEmpty && !viewArea.isHidden{
            return kvBlankArea
        }

        return String()
    }
    
    
    
    func setUpView() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: (Locale.preferredLanguages.first)!)
        
        if let weekdays = dateFormatter.veryShortWeekdaySymbols
        {
            arrWeekDays = (0...weekdays.count - 1).map { (index) -> WeeKDays in
                let week = WeeKDays()
                week.day = weekdays[index]
                week.startTime = nil
                week.endTime = nil
                return week
            }
            
        }
        
        arrPickrList = ["2" , "5" ,"10" ,"15" ,"30"]
        
        
        
        arrPerson = (0...10).map({ (index) -> PersonDetails in
            let person = PersonDetails()
            person.name = "User name"
            person.phone = "+00 123 123 1234"
            return person
        })
        
        txtArea.RightPaddingView(image: #imageLiteral(resourceName: "imgLocationGray"), frame: CGRect(x: 0, y: txtArea.titleHeight(), width: 30, height: txtArea.frame.size.height - txtArea.titleHeight()))
        txtMobile.RightPaddingView(image: #imageLiteral(resourceName: "imgCall"), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
        //        txtDuration.RightPaddingView(image: #imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtDuration.titleHeight(), width: 30, height: txtDuration.frame.size.height - txtDuration.titleHeight()))
        
        txtMobile.regex = "^[0-9]{0,16}$"
        txtName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        
        colWeekDays.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
        colDuration.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .left)
    
       
        colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = txtName.frame.size.height * 2
   
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnSelectAreaClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        
        let error = validate()
        if error != String() {
            GFunction.ShowAlert(message: error)
            return
        }
        
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddNumberCliked(_ sender: Any) {
        GFunction.sharedInstance.openContactVC(from: self) { data in
            if let details = data as? UserContact{
                self.txtMobile.text = details.phoneNumber
            }
        }
    }
  
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        txtDuration.lineColor = UIColor.clear
        txtDuration.selectedLineColor = UIColor.clear
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
      colTime.constraints.filter{ $0.firstAttribute == .height }.first?.constant = txtName.frame.size.height * 2 + 50
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension ShareArrivalParentVC: GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtArea.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
}

//------------------------------------------------------

//MARK:- Collection view method

extension ShareArrivalParentVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == colDuration {
            return arrPickrList.count
        }
        return arrWeekDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == colDuration {
            let cell : WeekCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeekCell", for: indexPath) as! WeekCell
            cell.lblDay.text = arrPickrList[indexPath.row].uppercased() + " Min"
            return cell
        }
        
        
        if collectionView == colWeekDays {
            let cell : WeekCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeekCell", for: indexPath) as! WeekCell
            cell.lblDay.text = arrWeekDays[indexPath.row].day.uppercased()
            return cell
        }
        
        let cell : SetTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetTimeCell", for: indexPath) as! SetTimeCell
        
        dateFormatter.dateFormat = "hh:mm a"
        
        cell.txtStartTime.text = arrWeekDays[indexPath.row].startTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].startTime))
        
        cell.txtEndTime.text = arrWeekDays[indexPath.row].endTime == nil ? "" : dateFormatter.string(from: Date(timeIntervalSince1970: arrWeekDays[indexPath.row].endTime))
        
        cell.txtStartTime.inputView = cell.datePicker
        cell.txtEndTime.inputView = cell.datePicker
        
        cell.txtStartTime2.inputView = cell.datePicker2
        cell.txtEndTime2.inputView = cell.datePicker2
        
        cell.datePicker.datePickerMode = .time
        cell.datePicker2.datePickerMode = .time
        
        
        cell.txtEndTime2.isHidden = false
        cell.txtStartTime2.isHidden = false
        cell.viewSecondTime.isHidden = false
        
        
        
        cell.txtStartTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("StartTime" , indexPath.row)
            cell.datePicker.minimumDate = nil
            
            if cell.txtStartTime.text!.isEmpty{
                
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                
                if (self.arrWeekDays.filter{ $0.startTime != nil }).isEmpty{
                    self.arrWeekDays = self.arrWeekDays.map({ (day) -> WeeKDays in
                        let dayToModify = day
                        dayToModify.startTime = Date().timeIntervalSince1970
                        dayToModify.endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                        return dayToModify
                    })
                }else{
                    self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                    self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                }
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
        }
        
        
        cell.txtStartTime2.addAction(for: .editingDidBegin) { [unowned self] in
            print("StartTime" , indexPath.row)
            cell.datePicker2.minimumDate = nil
            
            if cell.txtStartTime2.text!.isEmpty{
                cell.txtStartTime2.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime2.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime2 = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime2 = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
                
                
            }
        }
        
        
        
        cell.txtEndTime.addAction(for: .editingDidBegin) { [unowned self] in
            print("EndTime" , indexPath.row)
            
            
            if cell.txtStartTime.text!.isEmpty && cell.txtEndTime.text!.isEmpty{
                cell.txtStartTime.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.datePicker.minimumDate = Date().addingTimeInterval(60*60*1)
            }else if !cell.txtStartTime.text!.isEmpty
            {
                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime)
                cell.txtEndTime.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
                cell.datePicker.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
            
        }
        
        cell.txtEndTime2.addAction(for: .editingDidBegin) { [unowned self] in
            print("EndTime" , indexPath.row)
            
            
            if cell.txtStartTime2.text!.isEmpty && cell.txtEndTime2.text!.isEmpty{
                cell.txtStartTime2.text = self.dateFormatter.string(from: Date())
                cell.txtEndTime2.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime2 = Date().timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime2 = Date().addingTimeInterval(60*60*1).timeIntervalSince1970
                cell.datePicker2.minimumDate = Date().addingTimeInterval(60*60*1)
            }else if !cell.txtStartTime2.text!.isEmpty
            {
                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime2)
                cell.txtEndTime2.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
                cell.datePicker2.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
            
        }
        
        
        
        cell.datePicker.addAction(for: .valueChanged) { [unowned self] in
            
            if cell.txtStartTime.isFirstResponder
            {
                cell.txtStartTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date.addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime = cell.datePicker.date.timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.addingTimeInterval(60*60*1).timeIntervalSince1970
            }
            
            if cell.txtEndTime.isFirstResponder
            {
                cell.txtEndTime.text = self.dateFormatter.string(from: cell.datePicker.date)
                self.arrWeekDays[indexPath.row].endTime = cell.datePicker.date.timeIntervalSince1970
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
            
        }
        
        cell.datePicker2.addAction(for: .valueChanged) { [unowned self] in
            
            if cell.txtStartTime2.isFirstResponder
            {
                cell.txtStartTime2.text = self.dateFormatter.string(from: cell.datePicker2.date)
                cell.txtEndTime2.text = self.dateFormatter.string(from: cell.datePicker2.date.addingTimeInterval(60*60*1))
                self.arrWeekDays[indexPath.row].startTime2 = cell.datePicker2.date.timeIntervalSince1970
                self.arrWeekDays[indexPath.row].endTime2 = cell.datePicker.date.addingTimeInterval(60*60*1).timeIntervalSince1970
            }
            
            if cell.txtEndTime2.isFirstResponder
            {
                cell.txtEndTime2.text = self.dateFormatter.string(from: cell.datePicker2.date)
                self.arrWeekDays[indexPath.row].endTime2 = cell.datePicker2.date.timeIntervalSince1970
            }
            
            cell.updateWeeKCellColor(collectionView: self.colWeekDays, indexpath: indexPath)
            
        }
        
        return cell
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == colWeekDays {
            colTime.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == colDuration {
            
            return CGSize(width: (kScreenWidth - 80) / CGFloat(arrPickrList.count), height: collectionView.frame.size.height)
            
//            let fontAttributes = [NSAttributedStringKey.font: UIFont(name: fonts.montserratSemiBoldFont, size: 10.0 * scaleFactor) as Any]
//            let size = (arrPickrList[indexPath.row]  + " Min").size(withAttributes: fontAttributes)
//           // return size.width
//
//            return CGSize(width: size.width + 10, height: collectionView.frame.size.height)
        }
        
        if collectionView == colWeekDays {
            return CGSize(width: (kScreenWidth - 80 - 35) / 7, height: collectionView.frame.size.height)
        }
        return CGSize(width: (kScreenWidth - 80), height: collectionView.frame.size.height)
    }
    
    
    
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension ShareArrivalParentVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrPerson.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DriverCell = tableView.dequeueReusableCell(withIdentifier: "DriverCell") as! DriverCell
        
        cell.rightSwipeSettings.transition = .drag
        cell.imgPhoto.image = #imageLiteral(resourceName: "B.jpg")
        cell.lblUserName.text = arrPerson[indexPath.section].name
        cell.lblNumber.text = arrPerson[indexPath.section].phone
        
        let delete = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete"), backgroundColor: colors.deleteColor) { (cell) -> Bool in
            
            let alert = UIAlertController(title: nil, message: "Are you sure you want to remove this person?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                self.arrPerson.remove(at: indexPath.row)
                tableView.reloadData()
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
            return true
        }
        
        delete.buttonWidth = 60
        
        cell.rightButtons = [delete]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}





