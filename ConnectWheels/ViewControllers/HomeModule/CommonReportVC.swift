//
//  CommonReportVC.swift
//  ConnectWheels
//
//  Created by  on 03/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit


class GeofenceCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var btnOn: UIButton!
    
    override func awakeFromNib() {
        
        lblTitle.textColor = colors.blackColor
        lblTitle.font = UIFont(name: fonts.montserratMediumFont, size: 12.0 * scaleFactor)
        
        lblDetail.textColor = colors.blackColor
        lblDetail.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
        
    }
}

class CommonReportVC: UIViewController {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblVehicle: UITableView!
    
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var pageIndex : Int = 0
    
    var arrList : [String] = []
    
    let kTrip = "Trip"
    let kDistance = "Distance"
    let kTime = "Time"
    let kAlerts = "Alerts"
    let kDocuments = "Documents"
    let kPerson = "Person"
    let kGeofencing = "Geofencing"
    let kMessages = "Messages"
    let kNetwork = "Network Operator"
    
    var tripReport : WheelReport = WheelReport.init(fromJson: JSON.null)
    var lastApiParam : [String : Any] = [:]
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
       arrList = [kTrip,kDistance,kTime,kAlerts,/*kMessages,kDocuments,kPerson,kGeofencing*/]
       tblVehicle.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 20))
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    //------------------------------------------------------
    
    //MARK:- Ws
    
    func getVehicleTripWs(paramToPass : [String : Any])  {
        
        /**
         ===========API CALL===========
         
         Method Name : trip/get_trip_counts/
         
         Parameter   : vehicle_id
         
         Optional    : start_date(2019-01-28), end_date(2019-01-29)
         
         Comment     : This api for vehicle owner can get his trip counts.
         
         ==============================
         */
        
        var param = paramToPass
        param[kvehicle_id] = VehicleDataSource.shared.arrVehicles[pageIndex].id
        param[kDeviceImei] = VehicleDataSource.shared.arrVehicles[pageIndex].deviceImei
        self.lastApiParam = param
        
        ApiManger.shared.makeRequest(method: .getTripCount , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.tripReport = WheelReport.init(fromJson: response[kData])
                    self.tblVehicle.reloadData()
                    break
                
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//
        if let parent = self.parent?.parent as? WheelReportVC
        {
            parent.selectIndex = pageIndex
            var param : [String : Any] = [:]
            param[kstartDate] = parent.txtStartTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
            param[kendDate] = parent.txtEndTime.text!.convertFormatOnly(currentformat: "dd MMM yyyy", changeformat: "yyyy-MM-dd")
            getVehicleTripWs(paramToPass: param)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension CommonReportVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : VehicleDetailCell = tableView.dequeueReusableCell(withIdentifier: "VehicleDetailCell") as! VehicleDetailCell
        
        cell.lblTitle.text = arrList[indexPath.row]
        
        switch arrList[indexPath.row] {
        case kTrip:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_trip")
            
            let string = JSON(tripReport.totalTrips).stringValue
            let suffix = " Trips"
            
            let attStr = NSMutableAttributedString(string: string)
            
            let appendStr = NSAttributedString(string: suffix, attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor])
            
            attStr.append(appendStr)
            
            cell.lblDetail.attributedText = attStr
            
            break
            
        case kDistance:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_distance")
            
            let string = JSON(tripReport.totalDistance).stringValue
            let suffix = " Kms"
            
            let attStr = NSMutableAttributedString(string: string)
            
            let appendStr = NSAttributedString(string: suffix, attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor])
            
            attStr.append(appendStr)
            
            cell.lblDetail.attributedText = attStr
            
            break
        case kTime:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_time")
            
            
            let givenTime = JSON(tripReport.totalTime).intValue
            let hr = Int(givenTime / 60)
            let min = Int(givenTime % 60)
            
           /* let string = "\(hr) : \(min)"
            
            
            let attStr = NSMutableAttributedString(string: string)
         
            attStr.addAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor], range: (string as NSString).range(of: hrsuffix))
            
            attStr.addAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor], range: (string as NSString).range(of: minsuffix))*/
            
            cell.lblDetail.text = "\(hr) : \(min)"
            
            break
        case kAlerts:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_alert")
            
            let string = JSON(tripReport.totalAlertCount).stringValue
            let attStr = NSMutableAttributedString(string: string)
            cell.lblDetail.attributedText = attStr
            
            break
        case kDocuments:
            cell.imgIcon.image = #imageLiteral(resourceName: "Wheels_Reports_document")
            
            let string = "View"
            let attStr = NSAttributedString(string: string, attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.blueColor])
            cell.lblDetail.attributedText = attStr
            
            break
           
        case kPerson:
            cell.imgIcon.image = #imageLiteral(resourceName: "Person ")
            let string = JSON(tripReport.totalPerson).stringValue
            let attStr = NSMutableAttributedString(string: string)
            cell.lblDetail.attributedText = attStr
            break
        case kGeofencing:
            let cell : GeofenceCell = tableView.dequeueReusableCell(withIdentifier: "GeofenceCell") as! GeofenceCell
             cell.lblTitle.text = arrList[indexPath.row]
            cell.imgIcon.image = #imageLiteral(resourceName: "navigation")
            cell.btnOn.addAction(for: .touchUpInside) { [unowned self] in
                _ = self
                cell.btnOn.isSelected = !cell.btnOn.isSelected
            }
            return cell
            
        case kNetwork:
            cell.imgIcon.image = UIImage.init(named: "network")
            cell.lblDetail.text = JSON(tripReport.networkOperator).stringValue
            break
        case kMessages:
            cell.imgIcon.image = UIImage.init(named: "report_message")
            cell.lblDetail.text = JSON(tripReport.messages).stringValue
            break
        default:
            
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch arrList[indexPath.row] {
        case kTrip:
            let obj : TripListVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TripListVC") as! TripListVC
            var param = self.lastApiParam
            param["trip_count"] = self.tripReport.totalTrips
            obj.apiParam = param
            self.navigationController?.pushViewController(obj, animated: true)
           break
            
        case kDistance:
            
            break
        case kTime:
            
            
            break
        case kAlerts:
            let obj : AlertVC = HomeStoryBoard.instantiateViewController(withIdentifier: "AlertVC") as! AlertVC
            self.navigationController?.pushViewController(obj, animated: true)
            break
        case kDocuments:
            let obj : MyDocumentsVC = BookingStoryBoard.instantiateViewController(withIdentifier: "MyDocumentsVC") as! MyDocumentsVC
            obj.title = "Vehicle Documents"
            self.navigationController?.pushViewController(obj, animated: true)
            break
            
        case kPerson:
           /* let obj : ManagePersonVC = HomeStoryBoard.instantiateViewController(withIdentifier: "ManagePersonVC") as! ManagePersonVC
            self.navigationController?.pushViewController(obj, animated: true)*/
            
            break
            
        case kGeofencing:
            let obj : GeofenceVC = BookingStoryBoard.instantiateViewController(withIdentifier: "GeofenceVC") as! GeofenceVC
            self.navigationController?.pushViewController(obj, animated: true)
            
            break
       
        default:
            
            break
        }
    }
}
