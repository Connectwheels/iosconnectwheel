//
//  TripListVC.swift
//  ConnectWheels
//
//  Created by  on 03/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

//------------------------------------------------------

//MARK:- WheelStatusCell

class TripListCell : UITableViewCell {
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblTripName: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTripTime: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMaxSpeed: UILabel!
    @IBOutlet weak var lblAlerts: UILabel!
    @IBOutlet var lblTitles: [UILabel]!
    
    
    override func awakeFromNib() {
        
        lblTripName.montserrat(ofType: fonts.montserratBoldFont, withfontSize: 11.0, color: colors.blackColor)
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.redColor)
        lblTripTime.montserrat(ofType: fonts.montserratBoldFont, withfontSize: 6.0, color: colors.greyColor)
        lblDistance.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblTime.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblMaxSpeed.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblAlerts.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        
        _ = lblTitles.map({ (label) -> Void in
            label.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.blackColor)
        })
        
        viewBG.dropShadow()
    }
    
}


class TripListVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var tblTripList: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var apiParam : [String : Any] = [:]
    var arrTrips : [TripList] = []
    var isCallWS : Bool = false
    var totalTripCount : Int = 0
    var pageNumber : Int = 1
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        tblTripList.tableHeaderView = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 10))
        getVehicleTripListWs()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //------------------------------------------------------
    
    //MARK:- Ws
    
    func getVehicleTripListWs()  {
        
        /**
         ===========API CALL===========
         
         Method Name : trip/get_trip_list/
         
         Parameter   : vehicle_id , page
         
         Optional    : start_date(2019-01-28), end_date(2019-01-29)
         
         Comment     : This api for vehicle owner can get his trip counts.
         
         ==============================
         */
        
        var param = self.apiParam
        param["page"] = pageNumber
        
        
        
        ApiManger.shared.makeRequest(method: .getTripList , parameter: param , withLoader : pageNumber == 1) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    self.isCallWS = true
                    let data = JsonList<TripList>.createModelArray(model: TripList.self, json: response[kData].arrayValue)
                    self.arrTrips.append(contentsOf: data)
                    self.tblTripList.reloadData()
                    self.totalTripCount = response[kData]["total_trips"].intValue
                   
                    break
                case .nodata:
                    if self.arrTrips.count - 10 <= 0{
                        GFunction.ShowAlert(message: response[kMessage].stringValue)
                    }
                    break
                default :
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                }
            }else{
                self.isCallWS = true
                if self.pageNumber > 1
                {
                    self.pageNumber -= 1
                }
            }
        }
        
    }
 
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension TripListVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrTrips.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TripListCell = tableView.dequeueReusableCell(withIdentifier: "TripListCell") as! TripListCell
        
        cell.imgCar.image = #imageLiteral(resourceName: "defaultTrip")
        
        let data = arrTrips[indexPath.section]
        
        cell.lblTripName.text = "Trip " + JSON(max(JSON(self.apiParam["trip_count"] as Any).intValue - indexPath.section,1)).stringValue
        cell.lblMaxSpeed.text = String.init(format: "%.2f", data.maxSpeed) + " kmph"
        cell.lblDistance.text = String.init(format: "%.2f", data.distance) + " km"
        
        let givenTime = JSON(data.time).intValue
        let hr = Int(givenTime / 60)
        let min = Int(givenTime % 60)
        
        cell.lblTime.text = JSON(hr).stringValue + ":" + JSON(min).stringValue
        cell.lblAlerts.text = JSON(data.alertCounts).stringValue
        cell.lblCarName.text = JSON(data.carModel).stringValue + " - " + JSON(data.registrationNumber).stringValue
        cell.lblTripTime.text = JSON(data.startTrip).stringValue.convertFormatOnly(currentformat: "yyyy-MM-dd HH-mm-ss", changeformat: "dd/MM/yyyy hh:mm a")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj : TripDetailVC = HomeStoryBoard.instantiateViewController(withIdentifier: "TripDetailVC") as! TripDetailVC
        obj.apiParam = self.apiParam
        obj.selectIndex = indexPath.section
        obj.tripDetail = arrTrips[indexPath.section]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    
    
    
    
}

//--------------------------------------------------------------------------
//MARK:- ScrollDelegate

extension TripListVC : UIScrollViewDelegate{
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset: CGFloat = scrollView.contentOffset.y
        let maximumOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height + 20
        if maximumOffset - currentOffset <= 1  {
           /* if isCallWS  {
                isCallWS = false
                pageNumber += 1
                getVehicleTripListWs()
            }*/
            
        }
    }
    
}




