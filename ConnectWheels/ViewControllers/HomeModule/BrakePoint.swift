//
//  BrakePoint.swift
//  ConnectWheels
//
//  Created by  on 05/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class BrakePoint: UIView {

   
   
    //MARK:- Outlet
    
    //------------------------------------------------------
    
     @IBOutlet weak var lblDetails: UILabel!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func awakeFromNib() {
        lblDetails.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.blackColor)
    }

    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}


