//
//  MyDocumentsVC.swift
//  ConnectWheels
//
//  Created by  on 27/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class MyDocumentsVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var colDocuments: UICollectionView!
    
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var arrList : [MyDocuments] = []
  
    var isVehicleDocument = false
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    

    
    func setUpView() {
        
        arrList.removeAll()

        
        myDocumentsWs()
   
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    @IBAction func btnAddClicked(_ sender: Any) {
        
        
        view.endEditing(true)
        let obj : AddDoumnetVC = BookingStoryBoard.instantiateViewController(withIdentifier: "AddDoumnetVC") as! AddDoumnetVC
        obj.responseData = arrList
        obj.isVehicleDocument = isVehicleDocument
        obj.completion = { data in
            if let details = data as? MyDocuments {
                self.arrList.append(details)
                self.colDocuments.reloadData()
            }
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    
    func myDocumentsWs()  {

        /**
         ===========API CALL===========
         
         Method Name : get_vehicle_documents , save_vehicle_documents
         
         Parameter   : vehicle_id
         
         Optional    :
         
         Comment     :
         
         ==============================
         */
        
        var param : [String:Any] = [:]
        
        if isVehicleDocument , let id = VehicleDataSource.shared.selectedVehicle?.id{
            param[kvehicle_id] = id
        }
        
        ApiManger.shared.makeRequest(method: isVehicleDocument ? .getVehicleDocument : .getUserDocument , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    let arr : [MyDocuments] = MyDocuments.fromArray(json: response[kData].arrayValue)
                    self.arrList.append(contentsOf: arr)

                    self.colDocuments.reloadData()
                    
                    break
                    
                case .inactive:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                    
                default :
                    break
                }
            }
        }
    }

    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}



//------------------------------------------------------

//MARK:- Collection view method

extension MyDocumentsVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : MyDocumentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyDocumentCell", for: indexPath) as! MyDocumentCell
        
        let detail = arrList[indexPath.row]
        
        cell.lblDesc.text = JSON(detail.folderName as Any).stringValue
 
        if JSON(detail.validity as Any).stringValue.isEmpty
        {
            cell.lblExpires.isHidden = true
            cell.lblExpires.text = ""
        }else
        {
            cell.lblExpires.text = "Expires on : " + JSON(detail.validity as Any).stringValue
            cell.lblExpires.isHidden = false
        }
            let url = JSON(detail.image.first as Any).stringValue
//           let url = JSON(responseData.upload_url as Any).stringValue + JSON(detail.image as Any).stringValue
            cell.imgPhoto.sd_setImage(with: url.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultCarPlaceholder"))
            cell.lblDesc.isHidden = false
            cell.blackLayer.isHidden = false
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj : AddDoumnetVC = BookingStoryBoard.instantiateViewController(withIdentifier: "AddDoumnetVC") as! AddDoumnetVC
        obj.responseData = self.arrList
        obj.index = indexPath.row
        obj.isVehicleDocument = isVehicleDocument
        obj.completion = { data in
            if let details = data as? [MyDocuments] {
                self.arrList = details
                self.colDocuments.reloadData()
            }
        }
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (kScreenWidth - 90) / 2, height: (kScreenWidth - 90) / 2)
    }
}


//------------------------------------------------------

//MARK:- UploadCell

class MyDocumentCell : UICollectionViewCell {
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var blackLayer: UIImageView!
    @IBOutlet weak var lblExpires: UILabel!
    
    override func awakeFromNib() {
        if let _ = lblDesc
        {
            lblDesc.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.whiteColor)
            lblExpires.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.whiteColor)
            blackLayer.isHidden = true
            lblDesc.isHidden = true
        }
    }
}
