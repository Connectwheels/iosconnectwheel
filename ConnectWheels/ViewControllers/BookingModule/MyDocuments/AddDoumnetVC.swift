//
//  AddDoumnetVC.swift
//  ConnectWheels
//
//  Created by  on 27/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class AddDocumentCell : UICollectionViewCell{
    @IBOutlet var imgDocument : UIImageView!
}


class AddDoumnetVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var txtFolderName: FloatingCommon!
    @IBOutlet weak var txtValidity: FloatingCommon!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var btnAdd: ThemeActionButton!
    @IBOutlet weak var colDocument : UICollectionView!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    let datPicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    var completion : completion!
    var responseData : [MyDocuments] = []
    var index : Int? // keep for update or delete
    var document : MyDocuments = MyDocuments()
    
    var isVehicleDocument = false
    
//    var arrOptions : [String] = ["Vehicle Insurance","Pollution Certificate","Fitness Certificate","Road Permits","Registration Certificate","Add More"]
    var arrOptions : [String] = ["Life Insurance","Accidental Insurance","Aadhar Card","Election ID","PAN Card","Passport","Date of birth Certificate","Driving Licence","Add More"]
   // var arrDocument : [Any] = ["uplus"]
    
    var picker = UIPickerView()
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        
        //Check field fullfill valiadtion or not
        
        if txtFolderName.text!.trim().isEmpty{
            return kvBlankFolder
        }else if responseData.contains(where: { $0.folderName == txtFolderName.text }) && index == nil{
            return kvFolderExist
        }else if document.imageToAdd.count < 2 {
            return kvSelectImage
        }
        
        return String()
    }
    
    @objc func imgPickerOpen(isShowRemove : Bool = false) {
        
        self.view.endEditing(true)
        let imagePicker : UIImagePickerController = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if TARGET_OS_SIMULATOR == 1
            {
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    imagePicker.sourceType = .photoLibrary
                    OperationQueue.main.addOperation({() -> Void in
                        
                        self.present(imagePicker, animated: true, completion: nil)
                    })
                }
                
                return
            }
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(imagePicker, animated: true, completion: nil)
                })
                
            }
            
        }))
        
        
        
        actionSheet.addAction(UIAlertAction(title: "Gallery".Localized(), style: .default, handler: { (UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                imagePicker.sourceType = .photoLibrary
                OperationQueue.main.addOperation({() -> Void in
                    
                    self.present(imagePicker, animated: true, completion: nil)
                })
            }
            else {
                // GFunction.sharedInstance.alert("Gallery is not available")
            }
        }))

        
        actionSheet.addAction(UIAlertAction(title: "CANCEL".Localized(), style: .cancel, handler: { (UIAlertAction) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func setUpView() {
        txtValidity.inputView = datPicker
        datPicker.datePickerMode = .date
        datPicker.minimumDate = Date()
        
        if isVehicleDocument{
            arrOptions = ["Vehicle Insurance","Pollution Certificate","Fitness Certificate","Road Permits","Registration Certificate","Add More"]
            
        }
        
        datPicker.addAction(for: .valueChanged) { [unowned self] in
            self.dateFormatter.dateFormat = "dd-MM-yyyy"
            self.txtValidity.text = self.dateFormatter.string(from: self.datPicker.date)
        }
        

            if let index = self.index{
                document = self.responseData[index]
                if isVehicleDocument{
                    self.title = "Edit Vehicle Document"
                }else{
                    self.title = "Edit Document"
                }
                self.txtFolderName.text = document.folderName
                self.txtValidity.text = document.validity.convertFormatOnly(currentformat: "yyyy-MM-dd", changeformat: "dd-MM-yyyy")
                self.document.imageToAdd = self.responseData[index].image
//                let newImage = self.responseData[index].image.compactMap { (str) -> String? in
//                    return str.components(separatedBy: "/").last
//                }
//                self.responseData[index].image = newImage
                self.btnAdd.setTitle("Update", for: .normal)
                
            }else{
                if isVehicleDocument{
                    self.title = "Add Vehicle Documents"
                }else{
                    self.title = "Add Documents"
                }
                self.btnAdd.setTitle("Add", for: .normal)
                self.navigationItem.rightBarButtonItem = nil
            }
//        }
        
        document.imageToAdd.insert(UIImage.init(named: "uplus") as Any, at: 0)
        txtFolderName.RightPaddingView(image: UIImage (named:"downArrow"), frame: CGRect(x: 0, y: txtFolderName.titleHeight(), width: 30, height: txtFolderName.frame.size.height - txtFolderName.titleHeight()))
        txtFolderName.inputView = picker
        picker.delegate = self
        picker.dataSource = self
        txtFolderName.delegate = self
        colDocument.showsHorizontalScrollIndicator = false
        colDocument.delegate = self
        colDocument.dataSource = self
        colDocument.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnDeleteDocumentClicked(_ sender: Any) {
  
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: "Are you sure you want delete this document ?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
               self.deleteDocumentsWs()
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        
        let error = validate()
        
        view.endEditing(true)
        
        if error != String() {
            
            GFunction.ShowAlert(message: error)
            
            return
        }
        
        GFunction.sharedInstance.addLoader()
    
         let dispatchGroup = DispatchGroup()
        
        _ = (1...document.imageToAdd.count - 1).map({ (index) -> Void in
            dispatchGroup.enter()
            if document.imageToAdd[index] is UIImage {
                ImageUpload.shared.uploadImage(true, document.imageToAdd[index] as! UIImage, "image/jpeg", isVehicleDocument ?  .vehicleDocuments : .userDocuments) { (url, imgName) in
                    if let _ = imgName{
                        if self.document.preImages.isEmpty{
                            self.document.preImages = JSON(imgName as Any).stringValue
                        }else{
                            self.document.preImages = self.document.preImages + "," + JSON(imgName as Any).stringValue
                        }
                        
                        self.document.imageToAdd[index] = url as Any
                        self.document.image.append(JSON(url as Any).stringValue)
                    }
                    dispatchGroup.leave()
                    
                }
            } else {
                dispatchGroup.leave()
            }
        })
            
        dispatchGroup.notify(queue: .main) {
            if !self.document.image.isEmpty{
                if let _ = self.index{
                    self.editDocument()
                }else{
                    self.addDocument()
                }
            }
         }

    }
    
    @IBAction func btnAddImageClicked(_ sender: Any) {
        
        imgPickerOpen()
    }
    
    //------------------------------------------------------
    
    //MARK:- Web Service
    

    
    func deleteDocumentsWs() {
        
        /**
         ===========API CALL===========
         
         Method Name : user/remove_document/,  vehicle/save_vehicle_documents/
         
         Parameter   : document_id
         
         Optional    :
         
         Comment     :
         
         ==============================
         */
        
        if let index = self.index
        {
            self.responseData.remove(at: index)
            
        }
        
        let param : [String:Any] = ["document_id" : JSON(document.id).stringValue]
        
        ApiManger.shared.makeRequest(method: isVehicleDocument ? .deleteVehicleDocuments : .deleteUserDocuments , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    guard let cb = self.completion else { return }
                    cb(self.responseData)
                    self.navigationController?.popViewController(animated: true)
                    break
                case .nodata:
                    break
                default :
                    break
                }
            }
        }
        
    }
    
    func addDocument()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/save_documents/,  vehicle/save_vehicle_documents/
         
         Parameter   : vehicle_id, folder_name, validity, image
         
         Optional    :
         
         Comment     :
         
         ==============================
         */
        
        document.folderName = txtFolderName.text!.trim()
        document.validity =   txtValidity.text!.convertFormatOnly(currentformat: "dd-MM-yyyy", changeformat: "yyyy-MM-dd")
        var param : [String:Any] = document.toDictionary()
        param["image"] = document.preImages
        
        if isVehicleDocument , let id = VehicleDataSource.shared.selectedVehicle?.id{
            param[kvehicle_id] = id
        }
        
        ApiManger.shared.makeRequest(method: isVehicleDocument ? .addVehicleDocuments : .addUserDocuments , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    guard let cb = self.completion else { return }
                    if let index = self.index{
                        self.responseData[index] = self.document
                        cb(self.responseData)
                    }else{
                        self.document.image = (self.document.imageToAdd.filter{ !($0 is UIImage) }) as! [String]
                        cb(self.document)
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                    break
                case .nodata:
                    break
                default :
                    break
                }
            }
        }
    }
    
    func editDocument()  {
        
        /**
         ===========API CALL===========
         
         Method Name : user/save_documents/,  vehicle/edit_document/
         
         Parameter   : vehicle_id, folder_name, validity, image
         
         Optional    :
         
         Comment     :
         
         ==============================
         */
        
        document.folderName = txtFolderName.text!.trim()
        document.validity =   txtValidity.text!.convertFormatOnly(currentformat: "dd-MM-yyyy", changeformat: "yyyy-MM-dd")
        var param : [String:Any] = document.toDictionary()
        param[kdocuments_id] = JSON(document.id).stringValue
        param["image"] = document.preImages
        
        ApiManger.shared.makeRequest(method: isVehicleDocument ? .editVehicleDocuments : .editUserDocuments , parameter: param) { (response,httpCode,error,statuscode) in
            
            if error == nil{
                
                switch statuscode
                {
                case .invalidrequest:
                    GFunction.ShowAlert(message: response[kMessage].stringValue)
                    break
                case .success:
                    guard let cb = self.completion else { return }
                    if let index = self.index{
                        self.document.image = (self.document.imageToAdd.filter{ !($0 is UIImage) }) as! [String]
                        self.responseData[index] = self.document
                        cb(self.responseData)
                    }else{
                        cb(self.document)
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                    break
                case .nodata:
                    break
                default :
                    break
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//--------------------------------------------------------------------------
//MARK:- Image Picker Method

extension AddDoumnetVC : UINavigationControllerDelegate , UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        dismiss(animated: true) {
        }
        
        let imgPicke : UIImage = (info[UIImagePickerControllerEditedImage] as? UIImage)!
        document.imageToAdd.append(imgPicke)
        colDocument.reloadData()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        dismiss(animated: true){
        }
    }
}
//--------------------------------------------------------------------------
//MARK:- Image Picker Method
extension AddDoumnetVC : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrOptions.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrOptions[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtFolderName.text = row == arrOptions.count - 1 ? "" : arrOptions[row]
    }
    
}
extension AddDoumnetVC : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return document.imageToAdd.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : AddDocumentCell = colDocument.dequeueReusableCell(withReuseIdentifier: "AddDocumentCell", for: indexPath) as! AddDocumentCell

        cell.imgDocument.contentMode = .scaleAspectFill
        cell.imgDocument.layer.masksToBounds = true
        
        let imgData = document.imageToAdd[indexPath.row]
        
        if let imgToSet = imgData as? UIImage{
            cell.imgDocument.image = imgToSet
        }else{
            cell.imgDocument.sd_setImage(with: JSON(imgData).stringValue.getUrl(), placeholderImage: #imageLiteral(resourceName: "defaultCarPlaceholder"))
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0{
             imgPickerOpen()
            return
        }
        
        if let cell = collectionView.cellForItem(at: indexPath) as? AddDocumentCell{
            
            let configuration = ImageViewerConfiguration { config in
                config.imageView = cell.imgDocument
            }
            
            
            let imageViewerController = ImageViewerController(configuration: configuration)
            
            let imgData = document.imageToAdd[indexPath.row]
            imageViewerController.isHideDownloadButton = (imgData is UIImage)
            
            
            if let presented = appDelegate.window?.rootViewController?.presentedViewController{
                presented.present(imageViewerController, animated: true)
                return
            }
            
            appDelegate.window?.rootViewController?.present(imageViewerController, animated: true)
        }
    }
    
    
}

extension AddDoumnetVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFolderName && index != nil {
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtFolderName && picker.selectedRow(inComponent: 0) == arrOptions.count - 1{
            
            let alert = UIAlertController(title: "", message: "Add More", preferredStyle: .alert)
            
            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                textField.placeholder = "Add More"
            }
            
            // 3. Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak alert] (_) in
                let txtIput = alert?.textFields![0] // Force unwrapping because we know it exists.
                self.txtFolderName.text = txtIput?.text?.trim()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
                
            }))
            
            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
