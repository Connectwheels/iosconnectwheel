//
//  EditBookingVC.swift
//  ConnectWheels
//
//  Created by  on 14/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class EditBookingVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var txtName: FloatingCommon!
    @IBOutlet weak var txtMobile: FloatingCommon!
    @IBOutlet weak var txtArea: FloatingCommon!
    @IBOutlet weak var txtSelectDriver: ExpiryDate!
    @IBOutlet weak var txtRate: FloatingCommon!
    @IBOutlet weak var txtMinimum: FloatingCommon!
    
    @IBOutlet weak var lblSelectDateTimeTitle: UILabel!
    
    
    @IBOutlet weak var tblVehicle: UITableView!
    @IBOutlet  var tblDateTime: UITableView!
    
    @IBOutlet weak var constTblVehicleHeight: NSLayoutConstraint!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    let picker = UIPickerView()
    var selectedVehicleIndex = 0
    var isShowList = false
    {
        didSet{
            let height = self.navigationController?.navigationBar.frame.size.height ?? 64
            tblVehicle.layoutIfNeeded()
            constTblVehicleHeight.constant = isShowList ? kScreenHeight - height : 85
            
        }
    }
    
    var arrDates : [BookingTime] = []
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    
    func validate() -> String{
        
        
        //Check field fullfill valiadtion or not
        if selectedVehicleIndex > 4 {
            return kvBlankSelectVehicle
        }else if txtName.text!.isEmpty {
            return kvBlankName
        }else if txtMobile.text!.isEmpty{
            return kvBlankPhoneNumber
        }else if txtMobile.text!.count < 6 {
            return kvValidPhoneNumber
        }else if txtSelectDriver.text!.isEmpty {
            return kvBlankSelectDriver
        }else if txtSelectDriver.text!.isEmpty {
            return kvBlankSelectDriver
        }else if txtArea.text!.isEmpty {
            return kvBlankLocation
        }else if txtRate.text!.isEmpty {
            return kvBlankRate
        }else if txtMinimum.text!.isEmpty {
            return kvBlankMinimumKm
        }else if arrDates.isEmpty {
            return kvBlankMinBookingTime
        }
        
        
        return String()
    }
    
    func setUpView() {
        
        txtName.text = "User name"
        txtMobile.text = "+91 123457890"
        txtSelectDriver.text = "User name"
        txtArea.text = "3599 Wahington Street Corpus Christi"
        txtRate.text = "50"
        txtMinimum.text = "150"
        arrDates.append(BookingTime(date: "19/02/2018", fromTime: "10:00 Am", toTime: "10:00 Pm"))
        
        txtMobile.RightPaddingView(image: #imageLiteral(resourceName: "imgCall"), frame: CGRect(x: 0, y: txtMobile.titleHeight(), width: 30, height: txtMobile.frame.size.height - txtMobile.titleHeight()))
        txtSelectDriver.RightPaddingView(image: #imageLiteral(resourceName: "downArrow"), frame: CGRect(x: 0, y: txtSelectDriver.titleHeight(), width: 30, height: txtSelectDriver.frame.size.height - txtSelectDriver.titleHeight()))
        txtArea.RightPaddingView(image: #imageLiteral(resourceName: "imgLocationGray"), frame: CGRect(x: 0, y: txtArea.titleHeight(), width: 30, height: txtArea.frame.size.height - txtSelectDriver.titleHeight()))
        txtRate.RightPaddingView(image: #imageLiteral(resourceName: "Bpaper-bill"), frame: CGRect(x: 0, y: txtRate.titleHeight(), width: 30, height: txtRate.frame.size.height - txtRate.titleHeight()))
        txtMinimum.RightPaddingView(image: #imageLiteral(resourceName: "Bkm"), frame: CGRect(x: 0, y: txtMinimum.titleHeight(), width: 30, height: txtMinimum.frame.size.height - txtMinimum.titleHeight()))
        
        lblSelectDateTimeTitle.montserrat(ofType: fonts.montserratMediumFont, withfontSize: 10.0, color: colors.greyColor)
        
        
        txtMobile.regex = "^[0-9]{0,16}$"
        txtRate.defaultcharacterSet = CharacterSet.decimalDigits
        txtMinimum.defaultcharacterSet = CharacterSet.decimalDigits
        
        txtName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        txtSelectDriver.inputView = picker
        picker.delegate = self
        picker.dataSource = self
        
        tblDateTime.tableFooterView = UIView()
        
        tblVehicle.tableFooterView = UIView()
        tblVehicle.sectionHeaderHeight = UITableViewAutomaticDimension
        
        if #available(iOS 11.0, *) {
            tblVehicle.estimatedSectionHeaderHeight = UITableViewAutomaticDimension
        } else {
            tblVehicle.estimatedSectionHeaderHeight = 85
        }
        
        
    }
    
    
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnSelectDateTimeClicked(_ sender: Any) {
        
        let obj : SelectDateTimeVC = BookingStoryBoard.instantiateViewController(withIdentifier: "SelectDateTimeVC") as! SelectDateTimeVC
        obj.arrDates = self.arrDates
        
        obj.completion = { response in
            
            if response is [BookingTime] {
                
                let final =   (response as! [BookingTime]).filter({ (model) -> Bool in
                    
                    let details = self.arrDates.contains(where: { (modelArr) -> Bool in
                        return modelArr.date == model.date
                    })
                    
                    return !details
                })
                
                self.arrDates.append(contentsOf: final)
                self.tblDateTime.reloadData()
            }
        }
        
        let navi = UINavigationController(rootViewController: obj)
        self.present(navi, animated: true, completion: nil)
    }
    
    @IBAction func btnSelectAreaClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnBookClicked(_ sender: Any) {
        
        let error = validate()
        
        view.endEditing(true)
        
        if error != String() {
            
            GFunction.ShowAlert(message: error)
            
            return
        }
        
        //GFunction.ShowAlert(message: "Successful")
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        isShowList = false
    }
    
}

extension EditBookingVC : UIPickerViewDelegate , UIPickerViewDataSource
{
    
    //MARK:- UIPickerview Delegate Method
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return 10
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "User name"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtSelectDriver.text = "User name"
    }
    
    
    //------------------------------------------------------
    
}

//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension EditBookingVC : GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtArea.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
}

//------------------------------------------------------

//MARK:- Table View Method

extension EditBookingVC : UITableViewDelegate , UITableViewDataSource
{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == tblDateTime {
            return UIView()
        }
        
        
     //   if selectedVehicleIndex < 4 {
            
            let cell : BookingVehicleCell = tableView.dequeueReusableCell(withIdentifier: "BookingVehicleCell") as! BookingVehicleCell
            cell.btnCheck.setImage(#imageLiteral(resourceName: "downArrow"), for: .normal)
            cell.btnCheck.isHidden = false
            
            if self.isShowList
            {
                
                let angle =  CGFloat(Double.pi)
                let tr = CGAffineTransform.identity.rotated(by: angle)
                cell.btnCheck.transform = tr
                
            }else
            {
                cell.btnCheck.transform = .identity
            }
            
            cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
                self.isShowList = !self.isShowList
                self.tblVehicle.reloadData()
            }
            
            cell.contentView.dropShadow()
            
            return cell.contentView
            
      //  }
        
      
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblDateTime {
            return arrDates.count
        }
        
        return isShowList ? 4 : 0
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblDateTime {
            let cell : BookingTimeCell = tableView.dequeueReusableCell(withIdentifier: "BookingTimeCell", for: indexPath) as! BookingTimeCell
            let text = arrDates[indexPath.row].date + " - " + arrDates[indexPath.row].fromTime + " To " + arrDates[indexPath.row].toTime
            let strAttrIbuted = NSMutableAttributedString(string: text)
            strAttrIbuted.addAttributes([NSAttributedStringKey.foregroundColor : colors.redColor], range: (text as NSString).range(of: "To"))
            cell.lblDate.attributedText = strAttrIbuted
            cell.btnDelete.addAction(for: .touchUpInside) { [unowned self] in
                self.arrDates.remove(at: indexPath.row)
                self.tblDateTime.reloadData()
            }
            return cell
        }
        
        let cell : BookingVehicleCell = tableView.dequeueReusableCell(withIdentifier: "BookingVehicleCell", for: indexPath) as! BookingVehicleCell
        cell.btnCheck.isHidden = true
        
        if selectedVehicleIndex == indexPath.row {
            cell.btnCheck.isHidden = false
        }
        
        cell.btnList.addAction(for: .touchUpInside) { [unowned self] in
            self.selectedVehicleIndex = indexPath.row
            self.isShowList = false
            self.tblVehicle.reloadData()
        }
        
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedVehicleIndex = indexPath.row
        
    }
    
    
}
