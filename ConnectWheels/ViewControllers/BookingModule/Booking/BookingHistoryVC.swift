//
//  BookingHistoryVC.swift
//  ConnectWheels
//
//  Created by  on 12/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class BookingHistoryVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var btnUpcoming: UIButton!
     @IBOutlet var viewToAdd: UIView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var pageViewController : UIPageViewController = UIPageViewController()
    var arrViewController : [UIViewController] = []
    var arrOptionList = ["Upcomings" ,"Completed"]
    
    var selectIndex : Int = 0
    {
        didSet{
            
            if selectIndex == 0
            {
                btnUpcoming.isSelected = true
                btnCompleted.isSelected = false
               
                UIView.animate(withDuration: 0.3) {
                    self.viewLine.frame.origin.x = 0
                }
                
            }else if selectIndex == 1
            {
                btnUpcoming.isSelected = false
                btnCompleted.isSelected = true
                
                UIView.animate(withDuration: 0.3) {
                    self.viewLine.frame.origin.x = kScreenWidth / 2 + 3
                }
            }
          
        }
    }
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
        btnCompleted.setTitleColor(colors.redColor, for: .selected)
        btnUpcoming.setTitleColor(colors.redColor, for: .selected)
        
        btnCompleted.setTitleColor(UIColor.lightGray, for: .normal)
        btnUpcoming.setTitleColor(UIColor.lightGray, for: .normal)
        
        btnCompleted.setLayout(detail: [layout.kfont : UIFont(name: fonts.montserratMediumFont, size: 12.0 * scaleFactor) as Any])
        btnUpcoming.setLayout(detail: [layout.kfont : UIFont(name: fonts.montserratMediumFont, size: 12.0 * scaleFactor) as Any])
        
        _ = (0...arrOptionList.count - 1).map({ (i) -> Void in
            let obj : BookingHistoryCommonVC = BookingStoryBoard.instantiateViewController(withIdentifier: "BookingHistoryCommonVC") as! BookingHistoryCommonVC
            obj.pageIndex = i
            arrViewController.append(obj)
        })
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.backgroundColor = UIColor.clear
        pageViewController.view.frame = CGRect(x: 0, y: 0, width: viewToAdd.frame.size.width, height: viewToAdd.frame.size.height)
        self.addChildViewController(pageViewController)
        pageViewController.setViewControllers([arrViewController.first!], direction: .forward, animated: false, completion: nil)
        viewToAdd.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
        pageViewController.delegate = self
        pageViewController.dataSource = self
        
         self.selectIndex = 0

    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnUpcomingClicked(_ sender: Any) {
        self.selectIndex = 0
         pageViewController.setViewControllers([arrViewController[selectIndex]], direction: .reverse, animated: true, completion: nil)
    }
    
    @IBAction func btnCompletedClicked(_ sender: Any) {
        self.selectIndex = 1
         pageViewController.setViewControllers([arrViewController[selectIndex]], direction: .forward, animated: true, completion: nil)
    }
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}


//--------------------------------------------------------------------------
//MARK:- Page View Methods

extension BookingHistoryVC : UIPageViewControllerDelegate , UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard arrViewController.count > previousIndex else {
            return nil
        }
        
        return arrViewController[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = arrViewController.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = arrViewController.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return arrViewController[nextIndex]
    }
    
    
}
