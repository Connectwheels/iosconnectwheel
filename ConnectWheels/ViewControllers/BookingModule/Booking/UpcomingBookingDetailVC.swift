//
//  UpcomingBookingDetailVC.swift
//  ConnectWheels
//
//  Created by  on 12/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class UpcomingBookingDetailVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    @IBOutlet weak var lblCarName: UILabel!
 
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var constMapHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCarDetails: UIView!
    
    
    @IBOutlet var lblTitles: [UILabel]!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDriverPhoneNumber: UILabel!
    @IBOutlet weak var lblMinimumKm: UILabel!
    @IBOutlet weak var lblRatePerKm: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var btnCancelBooking: UIButton!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    

    var bounds = GMSCoordinateBounds()
    var currentIndex = 0
    var currentMarker = GMSMarker()
  
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
        constMapHeight.constant = kScreenHeight / 3.5
        
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        lblRegNo.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        
        addCar()
        
       _ = lblTitles.map { (lbl) -> Void in
            lbl.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 9.0, color: colors.blackColor)
        }
        
        lblUserName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.blackColor)
        lblFromDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblToDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblDriverName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblDriverPhoneNumber.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblMinimumKm.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblRatePerKm.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblLocation.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        
        let strFrom = NSMutableAttributedString.init(string: lblFromDate.text!)
        strFrom.addAttributes([NSAttributedStringKey.foregroundColor : colors.redColor], range: (strFrom.description as NSString).range(of: "To"))
        lblFromDate.attributedText = strFrom
        
        let strEnd = NSMutableAttributedString.init(string: lblToDate.text!)
        strEnd.addAttributes([NSAttributedStringKey.foregroundColor : colors.redColor], range: (strEnd.description as NSString).range(of: "To"))
        lblToDate.attributedText = strEnd
        
        btnDelete.addAction(for: .touchUpInside) { [unowned self] in
            
            let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this booking?".Localized(), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                self.navigationController?.popViewController(animated: true)
            }))
            
            alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                
            }))
            
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
        
        btnEdit.addAction(for: .touchUpInside) { [unowned self] in
            let obj : EditBookingVC = BookingStoryBoard.instantiateViewController(withIdentifier: "EditBookingVC") as! EditBookingVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
       
        btnCancelBooking.layer.masksToBounds = false
        
        btnCancelBooking.setLayout(detail: [layout.kRadiusRatio : 2,
                                layout.kfont : UIFont(name: fonts.montserratMediumFont, size: 10.0 * scaleFactor) as Any,
                                layout.kdefaultBackGroundColor : UIColor.colorFromHex(hex: 0xEE3840),
                                layout.ktitleColor : colors.whiteColor])
        
        btnCancelBooking.addAction(for: .touchUpInside) { [unowned self] in
            self.navigationController?.popViewController(animated: true)
        }
        
        lblStatus.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 9.0, color: colors.redColor)
        
        
    }
    
    
    func addCar()  {
        currentMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 31.87404, longitude: -94.10471))
        currentMarker.icon = UIImage.init(named: "CarG")
        currentMarker.map = map
        currentMarker.appearAnimation = .pop
        bounds = bounds.includingCoordinate(currentMarker.position)
       // map.animate(toLocation: currentMarker.position)
        self.map.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100))
    }
    
    
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    
    @IBAction func btnFullScreenMapClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseInOut], animations: {
            
            self.viewCarDetails.isHidden = sender.isSelected
            
            self.scroll.setContentOffset(CGPoint.zero, animated:true)
            if sender.isSelected {
                
                if #available(iOS 11.0, *) {
                    self.constMapHeight.constant = self.view.safeAreaLayoutGuide.layoutFrame.size.height
                } else {
                    
                    var height : CGFloat = 64.0
                    
                    if let heightBar = self.navigationController?.navigationBar.frame.size.height
                    {
                        height = CGFloat(heightBar)
                    }
                    
                    self.constMapHeight.constant = kScreenHeight - height
                }
                
                self.viewCarDetails.alpha =  0.0
                
            }else
            {
                
                self.constMapHeight.constant = kScreenHeight / 3.5
                self.viewCarDetails.alpha =  1.0
            }
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}
