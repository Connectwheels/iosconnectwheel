//
//  BookingHistoryCommonVC.swift
//  ConnectWheels
//
//  Created by  on 12/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class BookingHistoryCommonVC: UIViewController {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var tblHistory: UITableView!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var pageIndex : Int = 0
    var arrList : [String] = []
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        arrList = ["19/01/2017 - 10:00 Am","28/01/2017 - 10:00 Am","30/01/2017 - 10:00 Am","05/02/2018 - 10:00 Am","06/02/2018 - 10:00 Am","09/02/2018 - 10:00 Am"]
        tblHistory.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 20))
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let parent = self.parent?.parent as? BookingHistoryVC
        {
            parent.selectIndex = pageIndex
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension BookingHistoryCommonVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrList.count
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BookingHistoryCell = tableView.dequeueReusableCell(withIdentifier: "BookingHistoryCell") as! BookingHistoryCell
        
        cell.imgCar.image = #imageLiteral(resourceName: "2.jpg")
        
    
        
        if pageIndex == 0 {
 
            if indexPath.section == 0 {
                cell.lblStatus.text = "Current"
                cell.lblStatus.textColor = colors.blueColor
                cell.lblKmTitle.text = "Min. Driven Km"
            }else
            {
                cell.lblStatus.text = "Upcoming"
                cell.lblStatus.textColor = colors.redColor
                cell.lblKmTitle.text = "Minimum Km"
            }
            
            cell.rightSwipeSettings.transition = .drag
            
            let edit = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "editVehicle"), backgroundColor: colors.blueColor) { (cell) -> Bool in
                let obj : EditBookingVC =  BookingStoryBoard.instantiateViewController(withIdentifier: "EditBookingVC") as! EditBookingVC
                self.navigationController?.pushViewController(obj, animated: true)
                return true
            }
            
            edit.buttonWidth = 80
            
            let delete = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "delete"), backgroundColor: colors.deleteColor) { (cell) -> Bool in
                
                let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this booking?".Localized(), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Yes".Localized(), style: .default, handler: { (UIAlertAction) in
                    self.arrList.remove(at: indexPath.row)
                    tableView.reloadData()
                }))
                
                alert.addAction(UIAlertAction(title: "No".Localized(), style: .default, handler: { (UIAlertAction) in
                    
                }))
                
                appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
                
                return true
            }
            
            delete.buttonWidth = 80
            
            cell.rightButtons = [delete,edit]
            
            
        }else
        {
            cell.lblStatus.text = "Completed"
            cell.lblStatus.textColor = colors.greenColor
            cell.lblKmTitle.text = "Minimum Km"
        }
        
        cell.lblDate.text = arrList[indexPath.section]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if pageIndex == 0 {
            let obj : UpcomingBookingDetailVC = BookingStoryBoard.instantiateViewController(withIdentifier: "UpcomingBookingDetailVC") as! UpcomingBookingDetailVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else
        {
            let obj : CompletedBookingDetailVC = BookingStoryBoard.instantiateViewController(withIdentifier: "CompletedBookingDetailVC") as! CompletedBookingDetailVC
            self.navigationController?.pushViewController(obj, animated: true)
        }

    }
}

//------------------------------------------------------

//MARK:- VehicleDetailCell

class BookingHistoryCell : MGSwipeTableCell {
    
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCarDetails: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblKm: UILabel!
    @IBOutlet weak var lblRatePerKm: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblKmTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet var lblTitles: [UILabel]!
    
    
    override func awakeFromNib() {
        
        lblUserName.montserrat(ofType: fonts.montserratBoldFont, withfontSize: 10.0, color: colors.blackColor)
        lblCarDetails.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 8.0, color: colors.greyColor)
        lblDriverName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblKm.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblRatePerKm.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblStatus.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.blueColor)
        lblDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        
        _ = lblTitles.map({ (label) -> Void in
            label.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.blackColor)
        })
        
        viewBG.dropShadow()
    }
    
}
