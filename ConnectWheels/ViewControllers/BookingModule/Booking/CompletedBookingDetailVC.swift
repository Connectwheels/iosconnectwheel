//
//  CompletedBookingDetailVC.swift
//  ConnectWheels
//
//  Created by  on 12/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class CompletedBookingDetailVC: BlueNavigationBar {
    
    //MARK:- Outlet
    
    //------------------------------------------------------
    
    

    

    @IBOutlet weak var lblCarName: UILabel!
 
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tblAlerts: UITableView!
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var constMapHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCarDetails: UIView!

    
    @IBOutlet var lblTitles: [UILabel]!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDriverPhoneNumber: UILabel!
    @IBOutlet weak var lblMinimumKm: UILabel!
    @IBOutlet weak var lblRatePerKm: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var lblTotalKm: UILabel!
    
    @IBOutlet weak var lblTotalCost: UILabel!
    
    @IBOutlet var lblTotalTitles: [UILabel]!
    
    @IBOutlet weak var lblAlertsTitle: UILabel!
    
     @IBOutlet weak var lblBreakPoint: UILabel!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------

    
    var arrAlerts : [String] = []
    var bounds = GMSCoordinateBounds()
 
    var currentIndex = 0
    var currentMarker = GMSMarker()
 
    var arrCoordinates : [(lat : Double, long : Double)] = []
    var preLocation : CLLocation =  CLLocation()
    let path = GMSPath.init(fromEncodedPath: "geoaEfyk}P}@s@aBoA}D{CwBcBmB{AiCsBiBuA}@s@c@[eF}DoAaA")
    
    
    let kNotinRange = "Vehicle not in a range"
    let kMisHappen = "We found some mishappening"
    let kAcOn = "AC ON at ideal for more than 10 minutes"
    let kSharpTurn = "Sharp Turn"
    
  
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        
        arrAlerts = [kMisHappen , kNotinRange , kAcOn , kSharpTurn , kMisHappen , kNotinRange , kAcOn]
        
        constMapHeight.constant = kScreenHeight / 3.5
       
        tblAlerts.tableHeaderView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
        
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        lblRegNo.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)

        
        _ = lblTitles.map { (lbl) -> Void in
            lbl.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 9.0, color: colors.blackColor)
        }
        
        _ = lblTotalTitles.map { (lbl) -> Void in
            lbl.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.greyColor)
        }
        
        lblUserName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 12.0, color: colors.blackColor)
        lblFromDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblToDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblDriverName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblDriverPhoneNumber.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblMinimumKm.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblRatePerKm.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblLocation.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.greyColor)
        lblStatus.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 9.0, color: colors.greenColor)
        
        lblTotalKm.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 20.0, color: colors.redColor)
        lblTotalCost.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 20.0, color: colors.redColor)
        
        let attrIbutedString = NSMutableAttributedString(attributedString: NSAttributedString(string: "Rs. ", attributes: [NSAttributedStringKey.font  : UIFont(name:  fonts.montserratLightFont, size: 20.0 * scaleFactor) as Any]))
        attrIbutedString.append(NSAttributedString(string: "2500"))
        lblTotalCost.attributedText = attrIbutedString
        
        
        lblAlertsTitle.font = lblUserName.font
        
        let strFrom = NSMutableAttributedString.init(string: lblFromDate.text!)
        strFrom.addAttributes([NSAttributedStringKey.foregroundColor : colors.redColor], range: (strFrom.description as NSString).range(of: "To"))
        lblFromDate.attributedText = strFrom
        
        let strEnd = NSMutableAttributedString.init(string: lblToDate.text!)
        strEnd.addAttributes([NSAttributedStringKey.foregroundColor : colors.redColor], range: (strEnd.description as NSString).range(of: "To"))
        lblToDate.attributedText = strEnd
        
        let source = GMSMarker(position: CLLocationCoordinate2D(latitude: 31.86788, longitude: -94.1098))
        source.icon = #imageLiteral(resourceName: "trip_detail_pin_blue")
        source.map = map
        source.appearAnimation = .pop
        bounds = bounds.includingCoordinate(source.position)
        
        let current = GMSMarker(position: CLLocationCoordinate2D(latitude: 31.87404, longitude: -94.10471))
        current.icon = #imageLiteral(resourceName: "trip_detail_pin_orange")
        current.map = map
        current.appearAnimation = .pop
        
        bounds = bounds.includingCoordinate(current.position)
        
        arrCoordinates.append((31.87404,-94.10471))
        arrCoordinates.append((31.87364,-94.10504))
        arrCoordinates.append((31.87249,-94.10599))
        arrCoordinates.append((31.87231,-94.10613))
        arrCoordinates.append((31.872,-94.10639))
        arrCoordinates.append((31.87147,-94.10682))
        arrCoordinates.append((31.87078,-94.1074))
        arrCoordinates.append((31.87023,-94.10786))
        arrCoordinates.append((31.86963,-94.10836))
        arrCoordinates.append((31.86788,-94.1098))
        
        let icon = GMSMarker(position: CLLocationCoordinate2D(latitude: arrCoordinates[4].lat, longitude: arrCoordinates[4].long))
        
        let view : BrakePoint = BrakePoint.fromNib()
        view.lblDetails.text = "2 min , Satelite"
        
        
        icon.iconView = view
        icon.map = map
        //        icon.isFlat = true
        icon.tracksViewChanges = true
        bounds = bounds.includingCoordinate(icon.position)
        
        lblBreakPoint.text = "1. Stop 2 min in satelite"
        lblBreakPoint.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.redColor)
        
        let singleLine = GMSPolyline.init(path: path)
        
        DispatchQueue.main.async {
            var polyLine = GMSPolyline()
            polyLine = singleLine
            polyLine.map = nil
            polyLine.strokeWidth = 2.0
            polyLine.strokeColor = UIColor.black
            polyLine.map = self.map
            self.bounds = self.bounds.includingPath(polyLine.path!)
            
            self.map.animate(with: GMSCameraUpdate.fit(self.bounds, withPadding: 30))
            //            self.map.animate(toZoom: 10.0)
        }
    
    }
    
    
    func addCar()  {
        currentMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: 31.87404, longitude: -94.10471))
        currentMarker.icon = UIImage.init(named: "CarG")
        currentMarker.map = map
        currentMarker.appearAnimation = .pop
        bounds = bounds.includingCoordinate(currentMarker.position)
        //self.map.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100))
    }
    

    func removeCar()  {
        currentMarker.map = nil
    }
    
    @objc func changeLocation() {
        
        if currentIndex >= arrCoordinates.count {
            currentIndex = 0
            removeCar()
            self.currentMarker.position = CLLocationCoordinate2D(latitude: 31.87404, longitude: -94.10471)
            self.map.animate(with: GMSCameraUpdate.fit(self.bounds, withPadding: 30))
            //            self.map.animate(with: GMSCameraUpdate.setCamera(GMSCameraPosition(target: self.currentMarker.position, zoom: 16, bearing: self.preLocation.coordinate.getBearing(toPoint: self.currentMarker.position), viewingAngle: 0)))
            return
        }
        
        
        
        let currentLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: arrCoordinates[currentIndex].lat, longitude: arrCoordinates[currentIndex].long)
        
        
        if preLocation.coordinate.latitude != currentLocation.latitude {
            
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(2.0)
            let driverPosition  = currentLocation
            
            
            
            //only get bearing if driver change his location
            //            self.currentMarker.rotation = self.preLocation.coordinate.getBearing(toPoint: LocationManager.shared.getUserLocation().coordinate)
            
            self.currentMarker.position = driverPosition
            
            CATransaction.commit()
            
            
            //for bound the car
            //self.mapView.camera = GMSCameraPosition(target: self.currentMarker.position, zoom: 18, bearing: 0, viewingAngle: 0)
            
            /*
             self.mapView.moveCamera(GMSCameraUpdate.setCamera(GMSCameraPosition(target: LocationManager.shared().currentLocation.coordinate, zoom: 16, bearing: annotationDriver.position.getBearing(toPoint: LocationManager.shared().currentLocation.coordinate), viewingAngle: 0)))
             
             
             */
            
            
            
            self.map.animate(with: GMSCameraUpdate.setCamera(GMSCameraPosition(target: currentLocation, zoom: 16, bearing: self.preLocation.coordinate.getBearing(toPoint: currentLocation), viewingAngle: 0)))
            
            if currentIndex == 4 {
                currentIndex += 1
                self.preLocation = CLLocation.init(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
                self.perform(#selector(changeLocation), with: 0.0, afterDelay: 6.0)
                return
            }
            
            //            if !(GMSGeometryIsLocationOnPathTolerance(currentLocation, self.path!, true, 100)) {
            currentIndex += 1
            self.preLocation = CLLocation.init(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
            //changeLocation()
            self.perform(#selector(changeLocation), with: 0.0, afterDelay: 2.0)
            //            }
            
        }
    }
    
    @IBAction func btnPlayClicked(_ sender: Any) {
        
        if  currentIndex == 0 {
            
            addCar()
            currentIndex = 1
            preLocation = CLLocation.init(latitude: arrCoordinates[0].lat, longitude: arrCoordinates[0].long)
            self.currentMarker.position = CLLocationCoordinate2D(latitude: 31.87404, longitude: -94.10471)
            
            self.perform(#selector(changeLocation), with: 0.0, afterDelay: 2.0)
            
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    

    
    @IBAction func btnFullScreenMapClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseInOut], animations: {
            
            self.viewCarDetails.isHidden = sender.isSelected
            self.lblBreakPoint.isHidden = !sender.isSelected
            
            self.scroll.setContentOffset(CGPoint.zero, animated:true)
            if sender.isSelected {
                
                if #available(iOS 11.0, *) {
                    self.constMapHeight.constant = self.view.safeAreaLayoutGuide.layoutFrame.size.height
                } else {
                    
                    var height : CGFloat = 64.0
                    
                    if let heightBar = self.navigationController?.navigationBar.frame.size.height
                    {
                        height = CGFloat(heightBar)
                    }
                    
                    self.constMapHeight.constant = kScreenHeight - height
                }
                
                self.viewCarDetails.alpha =  0.0
                
            }else
            {
                
                self.constMapHeight.constant = kScreenHeight / 3.5
                self.viewCarDetails.alpha =  1.0
            }
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
}




//------------------------------------------------------

//MARK:- Table VIew Method

extension CompletedBookingDetailVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAlerts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : AlertDetailCell = tableView.dequeueReusableCell(withIdentifier: "AlertDetailCell") as! AlertDetailCell
        
        cell.lblMessage.text = arrAlerts[indexPath.section]
        cell.lblCarName.text = "Car name - ABC12345"
        cell.lblDate.text = "25/01/2017"
        cell.lblTime.text = "10:30 Am"
        
        switch arrAlerts[indexPath.section] {
            
        case kMisHappen:
            cell.imgIcon.image = #imageLiteral(resourceName: "trip_details_alert_mis")
            break
        case kNotinRange:
            cell.imgIcon.image = #imageLiteral(resourceName: "trip_details_alert_range")
            break
        case kAcOn:
            cell.imgIcon.image = #imageLiteral(resourceName: "trip_details_alert_ac")
            break
        case kSharpTurn:
            cell.imgIcon.image = #imageLiteral(resourceName: "trip_details_alert_turn")
            break
            
        default:
            
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj : VehiclesMapVC = HomeStoryBoard.instantiateViewController(withIdentifier: "VehiclesMapVC") as! VehiclesMapVC
        obj.title = "Track Vehicle"
        obj.isTrackLocation = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
}
