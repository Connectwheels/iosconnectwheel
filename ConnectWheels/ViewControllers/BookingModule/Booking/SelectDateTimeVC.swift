//
//  SelectDateTimeVC.swift
//  ConnectWheels
//
//  Created by  on 07/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit


class BookingCalendarCell : FSCalendarCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewBG: UIView!
    
 
    var isSetTime : Bool = false
    {
        didSet{
            
        }
    }
    
   
    func setSelectColor()  {
//        self.shapeLayer.isHidden = isSetTime ? false : !isSelected
//        titleLabel.textColor = self.isPlaceholder ? UIColor.lightGray : isSelected || dateIsToday ? colors.redColor : isSetTime ? colors.whiteColor : colors.blackColor
//        self.shapeLayer.fillColor =  isSelected ? UIColor.clear.cgColor : isSetTime ? colors.redColor.cgColor : UIColor.clear.cgColor
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
      
//        self.shapeLayer.borderWidth = 2.0
//        self.shapeLayer.borderColor = colors.redColor.cgColor
//        self.shapeLayer.isHidden = false
        
    }

    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    override func configureAppearance() {
        super.configureAppearance()
        // Override the build-in appearance configuration
        if self.isPlaceholder {
            self.eventIndicator.isHidden = true
            self.titleLabel.textColor = UIColor.lightGray
        }
    }
    
}

class SelectDateTimeVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var txtStartTime: FloatingCommon!
    @IBOutlet weak var txtEndTime: FloatingCommon!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    var datePicker = UIDatePicker()
    var dateFormatter = DateFormatter()
    var completion : completion!
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    var arrDates : [BookingTime] = []
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        calendar.dataSource = self
        calendar.delegate = self
        calendar.register(BookingCalendarCell.self, forCellReuseIdentifier: "BookingCalendarCell")
        calendar.swipeToChooseGesture.isEnabled = true
//        let scopeGesture = UIPanGestureRecognizer(target: calendar, action: #selector(calendar.handleScopeGesture(_:)));
//        calendar.addGestureRecognizer(scopeGesture)
//        calendar.collectionView.register(UINib(nibName: "BookingCalendarCell", bundle: nil), forCellWithReuseIdentifier: "BookingCalendarCell")
       // calendar.calendarHeaderView = UIView()
        calendar.appearance.titleDefaultColor = colors.blackColor
        calendar.appearance.titleFont = UIFont(name: fonts.montserratMediumFont, size: 10.0 * scaleFactor)
        calendar.appearance.weekdayTextColor = colors.greyColor
        calendar.appearance.borderRadius = 0
        calendar.appearance.headerDateFormat = "MMMM"
        
        calendar.appearance.todayColor = colors.redColor
        calendar.select(Date())
        calendar.appearance.titleSelectionColor = colors.redColor
        
        let formatterMonth = DateFormatter()
        formatterMonth.dateFormat = "MMMM"
        lblMonth.text = formatterMonth.string(from: calendar.currentPage)
        
        formatterMonth.dateFormat = "yyyy"
        lblYear.text = formatterMonth.string(from: calendar.currentPage)
        
        lblMonth.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 15.0, color: colors.blackColor)
        lblYear.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 12.0, color: colors.redColor)
        
        txtStartTime.RightPaddingView(image:#imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtStartTime.titleHeight(), width: 30, height: txtStartTime.frame.size.height - txtStartTime.titleHeight()))
        txtEndTime.RightPaddingView(image: #imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtEndTime.titleHeight(), width: 30, height: txtEndTime.frame.size.height - txtEndTime.titleHeight()))
        
        self.dateFormatter.dateFormat = "hh:mm a"
        
        datePicker.datePickerMode = .time
        txtStartTime.inputView = datePicker
        txtEndTime.inputView = datePicker
        
        
        
        txtStartTime.addAction(for: .editingDidBegin) { [unowned self] in
           
            if self.formatter.string(from: self.calendar.selectedDate!) == self.formatter.string(from: Date())
            {
                self.datePicker.minimumDate = Date()
            }else
            {
                self.datePicker.minimumDate = nil
            }
            
            
            let dateAtEnd = Calendar.current.date(bySettingHour: 22, minute: 59, second: 00, of: Date())
            self.datePicker.maximumDate = dateAtEnd
            
            if self.txtStartTime.text!.isEmpty{
                
                self.txtStartTime.text = self.dateFormatter.string(from: Date())
                self.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                let date = self.formatter.string(from: self.calendar.selectedDate!)
                
                if !self.arrDates.contains(where: { $0.date == date })
                {
                    
                    let model = BookingTime(date: date, fromTime: self.txtStartTime.text!, toTime: self.txtEndTime.text!)
                    self.arrDates.append(model)
                    
                }else
                {
                    if let index = self.arrDates.index(where: { $0.date == date }) , index < self.arrDates.count
                    {
                        self.arrDates[index].fromTime = self.txtStartTime.text
                        self.arrDates[index].toTime = self.txtEndTime.text
                    }
                }

            }
            
            
        }
        
        
        
        txtEndTime.addAction(for: .editingDidBegin) { [unowned self] in
            
            if self.formatter.string(from: self.calendar.selectedDate!) == self.formatter.string(from: Date())
            {
                self.datePicker.minimumDate = Date().addingTimeInterval(60*60*1)
            }
            
            self.datePicker.maximumDate = nil
            
            if self.txtStartTime.text!.isEmpty && self.txtEndTime.text!.isEmpty{
                
                self.txtStartTime.text = self.dateFormatter.string(from: Date())
                self.txtEndTime.text = self.dateFormatter.string(from: Date().addingTimeInterval(60*60*1))
                self.datePicker.minimumDate = Date().addingTimeInterval(60*60*1)
                
            }else if !self.txtStartTime.text!.isEmpty
            {
//                let startDate = Date(timeIntervalSince1970: self.arrWeekDays[indexPath.row].startTime)
//                self.txtEndTime.text = self.dateFormatter.string(from: startDate.addingTimeInterval(60*60*1))
//                self.datePicker.minimumDate = startDate.addingTimeInterval(60*60*1)
            }
            
            let date = self.formatter.string(from: self.calendar.selectedDate!)
            
            if !self.arrDates.contains(where: { $0.date == date })
            {
                
                let model = BookingTime(date: date, fromTime: self.txtStartTime.text!, toTime: self.txtEndTime.text!)
                self.arrDates.append(model)
                
            }else
            {
                if let index = self.arrDates.index(where: { $0.date == date }) , index < self.arrDates.count
                {
                    self.arrDates[index].fromTime = self.txtStartTime.text
                    self.arrDates[index].toTime = self.txtEndTime.text
                }
            }
            
        }
        
        datePicker.addAction(for: .valueChanged) { [unowned self] in
            
            if self.txtStartTime.isFirstResponder
            {
                self.txtStartTime.text = self.dateFormatter.string(from: self.datePicker.date)
                self.txtEndTime.text = self.dateFormatter.string(from: self.datePicker.date.addingTimeInterval(60*60*1))
                
                let date = self.formatter.string(from: self.calendar.selectedDate!)
                
                if let index = self.arrDates.index(where: { $0.date == date }) , index < self.arrDates.count
                {
                    self.arrDates[index].fromTime = self.txtStartTime.text
                    self.arrDates[index].toTime = self.txtEndTime.text
                }
               
            }
            
            if self.txtEndTime.isFirstResponder
            {
                self.txtEndTime.text = self.dateFormatter.string(from: self.datePicker.date)
                //self.timeDetail.endTime = self.datePicker.date.timeIntervalSince1970
                let date = self.formatter.string(from: self.calendar.selectedDate!)
                
                if let index = self.arrDates.index(where: { $0.date == date }) , index < self.arrDates.count
                {
                    self.arrDates[index].fromTime = self.txtEndTime.text
                    
                }
            }
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        
        guard let cb = completion else { return }
        cb(arrDates)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

extension SelectDateTimeVC : FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance{
    // MARK:- FSCalendarDataSource
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell : BookingCalendarCell = calendar.dequeueReusableCell(withIdentifier: "BookingCalendarCell", for: date, at: position) as! BookingCalendarCell
//        cell.isCurrentDate = false
//        cell.lblDate.text = cell.titleLabel.text
       
      //  cell.shapeLayer.isHidden = !cell.dateIsToday
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        cell.shapeLayer.isHidden = false
      self.configure(cell: cell, for: date, at: position)
        
    }
    

    
//    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
//        return 2
//    }
    
    // MARK:- FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendar.frame.size.height = bounds.height
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        
        if formatter.string(from: calendar.today!) == formatter.string(from: date) {
            return true
        }
        
        if Date().compare(date) == .orderedDescending {
            return false
        }
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.formatter.string(from: date))")
       
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
       // self.configureVisibleCells()
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        if arrDates.contains(where: { $0.date == formatter.string(from: date) }) {
            return colors.redColor
        }
        return UIColor.clear
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return UIColor.clear
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        return UIColor.clear
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        return colors.redColor
    }

    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        
        if arrDates.contains(where: { $0.date == formatter.string(from: date) }) {
            
            return colors.whiteColor
        }
        
        if calendar.today == date {
            return colors.redColor
        }
        return colors.blackColor
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let formatterMonth = DateFormatter()
        formatterMonth.dateFormat = "MMMM"
        lblMonth.text = formatterMonth.string(from: calendar.currentPage)
        
        formatterMonth.dateFormat = "yyyy"
        lblYear.text = formatterMonth.string(from: calendar.currentPage)
        
    }
    // MARK: - Private functions
    
    private func configureVisibleCells() {
        calendar.visibleCells().forEach { (cell) in
            let date = calendar.date(for: cell)
            let position = calendar.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        // let cell = (cell as! BookingCalendarCell)
        // cell.setSelectColor()
        
        if formatter.string(from: date) == formatter.string(from: calendar.selectedDate!) {
            
            if let index = arrDates.index(where: { $0.date == formatter.string(from: date) }) , index < arrDates.count
            {
                txtStartTime.text = arrDates[index].fromTime
                txtEndTime.text = arrDates[index].toTime
                
            }else
            {
                txtStartTime.text = ""
                txtEndTime.text = ""
            }
            
            return
        }
    
        if arrDates.contains(where: { $0.date == formatter.string(from: date) }) {
          
            cell.shapeLayer.fillColor = colors.redColor.cgColor
            cell.titleLabel.textColor = colors.whiteColor
            
            
        }

    }
    
   
}
