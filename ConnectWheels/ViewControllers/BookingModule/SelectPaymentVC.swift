//
//  SelectPaymentVC.swift
//  ConnectWheels
//
//  Created by  on 18/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class SelectPaymentVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var tblPayment: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        lblTotalAmount.montserrat(ofType: fonts.montserratBoldFont, withfontSize: 25.0, color: colors.redColor)
        tblPayment.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: tblPayment.frame.size.width, height: 10))
        tblPayment.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblPayment.frame.size.width, height: btnAdd.frame.size.height + 30))
        
        btnAdd.addAction(for: .touchUpInside) { [unowned self] in
            let obj : AddCardVC = BookingStoryBoard.instantiateViewController(withIdentifier: "AddCardVC") as! AddCardVC
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension SelectPaymentVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : PaymentCell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
        
        cell.imgPaymentType.image = #imageLiteral(resourceName: "visa")
        
        switch indexPath.section {
        case 0:
            cell.lblCardNumber.text = "Paytm"
            cell.imgPaymentType.image = #imageLiteral(resourceName: "paytm")
            break
        case 1:
            cell.lblCardNumber.text = "xxxx xxxx xxxx 1234"
            break
        case 2:
            cell.lblCardNumber.text = "xxxx xxxx xxxx 4567"
            break
        case 3:
            cell.lblCardNumber.text = "xxxx xxxx xxxx 8910"
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    
}

//------------------------------------------------------

//MARK:- PaymentCell

class PaymentCell : UITableViewCell {
    
    @IBOutlet weak var imgPaymentType: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblCardNumber: UILabel!
    
    
    override func awakeFromNib() {
        
        lblCardNumber.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
     viewBG.dropShadow()
        
    }
    
    
    
}
