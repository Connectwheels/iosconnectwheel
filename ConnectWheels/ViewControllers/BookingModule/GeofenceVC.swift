//
//  GeofenceVC.swift
//  ConnectWheels
//
//  Created by  on 13/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class GeofenceVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var slideBrake: GBSliderBubbleView!
    @IBOutlet weak var btnIn: UIButton!
    @IBOutlet weak var btnOut: UIButton!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    var circle : GMSCircle!
    let marker = GMSMarker()
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func SetUpSlider(slider : GBSliderBubbleView)  {
        slider.delegate = self
        slider.isBubbleHideAnimation = false
        slider.maxValue = 100
        slider.thumbImage = #imageLiteral(resourceName: "advance_setting_selector")
        var frame = slider.frame
        frame.size.width = kScreenWidth - 50
        slider.frame = frame
        slider.renderSliderBubbleView()
        slider.bubbleView.backgroundColor = UIColor.clear
        slider.slider.minimumTrackTintColor = colors.blueColor
        slider.slider.maximumTrackTintColor = colors.blueColor
        slider.valueLabel.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.blueColor)
        
    }
    
    func setUpView() {
        
        
        
        let loc = CLLocationCoordinate2D(latitude: 31.87404, longitude: -94.10471)
        let distance = 50.0 * 1000// 1609.34 for miles
        marker.position = loc
        
        slideBrake.defaultValue = 50.0
        circle = GMSCircle(position: marker.position , radius: CLLocationDistance(distance))
        
        marker.icon = #imageLiteral(resourceName: "home_location")
        marker.map = map
        
        var bounds = GMSCoordinateBounds()
        
        bounds = bounds.includingCoordinate(marker.position)
        circle.radius = CLLocationDistance(distance)
        
        circle.fillColor = colors.blueColor.withAlphaComponent(0.5)
        circle.strokeColor = colors.blueColor
        circle.map = map
        map.animate(with: GMSCameraUpdate.fit(circle.bounds, withPadding: 50.0))
        
        slideBrake.unit = "Km"
        SetUpSlider(slider: slideBrake)
        
        self.navigationItem.rightBarButtonItem?.tintColor = colors.whiteColor
      //  self.navigationController!.navigationItem.leftBarButtonItem!.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "Andes Rounded", size: 17)!], forState: .Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratMediumFont, size: 11.0 * scaleFactor) as Any], for: .normal)
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnLocationCLicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnInOutClicked(_ sender: UIButton) {
       btnIn.isSelected = sender == btnIn
        btnOut.isSelected = !btnIn.isSelected
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

   

}

//------------------------------------------------------

//MARK:- Slider Delegate

extension GeofenceVC : GBSliderBubbleViewDelegate
{
    func getSliderDidEndChangeValue(_ value: Int) {
        
    }
    
    func getSliderDidChangeValue(_ value: Int) {
        circle.radius = CLLocationDistance(Double(value) * 1000)
        map.animate(with: GMSCameraUpdate.fit(circle.bounds, withPadding: 50.0))
    }
}

//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension GeofenceVC : GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
       
        
        dismiss(animated: true) {
            self.lblLocation.text = place.formattedAddress
            
            DispatchQueue.main.async {
                self.circle.position = place.coordinate
                self.marker.position = place.coordinate
                self.map.animate(with: GMSCameraUpdate.fit(self.circle.bounds, withPadding: 50.0))
            }
        }
        

    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
}

extension GMSCircle {
    var bounds: GMSCoordinateBounds {
        return [0, 90, 180, 270].map {
            GMSGeometryOffset(position, radius, $0)
            }.reduce(GMSCoordinateBounds()) {
                $0.includingCoordinate($1)
        }
    }
}

