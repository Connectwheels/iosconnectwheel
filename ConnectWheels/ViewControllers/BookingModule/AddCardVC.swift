//
//  AddCardVC.swift
//  ConnectWheels
//
//  Created by  on 23/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit



class AddCardVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtCardHolderName: FloatingCommon!
    @IBOutlet weak var txtCardNumber: FloatingCommon!
    @IBOutlet weak var txtExpiryDate: ExpiryDate!
    @IBOutlet weak var txtCvv: FloatingCommon!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{
        
        //Check field fullfill valiadtion or not
        if txtCardHolderName.text!.isEmpty {
            return kCardHolderNameEmpty
        }else if txtCardNumber.text!.isEmpty {
            return kCardNumberEmpty
        }else if txtCardNumber.text!.count < 15{
            return kCardNumberInvalid
        }else if !txtExpiryDate.isValid() {
            return txtExpiryDate.errorMessageExp
        }else if txtCvv.text!.isEmpty {
            return kCvvEmpty
        }else if (txtCvv.text?.count)! < 3 {
            return kCvvInvalid
        }
        
        return String()
    }
    
    func setUpView() {
        
        txtExpiryDate.RightPaddingView(image: #imageLiteral(resourceName: "imgCalendar").withRenderingMode(.alwaysTemplate), frame: CGRect(x: 0, y: txtExpiryDate.titleHeight(), width: 30, height: txtExpiryDate.frame.size.height - txtExpiryDate.titleHeight()))
        txtExpiryDate.tintColor = colors.greyColor
        
        txtCardHolderName.defaultcharacterSet = ((CharacterSet.letters).union(CharacterSet.whitespaces))
        txtCardNumber.regex = "^[0-9]{0,16}$"
        txtCvv.regex = "^[0-9]{0,4}$"
//        txtExpiryDate.regex = "^([0-9]{0,2}+)(\\([0-9]{0,2})?)?$"
     
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    @IBAction func btnAddClicked(_ sender: UIButton) {
        
        
        let error = validate()
        
        view.endEditing(true)
        
        if error != String() {
            
            GFunction.ShowAlert(message: error)
            
            return
        }
        
        //GFunction.ShowAlert(message: "Successful")

        
        self.navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Text Filed Delegate Method



class ExpiryDate : FloatingCommon  {
    
    var errorMessageExp = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.delegate = self
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && (string == " ") {
            return false
        }
        
        if string.isEmpty 
        {
            textField.text = ""
            return false
        }
        
//        let regex = "^([0-9]{0,5}+)(\\/([0-9]{0,4})?)?$"
//        let backSpaceregex = "^([0-9]{0,3}+)(\\/([0-9]{0,5})?)?$"
//        let regexPattern = try! NSRegularExpression(pattern: string.isEmpty ? backSpaceregex : regex, options: [])
        
        if CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string)) {
           
            if !string.isEmpty && textField.text!.count > 6
            {
                return false
            }
            
            let groupSize = 3
            let separator = "/"
            
            let formatter = NumberFormatter()
            formatter.groupingSeparator = separator
            formatter.groupingSize = groupSize
            formatter.usesGroupingSeparator = true
            formatter.secondaryGroupingSize = 2
            if var number = textField.text, string != "" {
                number = number.replacingOccurrences(of: separator, with: "")
                if let doubleVal = Double(number) {
                    let requiredString = formatter.string(from: NSNumber.init(value: doubleVal))
                    textField.text = requiredString
                }
                
            }
            
            return true
        }
        
        
        
        return false
    }
    
    /*
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && (string == " ") {
            return false
        }
        
        
        
        let regex = "^([0-9]{0,2}+)(\\/([0-9]{0,4})?)?$"
        let regexPattern = try! NSRegularExpression(pattern: regex, options: [])
        
        if range.location == 2
        {
            if string.isEmpty
            {
                return false
                
            }else
            {
                if CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))
                {
                    self.text = self.text?.appending("/").appending(string)
                    
                }
                
                return false
            }
        }
        
        if range.location == 3 && string.isEmpty
        {
            //txtExpiryDate.text = txtExpiryDate.
            if self.text!.count > 4
            {
                return true
            }
            
            self.text = self.text!.dropLast(2).description
            
            return false
        }
        
        if (range.location == 0 || range.location == 1) && textField.text!.contains("/") && CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string)) {
            return true
        }
        
        
        
        if range.location == 1 && !string.isEmpty && regexPattern.matches(in: textField.text! + string, options: [], range: NSRange(location: 0, length: (textField.text! + string).count)).count > 0 {
            self.text = self.text?.appending(string).appending("/")
            return false
        }
        
        return regexPattern.matches(in: textField.text! + string, options: [], range: NSRange(location: 0, length: (textField.text! + string).count)).count > 0
        
        
    }
    */
    func isValid() -> Bool {
        
        errorMessageExp = ""
        
        if self.text!.isEmpty {
            errorMessageExp = "Please enter expiry date"
            return false
        }
        
        let currentYear = Calendar.current.component(.year, from: Date())
        
        let currentMonth = Calendar.current.component(.month, from: Date())
        
        if let arrDates = self.text?.components(separatedBy: "/")
        {
            
            if arrDates.isEmpty {
                
                errorMessageExp = "Please enter valid expiry date"
                return false
            }
            
            if let month = arrDates.first , let monthInt = Int(month.description)
            {
                if monthInt > 12
                {
                    errorMessageExp = "Please enter valid month"
                    return false
                }
                
                if let year = arrDates.last, !(year.count < 4) , let yearInt = Int(year)
                {
                    
                    if monthInt <= currentMonth && yearInt == currentYear
                    {
                        errorMessageExp = "Please enter valid month"
                        return false
                    }
                    
                    
                    if yearInt < currentYear
                    {
                        errorMessageExp = "Please enter valid year"
                        return false
                    }
                    
                }else
                {
                    errorMessageExp = "Please enter valid year"
                    return false
                }
                
            }else
            {
                errorMessageExp = "Please enter valid month"
                return false
            }
            
        }else
        {
            errorMessageExp = "Please enter valid month"
            return false
        }
        
        return true
    }
}
