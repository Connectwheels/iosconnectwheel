//
//  RechargeListVC.swift
//  ConnectWheels
//
//  Created by  on 18/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class RechargeListVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var tblRecharge: UITableView!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        tblRecharge.tableHeaderView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- Table VIew Method

extension RechargeListVC : UITableViewDelegate , UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : RechargeListCell = tableView.dequeueReusableCell(withIdentifier: "RechargeListCell") as! RechargeListCell
        
        cell.imgCar.image = #imageLiteral(resourceName: "2.jpg")
        
        if indexPath.section <= 1 {
            
            cell.lblRechargeDetail.attributedText = NSAttributedString(string: "Recharge Finished!", attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratLightFont, size: 8.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : UIColor.red])
            
            cell.btnRecharge.isHidden = false
            
        }else
        {
            let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(string: "Recharge Due Date : ", attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratLightFont, size: 8.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.blackColor]))
            
            attributedString.append(NSAttributedString(string: "25/03/2018", attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratLightFont, size: 8.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor]))
            
           
            cell.lblRechargeDetail.attributedText = attributedString
            
            cell.btnRecharge.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section <= 1 {
            let obj : SelectPaymentVC = BookingStoryBoard.instantiateViewController(withIdentifier: "SelectPaymentVC") as! SelectPaymentVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        viewFooter.backgroundColor = UIColor.clear
        return viewFooter
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
  
    
}


//------------------------------------------------------

//MARK:- RechargeListCell

class RechargeListCell : UITableViewCell {
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblRechargeDetail: UILabel!
    @IBOutlet weak var lblRegNumber: UILabel!
    @IBOutlet weak var btnRecharge: UIButton!
    
    
    
    override func awakeFromNib() {
        
        lblCarName.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
//        lblRechargeDetail.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblRegNumber.montserrat(ofType: fonts.montserratLightFont, withfontSize: 8.0, color: colors.greyColor)
      
        viewBG.dropShadow()
        btnRecharge.setLayout(detail: [layout.kRadiusRatio : 2,
                                layout.kfont : UIFont(name: fonts.montserratSemiBoldFont, size: 8.0 * scaleFactor) as Any,
                                layout.kdefaultBackGroundColor : colors.redColor,
                                layout.ktitleColor : colors.whiteColor])
    }
    
    
    
}
