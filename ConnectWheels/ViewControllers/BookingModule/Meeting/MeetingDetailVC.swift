//
//  MeetingDetailVC.swift
//  ConnectWheels
//
//  Created by  on 11/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class MeetingDetailVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet var lblTitles: [UILabel]!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblBeforeAlert: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        _ = lblTitles.map({ (lable) -> Void  in
            lable.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        })
        
        lblUserName.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 15.0, color: colors.blackColor)
        lblDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.greyColor)
        lblTime.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.greyColor)
        lblBeforeAlert.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.greyColor)
        lblLocation.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 11.0, color: colors.greyColor)
        lblDescription.montserrat(ofType: fonts.montserratLightFont, withfontSize: 11.0, color: colors.greyColor)
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}
