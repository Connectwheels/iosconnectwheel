//
//  CreateMeetingVC.swift
//  ConnectWheels
//
//  Created by  on 11/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class CreateMeetingVC: BlueNavigationBar {

    //MARK:- Outlet
    
    //------------------------------------------------------
    
    @IBOutlet weak var txtMeetingWith: FloatingCommon!
    @IBOutlet weak var txtLocation: FloatingCommon!
    @IBOutlet weak var txtSelectDate: FloatingTime!
    @IBOutlet weak var txtSelectTime: FloatingTime!
    @IBOutlet weak var txtAlert: FloatingCommon!
    @IBOutlet weak var txtDescription: IQTextView!
    @IBOutlet weak var btnCreate: ThemeActionButton!
    
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    
    let formatter = DateFormatter()
    var arrPickrList : [String] = [String]()
    var picker = UIPickerView()
    
    var isEdit = false
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func validate() -> String{

        
        //Check field fullfill valiadtion or not
        if txtMeetingWith.text!.isEmpty {
            return kvBlankMeetingWith
        }else if txtLocation.text!.isEmpty {
            return kvBlankLocation
        }else if txtSelectDate.text!.isEmpty {
            return kvBlankSelectDate
        }else if txtSelectTime.text!.isEmpty {
            return kvBlankSelectTime
        }else if txtAlert.text!.isEmpty {
            return kvBlankSelectAlertBefore
        }else if txtDescription.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty {
            return kvBlankDescription
        }
        
        return String()
    }
    
    func setUpView() {
        
        txtDescription.font = txtMeetingWith.font
        txtLocation.RightPaddingView(image: #imageLiteral(resourceName: "imgLocationGray"), frame: CGRect(x: 0, y: txtLocation.titleHeight(), width: 30, height: txtLocation.frame.size.height - txtLocation.titleHeight()))
         txtSelectTime.RightPaddingView(image: #imageLiteral(resourceName: "imgTime"), frame: CGRect(x: 0, y: txtLocation.titleHeight(), width: 30, height: txtLocation.frame.size.height - txtLocation.titleHeight()))
        txtSelectDate.RightPaddingView(image: #imageLiteral(resourceName: "Bcalendar"), frame: CGRect(x: 0, y: txtLocation.titleHeight(), width: 30, height: txtLocation.frame.size.height - txtLocation.titleHeight()))
         txtAlert.RightPaddingView(image: #imageLiteral(resourceName: "notification"), frame: CGRect(x: 0, y: txtLocation.titleHeight(), width: 30, height: txtLocation.frame.size.height - txtLocation.titleHeight()))
        
        datePicker.datePickerMode = .date
        timePicker.datePickerMode = .time
        
        txtMeetingWith.defaultcharacterSet =  (CharacterSet.letters).union(CharacterSet.whitespaces)
        
        datePicker.minimumDate = Date()
        
        datePicker.addAction(for: .valueChanged) { [unowned self] in
            self.formatter.dateFormat = "dd MMM yyyy"
            self.txtSelectDate.text = self.formatter.string(from: self.datePicker.date)
        }
        
        timePicker.addAction(for: .valueChanged) { [unowned self] in
            self.formatter.dateFormat = "hh:mm a"
            self.txtSelectTime.text = self.formatter.string(from: self.timePicker.date)
        }
        
        txtSelectDate.inputView = datePicker
        txtSelectTime.inputView = timePicker
        
         arrPickrList = ["2 Mins" , "5 Mins" ,"10 Mins" ,"15 Mins" ,"30 Mins"]
        
        picker.delegate = self
        picker.dataSource = self
        
        txtAlert.inputView = picker
        
        txtSelectTime.addAction(for: .editingDidEnd) { [unowned self] in
            self.formatter.dateFormat = "hh:mm a"
            self.txtSelectTime.text = self.formatter.string(from: self.timePicker.date)
        }
        
        txtSelectDate.addAction(for: .editingDidEnd) { [unowned self] in
            self.formatter.dateFormat = "dd MMM yyyy"
            self.txtSelectDate.text = self.formatter.string(from: self.datePicker.date)
        }
        
        DispatchQueue.main.async {
            if self.isEdit
            {
                self.title = "Edit Meeting"
                self.txtMeetingWith.text = "User name"
                self.txtLocation.text = "3599 Washington Street Corpus Christi"
                self.txtSelectDate.text = "28 Jun 2018"
                self.txtSelectTime.text = "05:24 Pm"
                self.txtAlert.text = "10 Mins"
                self.txtDescription.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
                self.btnCreate.setTitle("Update", for: .normal)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    @IBAction func btnCreateClicked(_ sender: Any) {
        
        let error = validate()
        
        view.endEditing(true)
        
        if error != String() {
            
            GFunction.ShowAlert(message: error)
            
            return
        }
        
        //GFunction.ShowAlert(message: "Successful")
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectAreaClicked(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}


//------------------------------------------------------

//MARK:- GMSPlacePickerViewControllerDelegate

extension CreateMeetingVC: GMSAutocompleteViewControllerDelegate {
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtLocation.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
    }
}


extension CreateMeetingVC : UIPickerViewDelegate , UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPickrList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrPickrList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtAlert.text = arrPickrList[row]
    }
}
