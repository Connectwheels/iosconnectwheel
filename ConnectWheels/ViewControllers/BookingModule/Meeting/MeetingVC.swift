//
//  MeetingVC.swift
//  ConnectWheels
//
//  Created by  on 11/06/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import UIKit

class MeetingVC: BlueNavigationBar , UIGestureRecognizerDelegate{

    //MARK:- Outlet
    
    //------------------------------------------------------
    @IBOutlet weak var tblMeeting: UITableView!
    
    @IBOutlet weak var calendar: FSCalendar!
   @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblMeetingCount: UILabel!
    //MARK:- Class Variable
    
    //------------------------------------------------------
    
    fileprivate lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendar, action: #selector(self.calendar.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    //MARK:- Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        
    }
    
    //------------------------------------------------------
    
    //MARK:- Custom Method
    
    func setUpView() {
        calendar.dataSource = self
        calendar.delegate = self
        self.calendar.select(Date())
        
        self.view.addGestureRecognizer(self.scopeGesture)
        self.tblMeeting.panGestureRecognizer.require(toFail: self.scopeGesture)
//        self.calendar.scope = .week
        
        calendar.appearance.titleDefaultColor = colors.blackColor
        calendar.appearance.titleFont = UIFont(name: fonts.montserratMediumFont, size: 10.0 * scaleFactor)
        calendar.appearance.weekdayTextColor = colors.greyColor
        calendar.appearance.borderRadius = 0
        calendar.appearance.headerDateFormat = "MMMM"
        
       
        
        calendar.appearance.todayColor = colors.redColor
        
        lblMeetingCount.montserrat(ofType: fonts.montserratMediumFont, withfontSize: 13.0, color: colors.blackColor)
        
        let attributedText = NSMutableAttributedString(string: "Meeting")
        attributedText.append(NSAttributedString(string: " (5)", attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.greyColor]))
        
        lblMeetingCount.attributedText = attributedText
        
        calendar.appearance.titleSelectionColor = colors.redColor
        
        let formatterMonth = DateFormatter()
        formatterMonth.dateFormat = "MMMM"
        lblMonth.text = formatterMonth.string(from: calendar.currentPage)
        
        formatterMonth.dateFormat = "yyyy"
        lblYear.text = formatterMonth.string(from: calendar.currentPage)
        
        lblMonth.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 15.0, color: colors.blackColor)
        lblYear.montserrat(ofType: fonts.montserratSemiBoldFont, withfontSize: 12.0, color: colors.redColor)
        
        calendar.appearance.separators = FSCalendarSeparators.init(rawValue: 0)!
       
        tblMeeting.tableFooterView = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 100))
    }
    
    // MARK:- UIGestureRecognizerDelegate
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.tblMeeting.contentOffset.y <= -self.tblMeeting.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendar.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
    //------------------------------------------------------
    
    //MARK:- Action Method
    
    
    //------------------------------------------------------
    
    //MARK:- Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


}

//------------------------------------------------------

//MARK:- FSCalandar Methods

extension MeetingVC : FSCalendarDataSource, FSCalendarDelegate , FSCalendarDelegateAppearance
{
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
   
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        if cell.isPlaceholder {
            cell.eventIndicator.isHidden = true
            cell.titleLabel.textColor = UIColor.lightGray
        }
        
        if formatter.string(from: date) == formatter.string(from:Date().addingTimeInterval(60*60*24*5)) {
            let view = UIView()
            view.frame = CGRect(x: (cell.frame.size.width / 2) - 3.0, y: (cell.frame.size.height - 1.0), width: 6.0, height: 2.0)
            view.backgroundColor = colors.blueColor
            cell.eventIndicator.isHidden = false
            
            cell.eventIndicator.addSubview(view)
        }
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.formatter.string(from: date))")
       
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
        print("did deselect date \(self.formatter.string(from: date))")
        
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
       
        return UIColor.clear
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return UIColor.clear
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        return UIColor.clear
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        return colors.redColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
      
        if calendar.today == date {
            return colors.redColor
        }
        return colors.blackColor
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        if formatter.string(from: date) == formatter.string(from:Date().addingTimeInterval(60*60*24*5)) {
            return 0
        }
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
  
        return [colors.blueColor]
    }
    
    
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let formatterMonth = DateFormatter()
        formatterMonth.dateFormat = "MMMM"
        lblMonth.text = formatterMonth.string(from: calendar.currentPage)
        
        formatterMonth.dateFormat = "yyyy"
        lblYear.text = formatterMonth.string(from: calendar.currentPage)
        
    }
}

//------------------------------------------------------

//MARK:- Table VIew Method

extension MeetingVC : UITableViewDelegate , UITableViewDataSource
{
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MettingCell = tableView.dequeueReusableCell(withIdentifier: "MettingCell") as! MettingCell
        
          cell.rightSwipeSettings.transition = .drag
        
        let edit = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "editVehicle"), backgroundColor: colors.blueColor) { (cell) -> Bool in
            let obj : CreateMeetingVC = BookingStoryBoard.instantiateViewController(withIdentifier: "CreateMeetingVC") as! CreateMeetingVC
            
            obj.isEdit = true
            self.navigationController?.pushViewController(obj, animated: true)
            return true
        }
        
        edit.buttonWidth = 60
        
         cell.rightButtons = [edit]
        
        let attributedString = NSMutableAttributedString(attributedString: NSAttributedString(string: "30", attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratSemiBoldFont, size: 10.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.redColor]))
        
        attributedString.append(NSAttributedString(string: " Min", attributes: [NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont, size: 8.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.redColor]))
        
        cell.lblTime.attributedText = attributedString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let obj : MeetingDetailVC = BookingStoryBoard.instantiateViewController(withIdentifier: "MeetingDetailVC") as! MeetingDetailVC
        self.navigationController?.pushViewController(obj, animated: true)
        
        
    }
    
    
    
 
    
    
}

//------------------------------------------------------

//MARK:- Meeting Cell

class MettingCell: MGSwipeTableCell {
    
    
  
    @IBOutlet weak var lblMettingUser: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblBeforeAlert: UILabel!
    
    
    
    
    override func awakeFromNib() {
        
        lblMettingUser.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.blackColor)
        lblDate.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblDetails.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        lblTime.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 10.0, color: colors.redColor)
        lblBeforeAlert.montserrat(ofType: fonts.montserratRegularFont, withfontSize: 8.0, color: colors.greyColor)
        
      
    }
    
    
    
}
