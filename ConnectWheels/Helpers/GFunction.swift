
//  GFunction.swift
//  Ned
//
//  Created by - on 15/06/17.
//  Copyright © 2017 -. All rights reserved.
//

import UIKit

import BFRadialWaveHUD



class GFunction: NSObject  {

    static let sharedInstance : GFunction = GFunction()
    
    var hud = BFRadialWaveHUD()
    
    //--------------------------------------------------------------------------
    //MARK:- Set Home Sceen
    
    class func SetHomeScreen() {
        
        
        
        if let _ = UserDefault.value(forKey: kUserDetail) {
            let startUpVC : UINavigationController = HomeStoryBoard.instantiateViewController(withIdentifier: "NaviHomeVC") as! UINavigationController
            appDelegate.window?.rootViewController = startUpVC
            appDelegate.window?.makeKeyAndVisible()
        }else
        {
            setLoginVC()
        }
        
        
    }
    
    //--------------------------------------------------------------------------
    //MARK:- Set Login VC
    
    class func setLoginVC() {
        
        VehicleDataSource.shared.clearData()
        
        let startUpVC : UINavigationController = AuthStoryBoard.instantiateViewController(withIdentifier: "NaviLoginVC") as! UINavigationController
        startUpVC.navigationBar.isTranslucent = true
        appDelegate.window?.rootViewController = startUpVC
        appDelegate.window?.makeKeyAndVisible()
    }
    
    class func logoutUser()  {
        VehicleDataSource.shared.isEmergency = false
        VehicleDataSource.shared.clearData()
        UserDefault.removeObject(forKey: kUserDetail)
        UserDefault.removeObject(forKey: kUserToken)
        UserDefault.synchronize()
        setLoginVC()
    }
    
    
    //--------------------------------------------------------------------------
    //MARK:- Valid Email
    
    class func isValidEmail(text : String) -> Bool {
        let emailRegex: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)
     }
    
    //--------------------------------------------------------------------------
    //MARK:- Show Alert
    
    class func ShowAlert(message : String , view : UIViewController? = appDelegate.window!.rootViewController!) {
        
      
        let alert = UIAlertController(title: nil, message: message.Localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: keyOK, style: .default, handler: nil))

        if let presentedView = appDelegate.window?.rootViewController?.presentedViewController
        {
            presentedView.present(alert, animated: true, completion: nil)
            return
        }
//
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
//        if let _ = appDelegate.window {
//
//            let hud = MBProgressHUD.init(view: appDelegate.window!)
//            hud.bezelView.color = UIColor.black.withAlphaComponent(0.7)
//            hud.bezelView.layer.borderColor = UIColor.clear.cgColor
//            hud.bezelView.layer.borderWidth = 1.0
//            hud.bezelView.layer.cornerRadius = 5.0
//            hud.mode = .text
//            hud.label.text = message.Localized()
//            hud.label.textColor = UIColor.white
//            hud.label.font = UIFont(name: fonts.regularFont , size: 15.0 * scaleFactor)
//            hud.margin = 10 // 0, MBProgressMaxOffset
////            hud.offset = CGPoint(x: 0, y: MBProgressMaxOffset)
//            hud.label.numberOfLines = 0
//            hud.bezelView.style = .solidColor
//            appDelegate.window?.addSubview(hud)
//            hud.removeFromSuperViewOnHide = true
//            hud.show(animated: true)
//            hud.hide(animated: true, afterDelay: TimeInterval.init(2.0))
//
//        }
        
        
        
    }
    
    //--------------------------------------------------------------------------------------
    
    //MARK:- Notification services check
    
    class func isNotificationServiceEnable () -> Bool {
        
        let curruntSettings = UIApplication.shared.currentUserNotificationSettings

        if curruntSettings?.types == [] {

            let action_Settings                 = UIAlertAction(title: "Setting", style: .default) { (UIAlertAction) in
                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
            }
            let alert     : UIAlertController   = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            alert.message                       = "Please go to Settings and turn on Notification Service for YallaKoora.".Localized()
            alert.title                         = "Notification Service".Localized()

            let action_Cancel                   = UIAlertAction(title: "Cancel".Localized(), style: .cancel) { (UIAlertAction) in

            }


            alert.addAction(action_Cancel)
            alert.addAction(action_Settings)

            if let present = appDelegate.window?.rootViewController?.presentedViewController {

                present.present(alert, animated: true, completion: nil)
                return false
            }

            //  alert.show()
            (appDelegate.window?.rootViewController)!.present(alert, animated: true, completion: nil)

            return false
        }
        return true
    }
    
   
    
    func addLoader() {
        
        DispatchQueue.main.async {
            
//            self.progressHud = MBProgressHUD.init(view: appDelegate.window!)
//            self.progressHud.isSquare = true
////            self.progressHud.mode = .annularDeterminate
//            appDelegate.window?.addSubview(self.progressHud)
//            self.progressHud.removeFromSuperViewOnHide = true
//            //self.progressHud.bezelView.color = colors.blueColor
//
////            self.progressHud.bezelView.style = .solidColor
//            self.progressHud.show(animated: true)
//
            if !self.hud.isShowing{
                self.hud = BFRadialWaveHUD.init(fullScreen: true, circles: 20, circleColor: colors.redColor, mode: BFRadialWaveHUDMode.default, strokeWidth: 3.0)
                self.hud.blurBackground = true
                self.hud.hudColor = UIColor.clear
                self.hud.show(in: appDelegate.window)
            }
            
        }
    }
    
    func removLoader()  {
        DispatchQueue.main.async {
            self.hud.dismiss()
        }
    }
    
    func showUpcommingAlert(){
        let alert = UIAlertController(title: nil, message: "Comming Soon".Localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: keyOK, style: .default, handler: nil))
        
        if let presentedView = appDelegate.window?.rootViewController?.presentedViewController
        {
            presentedView.present(alert, animated: true, completion: nil)
            return
        }
        //
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
   

    class func addBounceAnimation(sender : UIView, WithBlock completion : @escaping () -> Void) {
        
        
        sender.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.autoreverse , . curveEaseInOut], animations: {
            
            sender.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            
            
        }) { (isCompleted) in
            
            sender.isUserInteractionEnabled = true
            sender.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            completion()
        }
    
    }
    
    func openContactVC(from : UIViewController , completionBlock : completion?){
        
        let obj : SearchContactVC  = AuthStoryBoard.instantiateViewController(withIdentifier: "SearchContactVC") as! SearchContactVC
        let navi = UINavigationController.init(rootViewController: obj)
        obj.completion = { data in
            if let details = data as? UserContact , let cb = completionBlock{
                cb(details)
            }
        }
        from.present(navi, animated: true, completion: nil)
    }
    
    func getStatusWiseImage(type : String, status : status) -> UIImage {
        var imgName = VehicleTypes.getImageName(type: type)
        imgName = imgName + status.rawValue
        if let image = UIImage.init(named: imgName){
           return image
        }
        
        return UIImage.init(named: "CarG") ?? UIImage()
    }
    
    func share(vc : UIViewController, link : String , image : UIImage? = UIImage()) {
        
        let controller = UIActivityViewController(activityItems: [image! , link], applicationActivities: nil)
        
        vc.present(controller, animated: true, completion: nil)
        
    }
   
}


enum status : String{
    case off = "R"
    case moving = "G"
    case idel = "Y"
    case notinrange = "Gr"
}


