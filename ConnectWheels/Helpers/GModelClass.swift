//
//  GModelClass.swift
//  ConnectWheels
//
//  Created by  on 03/05/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import Foundation
import UIKit




//------------------------------------------------------

//MARK:- Upload Image

class UploadModel : NSObject {
    var image : UIImage!
    var title : String!
    var isDefaultImage : Bool = true
    var selectedImage : UIImage? = UIImage()
    var expiresOn : String = ""
    
    init(title : String , image : UIImage) {
        self.title = title
        self.image = image
        
    }
    
    override init() {
        
    }
    
    init(title : String , image : UIImage , selectedImage : UIImage , isDefaultImage : Bool ) {
        self.title = title
        self.image = image
        self.selectedImage = selectedImage
        self.isDefaultImage = isDefaultImage
        
    }
}

//------------------------------------------------------

//MARK:- Week Time Model

class WeeKDays : NSObject , mappable {
   
    
    var id : Int = 0
    var day : String!
    var startTime : Double!
    var endTime : Double!
    var startTime2 : Double!
    var endTime2 : Double!
    var keyDay : String = ""
    
    required init(fromJson json: JSON) {
        day = json["day"].stringValue
        startTime = json["start_time"].doubleValue
        endTime = json["end_time"].doubleValue
        startTime2 = json["start_time2"].doubleValue
        endTime2 = json["end_time2"].doubleValue
        id = json["id"].intValue
    }
    
    override init() {
       
        day = ""
        startTime = 0
        endTime = 0
        startTime2 = 0
        endTime2 = 0
        id = 0
    }
    
    func toDictionary() -> [String : Any] {
        var dictionary = [String:Any]()
        if day != nil{
            dictionary["day"] = keyDay
        }
        if startTime != nil{
            dictionary["start_time"] = Int32(startTime)
        }
        if endTime != nil{
            dictionary["end_time"] = Int32(endTime)
        }
//        if startTime2 != nil{
//            dictionary["start_time2"] = Int32(startTime2)
//        }
//        if endTime2 != nil{
//            dictionary["end_time2"] = Int32(endTime2)
//        }
        
        return dictionary
    }
}


//------------------------------------------------------

//MARK:- Car Detail Model

class CarDetails : NSObject {
    var name : String!
    var make : String!
    var regNo : String!
    var model : String!
    var variant : String!
    
    init(name : String = "" , make : String = "" ,model : String = "" , variant : String = "" , regNo : String = "" ) {
        self.name = name
        self.model = model
        self.variant = variant
        self.regNo = regNo
        
    }
   
}

//------------------------------------------------------

//MARK:- Person Detail Model


class PersonDetails : NSObject {
    var name : String!
    var phone : String!
    
    
    init(name : String = "" , phone : String = "" ) {
        self.name = name
        self.phone = phone
        
    }
    
}

//------------------------------------------------------

//MARK:- Countries

struct Countries : Decodable {
    let countries : [Codes]
}

struct Codes : Decodable{
    let code : String?
    let name : String?
    
    init(json : [String:Any]){
        self.code = json["code"] as? String ?? ""
        self.name = json["name"] as? String ?? ""
    }
}


//------------------------------------------------------

//MARK:- Booking Dates


class BookingTime : NSObject {
    var date : String!
    var fromTime : String!
    var toTime : String!
    
    
    init(date : String = "" , fromTime : String = "" , toTime : String = "") {
        self.date = date
        self.fromTime = fromTime
        self.toTime = toTime
        
    }
    
}



//------------------------------------------------------

//MARK:- DeviceInfo

class DeviceInfo : NSObject{
    
    static var shared  : DeviceInfo = DeviceInfo()
    
    var UUID : String!
    var OSVersion : String!
    var DeviceName : String!
    var ModelName :String!
    var IP : String!
    
    override init() {
        
        self.UUID = UIDevice.current.identifierForVendor!.uuidString
        self.OSVersion = UIDevice.current.systemVersion
        self.DeviceName = UIDevice.current.name
        self.ModelName = UIDevice.current.localizedModel
        self.IP = DeviceInfo.getIPAddress() ?? ""
        
    }

    class func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
  
    
    
}

public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

extension JSONCodable {
    
    func toDictionary() -> JSON? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .millisecondsSince1970
        guard let json = try? encoder.encode(self),
            let dict = try? JSONSerialization.jsonObject(with: json, options: []) as? [String: Any] else {
                return nil
        }
        return dict
    }
    
}

protocol JSONCodable: Codable {
    typealias JSON = [String : Any]
    func toDictionary() -> JSON?
    
}

protocol mappable {
    init(fromJson json : JSON)
}

class JsonList<T : mappable> : NSObject{
    
    class func createModelArray<T : mappable>(model : T.Type , json : [JSON]) -> [T] {
        return json.map{ model.init(fromJson : $0)  }
    }
}
