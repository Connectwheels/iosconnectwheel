//
//  Validations.swift
//  YallaKoora
//
//  Created by - on 06/03/18.
//  Copyright © 2018 -. All rights reserved.
//

import Foundation

//MARK:- ---------------------------------------- Validation Messages ----------------------------------------------------

let kvBlankEmailOrPhone                                         =   "Please enter email or mobile number";
let kvBlankEmail                                                =   "Please enter email";
let kvValidEmail                                                =   "Please enter valid email";
let kvBlankPassword                                             =   "Please enter password";

let kvBlankCountry                                              =   "Please select country code";
let kvBlankConfirmPassword                                      =   "Please enter confirm password";
let kvMinLenPassword                                            =   "Please enter password with at least 6 characters";
let kvMismatchPassword                                          =   "Password mismatch";
let kvTermsAgreement                                            =   "Please agree with terms and conditions";

let kvBlankOldPassword                                          =   "Please enter old password";
let kvBlankNewPassword                                          =   "Please enter new password";
let kvMinLenOldPassword                                         =   "Please enter old password with at least 6 characters";
let kvNewMinLenPassword                                         =   "Please enter new password with at least 6 characters";

let kvBlankPhoneNumber                                          =   "Please enter mobile number"
let kvValidPhoneNumber                                          =   "Please enter valid mobile number"


let kvBlankFirstName                                            =   "Please enter first name";
let kvBlankLastName                                             =   "Please enter last name";
let kvBlankOTP                                                  =   "Please enter verification code";
let kvValidOTP                                                  =   "Please enter valid verification code";
let kvBlankName                                                 =   "Please enter name";
let kvBlankCustomerName                                         =   "Please enter customer name";

let kCardHolderNameEmpty                                        =   "Please enter card holder name"
let kCardNumberEmpty                                            =   "Please enter card number"
let kCardNumberInvalid                                          =   "Please enter valid card number"
let kExpiryDateEmpty                                            =   "Please select expiry date"
let kCvvEmpty                                                   =   "Please enter CVV"
let kCvvInvalid                                                 =   "Please enter valid CVV"

let kvEnterComment                                              =   "Please enter comment"
let kvBlankCode                                                 =   "Please enter IMEI number"
let kvValidCode                                                 =   "Please enter valid IMEI number"


let kvBlankDriverName                                           =   "Please enter driver name";

let kvBlankArea                                                 =   "Please select area";
let kvBlankDuration                                             =   "Please select duration";

let kvBlankCarRegNo                                             =   "Please enter registration number";
let kvBlankVehicleType                                          =   "Please select vehicle type";
let kvBlankCarMake                                              =   "Please select car maker";
let kvBlankCarModel                                             =   "Please select car model";
let kvBlankCarVariant                                           =   "Please select variant";


let kvBlankMeetingWith                                          =   "Please enter meeting person name";
let kvBlankSelectDate                                           =   "Please select date";
let kvBlankSelectTime                                           =   "Please select time";
let kvBlankSelectAlertBefore                                    =   "Please select duration for alert";
let kvBlankDescription                                          =   "Please enter description";
let kvBlankLocation                                             =   "Please select location";


let kvBlankSelectVehicle                                        =   "Please select vehicle";
let kvBlankSelectDriver                                         =   "Please select driver";
let kvBlankRate                                                 =   "Please enter rate per km";
let kvBlankMinimumKm                                            =   "Please enter minimum km";
let kvBlankMinBookingTime                                       =   "Please select booking date & time";

let kvBlankFolder                                               =   "Please enter folder name"
let kvSelectImage                                               =   "Please select image"
let kvFolderExist                                               =   "This folder name is already exist"

let kvSelectAddress                                             =   "Please select address"
