//
//  GClasses.swift
//  Boujee
//
//  Created by Kushal on 18/12/2017.
//  Copyright © 2017 -. All rights reserved.
//

import UIKit
import Foundation


var AssociatedObjectHandle: UInt8 = 0

class ClosureSleeve {
    let closure: () -> ()
    
    init(attachTo: AnyObject, closure: @escaping () -> ()) {
        self.closure = closure
        objc_setAssociatedObject(attachTo, &AssociatedObjectHandle, self, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    @objc func invoke() {
        closure()
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControlEvents, action: @escaping () -> ()) {
        let sleeve = ClosureSleeve(attachTo: self, closure: action)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        
    }
  
}

typealias completion = (_ data : Any) ->()

//--------------------------------------------------------------------------
//MARK:- FLoating Text Filed

class FloatingAuth : SkyFloatingLabelTextField {
    
    override func awakeFromNib() {
        self.textColor = colors.whiteColor
        self.placeholderColor = colors.whiteColor
        self.titleColor = colors.whiteColor
        self.lineColor = colors.whiteColor.withAlphaComponent(0.7)
        self.selectedLineColor = colors.whiteColor.withAlphaComponent(0.7)
        self.lineHeight = 0.5
        self.selectedLineHeight = 0.5
        self.selectedTitleColor = colors.whiteColor
        self.title = self.placeholder == nil ? "" : self.placeholder!
        self.placeholder = self.placeholder == nil ? "" :  self.placeholder!
        self.font = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0)
        self.placeholderFont = UIFont(name: fonts.montserratMediumFont, size: scaleFactor * 10.0)
        self.titleFont = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0)!
        
        self.tintColor = self.textColor
    }
    
 
    
}

class FloatingCommon : SkyFloatingLabelTextField {
    
    override func awakeFromNib() {
        self.textColor = colors.blackColor
        self.placeholderColor = colors.greyColor
        self.titleColor = colors.greyColor
        self.lineColor = colors.greyColor.withAlphaComponent(0.3)
        self.selectedLineColor = colors.greyColor.withAlphaComponent(0.3)
        self.lineHeight = 0.5
        self.selectedLineHeight = 0.5
        self.selectedTitleColor = colors.greyColor
        self.title = self.placeholder == nil ? "" : self.placeholder!
        self.placeholder = self.placeholder == nil ? "" :  self.placeholder!
        self.font = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0)
        self.placeholderFont = UIFont(name: fonts.montserratMediumFont, size: scaleFactor * 10.0)
        self.titleFont = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0)!
        self.tintColor = self.textColor
        
    }
    
    
    
}

class FloatingTime : SkyFloatingLabelTextField {
    
    override func awakeFromNib() {
        self.textColor = colors.blackColor
        self.placeholderColor = colors.greyColor
        self.titleColor = colors.greyColor
        self.lineColor = colors.greyColor.withAlphaComponent(0.3)
        self.selectedLineColor = colors.greyColor.withAlphaComponent(0.3)
        self.lineHeight = 0.5
        self.selectedLineHeight = 0.5
        self.selectedTitleColor = colors.greyColor
        self.title = self.placeholder == nil ? "" : self.placeholder!
        self.placeholder = self.placeholder == nil ? "" :  self.placeholder!
        self.font = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0)
        self.placeholderFont = UIFont(name: fonts.montserratMediumFont, size: scaleFactor * 10.0)
        self.titleFont = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 10.0)!
        self.tintColor = self.textColor
    }
    
    
    
}

class FloatingAdvance : SkyFloatingLabelTextField {
    
    override func awakeFromNib() {
        self.textColor = colors.blackColor
        self.placeholderColor = colors.greyColor
        self.titleColor = colors.greyColor
        self.lineColor = colors.greyColor.withAlphaComponent(0.3)
        self.selectedLineColor = colors.greyColor.withAlphaComponent(0.3)
        self.lineHeight = 0.5
        self.selectedLineHeight = 0.5
        self.selectedTitleColor = colors.greyColor
        self.title = self.placeholder == nil ? "" : self.placeholder!
        self.placeholder = self.placeholder == nil ? "" :  self.placeholder!
        self.font = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 8.0)
        self.placeholderFont = UIFont(name: fonts.montserratMediumFont, size: scaleFactor * 8.0)
        self.titleFont = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 8.0)!
        self.tintColor = self.textColor
    }
    
    
    
}

//------------------------------------------------------

//MARK:- Dummy Field For Text View

class DummyFloat : SkyFloatingLabelTextField {
    
    override func awakeFromNib() {
        
        self.textColor = UIColor.clear
        self.placeholderColor = colors.greyColor
        self.titleColor = colors.greyColor
        self.lineColor = UIColor.clear
        self.selectedLineColor = UIColor.clear
        self.lineHeight = 0.0
        self.selectedLineHeight = 0.0
        self.selectedTitleColor = colors.greyColor
        self.title = self.placeholder == nil ? "" : self.placeholder!
        self.placeholder = self.placeholder == nil ? "" :  self.placeholder!
        self.font = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 13.0)
        self.placeholderFont = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 13.0)
        self.titleFont = UIFont(name: fonts.montserratRegularFont, size: scaleFactor * 12.0)!
        //        self.marginFromX = 10.0
        self.tintColor = self.textColor
        
        
        
    }
    
   
    
}

//------------------------------------------------------

//MARK:- For override Delete action


class OtpField : UITextField {
    
    override func awakeFromNib() {
        self.textColor = colors.whiteColor
        self.font = UIFont(name: fonts.montserratRegularFont, size: 10.0 * scaleFactor)
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedStringKey.foregroundColor : colors.whiteColor , NSAttributedStringKey.font : UIFont(name: fonts.montserratMediumFont, size: 10.0 * scaleFactor) as Any])
        self.borderStyle = .none
        self.keyboardType = .numberPad
    
    }
    
    override func deleteBackward() {
        super.deleteBackward()
        if self.text!.isEmpty {
            _ = self.delegate?.textFieldShouldClear!(self)
        }
    }
}



//------------------------------------------------------

//MARK:- Theme Action button

class ThemeActionButton: UIButton {
    override func awakeFromNib() {
        
        self.layer.masksToBounds = false
        
        self.setLayout(detail: [layout.kRadiusRatio : 2,
                                layout.kfont : UIFont(name: fonts.montserratMediumFont, size: 10.0 * scaleFactor) as Any,
                                layout.kdefaultBackGroundColor : colors.redColor,
                                layout.ktitleColor : colors.whiteColor])
       
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
    
    
}

//------------------------------------------------------

//MARK:- For try

public func changeMultiplier(constraint: NSLayoutConstraint, multiplier: CGFloat) -> NSLayoutConstraint {
    let newConstraint = NSLayoutConstraint(
        item: constraint.firstItem as Any,
        attribute: constraint.firstAttribute,
        relatedBy: constraint.relation,
        toItem: constraint.secondItem,
        attribute: constraint.secondAttribute,
        multiplier: multiplier,
        constant: multiplier)
    
    newConstraint.priority = constraint.priority
    NSLayoutConstraint.deactivate([constraint])
    NSLayoutConstraint.activate([newConstraint])
    return newConstraint
}








class imageView : UIImageView {
    
    override func awakeFromNib() {
        addObserver(self, forKeyPath: "bounds", options: [.new,.old], context: nil)
    }
    
    override var frame: CGRect {
        didSet {
            updateUI()
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "bounds")
    }
    
    private var radi : CGFloat = 0
    
    @IBInspectable var radius : CGFloat = 0 {
        didSet {
            radi = radius
            updateUI()
        }
    }
    
    @IBInspectable var isRounded : Bool = false {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        if isRounded {
            self.layer.cornerRadius = self.frame.size.height / 2
        } else {
            self.layer.cornerRadius = radi
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "bounds" && (object as! UIImageView) == self {
            updateUI()
        }
    }
}

//------------------------------------------------------

//MARK:- Label Classes

class MainLabel : UILabel {
    
    var fontStyle : String = fonts.montserratBoldFont
    {
        didSet{
            self.textColor = fontColor
        }
    }
    
    var size : CGFloat = 10.0
    {
        didSet{
            self.font = UIFont(descriptor: self.font.fontDescriptor, size: size * scaleFactor)
        }
    }
    
    var fontColor : UIColor = colors.blackColor
    {
        didSet{
            self.textColor = fontColor
        }
    }
    
   
}

class Bold: MainLabel {
   
    override func awakeFromNib() {
        self.textColor = colors.blackColor
        self.font = UIFont(name: fonts.montserratBoldFont, size: 16 * scaleFactor)
    }
}

class Light: MainLabel {
    
    override func awakeFromNib() {
        self.textColor = colors.greyColor
        self.font = UIFont(name: fonts.montserratLightFont, size: 10 * scaleFactor)
    }
}




//------------------------------------------------------

//MARK:-

//--------------------------------------------------------------------------
//MARK:- BlueNavigationBar

class BlueNavigationBar : UIViewController {
    
    override func viewDidLoad() {
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont , size: 10.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.whiteColor], for: .normal)
         self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont , size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.whiteColor], for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedStringKey.font : UIFont(name: fonts.montserratMediumFont , size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.barTintColor = colors.blueColor
        self.navigationController?.navigationBar.backgroundColor = colors.blueColor
        
        
        self.title = self.title?.capitalized
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            statusBar.backgroundColor = colors.blueColor
        }
        
       
        self.navigationController?.navigationBar.isTranslucent = false
        
        setNeedsStatusBarAppearanceUpdate()
        
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont , size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.whiteColor], for: .normal)
        
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont , size: 10.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.whiteColor], for: .normal)
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont , size: 10.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.whiteColor], for: .normal)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
}

//--------------------------------------------------------------------------
//MARK:- WhiteNavigationBar

class WhiteNavigationBar : UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedStringKey.font : UIFont(name: fonts.montserratMediumFont , size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.blackColor]
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        
        self.title = self.title?.capitalized
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            statusBar.backgroundColor = UIColor.white
        }
        
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = colors.blackColor
        self.navigationController?.navigationItem.rightBarButtonItem?.tintColor = colors.blackColor
        
        self.navigationController?.navigationBar.isTranslucent = false
       
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .default
    }
}

//--------------------------------------------------------------------------
//MARK:- WhiteNavigationBar

class TransparentBar : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedStringKey.font : UIFont(name: fonts.montserratMediumFont , size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.blackColor]
        
       // self.navigationController?.navigationBar.backgroundColor = UIColor.white
        
        self.title = self.title?.capitalized
        
        
        self.navigationItem.leftBarButtonItem?.tintColor = colors.whiteColor
        self.navigationItem.rightBarButtonItem?.tintColor = colors.whiteColor
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = .clear

        self.navigationController?.navigationBar.tintColor = UIColor.clear
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            statusBar.backgroundColor = UIColor.clear
        }
        
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes =  [NSAttributedStringKey.font : UIFont(name: fonts.montserratMediumFont , size: 12.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.blackColor]
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    
        self.navigationItem.leftBarButtonItem?.tintColor = colors.whiteColor
        self.navigationItem.rightBarButtonItem?.tintColor = colors.whiteColor
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.clear
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.view.backgroundColor = UIColor.clear
        
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            statusBar.backgroundColor = UIColor.clear
        }
    }
 
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.backgroundColor = colors.blueColor
        self.navigationController?.navigationBar.barTintColor = colors.blueColor
        self.navigationController?.view.backgroundColor = colors.blueColor
   }
    
}

//------------------------------------------------------

//MARK:- Check Before use // dynamic table view

class IntrinsicTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIViewNoIntrinsicMetric, height: contentSize.height)
    }
    
}

//------------------------------------------------------

//MARK:- Check Before use // dynamic collection view

class DynamicCollectionView: UICollectionView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
        
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
    
}

//------------------------------------------------------

//MARK:- subview //for iqkeyboarmanager

class subview : UIScrollView {
    
}




