//
//  GConstant.swift
//  PaidPV
//
//  Created by - on 22/09/17.
//  Copyright © 2017 -. All rights reserved.
//

import Foundation
import UIKit

//MARK:- ---------------------------------------- Layout Keys ----------------------------------------------------

let fonts = BasicFonts()

struct BasicFonts {
    let montserratBoldFont                                                  = "Montserrat-Bold"
    let montserratSemiBoldFont                                              = "Montserrat-SemiBold"
    let montserratRegularFont                                               = "Montserrat-Regular"
    let montserratExtraLightFont                                            = "Montserrat-ExtraLight"
    let montserratMediumFont                                                = "Montserrat-Medium"
    let montserratLightFont                                                 = "Montserrat-Light"
}

let colors = BasicColor()

enum UserType : Int {
    case Owner , Viewer , Driver
}


struct BasicColor {
    
    let greenColor  : UIColor = UIColor.colorFromHex(hex: 0x49ac4f)
    
    let greyColor  : UIColor = UIColor.colorFromHex(hex: 0x919191)
    
    let blackColor  : UIColor = UIColor.colorFromHex(hex: 0x353535)
    
    let whiteColor  : UIColor = UIColor.white
    
    let redColor    : UIColor = UIColor.colorFromHex(hex: 0xE36B2E)
    
    let blueColor    : UIColor = UIColor.colorFromHex(hex: 0x00AEE7)
   
    let deleteColor    : UIColor = UIColor.colorFromHex(hex: 0xF93F3F)
   
}


let layout = layoutKeys()



struct layoutKeys  {
    var kselectedTintColor                                      = "kselectedTintColor"
    var kdefaultTintColor                                       = "kdefaultTintColor"
    var kselectedBorderColor                                    = "kselectedBorderColor"
    var kdefaultBorderColor                                     = "kdefaultBorderColor"
    var kdefaultBackGroundColor                                 = "kdefaultBackGroundColor"
    var kselectedBackGroundColor                                = "kselectedBackGroundColor"
    var kstate                                                  = "kstate"
    var ktitleColor                                             = "ktitleColor"
    var kfont                                                   = "kfont"
    var kRadius                                                 = "kRadius"
    var kRadiusRatio                                            = "kRadiusRatio"
    var kBorderWidth                                            = "kBorderWidth"
    var kisRounded                                              = "kisRounded"
}

let kScreenWidth                             : CGFloat       =                 UIScreen.main.bounds.size.width
let kScreenHeight                            : CGFloat       =                 UIScreen.main.bounds.size.height
let UserDefault                                              =               UserDefaults.standard
let scaleFactor          = (kScreenWidth / 320.0)
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let keyOK                                                       =               "Ok"
let keyColor                                                    =               "Color"
var deviceTokenString : String = "0"
let keyisTutorialSeen                                              =   "isTutorialSeen"

let AuthStoryBoard                          : UIStoryboard   = UIStoryboard(name: "Auth", bundle: nil)
let HomeStoryBoard                          : UIStoryboard   = UIStoryboard(name: "Home", bundle: nil)
let BookingStoryBoard                       : UIStoryboard   = UIStoryboard(name: "Booking", bundle: nil)

let NaviHomeVC                                                  =   "NaviHomeVC"
let NaviMyBookingVC                                             =   "NaviMyBookingVC"
let NaviSettingsVC                                              =   "NaviSettingsVC"
let NaviNotificationVC                                          =   "NaviNotificationVC"
let NaviMatchesVC                                               =   "NaviMatchesVC"
let NaviPaymentSettingVC                                        =   "NaviPaymentSettingVC"
let NaviEditProfileVC                                           =   "NaviEditProfileVC"
let NaviLoginVC                                                 =   "NaviLoginVC"


let kBaseUrl = "http://13.232.41.240:5052/"

let kTermsUrl                                                   =   "http://13.232.41.240/home/terms_and_conditions"
let kPrivacyUrl                                                 =   "http://13.232.41.240/home/privacy_policy"
let kAboutUsUrl                                                 =   kBaseUrl + "about_us"
let kFaqUrl                                                     =   kBaseUrl + "help_faqs"

let kTermsTitle = "Terms & Conditions"
let kPolicyTitle = "Privacy Policy"

let kShareAppMessage = "Let me recommend you Connect Wheels application \n" + "http://itunes.apple.com/app/id1478872667"

//---------------------------------------------------------------------------------------------------------


var currentUser : UserData
{
    if let  data = UserDefault.value(forKey: kUserDetail)
    {
        if let user = try? JSONDecoder().decode(UserData.self, from: JSON(data).rawData())
        {
            
            return user
            
        }
    }
    
    return UserData()
}

enum AppCredential : String {
    
    //usage AppCredential.facebookAppID.rawValue
    //Client : sigafo01@ipfw.edu : APP ID: 521853145000539
    
    case facebookAppID                           =   "2377705302463755"
    case googleClientId                          =   "82751768452-3tcp0kapinsknfn7f29gnt538a17ccqr.apps.googleusercontent.com"
    case googleApiKey                            =   "AIzaSyBsaPcDhau4fRxkPl4JPDdcKrBXCzZ84bc"
    case goolePlaceApiKey                        =   "AIzaSyCKLA7nE4ZX4PY5i37xIr9dg5YwY4nZccc"
    
}

//------------------------------------------------------

//MARK:- Notification Observer

let kvehicleUpdateObserver                                       =  "vehicleUpdate"
let kEmegencyObserver                                            =  "emergency"
