//
//  Contacts.swift
//  ConnectWheels
//
//  Created by  on 13/12/18.
//  Copyright © 2018 Connect Wheels. All rights reserved.
//

import Foundation
@_exported import Contacts
@_exported import ContactsUI
@_exported import AddressBook

class UserContact : NSObject {
    var name : String!
    var phoneNumber : String!
    
    init(name : String , phoneNumber : String ) {
        super.init()
        self.name = name
        self.phoneNumber = phoneNumber
    }
    
    override init() {
        super.init()
        self.name = ""
        self.phoneNumber = ""
    }
}

class ContactsList : NSObject{
    
    static let shared : ContactsList = ContactsList()
    
    var arrContacts : [UserContact] = []
    let formatter = CNContactFormatter()
    let store = CNContactStore()
    
    let validTypes = [
        CNLabelPhoneNumberiPhone,
        CNLabelPhoneNumberMobile,
        CNLabelPhoneNumberMain
    ]
    
    override init() {
        super.init()
        self.fetchContacts()
    }
    
    func fetchContacts() {
       
        if CNContactStore.authorizationStatus(for: .contacts) == .notDetermined {
            
            store.requestAccess(for: .contacts) { (authorized, error) in
                if error != nil{
                    GFunction.ShowAlert(message: (error?.localizedDescription)!)
                    return
                }
                if authorized {
                    self.retrieveContactsWithStore(store: self.store)
                }
            }
        } else if CNContactStore.authorizationStatus(for: .contacts) == .authorized {
            self.retrieveContactsWithStore(store: store)
        }
    }
    
    func retrieveContactsWithStore(store: CNContactStore , completion : completion? = nil) {
        do {
            var allContainers: [CNContainer] = []
            do {
                allContainers = try store.containers(matching: nil)
            } catch {
                print("Error fetching containers")
            }
            
            _ = allContainers.map { (container) -> Void in
                
                do{
                    
                    let predicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                    //let predicate = CNContact.predicateForContactsMatchingName("John")
                    let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey] as [Any]
                    
                    let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                    
                    //contacts = contacts.filter{ !$0.phoneNumbers.isEmpty }
                    
                    arrContacts = contacts.map { (con) -> UserContact in
                        let name = JSON(formatter.string(from: con) as Any).stringValue
                        var number = JSON(con.phoneNumbers.first?.value.stringValue as Any).stringValue
                        
                        let validNumbers = con.phoneNumbers.compactMap { phoneNumber -> String? in
                            if let label = phoneNumber.label, validTypes.contains(label) {
                                return phoneNumber.value.value(forKey: "digits") as? String
                            }
                            return nil
                        }
                        
                        if let numberToAdd = validNumbers.first{
                            number = numberToAdd
                        }
                        
                        let userCon = UserContact.init(name: name, phoneNumber: number)
                        return userCon
                    }
                    
                    
                    //callback
                }catch {
                    print(error)
                }
                
            }
            
            if let cb = completion{
                cb(arrContacts)
            }
        }
            
            
    }
    
    func getContacts(completion : completion?)  {
        if let cb = completion{
            if arrContacts.isEmpty{
                retrieveContactsWithStore(store: store) { (response) in
                    if let con = response as? [UserContact]{
                        cb(con)
                    }
                }
            }else{
                cb(arrContacts)
            }
        }
    }
    
}
