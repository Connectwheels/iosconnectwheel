//
//  GExtensions.swift
//  PaidPV
//
//  Created by - on 25/09/17.
//  Copyright © 2017 -. All rights reserved.
//

import Foundation
import UIKit
//import IQKeyboardManagerSwift



typealias AlertActionHandler = ((UIAlertAction) -> Void)

//------------------------------------------------------

//MARK:- UIAlertControllerStyle

extension UIAlertControllerStyle {
    
    func controller(title: String, message: String, actions: [UIAlertAction]) -> UIAlertController {
        let _controller = UIAlertController(
            title: title,
            message: message,
            preferredStyle: self
        )
        actions.forEach { _controller.addAction($0) }
        return _controller
    }
}


//--------------------------------------------------------------------------
//MARK:- String

extension String
{
  
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func Localized() -> String {
        
       return self
    }
    
    func getUrl() -> URL {
        if let u = URL.init(string: self){
            return u
        }
        
        return URL.init(string: "https://www.google.com")!
    }
    
    func alertAction(style: UIAlertActionStyle = .default, handler: AlertActionHandler? = nil) -> UIAlertAction {
        return UIAlertAction(title: self, style: style, handler: handler)
    }
    
    func getAttributedText ( defaultDic : [NSAttributedStringKey : Any] , attributeDic : [NSAttributedStringKey : Any], attributedStrings : [String]) -> NSMutableAttributedString {
        
        let attributeText : NSMutableAttributedString = NSMutableAttributedString(string: self, attributes: defaultDic)
        for strRange in attributedStrings {
            if let range = self.range(of: strRange) {
                let startIndex = self.distance(from: self.startIndex, to: range.lowerBound)
                let range1 = NSMakeRange(startIndex, strRange.count)
                attributeText.setAttributes(attributeDic, range: range1)
            }
        }
        return attributeText
    }
    
    func separate(every: Int, with separator: String) -> String {
        return String(stride(from: 0, to: Array(self).count, by: every).map {
            Array(Array(self)[$0..<min($0 + every, Array(self).count)])
            }.joined(separator: separator))
    }
    
    func showAlertsWithAction(action : @escaping completion) {
        let alert = UIAlertController(title: nil, message: self.Localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: keyOK, style: .default, handler: { (actionReturn) in
            action(actionReturn)
        }))
        
        if let presentedView = appDelegate.window?.rootViewController?.presentedViewController
        {
            presentedView.present(alert, animated: true, completion: nil)
            return
        }
        //
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func convertUTCToLocal(currentformat : String , changeformat :  String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentformat
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        dateFormatter.locale = Locale.init(identifier: "en_US")
        if let timeUTC = dateFormatter.date(from: self) {
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = changeformat
            let localTime = dateFormatter.string(from: timeUTC)
            return localTime
        }
        
        return ""
    }
    
    
    
    func convertLocalToUTC(currentformat : String , changeformat :  String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentformat
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = Locale.init(identifier: "en_US")
        if let timeLocal = dateFormatter.date(from: self){
            dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
            dateFormatter.dateFormat = changeformat
            let timeUTC = dateFormatter.string(from: timeLocal)
            return timeUTC
        }
        return ""
    }
    
    func convertFormatOnly(currentformat : String , changeformat :  String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = currentformat
        dateFormatter.timeZone = NSTimeZone.local
        if let timeLocal = dateFormatter.date(from: self){
            dateFormatter.dateFormat = changeformat
            let timeUTC = dateFormatter.string(from: timeLocal)
            return timeUTC
        }
        return ""
    }
    
    func getDate(currentFormat : String) -> Date {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = currentFormat
        if let date = dateformatter.date(from: self){
            return date
        }
        
        return Date()
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }
        
        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
    
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    
}

//--------------------------------------------------------------------------
//MARK:- UILabel

extension UILabel {
    
    open override func awakeFromNib() {
        
       // self.font = UIFont(name: self.font.fontName, size: self.font.pointSize * scaleFactor)
    }
    
    @IBInspectable
    var isFontDynamic : Bool
    {
        set {
            
            if newValue == true
            {
               self.font = UIFont(descriptor: self.font.fontDescriptor, size: self.font.pointSize * scaleFactor)
                
            }
            objc_setAssociatedObject(self, &kisDynamic, newValue, .OBJC_ASSOCIATION_RETAIN)
            
        }
        
        get {
            if let value = (objc_getAssociatedObject(self, &kisDynamic) as? Bool) {
                return value
            }
            return false
        }
    }
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = self.textAlignment
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}

//--------------------------------------------------------------------------
//MARK:- UINavigationBar

extension UINavigationBar
{
    open override func awakeFromNib() {
//        self.titleTextAttributes = [NSAttributedStringKey.font : UIFont(name: fonts.montserratExtraLightFont , size: 18.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.whiteColor]
//        if #available(iOS 11.0, *) {
//            self.largeTitleTextAttributes = [NSAttributedStringKey.font : UIFont(name: fonts.montserratExtraLightFont , size: 25.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.whiteColor]
//        } else {
//            
//        }
    }
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
        
    }
}

//--------------------------------------------------------------------------
//MARK:- UINavigationController



extension UINavigationController
{
    
    open override func awakeFromNib() {
        
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = colors.whiteColor
        interactivePopGestureRecognizer?.delegate = nil
        
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}

extension UIBarButtonItem {
    open override func awakeFromNib() {
        self.tintColor = colors.blackColor
        self.setTitleTextAttributes([NSAttributedStringKey.font : UIFont(name: fonts.montserratRegularFont , size: 15.0 * scaleFactor) as Any , NSAttributedStringKey.foregroundColor : colors.blackColor], for: .normal)
    }
}

//--------------------------------------------------------------------------
//MARK:- UITextField

var kRegex = "kRegex"
var kdefaultCharacter = "kRegex"

extension UITextField : UITextFieldDelegate
{
    
    var regex : String? {
        
        set {
            self.delegate = self
            objc_setAssociatedObject(self, &kRegex, newValue, .OBJC_ASSOCIATION_RETAIN)
            
        }
        
        get {
            
            return objc_getAssociatedObject(self, &kRegex) as? String
        }
    }
    
    var defaultcharacterSet : CharacterSet? {
        
        set {
            self.delegate = self
            objc_setAssociatedObject(self, &kdefaultCharacter, newValue, .OBJC_ASSOCIATION_RETAIN)
            
        }
        
        get {
            
            return objc_getAssociatedObject(self, &kdefaultCharacter) as? CharacterSet
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if range.location == 0 && (string == " ") {
            return false
        }
        
        if let rx = self.regex {
            let regexPattern = try! NSRegularExpression(pattern: rx, options: [])
            return regexPattern.matches(in: textField.text! + string, options: [], range: NSRange(location: 0, length: (textField.text! + string).count)).count > 0
        }
        
        if let characters = self.defaultcharacterSet {
            
            let inverseSet = characters.inverted
            
            let components = string.components(separatedBy: inverseSet)
            
            let filtered = components.joined(separator: "")
            
            return string == filtered
            
            //return characters.isSuperset(of: NSCharacterSet.init(charactersIn: string) as CharacterSet)
        }
        
        return true
    }
    
    func LeftPaddingView(image : UIImage? , frame : CGRect?) {
        
        let paddingView : UIView = frame == nil ? UIView.init(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.size.height)) : UIView.init(frame:  frame!)
        
        if image != nil {
            let imageView = UIImageView()
            imageView.image = image
            imageView.contentMode = .center
            imageView.frame = paddingView.frame
            paddingView.addSubview(imageView)
        }
        
        
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    func RightPaddingView(image : UIImage? , frame : CGRect?) {
        
        let paddingView : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: frame == nil ? 30 : frame!.size.width  , height: frame == nil ? 30 : self.frame.size.height))
        
        if image != nil {
            let imageView = UIImageView()
            imageView.image = image
            imageView.contentMode = .center
            imageView.frame = frame == nil ? paddingView.frame : frame!
            paddingView.addSubview(imageView)
            
            
        }
        
        paddingView.isUserInteractionEnabled = false
        self.rightView = paddingView
        self.rightViewMode = UITextFieldViewMode.always
    }
}

//--------------------------------------------------------------------------
//MARK:- UIViewController

extension UIViewController
{
    @IBAction func btnBackClicked() {
        
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnMenuClicked() {
        
//        self.slideMenuController()?.openLeft()
        
    }
    
    func displayContentController(content: UIViewController , frame : CGRect?) {
        
        content.view.frame = CGRect(x: self.view.frame.size.width, y: 0, width: 10, height: self.view.frame.size.height)
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.curveEaseIn], animations: {
            if let  _ = frame {
                content.view.frame = frame!
            }
            self.addChildViewController(content)
            self.view.addSubview(content.view)
            
            content.didMove(toParentViewController: self)
        }, completion: nil)
        
        
    }
    
    func hideContentController(content: UIViewController , delay : TimeInterval = 0.0) {
        self.view.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.2, delay: delay, options: [.curveEaseOut], animations: {
            content.view.frame = CGRect(x: kScreenWidth, y: 0, width: content.view.frame.size.width, height: self.view.frame.size.height)
        }) { (isCompleted) in
            content.willMove(toParentViewController: nil)
            content.view.removeFromSuperview()
            content.removeFromParentViewController()
        }
        
    }

}

//--------------------------------------------------------------------------
//MARK:- UIColor

extension UIColor {
    
    class func colorFromHex(hex: Int) -> UIColor {
        return UIColor(red: (CGFloat((hex & 0xFF0000) >> 16)) / 255.0, green: (CGFloat((hex & 0xFF00) >> 8)) / 255.0, blue: (CGFloat(hex & 0xFF)) / 255.0, alpha: 1.0)
    }
}


//--------------------------------------------------------------------------
//MARK:- UIView

extension UIView {
    
    func dropShadow(color: UIColor = UIColor.black , offset : CGSize = CGSize(width: -1, height: 1)) {
      
        // shadow
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 2.0
        self.clipsToBounds = false
        
    }
    
    
    func blink() {
        self.alpha = 0.2
        self.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {
            self.alpha = 1.0
            self.transform = .identity
        }, completion: nil)
    }
    
    func removeBlink() {
        self.layer.removeAllAnimations()
        self.transform = .identity
    }
    
    func removeShadow()
    {
        self.layer.shadowOpacity = 0.0
    }
}

//------------------------------------------------------

//MARK:- UIApplication

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

//------------------------------------------------------

//MARK:- UITapGestureRecognizer

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
    
        
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
    func gettappedOnText(label: UILabel, text targetRange: String) -> (isTapped : Bool , value : String) {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        var rangeString = NSMakeRange(0, label.text!.count)

        var arrofRange : [NSRange] = []
        
        while (rangeString.length != NSNotFound && rangeString.location != NSNotFound) {
            
            //Get the range of search text
            let colorRange = (label.text! as NSString).range(of: targetRange, options: NSString.CompareOptions(rawValue: 0), range: rangeString)
            
            if (colorRange.location == NSNotFound) {
                //If location is not present in the string the loop will break
                break
            } else {
                //This line of code colour the searched text
//                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: colorRange)
//                lblContent.attributedText = attribute
              arrofRange.append(colorRange)
              rangeString = NSMakeRange(colorRange.location + colorRange.length, rangeString.length - (colorRange.location + colorRange.length))
                
            }
        }
        
        for range in arrofRange
        {
            if NSLocationInRange(indexOfCharacter, range)
            {
                return (true , targetRange)
                
            }
        }

        
        return (false , targetRange)
    }
}

//MARK: - UIFont

extension UILabel {
  
    func montserrat(ofType type : String, withfontSize size : CGFloat ,isAspectRasio : Bool = true , color : UIColor?) {
        self.font = UIFont.init(name: type, size: isAspectRasio ? size * scaleFactor : size)
        if let _ = color {
            self.textColor = color
        }
    }
}



extension CLLocationCoordinate2D {
    
    func getBearing(toPoint point: CLLocationCoordinate2D) -> Double {
        func degreesToRadians(degrees: Double) -> Double { return degrees * M_PI / 180.0 }
        func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / M_PI }
        
        let lat1 = degreesToRadians(degrees: latitude)
        let lon1 = degreesToRadians(degrees: longitude)
        
        let lat2 = degreesToRadians(degrees: point.latitude);
        let lon2 = degreesToRadians(degrees: point.longitude);
        
        let dLon = lon2 - lon1;
        
        let y = sin(dLon) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        let radiansBearing = atan2(y, x);
       
        return radiansToDegrees(radians: radiansBearing)
    }
    
    
    func getBearingBetweenTwoPoints1(point1 : CLLocationCoordinate2D, point2 : CLLocationCoordinate2D) -> Double {
        
        func degreesToRadians(degrees: Double) -> Double { return degrees * M_PI / 180.0 }
        func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / M_PI }
        
        let lat1 = degreesToRadians(degrees: point1.latitude)
        let lon1 = degreesToRadians(degrees: point1.longitude)
        
        let lat2 = degreesToRadians(degrees: point2.latitude)
        let lon2 = degreesToRadians(degrees: point2.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
    
    
}

extension CLLocationCoordinate2D: Equatable {}
public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return (lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude)
}


//------------------------------------------------------

//MARK:- Encodable


extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

