//
//  GCustomButton.swift
//  Ned
//
//  Created by - on 08/06/17.
//  Copyright © 2017 -. All rights reserved.
//

import Foundation
import UIKit

var kSelectedBackground = "kSelectedBackground"
var kNormalBackground = "kNormalBackground"
var kSelectedBorder = "kSelectedBorder"
var kNormalBorder = "kNormalBorder"
var kRatio = "kRatio"
var kisDynamic = "kisDynamic"

extension UIButton
{
    
    var radiusRatio : CGFloat? {
        
        set {
            
            objc_setAssociatedObject(self, &kRatio, newValue, .OBJC_ASSOCIATION_RETAIN)
            
        }
        
        get {
            
            return objc_getAssociatedObject(self, &kRatio) as? CGFloat
        }
    }
    
    @IBInspectable
    var isFontDynamic : Bool
    {
        set {
            
            if newValue == true
            {
                self.titleLabel?.font = UIFont(descriptor: self.titleLabel!.font.fontDescriptor, size: self.titleLabel!.font.pointSize * scaleFactor)
                
            }
            objc_setAssociatedObject(self, &kisDynamic, newValue, .OBJC_ASSOCIATION_RETAIN)
            
        }
        
        get {
            if let value = (objc_getAssociatedObject(self, &kisDynamic) as? Bool) {
                return value
            }
            return false
        }
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        perform(#selector(DelayForSetText), with: nil, afterDelay: 0.0)
        
    }
    
    @objc func DelayForSetText()  {
        
        if let title = self.title(for: .normal)
        {
            
            self.setTitle(title.Localized(), for: .normal)
            self.setNeedsDisplay()
                        self.layoutIfNeeded()
            
            
            
            
        }
        
        if let title = self.title(for: .selected)
        {
            self.setTitle(title.Localized(), for: .selected)
            self.setNeedsDisplay()
            self.layoutIfNeeded()
        }

//        if self.isFontDynamic
//        {
//            self.titleLabel?.font = UIFont(name: self.titleLabel!.font.familyName, size: self.titleLabel!.font.pointSize * scaleFactor)
//
//        }
        

//        if let text = self.titleLabel?.text
//        {
////            self.titleLabel?.text = text.Localized()
//            self.setTitle(text.Localized(), for: self.state)
//            self.setNeedsDisplay()
//            self.layoutIfNeeded()
//            
//        }
        
        
        
    }
    
    open override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        if let radi = self.radiusRatio {
            self.layer.cornerRadius =  self.frame.size.height / radi
            self.layer.masksToBounds = true
         
        }else
        {
            self.layer.cornerRadius = self.layer.cornerRadius
            self.layer.masksToBounds = true
        }
        
        self.setNeedsLayout()
    }
    

    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        let factor: CGFloat =  1
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount*factor, bottom: 0, right: insetAmount*factor)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount*factor, bottom: 0, right: -insetAmount*factor)
        self.contentEdgeInsets = UIEdgeInsets(top: self.contentEdgeInsets.top, left: insetAmount, bottom: self.contentEdgeInsets.bottom, right: insetAmount)
    }
    
    
    func imageUnderText(padding: CGFloat = 8.0) {
        if let image = self.imageView?.image {
            let imageSize: CGSize = image.size
            self.titleEdgeInsets = UIEdgeInsets(top: padding, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
            let labelString = NSString(string: self.titleLabel!.text!)
            let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font as Any])
            self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + padding), left: 0.0, bottom: 0.0, right: -titleSize.width)
        }
    }
    
    //for now set only for selected and normal
    
    
    var selectedBackgroundColor : UIColor? {
        
        set {
            
            if self.normalBackgroundColor == nil {
                
                objc_setAssociatedObject(self, &kNormalBackground, self.backgroundColor, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            
            if self.state == .selected && newValue != nil {
                self.backgroundColor = newValue
            }
            
            self.tintColor = UIColor.clear
            objc_setAssociatedObject(self, &kSelectedBackground, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
        }
        
        get {
            
            return objc_getAssociatedObject(self, &kSelectedBackground) as? UIColor
        }
        
    }
    
    var normalBackgroundColor : UIColor? {
        
        set {
            
            if self.state == .normal && newValue != nil {
                self.backgroundColor = newValue
            }
            
            objc_setAssociatedObject(self, &kNormalBackground,newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            
            return objc_getAssociatedObject(self, &kNormalBackground) as? UIColor
        }
        
    }
    
    var selectedBorderColor : UIColor? {
        
        set {
            
            if self.normalBorderColor == nil {
                
                objc_setAssociatedObject(self, &kNormalBorder, self.layer.borderColor , .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            
            if self.state == .selected && newValue != nil {
                self.layer.borderColor = newValue?.cgColor
                self.layer.borderWidth = 1.0
            }
            
            objc_setAssociatedObject(self, &kSelectedBorder, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
        }
        
        get {
            
            return objc_getAssociatedObject(self, &kSelectedBorder) as? UIColor
        }
        
    }
    
    var normalBorderColor : UIColor? {
        
        set {
            
            if self.state == .normal && newValue != nil {
                self.layer.borderColor = newValue?.cgColor
                self.layer.borderWidth = 1.0
            }
            
            objc_setAssociatedObject(self, &kNormalBorder,newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            
            return objc_getAssociatedObject(self, &kNormalBorder) as? UIColor
        }
        
    }
    
    // FIXME:- override isselect method get title color issue while working on same click
    // When you over write make sure you have set all properties that related with state 
   
    /*
    open override var isSelected: Bool {
        
        
//        willSet
//        {
//            self.imageView?.image = self.image(for: self.state)
//            self.titleLabel?.text = self.title(for: self.state)
// 
//        }
      
      didSet {
        
        if self.isSelected == oldValue {
            
            return
        }


        self.imageView?.image = self.image(for: self.state)
        self.titleLabel?.text = self.title(for: self.state)?.Localized()
        self.titleLabel?.textColor = self.titleColor(for: self.state)
        
        
            if (self.selectedBackgroundColor != nil) {
                
                if isSelected {
                    //self.tintColor = UIColor.clear
                    self.backgroundColor = selectedBackgroundColor
                }else
                {
                    self.backgroundColor =  objc_getAssociatedObject(self, &kNormalBackground) as? UIColor
                }
                
            }
            
            
            if (self.selectedBorderColor != nil) {
                
                if isSelected {
                    
                    self.layer.borderColor = selectedBorderColor?.cgColor
                    self.layer.borderWidth = 1.0
                    
                }else
                {
                    self.layer.borderWidth = 1.0
                    self.layer.borderColor = (objc_getAssociatedObject(self, &kNormalBorder) as? UIColor)?.cgColor
                }
                
            }
        
        
        }
    }
    
    */
    
    
    func setLayout(detail : Dictionary<String,Any>)  {
        
        // reason to check nil is : It will not over write its default if we dont pass
        
        if let _ = detail[layout.kdefaultBorderColor]
        {
            self.normalBorderColor = detail[layout.kdefaultBorderColor] as? UIColor
        }
        
        if let _ = detail[layout.kselectedBorderColor]
        {
           self.selectedBorderColor = detail[layout.kselectedBorderColor] as? UIColor
        }
        
        if let _ = detail[layout.kdefaultBackGroundColor]
        {
            self.normalBackgroundColor = detail[layout.kdefaultBackGroundColor] as? UIColor
        }
        
        if let _ = detail[layout.kselectedBackGroundColor]
        {
            self.selectedBackgroundColor = detail[layout.kselectedBackGroundColor] as? UIColor
        }
        
        if let _ = detail[layout.kfont]
        {
            self.titleLabel?.font = detail[layout.kfont] as? UIFont
        }
        
        
        if let radius = detail[layout.kRadius]
        {
            self.layer.cornerRadius = CGFloat(radius as! Int)
        }
        
        if let radiusRatio = detail[layout.kRadiusRatio]
        {
            
            self.radiusRatio = CGFloat(radiusRatio as! Int)
        }
        
    
        
        if let titelColor = detail[layout.ktitleColor]
        {
           self.setTitleColor(titelColor as? UIColor, for: UIControlState.normal)
          
        }
        
        
    }
    
    
}
