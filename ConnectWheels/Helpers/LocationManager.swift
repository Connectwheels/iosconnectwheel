//
//  LocationManager.swift
//  Boujee
//
//  Created by  on 1/5/18.
//  Copyright © 2018 -. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import UserNotificationsUI


/// Possible statuses for a permission.
public enum PermissionStatus: Int, CustomStringConvertible {
    case authorized, unauthorized, unknown, disabled
    
    public var description: String {
        switch self {
        case .authorized:   return "Authorized"
        case .unauthorized: return "Unauthorized"
        case .unknown:      return "Unknown"
        case .disabled:     return "Disabled" // System-level
        }
    }
}

let App_Name : String                   = "Connect Wheels"
typealias completionHandler = (_ data : Any) ->()

class LocationManager: NSObject , CLLocationManagerDelegate {
    
    
    let kPermissionDenied           : String! =  "To re-enable, please go to Settings and turn on Location Service for " + App_Name
    let kNotificatinPermionDenied   : String  = "Please go to Settings and turn on Notification Service for" + App_Name
    let kPermissionDeniedTitle      : String! = "App Permission Denied"
    let kChangeAuthorization      : String  = "kChangeAuthorization"
    var statusChange : completionHandler?
    var locationChange : completionHandler?
    static let shared : LocationManager = LocationManager()
    var location            : CLLocation = CLLocation()
    var locationManager     : CLLocationManager = CLLocationManager()

    func checkStatus() -> CLAuthorizationStatus{
        return CLLocationManager.authorizationStatus()
    }

    func getLocation() {
        
        switch checkStatus() {
        case .notDetermined:
            locationManager = CLLocationManager()
            locationManager.delegate = self;
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            return
        case .denied , .restricted :
            //open alert
            self.openDeniedPermissionAlert()
            
            break
        default:
            locationManager.delegate = self;
            locationManager.startUpdatingLocation()
            return
        }
    }
    
    func getUserLat() -> String {
        
        if location.coordinate.latitude == 0.0 {
            
            #if targetEnvironment(simulator)
            return "23.076000"
            #else
            return "0"
            #endif
        }
        else {
            return "\(location.coordinate.latitude)"
        }
    }
    
    func getUserLong() -> String {
        
        if location.coordinate.longitude == 0.0 {
            
            #if targetEnvironment(simulator)
            return "72.50000"
            //                return "38.7504604"
            #else
            return "0"
            #endif
        }
        else {
            return "\(location.coordinate.longitude)"
        }
    }
    
    func getUserLocation() -> CLLocation {
        if location.coordinate.longitude == 0.0 {
            return CLLocation(latitude: 23.0225, longitude: 72.5714)
        }
        return location
    }
    
    func getLocationUpdate(completion : completionHandler?)  {
        if let _ = completion {
            self.locationChange = completion
        }
    }
    
    func clearUpdates()  {
        self.locationChange = nil
    }
    
    func getStatusChangeUpdate(completion : completionHandler?)  {
        if let _ = completion {
            self.statusChange = completion
            if CLLocationManager.locationServicesEnabled() {
                completion!(CLLocationManager.authorizationStatus())
            }
        }
        
    }
    
    //TODO:- when we have permission but denied.
    func openDeniedPermissionAlert(titleMessage : String = LocationManager.shared.kPermissionDeniedTitle , message : String = LocationManager.shared.kPermissionDenied){
        
        let permissionAlert : UIAlertController = UIAlertController(title: titleMessage, message: message, preferredStyle: .alert)
        
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let settingAction : UIAlertAction = UIAlertAction(title: "Settings", style: .default) { (setting) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: {enabled in
                    // ... handle if enabled
                })
            } else {
                // Fallback on earlier versions
            }
        }
        
        permissionAlert.addAction(cancelAction)
        permissionAlert.addAction(settingAction)
        
        appDelegate.window?.rootViewController?.present(permissionAlert, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    //MARK: Location Manager delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        let userInfo = ["status" : status]
        
        switch status {
            
        case .authorizedWhenInUse:
            print("authorizedWhenInUse")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kChangeAuthorization), object: nil, userInfo: userInfo )
            break
        case .authorizedAlways:
            print("authorizedWhenInUse")
            break
        case .notDetermined:
            //default permission
            print("notDetermined")
            break
        case .restricted:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kChangeAuthorization), object: nil, userInfo: userInfo )
            print("restricted")
            break
        case .denied:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kChangeAuthorization), object: nil, userInfo: userInfo )
            print("denied")
            break
        }
        
        if let _ = self.statusChange {
            statusChange!(status)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let _ = locations.last {
            location = locations.last!
            if let _ = locationChange {
                locationChange!(location)
            }
        }
    }
    
    
    
}
